import os
import numpy as np
from PIL import Image

# Define the rectangle coordinates
# Define the coordinates of the rectangle to crop
top_left = (32, 0)
top_right = (64, 0)
bottom_right = (64, 32)
bottom_left = (32, 32)

# Convert corner points to a box tuple
left = top_left[0]
upper = top_left[1]
right = bottom_right[0]
lower = bottom_right[1]

# Extract the pixels from the defined rectangle


# Define the output image size
output_width = 32
output_height = 32

# Define the folder path
folder_path = './sprites/npc/Sprites'

# Get a list of all image files in the folder
image_files = [f for f in os.listdir(folder_path) if f.endswith('.jpg') or f.endswith('.png')]

# Create an empty array to store the extracted pixels
pixels_array = np.zeros((output_height, output_width * len(image_files), 4), dtype=np.uint8)

# Iterate over each image file
for i, image_file in enumerate(image_files):
    # Load the image
    image_path = os.path.join(folder_path, image_file)
    image = Image.open(image_path)

    # Extract the pixels from the defined rectangle
    pixels = np.array(image.crop((left, upper, right, lower)))

    # Insert the extracted pixels into the output array
    pixels_array[:, i * output_width:(i + 1) * output_width, :] = pixels

# Save the output image
output_image = Image.fromarray(pixels_array)
output_image.save('output.png')
