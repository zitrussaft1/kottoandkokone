-- Main Lua script of the quest.
-- See the Lua API! http://www.solarus-games.org/doc/latest

--require("scripts/features")
require("scripts/saltk/main").init()
require("scripts/multi_events")
require("scripts/globals")
require("scripts/utils")

-- Edit scripts/menus/initial_menus_config.lua to add or change menus before starting a game.
-- bats gosta de traveco
local initial_menus_config = require("scripts/menus/initial_menus_config")
local initial_menus = {}
-- This function is called when Solarus starts.
function sol.main:on_started()

  sol.main.load_settings()
  math.randomseed(os.time())

  local game_meta = sol.main.get_metatable("game")

  local start_menu = require("scripts/menus/start_menu")
  local notify_menu = require("scripts/menus/notification")
  sol.menu.start(sol.main, start_menu)
  game_meta:register_event("on_started", function(game)
      --sol.menu.stop(start_menu)
      sol.menu.start(sol.main, notify_menu)
  end)

end

-- Event called when the program stops.
function sol.main:on_finished()
  sol.main.save_settings()
end

-- Event called when the player pressed a keyboard key.
function sol.main:on_key_pressed(key, modifiers)

  local handled = false
  if key == "f11" or
    (key == "return" and (modifiers.alt or modifiers.control)) then
    -- F11 or Ctrl + return or Alt + Return: switch fullscreen.
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    handled = true
  --[[elseif key == "p" then
    local phone = require("scripts/menus/phone/phone")
    local gallery = require("scripts/menus/phone/gallery")
    if not sol.menu.is_started(phone) and 
      not sol.menu.is_started(gallery) then
      sol.menu.start(
        sol.main.get_game(),
        phone
      )   
    end
    handled = true--]]
  elseif key == "m" then
    BASE_ATTACK_POWER = 10
  elseif key == "f4" and modifiers.alt then
    -- Alt + F4: stop the program.
    sol.main.exit()
    handled = true
  elseif key == "escape" and sol.main.get_game() == nil then
    -- Escape in pre-game menus: stop the program.
    sol.main.exit()
    handled = true
  end

  return handled
end
