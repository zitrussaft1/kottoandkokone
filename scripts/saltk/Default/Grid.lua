local saltk = require("scripts/saltk/factory")

local Widget = {}
Widget.__index = Widget
local Factory = saltk.WidgetFactory(
    {"LayoutGrid", "DisplayOnFocusChangeColor", "MouseHover", "MousePressed", "KeyboardBasic"}
)
setmetatable(Widget, Factory)

function Widget.new(props)
    local instance = setmetatable(Factory.new(props), Widget)
    return instance 
end

return Widget 