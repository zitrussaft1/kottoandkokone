local saltk = require("scripts/saltk/factory")

local Widget = {}
Widget.__index = Widget
local Factory = saltk.WidgetFactory(
    {"DisplayImage", "MouseHover", "MousePressed", "KeyboardDefault"}
)
setmetatable(Widget, Factory)

function Widget.new(props)
    local instance = setmetatable(Factory.new(props), Widget)
    return instance 
end

return Widget