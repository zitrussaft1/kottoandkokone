local ColorBasic = require("scripts/saltk/Core/Display/Basic")
local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local color_utils = require("scripts/saltk/Core/Display/utils")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, ColorBasic)

local original_insenstive = nil
local original_focused = nil

function Widget.new(props)
    local instance = setmetatable(ColorBasic.new(props), Widget)

    original_insenstive = instance.sensitive_background or nil
    original_focused = instance.hover_background or nil

    instance:add_signal("focused", function (Widget)
        Widget.queue_draw = true
    end)

    instance:add_signal("unfocused", function (Widget)
        Widget.queue_draw = true
    end)

    Widget.set_background_color(instance, instance.background)

    return instance
end

function Widget:set_background_color(color)
    ColorBasic.set_background_color(self, color)

    if color and original_insenstive == nil then
        local hsv_bg = color_utils.rgb_to_hsv(self.background)
        hsv_bg[2] = hsv_bg[2] * 0.8
        hsv_bg[3] = hsv_bg[3] * 0.8
        self.sensitive_background = color_utils.hsv_to_rgb(hsv_bg)
    end

    if color and original_focused == nil then
        local hsv_bg = color_utils.rgb_to_hsv(self.background)
        hsv_bg[2] = hsv_bg[2] * 0.8
        hsv_bg[3] = hsv_bg[3] * 1.2
        self.hover_background = color_utils.hsv_to_rgb(hsv_bg)
    end
end

function Widget:render()
    WidgetBase.render(self)
    
    if self.background and not self.sensitive then
        self.surface:fill_color(self.sensitive_background)
    elseif self.background and self.has_focus then
        self.surface:fill_color(self.hover_background)
    elseif self.background then
        self.surface:fill_color(self.background)
    end

    self.surface:set_opacity(self.opacity)
end

return Widget