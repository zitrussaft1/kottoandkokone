local Basic = require("scripts/saltk/Core/Display/Basic")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, Basic)

function Widget.new(props)
    local instance = setmetatable(Basic.new(props), Widget)
    
    Widget.set_text(instance, props.text)
    Widget.set_color(instance, props.color or {0,0,0})
    Widget.set_font(instance, props.font or "8_bit")
    Widget.set_font_size(instance, props.font_size or 12)
    Widget.set_rendering_mode(instance, props.rendering_mode or "solid")


    return instance
end

function Widget:set_text(text)
    assert(type(text) == "string", "Widget:set_text(text) requires a string")
    self.text = text
    if self.surfaces then
        for _, surface in ipairs(self.surfaces) do
            surface:draw(self.surface, 0, self.font_size_real/2)
        end
    end
    self.queue_draw = true
end

function Widget:get_text()
    return self.text
end

function Widget:set_font(font)
    assert(type(font) == "string", "Widget:set_font(font) requires a string")
    self.font = font
    self.queue_draw = true
end

function Widget:set_font_size(font_size)
    assert(type(font_size) == "number", "Widget:set_font_size(font_size) requires a number")
    self.font_size = font_size
    self.queue_draw = true
end

function Widget:set_color(color)
    assert(type(color) == "table" and
        type(color[1]) == "number" and
        type(color[2]) == "number" and
        type(color[3]) == "number",
        "Widget:set_color(color) requires a table with 3 numbers")
    self.color = color
    self.queue_draw = true
end

function Widget:set_rendering_mode(rendering_mode)
    assert(rendering_mode == "solid" or rendering_mode == "antialiasing",
        "Widget:set_rendering_mode(rendering_mode) requires a string 'solid' or 'antialiasing'")
    self.rendering_mode = rendering_mode
    self.queue_draw = true
end


function Widget:render()
    Basic.render(self)

    self.font_size_real = self.font_size

    if self.scale then
        local quest_width, quest_height = sol.video.get_quest_size()
        local scale_x =SALTK_WINDOW_WIDTH/ quest_width
        local scale_y =SALTK_WINDOW_HEIGHT / quest_height
        scale_x = math.min(scale_x, scale_y)
        self.font_size_real = self.font_size * scale_x
    end

    local text_surfaces = self:_get_split_texts({
        text = self.text,
        font = self.font,
        font_size = self.font_size_real,
        color = self.color,
        rendering_mode = self.rendering_mode
    })

    self.surfaces = {}

    local i = 1
    for _, text_surface_text in ipairs(text_surfaces) do
        local text_surface = sol.text_surface.create({
            font = self.font,
            font_size = self.font_size_real,
            color = self.color,
            rendering_mode = self.rendering_mode,
            text = text_surface_text
        })

        table.insert(self.surfaces, text_surface)


        text_surface:draw(self.surface, 0, self.font_size_real/2 * i)
        i = i + 1
    end

    self.surface:set_opacity(self.opacity)
end

function Widget:_get_split_texts(params)
    local text = params.text
    local font = params.font
    local font_size = params.font_size
    local color = params.color
    local rendering_mode = params.rendering_mode

    local width, height = sol.text_surface.get_predicted_size(params.font, font_size, text)

    local surface_width, surface_height = self.surface:get_size()
    local text_surfaces = {}

    local text_surfaces_needed = math.ceil(width / surface_width)

    local text_parts = {}
    local current_part = ""
    for word in text:gmatch("%S+") do
        local test_text = current_part .. " " .. word
        local test_width, _ = sol.text_surface.get_predicted_size(params.font, font_size, test_text)
        if test_width > surface_width then
            table.insert(text_parts, current_part)
            current_part = word
        else
            current_part = test_text
        end
    end
    if #current_part > 0 then
        table.insert(text_parts, current_part)
    end

    if text_parts and text_parts[1] then
        text_parts[1] = text_parts[1]:sub(2)
    end

    return text_parts
end

return Widget