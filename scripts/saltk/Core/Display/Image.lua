local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, WidgetBase)

function Widget.new(props)
    local instance = setmetatable(WidgetBase.new(props), Widget)

    instance.image = {
        surface = nil,
        width = 0,
        height = 0,
    }

    Widget.set_image(instance, props.image)
    Widget.set_crop(instance, props.crop)
    Widget.set_opacity(instance, props.opacity or 255)
    Widget.set_special_scale(instance, props.special_scale or 0)

    return instance
end

function Widget:set_crop(crop)
    if crop == nil then return end
    assert(type(crop) == "table" and
        type(crop[1]) == "number" and
        type(crop[2]) == "number" and
        type(crop[3]) == "number" and
        type(crop[4]) == "number" and
        crop[1] >= 0 and crop[2] >= 0 and crop[3] >= 0 and crop[4] >= 0,
        "Widget:set_crop(crop) requires a table of four numbers")
    self.crop = crop
end

function Widget:set_image(image)
    assert(type(image) == "string" or 
        (type(image) == "userdata" and
        image.fill_color ~= nil) or
        image == nil
        , "Widget:set_image(image) requires a string or a surface")
        
    if type(image) == "string" then
        self.image.surface = sol.surface.create(image)
        assert(self.image.surface ~= nil, "Widget:set_image(image) requires a valid sprite")
    elseif type(image) == "userdata" then
        self.image.surface = image
    else
        self.image.surface = nil
        self.queue_draw = true
        return
    end

    local width, height = self.image.surface:get_size()

    self.image.width = width
    self.image.height = height
    self.queue_draw = true
end

function Widget:set_opacity(opacity)
    assert(type(opacity) == "number", "Widget:set_opacity(opacity) requires a number")
    self.opacity = opacity
    self.queue_draw = true
end

function Widget:set_special_scale(special_scale)
    assert(type(special_scale) == "number" and
        special_scale >= 0 and
        special_scale <= 3,
        "Widget:set_special_scale(special_scale) requires one of the following: \n \
            0: no scaling,\n \
            1: scale to fit, \n \
            2: scale to fill \n \
            3: scale to fit and fill")
    self.special_scale = special_scale
    self.queue_draw = true
end

function Widget:draw_crop()
    assert(self.image ~= nil, "Widget:draw_crop() requires an image set")
    local crop = self.crop
    if crop then
        self.image.surface:draw_region(crop[1], crop[2], crop[3], crop[4], self.surface)
    else
        self.image.surface:draw(self.surface)
    end
end

function Widget:render()
    WidgetBase.render(self)
    if self.image.surface == nil then
        return
    end

    local width, height = self:get_size()
    local image_width, image_height = self.image.width, self.image.height

    local scale_x = width / image_width
    local scale_y = height / image_height

    if self.special_scale == 1 then
        scale_x = math.min(scale_x, scale_y)
        scale_y = scale_x
    elseif self.special_scale == 2 then
        scale_x = math.max(scale_x, scale_y)
        scale_y = scale_x
    end

    if self.special_scale > 0 then
        self.image.surface:set_scale(scale_x, scale_y)
    end

    self:draw_crop()

    self.surface:set_opacity(self.opacity)
end

return Widget