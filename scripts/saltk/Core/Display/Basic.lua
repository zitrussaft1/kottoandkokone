local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, WidgetBase)

function Widget.new(props)
    local instance = setmetatable(WidgetBase.new(props), Widget)

    Widget.set_background_color(instance, props.background)

    local opacity = props.opacity or 255
    Widget.set_opacity(instance, opacity)

    return instance
end

function Widget:set_background_color(color)
    if color == nil then
        self.background = nil
        self.queue_draw = true
        return
    end

    assert(type(color) == "table" and
        type(color[1]) == "number" and
        type(color[2]) == "number" and
        type(color[3]) == "number",
        "Widget:set_background_color(color) requires a table with 3 numbers for color")

    self.background = color
    self.queue_draw = true
end

function Widget:set_opacity(opacity)
    assert(type(opacity) == "number", "Widget:set_opacity(opacity) requires a number")
    self.opacity = opacity
    self.queue_draw = true
end

function Widget:render()
    WidgetBase.render(self)

    if self.background then
        self.surface:fill_color(self.background)
    end
    self.surface:set_opacity(self.opacity)
end

return Widget