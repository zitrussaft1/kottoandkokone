local Image = require("scripts/saltk/Core/Display/Image")
local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, WidgetBase)

function Widget.new(props)
    local instance = setmetatable(WidgetBase.new(props), WidgetBase)
    instance.set_crop =  Image.set_crop
    instance.set_opacity = Image.set_opacity
    instance.set_special_scale = Image.set_special_scale
    instance.draw_crop = Image.draw_crop
    instance.render = Image.render

    instance.image = {
        surface = nil,
        width = 0,
        height = 0,
    }

    Widget.set_image(instance, props.image)
    instance:set_crop(props.crop)
    instance:set_opacity(props.opacity or 255)
    instance:set_special_scale(props.special_scale or 0)


    return instance
end

function Widget:set_image(image)
    assert(type(image) == "string" or 
        (type(image) == "userdata" and
        image.get_animation_set ~= nil)
        , "Widget:set_image(image) requires a string or a sprite")


    if type(image) == "string" then
        self.image.surface = sol.sprite.create(image)
        assert(self.image.surface ~= nil, "Widget:set_image(image) requires a valid sprite")
    else
        self.image.surface = image
    end

    local width, height = self.image.surface:get_size()
    
    self.image.width = width
    self.image.height = height
    self.queue_draw = true
end

function Widget:draw(surface)
    self:render()
    WidgetBase.draw(self, surface)
end

return Widget