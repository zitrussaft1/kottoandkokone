local function find_child(widget)
    if widget.children == nil then
        return nil
    end
    for _, child in pairs(widget.children) do
        if child.sensitive then
            return child
        end
        return find_child(child)
    end
end

local function find_next_or_wrap(widget)
    if widget.parent == nil then
        return
    end
    for i = widget.index + 1, #widget.parent.children do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
    for i = 1, widget.index - 1 do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
end

local function find_previous_or_wrap(widget)
    if widget.parent == nil then
        return
    end
    for i = widget.index - 1, 1, -1 do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
    for i = #widget.parent.children, widget.index + 1, -1 do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
end

local function find_down_or_wrap(widget)
    if widget.parent == nil then
        return
    end

    local jump = widget.parent.grid[1]
    for i = widget.index + jump, #widget.parent.children, jump do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
    for i = 1, widget.index, jump do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
end

local function find_up_or_wrap(widget)
    if widget.parent == nil then
        return
    end
    local jump = widget.parent.grid[1]
    for i = widget.index - jump, 1, -jump do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
    for i = #widget.parent.children, widget.index + 1, -jump do
        local child = widget.parent:get_child_by_index(i)
        if child then
            if child.sensitive then
                return child
            else
                local c = find_child(child)
                if c then
                    return c
                end
            end
        end
    end
end

local KeyboardBasic = require("scripts/saltk/Core/Keyboard/Basic")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, KeyboardBasic)

function Widget.new(props)
    local instance = setmetatable(KeyboardBasic.new(props), Widget)

    instance:add_signal("key_pressed", function(Widget, key, modifiers) 
        if key == "space" then
            Widget:signal_handler("selected")
        elseif key == "right" then
            local next = find_next_or_wrap(Widget)
            if next then
                next:set_focus(true)
            end
        elseif key == "left" then
            local previous = find_previous_or_wrap(Widget)
            if previous then
                previous:set_focus(true)
            end
        elseif key == "up" then
            local parent = find_up_or_wrap(Widget)
            if parent then
                parent:set_focus(true)
            end
        elseif key == "down" then
            local child = find_down_or_wrap(Widget)
            if child then
                child:set_focus(true)
            end
        end
        return true
    end)

    return instance
end

return Widget