local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, WidgetBase)

function Widget.new(props)
    local instance = setmetatable(WidgetBase.new(props), Widget)
    return instance
end

function Widget:on_key_pressed(key, modifiers)
    if self.sensitive and self.has_focus then
        self:signal_handler("key_pressed", key, modifiers)
        return true
    else
        local handled = false
        for _, child in pairs(self.children or {}) do
            if child.on_key_pressed then
                handled = child:on_key_pressed(key, modifiers)
                if handled then
                    return true
                end
            end
        end
    end
end

return Widget