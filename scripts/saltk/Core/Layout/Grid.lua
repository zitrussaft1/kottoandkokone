local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Grid = {}
Grid.__index = Grid
setmetatable(Grid, WidgetBase)

function Grid.new(props)
    local instance = setmetatable(WidgetBase.new(props), Grid)
    instance.grid = props.grid or {4,4}
    instance.padding = props.padding or {0,0}
    return instance
end

function Grid:add_child(child)
    child:set_size({
        self.size[1] / self.grid[1] - self.padding[1],
        self.size[2] / self.grid[2] - self.padding[2]
    })


    local number_of_children = 0
    if self.children then
        number_of_children = #self.children
    end

    local x = number_of_children % self.grid[1]
    local y = math.floor(number_of_children / self.grid[1])

    child:set_position({
        child.position[1] + (x * child.size[1]) + (self.padding[1] * (x + 1.5) / 2),
        child.position[2] + (y * child.size[2]) + (self.padding[2] * (y + 1.5) / 2)
    })

    WidgetBase.add_child(self, child)
end

return Grid