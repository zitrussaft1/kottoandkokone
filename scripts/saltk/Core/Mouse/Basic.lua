local WidgetBase = require("scripts/saltk/Core/WidgetBase")

local Widget = {}
Widget.__index = Widget
setmetatable(Widget, WidgetBase)

function Widget.new(props)
    local instance = setmetatable(WidgetBase.new(props), Widget)
    return instance
end

function Widget:find_child_mouse_hover(mouse_x, mouse_y)
    if self:mouse_inside(mouse_x, mouse_y) then
        for _, child in pairs(self.children or {}) do
            if child.find_child_mouse_hover then
                local found = child:find_child_mouse_hover(mouse_x, mouse_y)
                if found and found.sensitive then
                    return found
                end
            end
        end
        if self.sensitive then
            return self
        end
    end
    return nil
end

function Widget:mouse_inside(mouse_x, mouse_y)
    if self.position[1] <= mouse_x and mouse_x <= self.position[1] + self.size[1] and
        self.position[2] <= mouse_y and mouse_y <= self.position[2] + self.size[2] then
        return true
    end
    return false
end

return Widget