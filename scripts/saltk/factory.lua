local algos = require("scripts/saltk/algos")

local Core = {
    DisplayBasic = require("scripts/saltk/Core/Display/Basic"),
    DisplayOnFocusChangeColor = require("scripts/saltk/Core/Display/OnFocusChangeColor"),
    DisplayImage = require("scripts/saltk/Core/Display/Image"),
    DisplaySprite = require("scripts/saltk/Core/Display/Sprite"),

    KeyboardBasic = require("scripts/saltk/Core/Keyboard/Basic"),
    KeyboardDefault = require("scripts/saltk/Core/Keyboard/Default"),
    KeyboardGrid = require("scripts/saltk/Core/Keyboard/Grid"),

    MouseBasic = require("scripts/saltk/Core/Mouse/Basic"),
    MouseHover = require("scripts/saltk/Core/Mouse/Hover"),
    MousePressed = require("scripts/saltk/Core/Mouse/Pressed"),

    LayoutGrid = require("scripts/saltk/Core/Layout/Grid"),

    WidgetBase = require("scripts/saltk/Core/WidgetBase"),
}

function Core.WidgetFactory(parents)
    local function searchParents(key, parents)
        for _, parent in ipairs(parents) do
            if Core[parent][key] then
                return Core[parent][key]
            end
        end
        return nil
    end

    local function registerParents(parents)
        return {
            new = function(props)
                local instance = {}
                for _, parent in ipairs(parents) do
                    if Core[parent].new then
                        props = Core[parent].new(props)
                        algos.deep_merge(instance, props)
                    end
                end
                instance.root = instance
                return instance
            end,
            __index = function (t, k)
                if Core[k] then
                    return Core[k]
                end
                return searchParents(k, parents)
            end
        }
    end

    return registerParents(parents)
end

return Core