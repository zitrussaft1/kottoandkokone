local saltk = {
    WidgetColor = require("scripts/saltk/Default/WidgetColor"),
    Image = require("scripts/saltk/Default/Image"),
    Sprite = require("scripts/saltk/Default/Sprite"),
    Text = require("scripts/saltk/Default/Text"),
    Grid = require("scripts/saltk/Default/Grid"),
    GridElement = require("scripts/saltk/Default/GridElement"),
}
local menus_to_draw = {}

function saltk.init()
    local prev_menu_start = sol.menu.start

    sol.menu.start = function (context, menu, ...)
        if menu.scale then
            menus_to_draw[menu.id] = nil
            menus_to_draw[menu.id] = menu
        else
            menu.on_draw = menu.draw
        end
        
        --[[local on_started = menu.on_started
        menu.on_started = function (self)
            if on_started then
                on_started(self)
            end
            print("hello")
            self.queue_draw = true
        end

        local on_finished = menu.on_finished
        menu.on_finished = function (self)
            if on_finished then
                on_finished(self)
            end
            self.queue_draw = true
        end]]--

        prev_menu_start(context, menu, ...)
    end

    local prev_video_draw = sol.video.on_draw

    sol.video.on_draw = function (self, dst_surface)
        for _, menu in pairs(menus_to_draw) do
            menu:draw(dst_surface)
        end
        if prev_video_draw then
            prev_video_draw(self, dst_surface)
        end
    end
    return true
end

return saltk