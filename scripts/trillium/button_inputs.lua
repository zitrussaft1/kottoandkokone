local master_script = require("scripts/meta/master")
require("scripts/trillium/multi_events")

local menu = {}

local MENU_DIRECTIONS = {
	prev_menu = "left",
	next_menu = "right",
}


function menu:initialize(game)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local dash_manager = require"scripts/trillium/action/dash_manager"
  local button_mapping_menu = require"scripts/trillium/menus/button_mapping"
  local command_manager = require"scripts/trillium/misc/command_binding_manager"

  local function next_submenu() print("Submenu swap not yet implemented") end


  local function on_command_pressed(command)
    local hero = game:get_hero()
    local map = game:get_map()

    local handled = false

    local submenu_index = command and command:match"^menu_(%d+)$"
    if submenu_index and not game:is_dialog_enabled() and not sol.menu.is_started(button_menu) then
        --open (or close if already open) the corresponding pause submenu directly
        pause_menu:toggle_submenu(submenu_index)
        return true
    elseif command == "prev_menu" and sol.menu.is_started(pause_menu) then
      next_submenu"left"
    elseif command == "next_menu" and sol.menu.is_started(pause_menu) then
      next_submenu"right"

    elseif command == "action" and game:get_command_effect"action" == nil then
      game:queue_command_until_free"action"
      handled = true
    elseif command and not game:is_suspended() then
      game:queue_command_until_free(command)
      handled = true
    end
    return handled
  end


  function game:on_command_released(command)

  end


  function game:on_joypad_button_pressed(button)
    local command = command_manager:get_command_from_button(button)
    on_command_pressed(command)
  end

  function game:on_joypad_button_released(button)
    local command = command_manager:get_command_from_button(button)
    game:on_command_released(command)
  end


  function game:on_joypad_axis_moved(axis, state)
    --Watch out, axis are fucken tricky
    local command = command_manager:get_command_from_axis(axis, state)
    on_command_pressed(command)
  end


  function game:on_joypad_hat_moved(hat, direction8)
    local command = command_manager:get_command_from_hat(hat, direction8)
  end


  game:register_event("on_key_pressed", function(self, key, modifiers)
    local hero = game:get_hero()
    local command = command_manager:get_command_from_key(key)
    on_command_pressed(command)

    if key == "escape" then
      game:simulate_command_pressed"pause"

    elseif key == "f1" and not game:is_dialog_enabled() and not game:is_paused() then
      if not sol.menu.is_started(button_mapping_menu) then
        sol.menu.start(game, button_mapping_menu)
      else
        sol.menu.stop(button_mapping_menu)
      end

    end

  end)


  game:register_event("on_key_released", function(self, key)
    local command = command_manager:get_command_from_key(key)
    game:on_command_released(command)
  end)


  function game:set_controller_type(type)
    local mapping = require("scripts/trillium/joypad_defaults/" .. type)
    for button, command in pairs(mapping.button) do
      game:set_command_joypad_binding(command, "button " .. button)
    end
  end




  function game:queue_command_until_free(command)
    local QUEUE_LIFESPAN = 300 --length in which a command can sit in the queue
    local hero = game:get_hero()
    local hero_state, state_object = hero:get_state()
    if hero:get_state() == "free" then
      game:do_queued_command(command)
    elseif command == "attack" and state_object and state_object:get_description() == "aiming" then
      --hack to prevent attack from queuing when pressing the "fire" button for the gun weapon
    else
      if game.queued_command_timer then game.queued_command_timer:stop() end
      local queue_lifetime = 0
      game.queued_command_timer = sol.timer.start(game, 10, function()
        queue_lifetime = queue_lifetime + 10
        if hero:get_state() == "free" then
          game:do_queued_command(command)
        elseif queue_lifetime < QUEUE_LIFESPAN then
          return true
        end
      end)
    end
  end


  function game:do_queued_command(command)
    local can_dash = game:get_value("can_dash")
    local hero = game:get_hero()
    local intended_direction = game:get_commands_direction() or hero:get_direction()

    local function play_card(i)
      if not game:is_suspended() then
        local map = game:get_map()
        local card = map:get_card(i)
        print(card:is_usable())
        if card and card:is_usable() then
          map:activate_card(
            map:get_card(i)
          )
        end
      end
    end

    local is_beam_in_use = sol.menu.is_started(require("scripts/menus/using_light_beam"))
    --if command == "action" and hero:get_controlling_stream() == nil
    --and game:get_command_effect"action" == nil then
    --  if hero:get_facing_entity() and hero:get_facing_entity().on_interaction then return end
    local in_combat = (game:get_value("in_combat") == 1)
    if command == "dash" then
      if not game:is_suspended() and can_dash then
        hero:dash()
        can_dash = false
        sol.timer.start(game, 500, function() can_dash = true end)
      end
    elseif command == "card_1" and in_combat then
      play_card(1)
    elseif command == "card_2" and in_combat then
      play_card(2)
    elseif command == "card_3" and in_combat then
      play_card(3)
    elseif command == "open_phone" and not in_combat then
      print(game:get_map():get_boss())
      if game:get_map():get_boss() == nil then
        local phone = require("scripts/menus/phone/phone")
        local gallery = require("scripts/menus/phone/achievements")
        local deck_builder = require("scripts/menus/deck_builder")
        local deck_builder_home = require("scripts/menus/deck_builder_home")
        if sol.menu.is_started(deck_builder) then
          sol.menu.stop(deck_builder)
          sol.menu.start(
            sol.main.get_game(),
            deck_builder_home
          )
        elseif sol.menu.is_started(deck_builder_home) then
          sol.menu.stop(deck_builder_home)
          sol.menu.start(
            sol.main.get_game(),
            phone
          )
        elseif sol.menu.is_started(gallery) then
          sol.menu.stop(gallery)
          sol.menu.start(
            sol.main.get_game(),
            phone
          )   
        elseif sol.menu.is_started(phone) then
          sol.menu.stop(phone)
        else
          sol.menu.start(
            sol.main.get_game(),
            phone
          )   
        end
      end
    elseif command == "shield" and not is_beam_in_use and in_combat then
      local shield = not game:get_value("shield")
      if shield then
        hero:tint_hero({0,255,0})
      else
        hero:tint_hero({255,255,255})
      end
      game:set_value("shield", shield)
    end
    if game:get_value("shield") == false and in_combat then
      if command == "light_beam" then
        local light_beam = game:get_item("light_beam")
        hero:start_item(light_beam)
      elseif command == "bomb" then
        local bomb = game:get_item("bomb")
        hero:start_item(bomb)
      elseif command =="time_slow" then
        local time_slow = game:get_item("time_slow")
        hero:start_item(time_slow)
      elseif command == "attack" then
        local weapon = game:get_item(game:get_value("equipped_weapon"))
        weapon:on_using()
      end
    end
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end


return menu