local master_script = require("scripts/meta/master")
local game_meta = sol.main.get_metatable"game"

local manager = {}
local rate = 1000
local rate_multiplier = 1
local amount = 1
local amount_multiplier = 1


game_meta:register_event("on_started", function(game)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local timer = sol.timer.start(game, rate / rate_multiplier, function()
    game:add_magic(amount * amount_multiplier)
    return rate / (rate_multiplier or 1)
  end)
  timer:set_suspended_with_map(true)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


function game_meta:set_magic_regen_multiplier(rm, am)
  rate_multiplier = rm or rate_multiplier
  amount_multiplier = am or amount_multiplier
end


function game_meta:get_magic_regen_multiplier()
  return rate_multiplier, amount_multiplier
end



return manager