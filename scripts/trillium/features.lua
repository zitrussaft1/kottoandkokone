-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/trillium/features")

-- Features can be enabled or disabled independently by commenting
-- or uncommenting lines below.

local features = {}
--Scripts that need to be called first:
require"scripts/trillium/multi_events"
require"scripts/trillium/utility/item_name_retriever" --this one has to be near the top for other scripts to use it

--Scripts that can be called later:
require("items/trillium/charms/charm_manager")
require"scripts/trillium/action/dash_manager"
require"scripts/trillium/action/explosives"
require"scripts/trillium/action/fall_manager"
require"scripts/trillium/action/hole_drop_landing"
require"scripts/trillium/action/swim_manager"
require"scripts/trillium/checkpoint/checkpoint_actions"
require"scripts/trillium/checkpoint/enemy_respawn_manager"
require"scripts/trillium/elements/enemy_elemental_meta"
require"scripts/trillium/elements/hero_elemental_meta"
require"scripts/trillium/elements/map_elemental_meta"
require"scripts/trillium/fx/fog"
require"scripts/trillium/fx/lighting/lighting_manager"
require"scripts/trillium/fx/lighting/map_lighting"
require"scripts/trillium/fx/white_flash"
require"scripts/trillium/gameover"
require"scripts/trillium/hud/hud"
require"scripts/trillium/hud/title_slam"
--require"scripts/trillium/menus/dialog_box"
require"scripts/trillium/menus/inventory/pause_menu"
require"scripts/trillium/menus/shop/shop_manager"
require"scripts/trillium/meta/bush"
require"scripts/trillium/meta/camera"
require"scripts/trillium/meta/custom_entity"
require"scripts/trillium/meta/enemy"
require"scripts/trillium/meta/game"
require"scripts/trillium/meta/hero"
require"scripts/trillium/meta/item"
require"scripts/trillium/meta/map"
require"scripts/trillium/meta/switch"
require("scripts/trillium/misc/magic_regeneration_manager")
--require"scripts/trillium/misc/solid_ground_manager"
require"scripts/trillium/status_effects/enemy_status_manager"
require"scripts/trillium/status_effects/status_manager"
require"scripts/trillium/trillium_dialog/dialog_manager"
require"scripts/trillium/utility/draw_bounding_boxes"
require("scripts/trillium/utility/ammo_tracker").add_to_item_api()
require"scripts/trillium/utility/angle_utility"
require"scripts/trillium/utility/draw_bounding_boxes"
require"scripts/trillium/utility/savegame_tables"
require"scripts/trillium/utility/table_duplication"
require"scripts/trillium/utility/text_stamper"
require"scripts/trillium/weather/weather_manager"


--Some scripts need a game passes to initialize. features.init(game) is called by the game manager, so those scripts are initialized here:
function features.init(game)
  require("items/trillium/materials/materials_manager"):init(game)
  require("items/trillium/meal_manager"):init(game)
  require("scripts/trillium/button_inputs"):initialize(game)
  require("scripts/trillium/debug_keys"):initialize(game)
  --require("scripts/trillium/fx/lighting_effects"):initialize()
end

return features