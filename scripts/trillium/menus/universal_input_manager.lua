--[[
By Max Mraz, licensed MIT
A script to allow a menu to process inputs at a command level whether or not the game is running. Works with llamazing's command binding manager
To use, call this script's enable function and pass the menu you want to enable universal inputs on. Like:
require("scripts/trillium/menus/universal_input_manager").enable(menu)

--]]

local manager = {}
local command_manager = require("scripts/trillium/misc/command_binding_manager")

function manager.enable(menu)
  --Keyboard inputs
  function menu:on_key_pressed(key)
    local command = command_manager:get_command_from_key(key)
    return menu:process_input(command)
  end

  --Joypad inputs
  function menu:on_joypad_button_pressed(button)
    local command = command_manager:get_command_from_button(button)
    return menu:process_input(command)
  end

  function menu:on_joypad_hat_moved(hat,direction8)
    local command = command_manager:get_command_from_hat(hat, direction8)
    return menu:process_input(command)
  end
  --Avoid analog stick wildly jumping
  local joy_avoid_repeat = {-2, -2}
  local first_joypress = true

  function menu:on_joypad_axis_moved(axis,state)
    local handled = joy_avoid_repeat[axis % 2] == state
    joy_avoid_repeat[axis % 2] = state

    if handled or first_joypress then
      first_joypress = false
      return
    end
    if menu.joypad_just_moved then return end
    local command = command_manager:get_command_from_axis(axis, state)
    menu.joypad_just_moved = true
    sol.timer.start(sol.main, 20, function() menu.joypad_just_moved = false end)
    return menu:process_input(command)
  end
end

return manager