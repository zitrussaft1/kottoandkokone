local master_script = require("scripts/meta/master")
--Create inventory equipment menu
local item_list = {
  "bow",
  "bow_fire",
  "bow_ice",
  "bow_electric",
  "bow_bomb",
  "bombs_counter",
  "grenade",
  "clockwork_bombs_counter",
  "boomerang",
  "bugnet",
  "crystal_dash",
  "drillspear",
  "feather",
  "fire_rod",
  "flame_spell",
  "flamethrower",
  "gale_fan",
  "homeward_talisman",
  "hookseed_satchel",
  "hookshot",
  "ice_rod",
  "ice_spell",
  "magnet_gauntlet",
  "metal_block_cane",
  "spark_rod",
  "spark_spell",
  "speed_boots",
  "spell_arcane_bolt",
  "spell_arcane_dart",
  "spell_arrow",
  "spell_arrow_storm",
  "spell_backdraft",
  "spell_boltpunch",
  "spell_bomb",
  "spell_cinder",
  "spell_earthquake",
  "spell_firestorm",
  "spell_flame_line",
  "spell_groundspark",
  "spell_hail",
  "spell_heal",
  "spell_hush",
  "spell_hyperbeam",
  "spell_ice_wind",
  "spell_life_siphon",
  "spell_life_siphon_circle",
  "spell_mending_tree",
  "spell_plant_growth",
  "spell_poison_gas",
  "spell_soulsword",
  "spell_sunbeam",
  "spell_thunderstorm",
  "spell_wraith",
  "staff_of_mercury",
  "staff_of_teningur",
  "summoning_spear",
}

local allowed_objects = {}

for _, weapon in pairs(item_list) do
  local object = {}
  object.name = "inventory/" .. weapon
  object.sprite = "_item"
  object.display_function = "_item"
  object.description_dialog = "item_descriptions.weapons.basic_axe"
  object.description_sprite = "entities/trillium/items"
  object.description_sprite_animation = "inventory/" .. weapon
  object.text_function = function()
    local equip_item = sol.main.get_game():get_item(object.name)
    if equip_item and equip_item:has_amount() then
      return equip_item:get_amount()
    end
  end
  object.text_offset = { x = 16, y = 24}
  object.text_config = {
    font = "white_digits",
  }
  table.insert(allowed_objects, object)
end

local description_panel_config = require("scripts/trillium/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/trillium/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/trillium/menus/inventory/grid_config")
config.allowed_objects = allowed_objects
config.aux_menus = { description_panel }

local menu = require"scripts/trillium/menus/trillium_menu_lib/grid_menu".create(config)



menu:register_event("on_command_pressed", function(self, command)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  --Add control behaviour here.
  local game= sol.main.get_game()
  if command == "action" then
    if not menu:get_selected_object() then return end
    sol.audio.play_sound"cursor"
    local item = game:get_item(menu:get_selected_object().name)
    if not item:is_assignable() and item.on_using then
      sol.menu.stop(menu)
      game:set_paused(false)
      sol.timer.start(game, 50, function()
        item:on_using()
      end)
    end
  elseif command == "item_1" then
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
    if not menu:get_selected_object() then return end
    local item = game:get_item(menu:get_selected_object().name)
    sol.audio.play_sound"ok"
    local item = game:get_item(menu:get_selected_object().name)
    if game:get_item_assigned(1) ~= item and item:is_assignable() then game:set_item_assigned(1, item) end

  elseif command == "item_2" then
    if not menu:get_selected_object() then return end
    local item = game:get_item(menu:get_selected_object().name)
    sol.audio.play_sound"ok"
    local item = game:get_item(menu:get_selected_object().name)
    if game:get_item_assigned(2) ~= item and item:is_assignable() then game:set_item_assigned(2, item) end

  end
end)

return menu