local master_script = require("scripts/meta/master")
--Create objects
local item_list = {
  "warrior_health",
  "warrior_magic",
  "warrior_restoration",
  "battlemage",
  "mp_solace",
  "everfrost",
  "everwarm",
  "quickstep",
  "flamestep",
  "sharp_dash",
  "wounded_evasion",
  "gradual_heal",
  "gradual_magic",
}

local allowed_objects = {}

local i = 1
for _, item in pairs(item_list) do
  local object = {}
  object.name = "charms/" .. item
  object.sprite = "_item"
  object.text_offset = {x = 8, y = 8}
  object.description_dialog = "item_descriptions.charms." .. item
--  object.description_sprite = "entities/trillium/items"
  object.description_sprite_animation = "charms/" .. item
  object.display_function = function() return true end
  table.insert(allowed_objects, object)
end

local description_panel_config = require("scripts/trillium/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/trillium/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local slot_menu = require("scripts/trillium/menus/inventory/charm_slots")
slot_menu.x, slot_menu.y = 206, 72
local config = require("scripts/trillium/menus/inventory/grid_config")

config.allowed_objects = allowed_objects
config.aux_menus = { description_panel, slot_menu }

local menu = require"scripts/trillium/menus/trillium_menu_lib/grid_menu".create(config)


menu:register_event("on_command_pressed", function(self, command)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local handled = false
  if command == "action" then
    handled = true
    local selected_object = menu:get_selected_object() 
    if not selected_object then return end
    local game= sol.main.get_game()
    assert(game.equipped_charms, "game.equipped_charms table is nil - check that items/charms/charm_manager.lua is in place to use the charms menu")

    local item_name = selected_object.name

    if not game.equipped_charms[item_name]then --charm is not equipped
      game.charm_manager:equip_charm(item_name)
    else --charm is already equipped, unequip it
      game.charm_manager:unequip_charm(item_name)
    end
    menu:notify_aux_menus()
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  end
  return handled
end)

return menu