local master_script = require("scripts/meta/master")
--Create weapon objects
local weapon_list = {
  "basic_sword",
  "basic_spear",
  "basic_axe",
  "basic_dagger",
  "claymore",
  "fire_sword",
}

local allowed_objects = {}

local i = 1
for _, weapon in pairs(weapon_list) do
  local object = {}
  object.name = "solforge/" .. weapon
  object.sprite = "_item"
  object.text_offset = {x = 8, y = 8}
  object.description_dialog = "item_descriptions.weapons." .. weapon
  object.description_sprite = "menus/item_portraits/" .. weapon
  object.display_function = function() return true end
  table.insert(allowed_objects, object)
end

local description_panel_config = require("scripts/trillium/menus/inventory/item_description_panel_config")
local description_panel = require("scripts/trillium/menus/trillium_menu_lib/description_panel").create(description_panel_config)
local config = require("scripts/trillium/menus/inventory/grid_config")
config.allowed_objects = allowed_objects
config.aux_menus = { description_panel }

local menu = require"scripts/trillium/menus/trillium_menu_lib/grid_menu".create(config)


menu:register_event("on_command_pressed", function(self, command)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  --Add control behaviour here.
  if command == "action" then
    if not menu:get_selected_object() then return end
    local game= sol.main.get_game()
    game:set_value("equipped_weapon", menu:get_selected_object().name)
    sol.audio.play_sound"ok"
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  end
end)

return menu