local master_script = require("scripts/meta/master")
local materials = {
  "ingredients/apple",
  "ingredients/berry",
  "ingredients/bok_choy",
  "ingredients/carrot",
  "ingredients/chantrelle",
  "ingredients/crab",
  "ingredients/drumstick",
  "ingredients/egg",
  "ingredients/field_greens",
  "ingredients/fish",
  "ingredients/onion",
  "ingredients/pepper",
  "ingredients/radish",
  "ingredients/rice",
  "ingredients/shitake",
  "ingredients/steak",
  "animal/butterfly",
  "plant/adderwort",
  "plant/emberroot",
  "plant/greater_dandelion",
  "plant/hexberry",
  "plant/nightshade",
  "animal/dragonfly",
  "mineral/amber",
  "mineral/amethyst",
  "mineral/diamond",
  "mineral/emerald",
  "mineral/iron",
  "mineral/meteorite",
  "mineral/ruby",
  "mineral/sapphire",
}
local allowed_objects = {}

for i = 1, #materials do
  local object = {}
  object.name = "materials/" .. materials[i]
  object.sprite = "_item"
  object.text_function = function()
    local equip_item = sol.main.get_game():get_item(object.name)
    if equip_item and equip_item:has_amount() then
      return equip_item:get_amount()
    end
  end
  object.text_offset = { x = 18, y = 24}
  object.text_config = {
    font = "white_digits",
  }
  object.display_function = "_item"
  object.description_sprite = "entities/trillium/items"
  object.description_sprite_animation = "materials/" .. materials[i]
  allowed_objects[i] = object
end

local config = require("scripts/trillium/menus/inventory/grid_config")
config.allowed_objects = allowed_objects

local menu = require"scripts/trillium/menus/trillium_menu_lib/grid_menu".create(config)

--Define the controls specific to this menu. These will be added to
--(or replace?) the controls that may be defined in the template script.
menu:register_event("on_command_pressed", function(self, command)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
    --Add control behaviour here.
    if command == "action" then
      print("Selected:", menu:get_selected_object().name)
    end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  end)

return menu