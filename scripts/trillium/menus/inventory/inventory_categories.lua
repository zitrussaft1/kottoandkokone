local sub_menus = {
  "weapons",
  "tools",
  "materials",
  "meals",
  "charms",
}

local categories = {}

for i = 1, #sub_menus do
  local cat = {}
  cat.menu = require("scripts/trillium/menus/inventory/" .. sub_menus[i] )
  --cat.name = sub_menus[i]
  cat.icon = "menus/inventory/category_" .. sub_menus[i]
  categories[i] = cat
end

local menu = require("scripts/trillium/menus/trillium_menu_lib/category_selection_menu").create{
  categories = categories,
  origin = {x=8, y = 50},
  size = {width = 184, height = 16},
  category_padding = 4,
  background_png = "menus/inventory/category_selection_background.png",
  cursor_sound = "cursor_category",
}

return menu