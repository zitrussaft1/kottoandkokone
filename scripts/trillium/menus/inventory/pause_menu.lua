local sub_menus = {
  "scripts/trillium/menus/inventory/inventory_categories",
  "scripts/trillium/menus/inventory/options",
}

local names = { --display names for categories. Must correspond with above table. I know, I know.
  "Inventory",
  "Options",
}

local categories = {}

for i = 1, #sub_menus do
  local cat = {}
  cat.menu = require(sub_menus[i] )
  cat.name = names[i]
  --cat.icon = "menus/inventory/category_" .. sub_menus[i]
  categories[i] = cat
end

local menu = require("scripts/trillium/menus/trillium_menu_lib/category_selection_menu").create{
  categories = categories,
  origin = {x = 140, y = 24},
  size = {width = 186, height = 16},
  background_png = "menus/inventory/category_selection_background.png",
  cursor_sound = "cursor_category",
}


--Make this the pause menu:
--This method means this menu needs to be required in features.lua to do itself
local game_meta = sol.main.get_metatable"game"
--local menu = require"scripts/trillium/menus/inventory/pause_menu"

function game_meta:on_paused()
  sol.menu.start(self, menu)
end

function game_meta:on_unpaused()
  sol.menu.stop(menu)
end

return menu