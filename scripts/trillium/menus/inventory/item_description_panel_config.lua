local language_manager = require("scripts/trillium/language_manager")

local font, font_size, font_spacing = language_manager:get_menu_font()

return {
  panel_size = {width=200, height=186},
  origin = {x = 200, y = 50},
  sprite_surface_size = {width = width, height = 48},
  text_surface_size = {width = width, height = 256},
  sprite_surface_y_offset = 16,
  text_surface_y_offset = 72,
  text_margin = 4,
  font = font,
  font_size = font_size,
  font_color = {240,230,200},
  background_png = "menus/inventory/description_panel_background.png",
}