--[[
Created by Max Mraz, licensed MIT
Show how many charms you have equipped, and how many slots are remaining
--]]
local language_manager = require("scripts/trillium/language_manager")

local menu = {x = 0, y = 0}

local slot_width = 10
local charm_width = 20

local slot_surface = sol.surface.create()
local charm_surface = sol.surface.create()
local font, font_size, font_spacing = language_manager:get_menu_font()
local text_surface = sol.text_surface.create({
  font = font, font_size = font_size,
  vertical_alignment = "middle",
  text_key = "menu.charms.slots",
})
local text_width = sol.text_surface.get_predicted_size(font, font_size, sol.language.get_string("menu.charms.slots"))



function menu:on_started()
  menu:update()
end


function menu:update()
  local game = sol.main.get_game()
  local charm_slots = game:get_value("charm_slots")
  local slots_used = game:get_value("charm_slots_used") or 0
  assert(charm_slots, "Savegame value 'charm_slots' is not defined, a default limit for charm slots must be set to use the charm menu")

  menu.slot_sprites = {}
  for i = 1, charm_slots do
    local sprite = sol.sprite.create("menus/inventory/charm_slot")
    if slots_used >= i then
      sprite:set_animation("used")
    else
      sprite:set_animation("unused")
    end
    menu.slot_sprites[i] = sprite
  end

  menu.charm_sprites = {}
  local j = 1
  for item_name, item in pairs(game.equipped_charms) do
    local sprite = sol.sprite.create("entities/trillium/items")
    sprite:set_animation(item_name)
    menu.charm_sprites[j] = sprite
    j = j + 1
  end

  slot_surface:clear()
  for i, sprite in ipairs(menu.slot_sprites) do
    sprite:draw(slot_surface, slot_width / 2 + slot_width * (i - 1), 10)
  end
  charm_surface:clear()
  for i, sprite in ipairs(menu.charm_sprites) do
    sprite:draw(charm_surface, charm_width / 2 + charm_width * (i - 1), 13)
  end
end


function menu:notify(grid_menu)
  menu:update()
end


function menu:on_draw(dst)
  local row_offset = 26
  text_surface:draw(dst, menu.x, menu.y + row_offset + 4)
  slot_surface:draw(dst, menu.x + 2 + text_width, menu.y + row_offset)
  charm_surface:draw(dst, menu.x, menu.y)
end



return menu