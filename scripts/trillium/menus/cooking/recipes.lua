return {
  dolma = {
    "rice",
    "green",
  },

  latke = {
    "potato",
    "onion",
    "egg",
  },

  scallion_pancakes = {
    "wheat",
    "onion",
  },

  spring_roll = {
    "wheat",
    "vegetable",
  },

  dumpling = {
    "wheat",
    "meat",
  },

  samosa = {
    "wheat",
    "potato",
    "onion",
    "warm_spice",
  },

  sushi = {
    "rice",
    "seafood",
    "vegetable",
  },

  chicken_tandoori = {
    "drumstick",
    "warm_spice",
  },

  zongzi = {
    "rice",
    "green",
    "meat",
  },

  mushroom_rice_ball = {
    "rice",
    "mushroom",
  },

  rice_ball = {
    "rice",
  },

  pepper_omelette = {
    "egg",
    "meat",
    "pepper",
    "onion",
  },

  frittata = {
    "egg",
    "meat",
  },

  omelette = {
    "egg",
    "vegetable",
  },

  seafood_hotpot = {
    "broth",
    "vegetable",
    "seafood",
    "pepper",
  },

  hotpot = {
    "broth",
    "vegetable",
    "meat",
    "pepper",
  },

  hearty_soup = {
    "broth",
    "vegetable",
    "meat",
  },

  seafood_ramen = {
    "broth",
    "vegetable",
    "seafood",
  },

  vegetable_soup = {
    "broth",
    "vegetable",
  },

  fruit_crepe = {
    "wheat",
    "egg",
    "sweetener",
    "fruit",
  },

  crepe = {
    "wheat",
    "egg",
    "honey",
  },

  fruit_cake = {
    "fruit",
    "sugar",
    "wheat",
    "butter",
  },

  carrot_cake = {
    "carrot",
    "sugar",
    "wheat",
    "butter",
  },

  cake = {
    "sugar",
    "wheat",
    "butter",
  },

  berry_pie = {
    "berry",
    "wheat",
    "butter",
  },

  apple_pie = {
    "apple",
    "wheat",
    "butter",
  },

  candy_apple = {
    "apple",
    "sweetener",
  },

  honey_sesame_mushrooms = {
    "mushroom",
    "honey",
  },

  glazed_mushrooms = {
    "mushroom",
    "fruit",
  },

  salad = {
    "vegetable",
    "green",
  },

  mushroom_beef_stir_fry = {
    "meat",
    "mushroom",
  },

  honey_ginger_meat = {
    "meat",
    "sweetener",
  },

  lettuce_wrap = {
    "meat",
    "green",
  },

  stir_fry = {
    "meat",
    "vegetable",
  },

  steamed_seafood = {
    "seafood",
    "green",
  },

  seafood_stir_fry = {
    "seafood",
    "vegetable",
  },

  chicken_fried_rice = {
    "rice",
    "egg",
    "drumstick",
  },

  shrimp_fried_rice = {
    "rice",
    "egg",
    "seafood",
  },

  fried_rice = {
    "rice",
    "egg",
  },



  fried_egg = {
    "egg",
  },

  grain_bowl = {
    "grain",
  },

  seafood_skewer = {
    "seafood",
  },

  kebab = {
    "meat",
  },

  veggie_stir_fry = {
    "vegetable",
  },

  chutney = {
    "fruit",
  },

  fried_mushrooms = {
    "mushroom",
  },

  fried_greens = {
    "green",
  },

  hard_candy = {
    "sweetener",
  },

  suspicious_dish = {},
}