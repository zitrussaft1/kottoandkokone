--[[
This is a list of all acceptable "types" for ingredients.
Used by the cooking system to tell whether an ingredient should be evaluated for the specific ingredient it is, or the type of ingredient it is.
For example, if the recipe calls for any "meat", or calls for "chicken"
--]]

return {
  seafood = true,
  egg = true,
  meat = true,
  vegetable = true,
  green = true,
  fruit = true,
  mushroom = true,
  grain = true,
  sweetener = true,
}
