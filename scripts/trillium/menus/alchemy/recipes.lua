return {
    ["potions/burn_resist"] = {
      "plant/adderwort",
      "plant/greater_dandelion",
      "monster/shell",
    },
    ["potions/cold_resist"] = {
      "plant/emberroot",
      "plant/greater_dandelion",
      "monster/shell",
    },
    ["potions/health_regen"] = {
      "plant/hexberry",
      "plant/greater_dandelion",
      "monster/bone",
    },
    ["potions/magic_regen_rate"] = {
      "plant/adderwort",
      "plant/greater_dandelion",
      "monster/shell",
    },
    ["potions/poison_resist"] = {
      "plant/adderwort",
      "plant/greater_dandelion",
      "monster/shell",
    },
    ["potions/burn_resist"] = {
      "plant/adderwort",
      "plant/greater_dandelion",
      "monster/shell",
    },
    ["potions/burn_resist"] = {
      "plant/adderwort",
      "plant/greater_dandelion",
      "monster/shell",
    },
}