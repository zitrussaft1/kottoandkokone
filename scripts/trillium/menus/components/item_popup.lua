local font, font_size = require("scripts/trillium/language_manager"):get_menu_font()

local menu = {}

local screen_width, screen_height = sol.video.get_quest_size()
local width, height = 208, 48
local menu = {x = screen_width / 2 - width / 2, y = screen_height / 3}

local menu_surface = sol.surface.create()

local bg_surface = require("scripts/trillium/menus/trillium_menu_lib/9slice_background_manager").get_surface{
  width = width, height = height,
  source_png = "menus/panel_blocks/small.png",
  tile_width = 8, tile_height = 8,
}

local item_sprite = sol.sprite.create"entities/trillium/items"
item_sprite:set_animation"empty"

local name_surface = sol.text_surface.create{
  font = font, font_size = font_size,
  vertical_alignment = "middle",
  horizontal_alignment = "left",
}


function menu:set_item(item_id, variant)
  variant = variant or 1
  item_sprite:set_animation(item_id)
  if item_sprite:get_num_directions() > (variant - 1) then
    item_sprite:set_direction(variant - 1)
  end
  local name = sol.language.get_string( "items." .. item_id:gsub("/", ".") )
  name_surface:set_text(name or "")

  menu_surface:clear()
  bg_surface:draw(menu_surface)
  item_sprite:draw(menu_surface, 32, height - 16)
  name_surface:draw(menu_surface, 64, height / 2)
end


function menu:on_draw(dst)
  menu_surface:draw(dst, menu.x, menu.y)
end


function menu:on_command_pressed(command)
  local handled = false
  sol.menu.stop(menu)
  local handled = true
  return handled
end

return menu