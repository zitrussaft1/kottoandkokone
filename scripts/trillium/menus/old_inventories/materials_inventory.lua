local master_script = require("scripts/meta/master")
local possible_items = require("items/trillium/materials/materials_manager"):get_all_materials()

local menu = require("scripts/trillium/menus/inventory/bottomless_list"):build{
  all_items = possible_items,
  num_columns = 3,
  num_rows = 3,
  menu_x = 16
}


menu:register_event("on_command_pressed", function(self, cmd)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local game = sol.main.get_game()
  if cmd == "action" then
    local item = menu:get_current_item()
    if item.on_using then
      item:on_using()
      menu:rebuild_items()
    end
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

menu:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  sol.main.get_game():set_suspended(true)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

menu:register_event("on_finished", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  sol.main.get_game():set_suspended(false)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


return menu