local master_script = require("scripts/meta/master")
local possible_items = {
  "inventory/flame_spell",
  "inventory/fire_rod",
  "inventory/spark_spell",
  "inventory/spark_rod",
  "inventory/ice_spell",
  "inventory/ice_rod",
  "inventory/drillspear",
  "inventory/summoning_spear",
  "inventory/hookseed_satchel",
  "inventory/feather",
  "inventory/metal_block_cane",
  "inventory/staff_of_teningur",
  "inventory/staff_of_mercury",
  "inventory/hookshot",
  "inventory/boomerang",
  "inventory/bombs_counter",
  "inventory/clockwork_bombs_counter",
  "inventory/bow",
  "inventory/bow_fire",
  "inventory/bow_ice",
  "inventory/bow_electric",
  "inventory/bow_bomb",
  "inventory/magnet_gauntlet",
  "inventory/homeward_talisman",
  "inventory/bugnet",
  "inventory/flamethrower",
  "inventory/crystal_dash",
  "inventory/gale_fan",
  "test/gun",
}

local menu = require("scripts/trillium/menus/inventory/bottomless_list"):build{
  all_items = possible_items,
  num_columns = 6,
  num_rows = 4,
}


function menu:init(game)
--
  function game:on_paused() sol.menu.start(game, menu) end
  function game:on_unpaused() sol.menu.stop(menu) end
  if sol.main.debug_mode then
    for _, v in ipairs(possible_items) do game:get_item(v):set_variant(1) end
  end
--]]
end

menu:register_event("on_command_pressed", function(self, cmd)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local game = sol.main.get_game()
  local hero = game:get_hero()
  if cmd == "item_1" then
    local item = menu:get_current_item()
    if game:get_item_assigned(1) ~= item and item:is_assignable() then game:set_item_assigned(1, item) end

  elseif cmd == "item_2" then
    local item = menu:get_current_item()
    if game:get_item_assigned(2) ~= item and item:is_assignable() then game:set_item_assigned(2, item) end

  elseif cmd == "action" then
    local item = menu:get_current_item()
    if not item:is_assignable() and item.on_using then
      sol.menu.stop(menu)
      game:set_paused(false)
      sol.timer.start(game, 50, function()
        item:on_using()
      end)
    end
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


return menu