local master_script = require("scripts/meta/master")
local possible_items = {
  "solforge/basic_sword",
  "solforge/basic_dagger",
  "solforge/basic_spear",
  "solforge/basic_axe",
  "solforge/fire_sword",
  "solforge/claymore",
}
--could call this instead: local possible_items = sol.main.get_items_in_directory("items/trillium/solforge")
--but then you couldn't order them non-alphabetically

local menu = require("scripts/trillium/menus/inventory/bottomless_list"):build{
  all_items = possible_items,
  num_columns = 4,
  num_rows = 5,
  menu_x = 16
}

function menu:init(game)
  if sol.main.debug_mode then
    for _, v in ipairs(possible_items) do game:get_item(v):set_variant(1) end
  end
end

menu:register_event("on_command_pressed", function(self, cmd)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local game = sol.main.get_game()
  if cmd == "action" then
    local item = menu:get_current_item()
    game:set_value("equipped_weapon", item:get_name())
    sol.audio.play_sound"cursor"

  elseif cmd == "attack" then

  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

menu:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  sol.main.get_game():set_suspended(true)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

menu:register_event("on_finished", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  sol.main.get_game():set_suspended(false)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


return menu