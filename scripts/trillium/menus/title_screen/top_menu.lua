local command_manager = require("scripts/trillium/misc/command_binding_manager")
command_manager:init()

local top_menu = {}
local current_submenu

function top_menu:on_started()
  --Start a background
  sol.menu.start(top_menu, require"scripts/trillium/menus/title_screen/background", false)
  --Start the first title screen menu and set top menu as its parent
  local main_menu = require"scripts/trillium/menus/title_screen/main_menu"
  sol.menu.start(top_menu, main_menu)
  top_menu:set_current_submenu(main_menu)
end

function top_menu:set_current_submenu(new_menu)
  current_submenu = new_menu
  new_menu.top_menu = top_menu
end


local ALLOWED_COMMANDS = {
  down = true,
  up = true,
  action = true,
}

---KEYBOARD---------------------------------------------------------------

function top_menu:on_key_pressed(key)
  local command = command_manager:get_command_from_key(key)
  if ALLOWED_COMMANDS[command] then return current_submenu:process_input(command) end
end


----JOYPAD---------------------------------------------------------------------
function top_menu:on_joypad_button_pressed(button)
  local command = command_manager:get_command_from_button(button)
  if ALLOWED_COMMANDS[command] then return current_submenu:process_input(command) end
end

function top_menu:on_joypad_hat_moved(hat,direction8)
  local command = command_manager:get_command_from_hat(hat, direction8)
  if ALLOWED_COMMANDS[command] then return current_submenu:process_input(command) end
end

--Avoid analog stick wildly jumping
local joy_avoid_repeat = {-2, -2}
local first_joypress = true

function top_menu:on_joypad_axis_moved(axis,state)
  local handled = joy_avoid_repeat[axis % 2] == state
  joy_avoid_repeat[axis % 2] = state

  if handled or first_joypress then
    first_joypress = false
    return
  end

  if top_menu.joypad_just_moved then return end

  local command = command_manager:get_command_from_axis(axis, state)
  if ALLOWED_COMMANDS[command] then
    top_menu.joypad_just_moved = true
    sol.timer.start(sol.main, 20, function() top_menu.joypad_just_moved = false end)
    return current_submenu:process_input(command)
  end
end


return top_menu