local game_manager = require("scripts/trillium/game_manager")
local confirm_menu = require("scripts/trillium/menus/title_screen/confirm")

local save_file = "save1.dat"

local common_config = require("scripts/trillium/menus/title_screen/common_config"):get_config()
common_config.options = {
  "continue",
  "new_game",
  "options",
  "quit",
}
local menu = require("scripts/trillium/menus/title_screen/title_menu_factory").new(common_config)

function menu:process_selection(option)
  if option == "continue" then
    if menu.previous_save_exists then
      local game = game_manager:create(save_file)
      game:start()
    else
      sol.audio.play_sound("wrong")
    end
  elseif option == "new_game" then
    confirm_menu.parent_menu = menu
    confirm_menu.confirmation_callback = function()
      sol.game.delete(save_file)
      local game = game_manager:create(save_file)
      sol.timer.start(sol.main, 20, function() game:start() end)
    end
    menu:switch_to_menu(confirm_menu)
  elseif option == "options" then
    menu:switch_to_menu(require("scripts/trillium/menus/title_screen/options"))
  elseif option == "quit" then
    sol.main.exit()
  end
end

function menu:on_started()
  menu.previous_save_exists = sol.game.exists("save1.dat")
  if not menu.previous_save_exists then
    menu:set_option_color("continue", {150,150,150})
    menu:set_cursor_index(2)
  else
    menu:set_option_color("continue", {255,255,255})
  end
end


return menu