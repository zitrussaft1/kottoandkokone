local master_script = require("scripts/meta/master")
local language_manager = require"scripts/trillium/language_manager"
  master_script:set("default", "bush")
local font, font_size = language_manager:get_menu_font()
  master_script:set("default", "enemy")

local origin_x, origin_y = 30, 50
local stat_width = 176

--Exp requirement formula:
local function required_exp(current_level)
  local needed_exp = 1.545 * (current_level + 1) - 1.454
  return math.floor(needed_exp)
end

--Config:
local config = {
  origin = {x=origin_x, y=origin_y},
  grid_size = {columns = 1, rows = 5},
  cell_size = {width = stat_width, height = 16},
  cell_spacing = 4,
  edge_spacing = 8,
  cursor_style = "menus/arrow",
  cursor_offset = {x=-8, y=3},
  cursor_sound = "cursor",
  background_png = "menus/inventory/options_background.png",
  background_offset = {x=0, y=-3},
}

--Set up stats:
local stats_list = {
  "life",
  "magic",
  "attack",
  "defense",
  "magic_attack",
}
local stats_max = {
  life = 20,
  magic = 20,
  attack = 20,
  defense = 20,
  magic_attack = 20,
}
local allowed_objects = {}
for i, stat in pairs(stats_list) do
  local ob = {}
  ob.name = stat
  ob.text_config = {
    font = font,
    font_size = font_size,
    vertical_alignment = "top",
    text_key = "menu.level_up.stat." .. stat,
  }
  ob.text_offset = {x=4, y=0}
  allowed_objects[i] = ob
end
config.allowed_objects = allowed_objects

--Set up aux menus:
local stat_description_panel = require("scripts/trillium/menus/level_up/stat_description")
stat_description_panel.x, stat_description_panel.y = origin_x + 12, origin_y + 112

config.aux_menus = {
  stat_description_panel,
}

local menu = require("scripts/trillium/menus/trillium_menu_lib/grid_menu").create(config)

local requirements_surface = sol.surface.create()
local requirement_icon = sol.sprite.create"entities/trillium/items"
  master_script:set("trillium", "enemy")
requirement_icon:set_animation("collectibles/soul_crystal")
  master_script:set("trillium", "bush")
local requirement_txt = sol.text_surface.create{
  font = font,
  font_size = font_size,
}

menu:register_event("on_started", function()
  local game = sol.main.get_game()
  game:set_suspended()
  menu:update_stat_display()
end)


function menu:on_finished()
  local game = sol.main.get_game()
  game:set_suspended(false)
end


function menu:update_stat_display()
  local game = sol.main.get_game()
  --Set stat levels
  for _, stat_ob in pairs(menu.allowed_objects) do
    local stat = stat_ob.name
    local stat_level = game:get_value("stat_" .. stat) or 0
    local predicted_size = sol.text_surface.get_predicted_size(font, font_size, sol.language.get_string("menu.level_up.stat." .. stat) )
    local num_spaces = math.floor((stat_width - predicted_size) / 6)
    local final_text = sol.language.get_string("menu.level_up.stat." .. stat)
    for i = 1, num_spaces do
      final_text = final_text .. " "
    end
    final_text = final_text .. stat_level
    print(final_text)
    stat_ob.text_config.text = final_text
    if stat_level >= stats_max[stat] then
      --ideally, do some color modulation to indicate its at max level
    end
  end
  --Update stat text
  menu:update_objects()

  --Set levelup exp requirement
  requirements_surface:clear()
  requirement_icon:draw(requirements_surface, 16, 13)
  local hero_level = game:get_value("stat_hero_level") or 1
  local current_exp = game:get_item("collectibles/soul_crystal"):get_amount() or 0
  local req_txt = sol.language.get_string("menu.level_up.requirement")
  requirement_txt:set_text(req_txt .. "          " .. current_exp .. " / " .. required_exp(hero_level) )
  requirement_txt:draw(requirements_surface, 24, 8)
end


menu:register_event("on_draw", function(self, dst)
  requirements_surface:draw(dst, origin_x, origin_y + 136)
end)


menu:register_event("on_command_pressed", function(self, command)
  local game = sol.main.get_game()
  local handled = false
  if command == "action" then
    menu:process_stat_increase()

  elseif command == "attack" then
    sol.menu.stop(menu)
    handled = true
  end
  return handled
end)


function menu:process_stat_increase()
  local game = sol.main.get_game()
  local stat = menu:get_selected_object().name
  local stat_save_id = "stat_" .. stat
  local current_level = game:get_value(stat_save_id) or 0
  local hero_level = game:get_value("stat_hero_level") or 1

  if current_level >= stats_max[stat] then
    sol.audio.play_sound"wrong"
    return
  end

  local exp_cost = required_exp(hero_level)
  local current_exp = game:get_item("collectibles/soul_crystal"):get_amount()
  if current_exp < exp_cost then
    sol.audio.play_sound"wrong"
    return
  end
  game:get_item("collectibles/soul_crystal"):remove_amount(exp_cost)

  game:set_value(stat_save_id, current_level + 1)
  game:set_value("stat_hero_level", hero_level + 1)
  menu:update_stat_display()

  if stat == "life" then
    game:add_max_life(2)
    game:set_life(game:get_max_life())
  elseif stat == "magic" then
    game:set_max_magic(game:get_max_magic() + 10)
    game:set_magic(game:get_max_magic())
  end
end


return menu