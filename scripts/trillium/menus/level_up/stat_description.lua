local language_manager = require"scripts/trillium/language_manager"
local font, font_size = language_manager:get_menu_font()

local menu = {x=0, y=0}

local text_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
  vertical_alignment = "top"
}

function menu:notify(parent)
  local selected_stat = parent:get_selected_object().name
  text_surface:set_text_key("menu.level_up.description." .. selected_stat)
end

function menu:on_draw(dst)
  text_surface:draw(dst, menu.x, menu.y)
end

return menu