-- This script initializes game values for a new savegame file.
-- You should modify the initialize_new_savegame() function below
-- to set values like the initial life and equipment
-- as well as the starting location.
--
-- Usage:
-- local initial_game = require("scripts/trillium/initial_game")
-- initial_game:initialize_new_savegame(game)

local initial_game = {}

-- Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game)

  sol.audio.set_sound_volume(50)
  sol.audio.set_music_volume(50)

  game:set_starting_location("debug", "destination")  -- Starting location.

  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_max_money(1000)
  game:set_max_magic(100)
  game:set_magic(100)
  game:set_ability("lift", 1)
  game:set_ability("sword", 0)
  game:set_ability("swim", 1)
  game:set_value("equipped_weapon", "solforge/basic_sword")

  game:set_value("charm_slots", 5)

end

return initial_game