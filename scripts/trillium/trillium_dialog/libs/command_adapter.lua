--[[
Created by Max Mraz, licensed MIT
Adapter to interface with whatever button/command binding system is in place. Should be rewritten to be simpler with Solarus 1.7
--]]

local command_binding_manager = require("scripts/trillium/misc/command_binding_manager")

local manager = {}

local button_name_conversions = {
  ["button 0"] = "a",
  ["button 1"] = "b",
  ["button 2"] = "x",
  ["button 3"] = "y",
  ["button 4"] = "left_shoulder",
  ["button 5"] = "right_shoulder",
  ["button 6"] = "back", --"select", but it's "share" on Xbox
  ["button 7"] = "start",
  ["button 8"] = "left_stick",
  ["button 9"] = "right_stick",
  ["axis 0 -"] = "axis_left",
  ["axis 0 +"] = "axis_left",
  ["axis 1 -"] = "axis_left",
  ["axis 1 +"] = "axis_left",

  ["axis 3 -"] = "axis_right",
  ["axis 3 +"] = "axis_right",
  ["axis 4 -"] = "axis_right",
  ["axis 4 +"] = "axis_right",

  ["axis 2 +"] = "trigger_left",
  ["axis 2 -"] = "trigger_left",
  ["axis 5 +"] = "trigger_right",
  ["axis 5 -"] = "trigger_right",
}


function manager:get_sprite_for_command(command)
  --Get xbox-style button name (used as animation name for sprite)
  local button = manager:get_button_binding(command)
  --Convert to format path/to/sprite?animation=button_name , to be used as inline dialog sprite
  local sprite_id = manager:get_sprite_id_for_button(button)
  return sprite_id
end


function manager:get_button_binding(command)
  local button = command_binding_manager:get_command_joypad_binding(command)
  --Convert to normal button names (prep for Solarus 1.7):
  local button_name = button_name_conversions[button]
  return button_name
end


function manager:get_sprite_id_for_button(button_name)
  --For now, hardcoded to use "generic" button icons, rather than checking Xbox, PS, or Switch
  local sprite_id = "hud/button_icons/generic?animation=" .. button_name
  return sprite_id
end


return manager