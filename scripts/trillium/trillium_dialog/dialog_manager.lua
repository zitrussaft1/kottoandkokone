local master_script = require("scripts/meta/master")
local game_meta = sol.main.get_metatable"game"
local dialog_menu = require("scripts/trillium/trillium_dialog/libs/dialog_menu")
local overrun_checker = require("scripts/trillium/trillium_dialog/libs/overrun_checker")


--Start dialog
function game_meta:start_dialog(dialog_id, info, callback)
  local game = self
  --Allow info to be optional:
  if type(info) == "function" then
    callback = info
    info = {}
  end
  if info == nil then
    info = {}
  end
  if callback == nil then
    callback = function() end
  end
  dialog_menu.dialog = sol.language.get_dialog(dialog_id)
  dialog_menu.info = info
  dialog_menu.callback = callback

  local hero = game:get_hero()
  hero:set_animation("stopped")

  --Suspend game if not already suspended
  if not game:is_suspended() then
    dialog_menu.activation_suspended_game = true
    game:set_suspended(true)
  else
    dialog_menu.activation_suspended_game = false
  end

  --Hide HUD
  local hud = game:get_hud()
  if hud:is_enabled() then
    hud:set_enabled(false)
    dialog_menu.activation_disabled_hud = true
  else
    dialog_menu.activation_disabled_hud = false
  end

  sol.menu.start(game, dialog_menu)
end


-- End dialog
game_meta:register_event("on_dialog_finished", function(game, dialog)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  if sol.menu.is_started(dialog_menu) then
    sol.menu.stop(dialog_menu)
  end
  --Unsuspend the game if the dialog suspended it initially
  if dialog_menu.activation_suspended_game then
    dialog_menu.activation_suspended_game = nil
    game:set_suspended(false)
  end
  --Show HUD if we hid it
  if dialog_menu.activation_disabled_hud then
    dialog_menu.activation_disabled_hud = nil
    game:get_hud():set_enabled(true)
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)



function sol.main:check_dialog_overruns(max_length)
  if not max_length then max_length = 352 end
  for _, language_id in pairs(sol.language.get_languages()) do
    overrun_checker:check_dialog_overruns(language_id, max_length)
  end
end