local master_script = require("scripts/meta/master")
local hero_meta = sol.main.get_metatable"hero"

local dash_distances = {80, 104}
local dash_speeds = {300, 400}
local dash_animations = {"roll", "roll"}
local dash_sounds = {"dash", "dash"}
local invincibility_lengths = {500, 900}
local minimum_distance = 12 --after this distance, the dash will stop before you hit water, a hole, etc
local recovery_length = 200

local movement_id = 1
local current_movement


local dash_state = sol.state.create("dashing")
dash_state:set_can_control_direction(false)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
dash_state:set_can_control_movement(false)
dash_state:set_can_traverse_ground("hole", true)
dash_state:set_can_traverse_ground("deep_water", true)
dash_state:set_can_traverse_ground("lava", true)
dash_state:set_affected_by_ground("hole", false)
dash_state:set_affected_by_ground("deep_water", false)
dash_state:set_affected_by_ground("lava", false)
dash_state:set_gravity_enabled(false)
dash_state:set_can_come_from_bad_ground(false)
dash_state:set_can_be_hurt(false)
dash_state:set_can_use_sword(false)
dash_state:set_can_use_item(false)
dash_state:set_can_interact(false)
dash_state:set_can_grab(false)
dash_state:set_can_push(false)
dash_state:set_can_pick_treasure(true)
dash_state:set_can_use_teletransporter(true)
dash_state:set_can_use_switch(true)
dash_state:set_can_use_stream(false)
dash_state:set_can_use_stairs(true)
dash_state:set_can_use_jumper(true)
dash_state:set_carried_object_action("throw")

local dash_recovery_state = sol.state.create("dash_recovery")
dash_recovery_state:set_can_control_direction(true)
dash_recovery_state:set_can_control_movement(true)
dash_recovery_state:set_gravity_enabled(false)
dash_recovery_state:set_can_come_from_bad_ground(false)
dash_recovery_state:set_can_be_hurt(false)
dash_recovery_state:set_can_use_sword(false)
dash_recovery_state:set_can_use_item(false)
dash_recovery_state:set_can_interact(false)
dash_recovery_state:set_can_grab(false)
dash_recovery_state:set_can_push(false)

function dash_recovery_state:on_started()
  local hero = dash_recovery_state:get_entity()
  hero.dash_cooldown = true
  sol.timer.start(hero, (BASE_DASH_COOLDOWN + BONUS_DASH_COOLDOWN) * 1000, function() hero.dash_cooldown = false end)
  if hero:get_sprite():has_animation("dash_recovery") then
    hero:set_animation("dash_recovery", function()
      hero:set_animation"stopped"
    end)
  else
    hero:set_animation"stopped"
  end
  sol.timer.start(hero, recovery_length, function()
    hero:unfreeze()
  end)
end



function hero_meta:dash()
  local hero = self
  local game = hero:get_game()
  local map = hero:get_map()
  local intended_direction8 = (game:get_commands_direction() or hero:get_direction() * 2)
  local intended_angle = intended_direction8 * math.pi / 4

  if hero.dash_cooldown then return end

  --OPTIONAL: if there are dash upgrades, set dash level. These correspond to values in dash_distances and dash_speeds tables
  local dash_level = game:get_value("dash_level") or 1

  --Start state
  dash_state:set_can_be_hurt(false)
  hero:start_state(dash_state)
  sol.timer.start(dash_state, invincibility_lengths[dash_level], function()
    dash_state:set_can_be_hurt(true)
  end)

  --i-frames
  hero:set_invincible(true, invincibility_lengths[dash_level])

  --destroy any breakable objects that are close
  local hx, hy, hz = hero:get_position()
  for entity in map:get_entities_in_rectangle(hx - 2, hy - 2, 4, 4) do
    if (entity:get_layer() == hz) and (entity:get_type() == "custom_entity") and (entity:get_model() == "world_objects/breakable_object") then
      entity:destroy()
    end
  end


  local function end_dash()
    local state, state_ob = hero:get_state()
    local ground = map:get_ground(hero:get_position())
    if ground == "hole" then
      hero:unfreeze()
    elseif state_ob and state_ob:get_description() ~= "dash_recovery" then
      hero:start_state(dash_recovery_state)
    end
  end

  --create movement
  hero:set_direction(intended_direction8 / 2)
  local m = sol.movement.create("straight")
  m:set_angle(intended_angle)
  m:set_speed(dash_speeds[dash_level])
  m:set_max_distance(dash_distances[dash_level])
  m:set_smooth(true)
  m:start(hero, function()
      hero.dashing = false
      end_dash()
  end)
  function m:on_obstacle_reached()
    hero.dashing = false
    end_dash()
  end

  --check to see if there's room to dash
  local obstacle_test_distance = 8
  local is_obstacle = hero:test_obstacles(obstacle_test_distance * math.cos(intended_angle), obstacle_test_distance * math.sin(intended_angle) * -1 )
  if is_obstacle then
    hero:unfreeze()
    return
  end

  --Animation/Sound
  hero:set_animation(dash_animations[dash_level], function()
    hero:set_animation("walking")
  end)
  sol.audio.play_sound(dash_sounds[dash_level])


  --create little dust effects
  --local x, y, z = hero:get_position()
  local num_clouds = 4
  local cloud_delay = 40
  for i = 0, num_clouds - 1 do
    sol.timer.start(map, cloud_delay * i, function()
      local hx, hy, hz = hero:get_position()
      local ground = map:get_ground(hx, hy, hz)
      local dust_cloud = map:create_custom_entity({
        direction = 0, x = hx, y = hy, layer = hz, width = 16, height = 16,
        sprite = "entities/trillium/roll_effect", model = "ephemeral_effect"
      })
      if ground == "shallow_water" then
        dust_cloud:get_sprite():set_animation("ripple")
      end
    end)
  end


  if game:get_value("have_fun") == "true" then
    -- Increase hero movement speed by 15% for 10 seconds
    local speed = hero:get_walking_speed()*0.15
    hero:set_walking_speed(
      hero:get_walking_speed() + speed
    )
    sol.timer.start(hero, 10000, function()
      hero:set_walking_speed(
        hero:get_walking_speed() - speed
      )
    end)
  end

  --Stop short if you're about to go into a hole
  sol.timer.start(hero, minimum_distance / dash_speeds[dash_level] * 1000, function()
    local x, y, z = hero:get_position()
    local ground = map:get_ground(x, y, z)
    --if you're already over dangerous ground
    if (ground == "hole") or (ground == "deep_water") or (ground == "lava") or (ground == "prickles") then
      --stop the hero so they fall into the hole or whatever, or comment out to allow for dashing over holes:
      m:stop()
      hero:unfreeze()
    else
      m:register_event("on_position_changed", function()
        local x, y, z = hero:get_position()
        local angle = m:get_angle()
        local tx, ty = x + math.cos(angle), y + math.sin(angle)
        local ground = map:get_ground(tx, ty, z)
        if (ground == "hole") or (ground == "deep_water") or (ground == "lava") or (ground == "prickles") then
          m:stop()
          sol.timer.start(hero, 100, function()
            hero:unfreeze()
          end)
        end
      end)
    end
  end)

  --Add a trigger for charms that the hero has just started dashing
  game.charm_manager:trigger_event("on_dashing")
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end