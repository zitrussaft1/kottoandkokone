local game_meta = sol.main.get_metatable"game"

local max_slots = 3
local all_categories = {"melee", "gun", "item_1", "item_2"}


--Equips appropriate items based on active quickswap slots
--Note: this does equip items for every category, so most of it is redundant whenever its called, but you would never have 
function game_meta:equip_active_quickswap_slots()
  local game = self

  for _, category in pairs(all_categories) do
    local active_slot = game:get_quickswap_active_slot(category) or 1
    local new_item = game:get_quickswap_item(category, active_slot)
    if category == "melee" then
      game:set_value("equipped_weapon", new_item)
    elseif category == "gun" then
      game:set_value("equipped_gun", new_item)
    elseif category == "item_1" and new_item then
      game:set_item_assigned(1, game:get_item(new_item))
    elseif category == "item_2" and new_item then
      game:set_item_assigned(2, game:get_item(new_item))
    end
  end

end

--Quickswap
function game_meta:quickswap(category)
  local game = self
  local current_slot = game:get_quickswap_active_slot(category)
  local current_item = game:get_quickswap_item(category, current_slot)

  local assigned_items = {}
  for i = 1, max_slots do
    assigned_items[i] = game:get_quickswap_item(category, i)
  end
  local new_slot = (current_slot) % #assigned_items + 1
  local new_item = assigned_items[new_slot]

  game:set_quickswap_active_slot(category, new_slot)
  game:equip_active_quickswap_slots()

  if current_item ~= new_item then
    sol.audio.play_sound"switch"
  end
end


--Getters/setters
function game_meta:get_quickswap_item(category, slot)
  return self:get_value("quickswap_slot_" .. category .. "_" .. slot)
end

function game_meta:set_quickswap_item(category, slot, item_id)
  local game = self
  --If this item is already equipped to a different slot, unequip it
  local already_equipped_slot = nil
  for i = 1, max_slots do
    local already_item = self:get_quickswap_item(category, i)
    if already_item and already_item == item_id then
      self:set_quickswap_item(category, i, nil)
    end
  end
  self:set_value("quickswap_slot_" .. category .. "_" .. slot, item_id)
  self:equip_active_quickswap_slots() --equip active slots in case the currently active slot has been overwritten with a new item
end

function game_meta:get_quickswap_active_slot(category)
  return self:get_value("quickswap_active_slot_" .. category) or 1
end

function game_meta:set_quickswap_active_slot(category, new_slot)
  self:set_value("quickswap_active_slot_" .. category, new_slot)
end
