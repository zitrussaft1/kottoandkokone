local hero_meta = sol.main.get_metatable"hero"

local walking_speed
local speed_boost = 40

local state = sol.state.create"jogging"
state:set_can_control_movement(true)
state:set_can_control_direction(true)

function state:on_started()
  local hero = state:get_entity()
  hero:set_animation"walking"
  walking_speed = hero:get_walking_speed()
  hero:set_walking_speed(walking_speed + speed_boost)
end

function state:on_finished()
  local hero = state:get_entity()
  hero:set_walking_speed(walking_speed)
end

function state:on_command_released(command)
  local hero = state:get_entity()
  if command == "action" then
    hero:unfreeze()
  end
end


function hero_meta:start_jogging()
  local hero = self
  hero:start_state(state)
end

