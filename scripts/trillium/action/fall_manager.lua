--[[
Created by Max Mraz, licensed MIT
Allows an entity to call entity:fall([props], callback)  (props is optional)
Props table values:
speed: between 1 and 100 (default 100)
distance: how far above its position the entity falls from (default 300px)
step: how much the entity moves per distance ms (default ??)
angle: unused
--]]


local hero_meta = sol.main.get_metatable"hero"
local enemy_meta = sol.main.get_metatable"enemy"
local custom_entity_meta = sol.main.get_metatable"custom_entity"

local function fall(entity, props, callback)
  --allow optional param
  if type(props) == "function" then
    callback = props
    props = {}
  end
  local speed = props.speed or 100
  local distance = props.distance or 300
  local step = props.step or 5
  local angle = props.angle or 3 * math.pi / 2

  local sprite = entity:get_sprite()
  local offset = distance
  sprite:set_xy(0, offset * -1)
  sol.timer.start(entity, 110 - (speed or 100), function()
    offset = offset - step
    if offset < 0 then offset = 0 end
    sprite:set_xy(0, offset * -1)
    if offset > 0 then
      return true
    else
      callback()
    end
  end)
end

hero_meta.fall = fall
enemy_meta.fall = fall
custom_entity_meta.fall = fall