local master_script = require("scripts/meta/master")
local debug_keys = {}

function debug_keys:initialize(game)
  master_script:set("trillium", "map")
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")

  local DEBUG_MODE = sol.main.debug_mode
  local ignoring_obstacles

  function game:set_debug_mode(mode) DEBUG_MODE = mode end

  --Debug Keys
  game:register_event("on_key_pressed", function(self, key, modifiers)
    local hero = game:get_hero()

    if key == "r"  and DEBUG_MODE then
      if hero:get_walking_speed() == 300 then
        hero:set_walking_speed(debug.normal_walking_speed)
      else
        debug.normal_walking_speed = hero:get_walking_speed()
        hero:set_walking_speed(300)
      end

    elseif key == "t" and DEBUG_MODE then
      if not ignoring_obstacles then
        hero:get_movement():set_ignore_obstacles(true)
        ignoring_obstacles = true
      else
        hero:get_movement():set_ignore_obstacles(false)
        ignoring_obstacles = false
      end

    elseif key == "h" and DEBUG_MODE and modifiers.control then
      if modifiers.shift then
        game:set_max_life(game:get_max_life() - 2)
      else
        game:add_max_life(2)
        game:set_life(game:get_max_life())
      end

    elseif key == "h" and DEBUG_MODE then
      if modifiers.shift then
        game:remove_life(2)
      else
        game:set_life(game:get_max_life())
      end

    elseif key == "j" and DEBUG_MODE then
      game:set_magic(game:get_max_magic())

    --Sword, Bow, and Defense
    elseif key == "k" and DEBUG_MODE and modifiers.control then
      sol.audio.play_sound"cursor"
      if modifiers.shift then
        game:set_value("bow_damage", game:get_value("bow_damage") - 1)
      else
        game:set_value("bow_damage", game:get_value("bow_damage") + 1)
      end
      print("Bow Damage: ", game:get_value"bow_damage")

    elseif key == "k" and DEBUG_MODE then
      sol.audio.play_sound"cursor"
      if modifiers.shift then
        game:set_value("sword_damage", game:get_value("sword_damage") - 1)
      else
        game:set_value("sword_damage", game:get_value("sword_damage") + 1)
      end
      print("Sword damage: ", game:get_value"sword_damage")

    elseif key == "l" and DEBUG_MODE then
      sol.audio.play_sound"cursor"
      if modifiers.shift then
        game:set_value("defense", game:get_value("defense") - 1)
      else
        game:set_value("defense", game:get_value("defense") + 1)
      end
      print("Defense: ", game:get_value"defense")
    --------

    elseif key == "l" and DEBUG_MODE then

    elseif key == "m" and DEBUG_MODE then
      print("You are on map: " .. game:get_map():get_id())
      local x, y, l = hero:get_position()
      print("at coordinates: " .. x .. ", " .. y .. ", " .. l)

    elseif key == "y" and DEBUG_MODE then
      --helicopter shot
      if not game.helicopter_cam then
        game:get_map():helicopter_cam()
      else
        game:get_map():exit_helicopter_cam()
        require("scripts/trillium/action/hole_drop_landing"):play_landing_animation()
      end

    elseif key == "n" and DEBUG_MODE then
      game:set_value("hard_mode", not game:get_value"hard_mode")
      print("Hard mode: ", game:get_value"hard_mode")

    elseif key == "u" and DEBUG_MODE then
      game:get_hud():set_enabled(not game:get_hud():is_enabled())

    elseif key == "i" and DEBUG_MODE then
      game:set_ability("lift", 1)
      game:set_ability("sword", 1)
      game:set_ability("swim", 1)

    end
  end)

  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  master_script:set("default", "map")
end

return debug_keys