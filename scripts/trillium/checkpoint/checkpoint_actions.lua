local master_script = require("scripts/meta/master")
local game_meta = sol.main.get_metatable"game"

function game_meta:save_checkpoint(x, y, z)
  local game = self
  local hero = game:get_hero()
  if not x then --assumes the code is just called like game:save_checkpoint()
    x, y, z = hero:get_position()
  end
  game:set_value("checkpoint_map", game:get_map():get_id())
  game:set_value("checkpoint_x", x)
  game:set_value("checkpoint_y", y)
  game:set_value("checkpoint_z", z)
end


function game_meta:refill_respawn_items()
  master_script:set("trillium", "item")
  local game = self
  master_script:set("default", "item")
  if not game.fill_on_checkpoint_items then return end
  --Items to be refilled on checkpoints must register themselves to the game.fill_on_checkpoint_items table
  for k, item in pairs(game.fill_on_checkpoint_items) do
    item:set_amount(item:get_max_amount())
  end
end


function game_meta:respawn_at_checkpoint(callback)
  local game = self
  local hero = game:get_hero()
  local checkpoint_map = game:get_value"checkpoint_map"
  if not checkpoint_map then return end
  game:set_suspended(true)
  sol.timer.start(sol.main, 10, function()
    hero:teleport(checkpoint_map, "_same", "immediate")
    hero:set_position(game:get_value"checkpoint_x", game:get_value"checkpoint_y", game:get_value"checkpoint_z")
    hero:set_direction(3)
    game:set_life(game:get_max_life())
    game:set_magic(game:get_max_magic())
    game:refill_respawn_items()
    if callback then callback() end
  end)
end
