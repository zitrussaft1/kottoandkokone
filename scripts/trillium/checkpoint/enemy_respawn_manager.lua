local master_script = require("scripts/meta/master")
--[[
Created by Max Mraz, licensed MIT
Sets up a Dark Souls style respawn system for enemies. Enemies will not respawn unless you die, or map:respawn_enemies() is called

Note: how to match CID:
cid:match("%d+,%d+,%d+,[%w_/]")
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
--]]

local map_meta = sol.main.get_metatable"map"
local game_meta = sol.main.get_metatable"game"
local enemy_meta = sol.main.get_metatable"enemy"

local manager = {}
local master_enemy_table = {}
local master_age_table = {}

--Optional: allow enemies to respawn after you travel to X number of maps
local enemies_respawn_after_time = true
local map_age_respawn_threshold = 8


local function save_enemy_table()
  local game = sol.main.get_game()
  game:set_table_value("respawn_enemy_table", master_enemy_table)
end

game_meta:register_event("on_started", function(self)
  master_enemy_table = self:get_table_value("respawn_enemy_table") or {}
end)


local function reset_enemy_table()
  for map_id, map_table in pairs(master_enemy_table) do
    for cid, enemy in pairs(map_table) do
      enemy.killed = false
    end
  end
end


local function is_dynamic_enemy(map_id, cid)
  --If the enemy isn't on the table, it was dynamically created, we can ignore it re: respawning, etc
  return (master_enemy_table[map_id][cid] == nil)
end


function map_meta:manage_spawns() --called on map:on_started
  local map = self
  local map_id = map:get_id()
  if not master_enemy_table[map_id] then --first map load, save enemies to memory
    master_enemy_table[map_id] = {}
    --create an environment to read the map.dat, set a metatable function so most entries on the map.dat will be functions that return nothing
    local env = setmetatable({}, {__index = function() return function() end end})
    --process enemy entiries on the map.dat --each "enemy" on the map.dat is a function with a single argument, a table of properties. Save that table to master_enemy_table[current_map_id]
    function env.enemy(props)
      local x, y, layer, breed = props.x, props.y, props.layer, props.breed
      local cid = x .. "," .. y .. "," .. layer .. "," .. breed
      local enemy_table = {
        cid = cid,
        killed = false,
      }
      for k, v in pairs(props) do
        enemy_table[k] = v
      end
      master_enemy_table[map_id][cid] = enemy_table
    end
    local chunk = sol.main.load_file("maps/trillium/" .. map_id .. ".dat")
    setfenv(chunk, env)
    chunk()
    --Initialize the map's "age"
    master_age_table[map_id] = 0
  end

  for enemy in map:get_entities_by_type("enemy") do
    local x, y, layer = enemy:get_position()
    local breed = enemy:get_breed()
    local cid = x .. "," .. y .. "," .. layer .. "," .. breed
    --Save enemies to table when killed
    enemy:register_event("on_dying", function()
      if is_dynamic_enemy(map_id, cid) then return end
      master_enemy_table[map_id][cid].killed = true
      save_enemy_table()
    end)
    --If the enemy isn't on the table, it was dynamically created: ignore it
    if is_dynamic_enemy(map_id, cid) then

    --if enemy has already been killed, remove it
    elseif master_enemy_table[map_id][cid].killed then
      --Call on_unspawned event - enemies might need to manage some dynamic enemies they've created if they get unspawned
      if enemy.on_unspawned then enemy:on_unspawned() end
      enemy:remove()
    end

  end

  --Optional: maps "age" when you aren't on them, after a threshold, enemies may come back
  for idno, age in pairs(master_age_table) do
    --Don't iterate age on our current map
    if map_id == idno then
      master_age_table[idno] = 0
    else
      master_age_table[idno] = age + 1
    end
    if (age > map_age_respawn_threshold) and enemies_respawn_after_time then
      master_age_table[idno] = nil
    end
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end



function map_meta:respawn_enemies()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local map = self
  local map_id = map:get_id()
  reset_enemy_table()
  save_enemy_table()
  --Reset the map's "age"
  master_age_table[map_id] = 0
  --Remove all enemies from the map
  for enemy in map:get_entities_by_type("enemy") do
    if enemy.on_unspawned then enemy:on_unspawned() end
    enemy:remove()
  end
  --Create a new enemy for every enemy in the table for the current map
  for cid, enemy in pairs(master_enemy_table[map_id]) do
    local created_enemy = map:create_enemy{
      name = enemy.name,
      x = enemy.x, y = enemy.y, layer = enemy.layer,
      direction = enemy.direction,
      breed = enemy.breed,
      savegame_variable = enemy.savegame_variable,
      treasure_name = enemy.treasure_name, treasure_variant = enemy.treasure_variant, treasure_savegame_variable = enemy.treasure_savegame_variable,
      enabled_at_start = enemy.enabled_at_start,
      properties = enemy.properties,
    }
    --Save enemies to table when killed
    created_enemy:register_event("on_dying", function()
      master_enemy_table[map_id][cid].killed = true
      save_enemy_table()
    end)
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end


map_meta:register_event("on_started", function(self)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  self:manage_spawns()
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


game_meta:register_event("on_game_over_started", function(self)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  reset_enemy_table()
  save_enemy_table()
  --self:get_map():respawn_enemies()
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

