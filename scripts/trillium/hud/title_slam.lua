--[[
Created by Max Mraz, licensed MIT
A script to do a title slam. Must be required (probably in features.lua)
--]]

local menu = {}

local screen_width, screen_height = sol.video.get_quest_size()

local txt_surface = sol.text_surface.create{
  font = "roboto_condensed",
  font_size = 64,
  vertical_alignment = "middle",
  horizontal_alignment = "center",
}

function menu:on_started()
  sol.audio.play_sound"running_obstacle"
  txt_surface:set_text(menu.title_text)
  if menu.display_timer then menu.display_timer:stop() end
  menu.display_timer = sol.timer.start(menu, 2500, function()
    sol.menu.stop(menu)
  end)
end


function menu:on_draw(dst)
  txt_surface:draw(dst, screen_width / 2, screen_height / 2 )
end


local map_meta = sol.main.get_metatable"map"

function map_meta:title_slam(text)
  menu.title_text = text
  sol.menu.start(self, menu)
end

return menu