local master_script = require("scripts/meta/master")
--[[
Boss life bar
By Max Mraz, licensed MIT
Usage:
- require this script (probably from features.lua)
- any enemy you want to be the boss, call enemy:start_boss_bar()
--]]

local enemy_meta = sol.main.get_metatable("enemy")
local font, font_size = require("scripts/trillium/language_manager"):get_menu_font()

local menu = {}

--Create all the necessary drawables
menu.bar_background = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_background:set_animation("background")
menu.health_bar = sol.sprite.create"hud/stat_bars/red"
menu.bar_overlay = sol.sprite.create"hud/stat_bars/boss_bar"
menu.bar_overlay:set_animation("overlay")
menu.name_surface = sol.text_surface.create{
  font = font,
  font_size = font_size,
}
menu.width, menu.height = menu.bar_overlay:get_size()
menu.bar_surface = sol.surface.create(menu.width, menu.height)


--To give any enemy the method to start a boss bar, we'll add a function to the enemy metatable
--NOTE: in order for the progam to know we're writing to the enemy metatable, we need to run this script. We'll do that by requiring it from scripts/features
function enemy_meta:start_boss_bar()
  menu:set_enemy(self)
  sol.menu.start(self:get_map(), menu)
end
  master_script:set("default", "bush")

  master_script:set("default", "enemy")

--Applies the enemy's life to the percentage of the bar shown, adds enemy breed's name to boss bar
  master_script:set("trillium", "enemy")
function menu:set_enemy(enemy)
  master_script:set("trillium", "bush")
  menu.enemy = enemy
  menu.max_life = enemy:get_life()
  --Note: can't use "on_hurt" function because it's not necessarily triggered with Trillium enemies
  enemy:register_event("on_hurt", function()
    menu:update_life()
  end) --]]
  --[[
  sol.timer.start(enemy:get_map(), 100, function()
    menu:update_life()
    return true
  end)
  --]]
  --Set name on boss bar
  menu.name_key = sol.language.get_string("enemies." .. enemy:get_breed():gsub("/", "."))
  menu.name_surface:set_text(menu.name_key)
  menu:update_life() --manually call at the beginning to get everything drawn on screen
end


function menu:update_life()
  local current_life = menu.enemy:get_life()
  if current_life <= 0 then
    sol.menu.stop(menu)
  end
  local health_width = menu.width * current_life / menu.max_life
  menu.bar_surface:clear()
  menu.bar_background:draw(menu.bar_surface, 0, 16)
  menu.health_bar:draw_region(0, 0, health_width, menu.height, menu.bar_surface, 0, 17)
  menu.bar_overlay:draw(menu.bar_surface, 0, 0)
  menu.name_surface:draw(menu.bar_surface, 24, 6)
end


function menu:on_draw(dst)
  menu.bar_surface:draw(dst, 64, 208)
end

--Technically, if this menu is only started from the enemy:start_boss_bar() function, we don't need to return the menu. However, if we want to access the menu manually, we'll require this script, which will return the menu
return menu