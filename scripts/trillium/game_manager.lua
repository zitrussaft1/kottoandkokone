-- Script that creates a game ready to be played.

-- Usage:
-- local game_manager = require("scripts/trillium/game_manager")
-- local game = game_manager:create("savegame_file_name")
-- game:start()

require("scripts/trillium/multi_events")
local initial_game = require("scripts/trillium/initial_game")

local game_manager = {}

-- Creates a game ready to be played.
function game_manager:create(file)

  -- Create the game (but do not start it).
  local exists = sol.game.exists(file)
  local game = sol.game.load(file)
  if not exists then
    -- This is a new savegame file.
    initial_game:initialize_new_savegame(game)
  end

  --Initialize all scripts that need it
  require("scripts/trillium/features").init(game)

  return game
end

return game_manager