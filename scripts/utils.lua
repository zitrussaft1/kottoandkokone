
-- Table utils
Table = {}

function Table:loopdic(table, func, object)
    for k, v in pairs(table) do
        func(object,k,v)
    end
end

function Table:contains(table, value)
    for _, v in pairs(table) do
        if v == value then
            return true
        end
    end
    return false
end

function Table:methodWrapper(method, new_method)
    return function(self, ...)
        method(self, ...)
        new_method(self, ...)
    end
end

function Table:print(tbl, indent)
  if not indent then indent = 0 end
  if indent == 5 then return end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      Table:print(v, indent+1)
    elseif type(v) ~= 'boolean' and type(v) ~= 'userdata' and type(v) ~= 'function' then
      print(formatting .. v)
    end
  end
end

function Table:deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[Table:deepcopy(orig_key)] = Table:deepcopy(orig_value)
        end
        setmetatable(copy, Table:deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function Table:merge(table1, table2)
    -- Loop through table2 and add the items to table1
    for k, v in pairs(table2) do
        table1[k] = v
    end
    return table1
end
    

-- Sets the dialog
function set_dialog(dialog)
  DIALOG = dialog
end
function get_dialog()
  return DIALOG
end

function os_type()
  if os.getenv("HOME") then
    return "unix"
  else
    return "windows"
  end
end

-- String utils
String = {}

function String:split(str, sep)
    local sep, fields = sep or " ", {}
    local pattern = string.format("([^%s]+)", sep)
    str:gsub(pattern, function(c) fields[#fields+1] = c end)
    return fields
end

-- Path utils
Path = {}

function Path:exists(path)
    if path == nil then
        return nil
    end
    return sol.file.exists(path)
end

function Path:is_dir(path)
    if path == nil then
        return nil
    end
    return sol.file.is_dir(path)
end

function Path:is_file(path)
    return not sol.file.is_dir(path) and sol.file.exists(path)
end

function Path:list_files(path)
    if not Path:is_dir(path) then
        return nil
    end

    local last_char = path:sub(-2, -1)
    if last_char == "/ " then
        path = path:sub(1, -3)
    end
    return sol.file.list_dir(path)
end

function Path:remove_last_dir(path)
    local path = path:match("(.+)/")
    return path
end

function Path:first_file(path)
    local files = Path:list_files(path)
    if files == nil then
        return nil
    end
    for _, file in pairs(files) do
        if Path:is_file(path.."/"..file) then
            return path.."/"..file
        end
    end
    return nil
end

function Path:find_closest_file(path)
    if Path:is_file(path) then
        return path
    end
    local path = Path:remove_last_dir(path)
    local file = Path:first_file(path)
    while file == nil do
        path = Path:remove_last_dir(path)
        file = Path:first_file(path)
        if path == nil then
            return nil
        end
    end
    return file
end
--[[
paths = {}
files = {}
function Path:update(path)
    files = {}
    paths = {}
    t = Path:sys_list_files(path)
    for _, file in pairs(t) do
        file = file:gsub("\\", "/")
        file = file:gsub(path, "")
        file = file:gsub("^/", "")
        if Path:get_extension(file) == nil then
            if not Table:contains(paths, file) then
                table.insert(paths, file)
            end
        else
            table.insert(files, file)
        end
    end
end

-- Get file extension
function Path:get_extension(path)
    local path = path:gsub("\\", "/")
    local extension = path:match("^.+(%..+)$")
    return extension
end

-- List files in all directories and subdirectories
function Path:sys_list_files(path)
  local i, t, t_final, popen = 0, {}, {}, io.popen
  if os_type() == "windows" then
    path = path:gsub("/", "\\")
    t = popen('dir "'..path..'" /b'):lines()
  else
    t = popen('find "'..path..'"'):lines()
  end
  for file in t do
    if file ~= "." and file ~= ".." then
      i = i + 1
      t_final[i] = file
    end
  end
  return t_final
end

-- Checks if is a file
function Path:is_file(path)
    return Table:contains(files, path)
end

-- List files in a directory
function Path:is_dir(path)
    for _, file in pairs(paths) do
        if file:match(path) then
            return true
        end
    end
    return Table:contains(paths, path)
end

-- List files in a directory
function Path:list_files(path)
    local files_final = {}
    if path == nil then
        return nil
    end
    for _, file in pairs(files) do
        if file:match(path) then
            table.insert(files_final, file:match(path.."/(.*)"))
        end
    end
    return files_final
end

-- Checks if a path exists
function Path:exists(path)
    if Path:is_file(path) or Path:is_dir(path) then
        return true
    end
    return false
end

-- Remove last directory
function Path:remove_last_dir(path)
    local path = path:gsub("\\", "/")
    local path = path:match("(.-)/")
    return path
end

Path:update("sprites")

-- Tests
print(Path:is_file("light.png"), "true")
print(Path:is_file("doors"), "false")

for _, file in pairs(Path:list_files("switches")) do
    print(file, "*.dat or *.png")
end
print(Path:remove_last_dir("scripts/utils.lua"), "scripts")
print(Path:first_file("sprites/shadows"), "shadows/*.png or shadows/*.dat")
print(Path:find_closest_file("sprites/shadows/lua.png"), "shadows/*.png or shadows/*.dat")

]]
