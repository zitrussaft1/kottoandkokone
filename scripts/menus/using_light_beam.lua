local saltk = require("scripts/saltk/main")

local timer
local can_quit = true
local root = saltk.Image.new({id="root_light_beam",
    position={0, 0}, size={320, 240}, scale=true,
    special_scale=3, sensitive=true})
local angle = 0
root:add_signal("key_pressed", function (Widget, key, modifiers)
    print(key, angle)
    if key == "left" then
        angle = angle - math.pi / 4
        if angle <= -2* math.pi +0.1 then
            angle = 0
        end
        local direction = math.ceil((angle /-(math.pi / 2))) % 4
        sol.main.get_game():get_hero():set_direction(direction)
    elseif key == "right" then
        angle = angle + math.pi / 4
        if angle >= math.pi * 2 -0.1 then
            angle = 0
        end
        local direction = math.ceil((angle/-(math.pi / 2))) % 4
        sol.main.get_game():get_hero():set_direction(direction)
    elseif key == "e" then
        if can_quit then
            sol.menu.stop(root)
        else
            can_quit = true
        end
    end
    return true
end)



local lasers = {}
local hitten_entities = {}
local function summon_laser()
    local game = sol.main.get_game()
    local hero = game:get_hero()
    local map = game:get_map()
    local x, y, layer = hero:get_position()
    x,y = x + math.cos(angle) * 48, y + math.sin(angle) * 48
    
    local has_touched_entity = false

    for _, laser in ipairs(lasers) do
        laser:remove()
    end
    lasers = {}

    while not has_touched_entity do
        local laser = map:create_custom_entity({
            name = "laser",
            x = x,
            y = y,
            layer = layer,
            width = 64,
            height = 64,
            direction = 0,
        })
        laser:set_origin(0,0)

        local spri = laser:create_sprite("ground_effects/red_square")
        spri:set_blend_mode("add")
        spri:set_rotation(-angle)
        spri:set_scale(4.8,2)

        table.insert(lasers, laser)
        if #lasers == 50 then
            has_touched_entity = true
            break
        end

        local function test_entity(entity)
            local hit = laser:overlaps(entity, "sprite") 
            if hit and entity:get_name() ~= "lekops" then
                print(entity:get_name())
                has_touched_entity = true
            end
            return hit
        end

        for entity in map:get_entities_by_type("npc") do
            test_entity(entity)
        end
        
        for entity in map:get_entities_by_type("enemy") do
            if test_entity(entity) and not hitten_entities[entity] then
                if entity.process_hit then
                    entity:on_hurt("laser")
                else
                    entity:hurt(AP())
                end
                hitten_entities[entity] = true
                sol.timer.start(1000, function()
                    hitten_entities[entity] = false
                end)
            end
        end
        for entity in map:get_entities_by_type("custom_entity") do
            if not entity:exists() then
                -- Fix ghost bombs
                entity:remove()
            elseif (entity:get_name() and
                entity:get_name():find("wall")) and
                entity:overlaps(laser) then
                has_touched_entity = true
            elseif (entity:get_name() and entity:get_name():find("bomb")) and test_entity(entity) then
                entity:stop_movement()
                local movement = sol.movement.create("straight")
                movement:set_speed(128)
                movement:set_angle(-angle)
                movement:set_smooth(false)
                movement:set_max_distance(16)
                movement:start(entity)
            end
        end
        x = x + math.cos(angle) * 16
        y = y + math.sin(angle) * 16
    end
end

function root:on_finished()
    if timer then
       timer:stop()
       timer = nil
    end
    for _, laser in ipairs(lasers)  do
        laser:remove()
    end
    sol.main.get_game():get_hero():unfreeze()
end

function root:set_angle(new_angle)
    angle = new_angle
end

function root:can_quit(state)
    can_quit = state
    if not state then
        timer = sol.timer.start(100, function()
            summon_laser()
            return sol.menu.is_started(root)
        end)
    else
        if timer then
            timer:stop()
            timer = nil
        end
    end
end

timer = sol.timer.start(100, function()
    if sol.menu.is_started(root) then
        summon_laser()
        return true
    else
        root:on_finished()
    end
end)

return root