local saltk = require("scripts/saltk/main")

local is_choose_skill = false
local nb_visible_lines = 3     -- Maximum number of lines in the dialog box.

local dialog_box

local sfx_vfx_and_other_vn_actions = {}

function sfx_vfx_and_other_vn_actions:insert_first(item)
  table.insert(self, 1, item)
end

function sfx_vfx_and_other_vn_actions:pop()
  local item = self[1]
  table.remove(self, 1)
  return item
end

function sfx_vfx_and_other_vn_actions:execute(is_done)
  local item = self:pop()
  if item:match("wait") then
    function wait()
      if is_done ~= nil then
        is_done[1] = true
      end
    end
  else
    is_done[1] = true
  end
  if item ~= nil then
    load(item)()
  end
end

dialog_box = saltk.Image.new({id="root_dialog_box",
    size={320,240}, scale=true, position = {0,0},
    special_scale=2,
    sensitive=true, has_focus=true
})

local hud_path = "hud/battle_dialog/" 
local box = saltk.Image.new({id="box",
    size={320,48}, position = {0,0},
   special_scale=1, image=hud_path .. "P3.png"
})

local character_image = saltk.Image.new({id="character_image",
    size={64, 64}, position = {279,6},
    special_scale=1, crop={48,0,142,128}
})
box:add_child(character_image)

local character_image2 = saltk.Image.new({id="character_image2",
    size={64, 64}, position = {4,6},
    special_scale=1, crop={48,0,142,128}
})
box:add_child(character_image2)

local lines = {}

for i=1, nb_visible_lines do
    lines[i] = saltk.Text.new({id="line_" .. i,
        size={300,80}, position = {64,8 + (i*8)},
        special_scale=1, text="",
        font="Osaka Regular-Mono", font_size=8,
        rendering_mode="antialiasing"
    })
    box:add_child(lines[i])
end

dialog_box:add_child(box)
function dialog_box:on_started()
    self.char_delay = 1
    self.lines = {}

    self:show_dialog()
end

dialog_box.on_key_pressed = function()
  return false
end

function dialog_box:set_dialog(dialog_id)
  self.dialog = sol.language.get_dialog(dialog_id)
end

function dialog_box:quit()
    if sol.menu.is_started(dialog_box) then
        sol.menu.stop(dialog_box)
    end
end

local function repeat_show_character()
    dialog_box:check_full()
    while not dialog_box:is_full()
      and dialog_box.char_index > #dialog_box.lines[dialog_box.line_index] do
      -- The current line is finished.
        dialog_box.char_index = 1
        dialog_box.line_index = dialog_box.line_index + 1
        dialog_box:check_full()
    end

    if not dialog_box:is_full() then
        dialog_box:add_character()
    else
        sol.audio.play_sound("message_end")
    end
end

function dialog_box:show_dialog()

    -- Initialize this dialog.
    local dialog = self.dialog

    local text = dialog.text
    local text = text:gsub("\r\n", "\n"):gsub("\r", "\n")
    -- Using regex match all text surrounded by {{ and }}, push it to the sfx_vfx_and_other_vn_actions, replace it by |
    local doing_regex = true
    while doing_regex do
      doing_regex = false
      for text_begin, item, text_end in text:gmatch("(.*){{(.*)}}(.*)") do
        text = text_begin .. "|" .. text_end
        sfx_vfx_and_other_vn_actions:insert_first(item)
        doing_regex = true
      end
    end
    
    -- Split the text in lines.
    self.line_it = text:gmatch("([^\n]*)\n")  -- Each line including empty ones.

    self.next_line = self.line_it()

    self.line_index = 1
    self.char_index = 1
    self.skipped = false
    self.full = false


    -- Start displaying text.
    self:show_more_lines()
end

function dialog_box:start_dialog(dialog_id)
    local game = sol.main.get_game()
    if sol.menu.is_started(dialog_box) then
      sol.menu.stop(dialog_box)
    end
    self:set_dialog(dialog_id)
    sol.menu.start(game, dialog_box)
    sol.timer.start(dialog_box, 3000, function()
      if dialog_box:is_full() then
        dialog_box:show_more_lines()
      end
      return sol.menu.is_started(dialog_box)
    end)
end

  -- Returns whether there are more lines remaining to display after the current
  -- 3 lines.
function dialog_box:has_more_lines()
    return self.next_line ~= nil
end

function dialog_box:check_full()
    if self.line_index >= nb_visible_lines
      and self.char_index > #self.lines[nb_visible_lines] then
        self.full = true
    else
        self.full = false
    end
end

  -- Returns whether all 3 current lines of the dialog box are entirely
  -- displayed.
function dialog_box:is_full()
    return self.full
end


function dialog_box:change_of_environment()
  local game = sol.main.get_game()
    -- If the line is Mood: xxx then set the mood to xxx
    -- If the line is Char: xxx then set the character to xxx
    if self.next_line ~= nil and self.next_line:sub(1, 5) == "Char:" then
      local char = tostring(self.next_line:sub(7))
      if char == "Kokone" then
        character_image2:set_image("characters/kokone_sora/Happeh.png")
        character_image2:set_crop({180, 40, 180 + 600, 40 + 600})
        box:set_image(hud_path .. "P1.png")
      elseif char == "" or char == " " then
        box:set_image(hud_path .. "P3.png")
      else
        local path = Path:find_closest_file(
          "sprites/characters/" .. char .. "/Normal.png"
        ):gsub("^sprites/", "")
        character_image:set_image(path)
        character_image:set_crop({180, 40, 180 + 600, 40 + 600})
        box:set_image(hud_path .. "P2.png")
      end
      self.next_line = self.line_it()
    end
end

function dialog_box:show_more_lines()

    self.gradual = true

    if not self:has_more_lines() then
      self:quit()
      return
    end
    self:get_child_by_id("box"):render()
    -- Prepare the 3 lines.
    for i = 1, nb_visible_lines do
      self:change_of_environment()
      self:get_child_by_id("box"):get_child_by_id("line_" .. i):set_text("")
      if self:has_more_lines() then
        self.lines[i] = self.next_line
        self.next_line = self.line_it()
      else
        self.lines[i] = ""
      end
    end
    self.line_index = 1
    self.char_index = 1

    if self.gradual then
      sol.timer.start(self, self.char_delay, repeat_show_character)
    end
end

function dialog_box:add_character()
    local done = {true}
    local line = self.lines[self.line_index]
    local current_char = line:sub(self.char_index, self.char_index)
    if current_char == "" then
      error("No remaining character to add on this line")
    end
    local timer
    self.char_index = self.char_index + 1
    local additional_delay = 0
    local text_surface = self:get_child_by_id("box"):get_child_by_id("line_" .. self.line_index)

    local special = false
    if current_char == "|" then
      special = true
      done[1] = false
      sfx_vfx_and_other_vn_actions:execute(done)
      additional_delay = 999999

      sol.timer.start(sol.main.get_game(), 50, function ()
        if done[1] then
          current_char = line:sub(self.char_index, self.char_index)
          self.char_index = self.char_index + 1
          if timer ~= nil then
            timer:stop()
            repeat_show_character()
          end
        end
        return not done[1]
      end)
      
    end

    if not special then
      -- Normal character to be displayed.
      text_surface:set_text(text_surface:get_text() .. current_char)

      -- If this is a multibyte character, also add the next byte.
      local byte = current_char:byte()
      if byte >= 192 and byte < 224 then
        -- The first byte is 110xxxxx: the character is stored with
        -- two bytes (utf-8).
        current_char = line:sub(self.char_index, self.char_index)
        self.char_index = self.char_index + 1
        text_surface:set_text(text_surface:get_text() .. current_char)
      end
      text_surface:render()

      if current_char == " " then
        -- Remove the delay for whitespace characters.
        additional_delay = -self.char_delay
      end
    end

    if not special and current_char ~= nil and self.need_letter_sound then
      -- Play a letter sound sometimes.
      sol.audio.play_sound("message_letter")
      self.need_letter_sound = false
      sol.timer.start(self, 100, function()
        self.need_letter_sound = true
      end)
    end

    if self.gradual then
      timer = sol.timer.start(self, self.char_delay + additional_delay, repeat_show_character)
    end
end

  -- Commands to control the dialog box.
function dialog_box:display_more_lines()
    -- Display more lines.
    if self:is_full() then
      self:show_more_lines()
    end

    -- Don't propagate the event to anything below the dialog box.
    return true
end



return dialog_box