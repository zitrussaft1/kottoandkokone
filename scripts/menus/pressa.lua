local saltk = require("scripts/saltk/main")

local root = saltk.Image.new({id="root_pressa", image="menus/pressa/unpressed.png",
    position={0, 0}, size={320, 240}, scale=true,
    special_scale=3, sensitive=true})
root:render()
root:set_focus(true)

local counter = 5

local counter_text = saltk.Text.new({id="counter", text=tostring(counter),
    position={152, 140}, size={36, 36}, sensitive=false, font="enter_command",
    font_size=32, color={255, 255, 255}})

root:add_child(counter_text)

root:add_signal("key_pressed", function (Widget, key, modifiers)
    if key == "a" then
        counter = counter - 1
        counter_text:set_text(tostring(counter))
        root:set_image("menus/pressa/pressed.png")
        root:render()
        counter_text:render()
        sol.timer.start(root, 50, function ()
            root:set_image("menus/pressa/unpressed.png")
        end)
        if counter == 0 then
            local game = sol.main.get_game()
            if game then
                local hero = game:get_hero()
                hero:unfreeze()
                game:set_value("can_dash", true)
            end
            sol.menu.stop(root)
            counter = 5
            counter_text:set_text(tostring(counter))
            root:render()
            counter_text:render()
        end
    end
    return true
end)

return root