local saltk = require("scripts/saltk/main")
local game_manager = require("scripts/game_manager")

local root = saltk.Image.new({id="root_start_menu", size={320, 240},
    position={0, 0}, scale=true, sensitive=false,
    image="menus/background.png",special_scale =1,
})

local kokone_path = "characters/kokone_sora/"
local current_sprite = kokone_path .. "Happeh.png"
local kokone_sprite = saltk.Image.new({id="kokone_sora_main",
    size={218, 240}, position={105, 0}, sensitive=true,
    margin={0,0}, padding={0,0}, scale=true,
    image=current_sprite, special_scale =1,
    has_focus = true})


local start_menu_title = saltk.Sprite.new({id="start_menu_title",
        size={124,16},
        position={20, 120}, sensitive=true,
        margin={0,0}, padding={0,0},
        image="menus/press_start",
        special_scale =1,
})

local image_cursor_start = saltk.Image.new({id="image_cursor",
    image="menus/mouse.png", size={11, 16},
    position={210, 40}, scale=true, sensitive=false,
    visible=true, special_scale =1,
})

start_menu_title:add_signal("focused", function(Widget)
    image_cursor_start:set_position({80, 130})
end)

local counter = 0

local function set_cursor_kokone()
    local cursor_x, cursor_y
    if counter < 5 then
        cursor_x, cursor_y = 210, 40
    elseif counter >= 5 and counter < 12 then
        cursor_x, cursor_y = 200, 130
    elseif counter >= 12 and counter < 20 then
        cursor_x, cursor_y = 227, 194 
    elseif counter >= 20 and counter < 30 then
        cursor_x, cursor_y = 245, 170
    elseif counter >= 30 then
        cursor_x, cursor_y = 205, 235
    end
    image_cursor_start:set_position({cursor_x, cursor_y})
end

kokone_sprite:add_signal("focused", set_cursor_kokone)

local function on_select_kokone(Widget)
    counter = counter + 1
    set_cursor_kokone()
    local current_cursor_pos = image_cursor_start:get_position()
    image_cursor_start:set_size({8, 16})
    image_cursor_start:set_position({current_cursor_pos[1] + 1, current_cursor_pos[2] + 2})
    image_cursor_start:render()

    sol.timer.start(100, function()
        image_cursor_start:set_size({11, 16})
        image_cursor_start:set_position({current_cursor_pos[1], current_cursor_pos[2]})
        image_cursor_start:render()
    end)

    sol.audio.play_sound("jump")

    if counter < 5 then
        current_sprite = kokone_path .. "Surprised.png"
        sol.timer.start(1000, function()
            current_sprite = kokone_path .. "Normal.png"
        end)
    elseif counter >= 5 and counter < 12 then
        current_sprite = kokone_path .. "Angy.png"
    elseif counter >= 12 and counter < 20 then
        current_sprite = kokone_path .. "Sadge.png"
    elseif counter >= 20 and counter < 30 then
        current_sprite = kokone_path .. "Surprised.png"
    elseif counter >= 30 then
        current_sprite = kokone_path .. "Horn.png"
    end
    Widget:set_image(current_sprite)
end

kokone_sprite:add_signal("selected",on_select_kokone)
kokone_sprite:add_signal("clicked",on_select_kokone)

sol.timer.start(2000, function()
    if counter < 7 then
        local random = math.random(0, 2)
        if random == 0 then
            current_sprite = kokone_path .. "Normal.png"
        else
            current_sprite = kokone_path .. "Happeh.png"
        end
        kokone_sprite:set_image(current_sprite)
    end
    return sol.menu.is_started(root)
end)

local function start_game()
    sol.audio.stop_music()
    local game = game_manager:create("save2.dat")
    sol.menu.stop(root)
    game:start()
end

start_menu_title:add_signal("selected", start_game)
start_menu_title:add_signal("clicked", start_game)

root:add_child(start_menu_title)
root:add_child(kokone_sprite)
root:add_child(image_cursor_start)

sol.audio.play_music("eduardo/village")
sol.audio.set_music_volume(33)


return root