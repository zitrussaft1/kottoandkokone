local saltk = require("scripts/saltk/main")

local root = saltk.Image.new({id="root_skillz",
    position={0, 0}, size={320, 240}, scale=true,
    special_scale=3, sensitive=true})

root:add_signal("key_pressed", function (Widget, key, modifiers)
    print("key pressed", key)
    if key == "q" then
        print("q pressed")
        return true
    elseif key == "w" then
        print("w pressed")
        return true
    elseif key == "e" then
        print("e pressed")
        return true
    elseif key == "r" then
        print("r pressed")
        return true
    end
end)

return root