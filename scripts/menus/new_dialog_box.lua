local saltk = require("scripts/saltk/main")

local is_choose_skill = false
local nb_visible_lines = 3     -- Maximum number of lines in the dialog box.

local dialog_box

local sfx_vfx_and_other_vn_actions = {}

function sfx_vfx_and_other_vn_actions:insert_first(item)
  table.insert(self, 1, item)
end

function sfx_vfx_and_other_vn_actions:pop()
  local item = self[1]
  table.remove(self, 1)
  return item
end

function sfx_vfx_and_other_vn_actions:execute(is_done)
  local item = self:pop()
  if item:match("wait") then
    function wait()
      if is_done ~= nil then
        is_done[1] = true
      end
    end
  else
    is_done[1] = true
  end
  if item ~= nil then
    load(item)()
  end
end

dialog_box = saltk.Image.new({id="root_new_dialog_box",
    size={320,240}, scale=true, position = {0,0},
    special_scale=2, 
    sensitive=true, has_focus=true
})

local overlay = saltk.WidgetColor.new({id="overlay",
    size={320,240}, scale=true, position = {0,0},
    background={0,0,0}, opacity=0,
    sensitive=false, has_focus=false
})

dialog_box:add_child(overlay)

local character_image = saltk.Image.new({id="character_image",
    size={190, 240}, position = {160,10},
    special_scale=1, image="characters/kokone_sora/aa.png"
})
dialog_box:add_child(character_image)


local box = saltk.Image.new({id="box",
    size={320,105}, position = {10,150}, crop={0,0,220,60},
   special_scale=1, image="hud/dialog_box.png"
})

function box:update()
    self:render()
    self:set_image("hud/dialog_box.png")
    for _, child in pairs(self:get_children()) do
        child:render()
    end
end

local dialog_box_end_sprite = saltk.Sprite.new({id="dialog_box_end_sprite",
    size={16,16}, position = {275,60},
    special_scale=1, image="hud/dialog_box_message_end"
})

box:add_child(dialog_box_end_sprite)

local character_name = saltk.Text.new({id="character_name",
    size={300,80}, position = {180,10},
    special_scale=1, text="Kokone Sora",
    color = {255,255,255},
    font="PressStart2P-Regular", font_size=10
})

box:add_child(character_name)

local lines = {}

for i=1, nb_visible_lines do
    lines[i] = saltk.Text.new({id="line_" .. i,
        size={300,80}, position = {14,14 + (i*14)},
        special_scale=1, text="",
        font="Osaka Regular-Mono", font_size=12,
        color={255,255,255},
        rendering_mode="antialiasing"
    })
    box:add_child(lines[i])
end

dialog_box:add_child(box)
function dialog_box:on_started()
    self.char_delay = 1
    self.lines = {}

    self:show_dialog()
end

function dialog_box:quit()
    if sol.menu.is_started(dialog_box) then
        sol.menu.stop(dialog_box)
    end
end

local function repeat_show_character()
    dialog_box:check_full()
    while not dialog_box:is_full()
      and dialog_box.char_index > #dialog_box.lines[dialog_box.line_index] do
      -- The current line is finished.
        dialog_box.char_index = 1
        dialog_box.line_index = dialog_box.line_index + 1
        dialog_box:check_full()
    end

    if not dialog_box:is_full() then
        dialog_box:add_character()
    else
        sol.audio.play_sound("message_end")
    end
end

function dialog_box:show_dialog()

    -- Initialize this dialog.
    local dialog = self.dialog

    local text = dialog.text
    local text = text:gsub("\r\n", "\n"):gsub("\r", "\n")
    -- Using regex match all text surrounded by {{ and }}, push it to the sfx_vfx_and_other_vn_actions, replace it by |
    local doing_regex = true
    while doing_regex do
      doing_regex = false
      for text_begin, item, text_end in text:gmatch("(.*){{(.*)}}(.*)") do
        text = text_begin .. "|" .. text_end
        sfx_vfx_and_other_vn_actions:insert_first(item)
        doing_regex = true
      end
    end
    
    -- Split the text in lines.
    self.line_it = text:gmatch("([^\n]*)\n")  -- Each line including empty ones.

    self.next_line = self.line_it()

    self.line_index = 1
    self.char_index = 1
    self.skipped = false
    self.full = false


    -- Start displaying text.
    self:show_more_lines()
end

  -- Returns whether there are more lines remaining to display after the current
  -- 3 lines.
function dialog_box:has_more_lines()
    return self.next_line ~= nil
end

function dialog_box:check_full()
    if self.line_index >= nb_visible_lines
      and self.char_index > #self.lines[nb_visible_lines] then
        self.full = true
    else
        self.full = false
    end
end

  -- Returns whether all 3 current lines of the dialog box are entirely
  -- displayed.
function dialog_box:is_full()
    return self.full
end


function dialog_box:change_of_environment()
  local game = sol.main.get_game()
    -- If the line is Mood: xxx then set the mood to xxx
    if self.next_line ~= nil and self.next_line:sub(1, 5) == "Mood:" then
      game:set_value("mood", self.next_line:sub(7))
      self.next_line = self.line_it()
    end

        -- If the line is Clothes: xxx then set the clothes to xxx
    if self.next_line ~= nil and self.next_line:sub(1, 7) == "Clothes:" then
      game:set_clothes(self.next_line:sub(9))
      self.next_line = self.line_it()
    end

    -- If the line is Char: xxx then set the character to xxx
    if self.next_line ~= nil and self.next_line:sub(1, 5) == "Char:" then
      character_name:set_text(tostring(self.next_line:sub(7)))
      print(self.next_line:sub(7))
      game:set_character_dialog(self.next_line:sub(7))
      box:update()
      self.next_line = self.line_it()
    end

        -- If the line is Hide: xxx then hide the character
    if self.next_line ~= nil and self.next_line:sub(1, 5) == "Hide:" then
      game:set_value("hide", self.next_line:sub(7))
      self.next_line = self.line_it()
    end

        -- If the line is BG: xxx then set the background to xxx
    if self.next_line ~= nil and self.next_line:sub(1, 3) == "BG:" then
      print(self.next_line:sub(5))
      local BG = self.next_line:sub(5)
      if BG == "" or BG == " " then
        BG = nil
      end
      dialog_box:set_image(BG)
      print(BG)
      self.next_line = self.line_it()
    end

    if self.next_line and self.next_line:sub(1,16) == "SHOW_ALL_SKILLS:" then
      local skills_names = String:split(
        self.next_line:sub(18),
        ","
      )
    end
      -- The number of skills might change, so use a loop
    if game:get_value("hide") == "false" then
      local path = Path:find_closest_file(
        "sprites/characters/" .. game:get_value("character_dialog") .. "/" .. game:get_value("mood") .. ".png"
      ):gsub("^sprites/", "")
      character_image:set_image(path)
    else
      character_image:set_image()
    end
end

function dialog_box:show_more_lines()

    self.gradual = true

    if not self:has_more_lines() then
      sol.main.get_game():stop_dialog()
      return
    end
    self:get_child_by_id("box"):render()
    -- Prepare the 3 lines.
    for i = 1, nb_visible_lines do
      self:change_of_environment()
      self:get_child_by_id("box"):get_child_by_id("line_" .. i):set_text("")
      if self:has_more_lines() then
        self.lines[i] = self.next_line
        self.next_line = self.line_it()
      else
        self.lines[i] = ""
      end
    end
    box:update()
    self.line_index = 1
    self.char_index = 1

    if self.gradual then
      sol.timer.start(self, self.char_delay, repeat_show_character)
    end
end

function dialog_box:add_character()
    local done = {true}
    local line = self.lines[self.line_index]
    local current_char = line:sub(self.char_index, self.char_index)
    if current_char == "" then
      error("No remaining character to add on this line")
    end
    local timer
    self.char_index = self.char_index + 1
    local additional_delay = 0
    local text_surface = self:get_child_by_id("box"):get_child_by_id("line_" .. self.line_index)

    local special = false
    if current_char == "|" then
      special = true
      done[1] = false
      sfx_vfx_and_other_vn_actions:execute(done)
      additional_delay = 999999

      sol.timer.start(sol.main.get_game(), 50, function ()
        if done[1] then
          current_char = line:sub(self.char_index, self.char_index)
          self.char_index = self.char_index + 1
          if timer ~= nil then
            timer:stop()
            repeat_show_character()
          end
        end
        return not done[1]
      end)
      
    end

    if not special then
      -- Normal character to be displayed.
      text_surface:set_text(text_surface:get_text() .. current_char)

      -- If this is a multibyte character, also add the next byte.
      local byte = current_char:byte()
      if byte >= 192 and byte < 224 then
        -- The first byte is 110xxxxx: the character is stored with
        -- two bytes (utf-8).
        current_char = line:sub(self.char_index, self.char_index)
        self.char_index = self.char_index + 1
        text_surface:set_text(text_surface:get_text() .. current_char)
      end
      text_surface:render()

      if current_char == " " then
        -- Remove the delay for whitespace characters.
        additional_delay = -self.char_delay
      end
    end

    if not special and current_char ~= nil and self.need_letter_sound then
      -- Play a letter sound sometimes.
      sol.audio.play_sound("message_letter")
      self.need_letter_sound = false
      sol.timer.start(self, 100, function()
        self.need_letter_sound = true
      end)
    end

    if self.gradual then
      timer = sol.timer.start(self, self.char_delay + additional_delay, repeat_show_character)
    end
end

  -- Commands to control the dialog box.
function dialog_box:display_more_lines()
    -- Display more lines.
    if self:is_full() then
      self:show_more_lines()
    end

    -- Don't propagate the event to anything below the dialog box.
    return true
end
dialog_box:add_signal("key_pressed", function(dialog_box, key)
    if key == "space" then
        dialog_box:display_more_lines()
        return true
    end
end)

dialog_box:add_signal("clicked", function(dialog_box, button, x, y)
    dialog_box:display_more_lines()
  return true
end)

local function initialize_dialog_box_features(game)
    sol.timer.start(game, 10, function()
      overlay:set_opacity(DIALOG_BACKGROUND_OPACITY)
      overlay:set_background_color(DIALOG_BACKGROUND_COLOR)
      return true
    end)
    if game.get_dialog_box ~= nil then
        -- Already done.
        return
    end
    game:register_event("on_dialog_started", function(game, dialog, is_cs)
        dialog_box.dialog = dialog
        is_choose_skill = is_cs
        sol.menu.start(game, dialog_box)
    end)

    -- Called by the engine when a dialog finishes.
    game:register_event("on_dialog_finished", function(game, dialog)
        dialog_box:quit()
        dialog_box.dialog = nil
        if is_choose_skill == true then
            game:get_map():set_reducer(0)
            is_choose_skill = false
        end
    end)
end

local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", initialize_dialog_box_features)

return dialog_box