-- Create a 3x3 grid of cards, a return button and two arrows to scroll up and down the list
local saltk = require("scripts/saltk/main")
local card_manager = require("scripts/cards")

local root = saltk.Image.new({id="root_card_chooser_3", image="menus/card_chooser_background.png",
    position={0, 0}, size={320, 240}, scale=true,
    sensitive=false})

local grid = saltk.Grid.new({id="grid", size={280, 200}, position={0, 20},
    grid={3,2}, margin={0, 0}, padding={10, 10},
    sensitive=false})

local return_button = saltk.WidgetColor.new({id="return_button", size={50, 50},
    position={270, 190}, scale=true,
    background={255, 255, 255}, sensitive=true})
local return_button_text = saltk.Text.new({id="return_button_text", text="<",
    position={275, 195}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

local up_button = saltk.WidgetColor.new({id="up_button", size={50, 50},
    position={270, 20}, scale=true,
    background={255, 255, 255}, sensitive=true})
local up_button_text = saltk.Text.new({id="up_button_text", text="^",
    position={275, 25}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

local down_button = saltk.WidgetColor.new({id="down_button", size={50, 50},
    position={270, 120}, scale=true,
    background={255, 255, 255}, sensitive=true})
local down_button_text = saltk.Text.new({id="down_button_text", text="v",
    position={275, 125}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

root:add_child(grid)
for i = 1, 6 do
    local w_card = saltk.GridElement.new({id="card_" .. i, size={80, 130},
        position={0,0}, scale=true,
        background={255, 255, 255}, sensitive=true})
    
    local card_title = saltk.Text.new({id="card_title_" .. i, text="Card " .. i,
        position={4, 4}, size={75, 100}, sensitive=false,
        font="enter_command", font_size=16, color={0, 0, 0}})
    
    local card_description = saltk.Text.new({id="card_description_" .. i, text="Card " .. i,
        position={4, 24}, size={75, 200}, sensitive=false,
        font="enter_command", font_size=8, color={0, 0, 0}})

    local card_is_active = saltk.WidgetColor.new({id="card_is_active_" .. i, size={10, 10},
        position={0,0}, scale=true,
        background={255, 255, 0}, visible = true, sensitive=false})

    -- If the card is clicked, toggle its active state if the user has less than 7, 3, 1 cards (level dependent)
    local function update_card(Widget)
        if Widget.card then
            print(
                card_manager:get_number_of_active_cards(Widget.card.lvl),
                card_manager:min_active_cards(Widget.card.lvl)
            )
            if card_manager:is_an_active_card(Widget.card) and card_manager:get_number_of_active_cards(Widget.card.lvl) > card_manager:min_active_cards(Widget.card.lvl) then
                if card_manager:remove_active_card(Widget.card) then
                    card_is_active:hide()
                end
            elseif card_manager:get_number_of_active_cards(Widget.card.lvl) < card_manager:max_active_cards(Widget.card.lvl) then
                if card_manager:set_active_card(Widget.card) then
                    card_is_active:show()
                end
            end
        end
    end
    w_card:add_signal("selected", update_card)
    w_card:add_signal("clicked", update_card)


    grid:add_child(w_card)
    w_card:add_child(card_title)
    w_card:add_child(card_description)
    w_card:add_child(card_is_active)
end

root:add_child(return_button)
root:add_child(return_button_text)
root:add_child(up_button)
root:add_child(up_button_text)
root:add_child(down_button)
root:add_child(down_button_text)

local cards = {}
local current_page = 1
local max_page = 1
local cards_per_page = 6

local function update_cards()
    for i = 1, cards_per_page do
        local card = cards[(current_page - 1) * cards_per_page + i]
        if card then
            grid:set_card(i, card)
        else
            grid:set_card(i, nil)
        end
    end
end

local function update_page()
    if current_page == 1 then
        up_button:set_sensitive(false)
    else
        up_button:set_sensitive(true)
    end
    if current_page == max_page then
        down_button:set_sensitive(false)
    else
        down_button:set_sensitive(true)
    end
    up_button:render()
    down_button:render()
end

local function update()
    update_cards()
    update_page()
end

function grid:set_card(i, card)
    local w_card = self:get_child_by_id("card_" .. i)
    local card_title = w_card:get_child_by_id("card_title_" .. i)
    local card_description = w_card:get_child_by_id("card_description_" .. i)
    local card_is_active = w_card:get_child_by_id("card_is_active_" .. i)

    if card then
        w_card.card = card

        -- Color the card according to its type
        if card.type == "aggressive" then
            w_card:set_background_color({200, 0, 0})
        elseif card.type == "playful" then
            w_card:set_background_color({0, 200, 0})
        elseif card.type == "kindness" then
            w_card:set_background_color({0, 0, 200})
        end

        w_card:show()
        card_title:set_text(card.title)
        card_description:set_text(card.description)
        if card_manager:is_an_active_card(card) then
            card_is_active:show()
        else
            card_is_active:hide()
        end
    else
        w_card.card = nil
        w_card:hide()
        card_title:set_text("")
        card_description:set_text("")
        card_is_active:hide()
    end
    w_card:render()
    card_title:render()
    card_description:render()
end

function root:init(lvl)
    current_page = 1
    cards = card_manager:get_player_cards(lvl)
    max_page = math.ceil(#cards / cards_per_page)
    update()
    -- Set the focus to the first card
    grid:get_children()[1]:set_focus(true)
end

local function go_back()
    sol.menu.stop(root)
    sol.menu.start(
        sol.main.get_game(),
        require("scripts/menus/deck_builder_home")
    )
end

local function back_page()
    current_page = current_page - 1
    update()
end

local function next_page()
    current_page = current_page + 1
    update()
end

-- Add signals to the buttons
return_button:add_signal("selected", go_back)
return_button:add_signal("clicked", go_back)

up_button:add_signal("selected", back_page)
up_button:add_signal("clicked", back_page)

down_button:add_signal("selected", next_page)
down_button:add_signal("clicked", next_page)

return root