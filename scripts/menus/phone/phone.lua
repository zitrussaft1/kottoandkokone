local saltk = require("scripts/saltk/main")

local root = saltk.Image.new({id="root_phone", image="phone/phone.png",
    position={80, 30}, size={160, 240}, scale=true,
    special_scale=3, sensitive=false})

local grid = saltk.Grid.new({id="grid", position={16, 32}, size={160, 160},
    grid={4, 4}, margin={0, 0}, padding={0, 0},
    sensitive=false})

root:add_child(grid)

local function start_app(app)
    sol.menu.stop(root)
    sol.menu.start(
        sol.main.get_game(),
        require("scripts/menus/phone/" .. app)
    )
end

local apps = {
    achievements = {
        icon="phone/achievements.png",
        name="Trophies",
        action=function ()
            start_app("achievements")
        end
    },
    deck = {
        icon="phone/contacts.png",
        name="Deck Builder",
        action=function ()
            sol.menu.stop(root)
            sol.menu.start(
                sol.main.get_game(),
                require("scripts/menus/deck_builder_home")
            )
        end
    }
}

for app, info in pairs(apps) do
    local app = saltk.GridElement.new({id=app, position={0, 0}, size={36, 36},
        sensitive=true})
    local icon = saltk.Image.new({id="icon", image=info.icon,
        position={0, 0}, size={36, 36}, sensitive=false, special_scale=1,})
    local icon_selected = saltk.WidgetColor.new({
        background={255,255,255}, size={36,36}, sensitive=false,
        opacity=0
    })
    local notification_icon = saltk.WidgetColor.new({
        background={255,0,0}, size={8,8}, sensitive=false,
        opacity=0, visible=false, position={-4, -4}
    })

    local text_name = saltk.Text.new({id="text_name", text=info.name,
        position={8, 38}, size={36, 16}, sensitive=false, font="enter_command",
        font_size=8})

    app:add_signal("selected", function ()
        info.action()
    end)
    app:add_signal("clicked", function ()
        info.action()
    end)
    local timer
    app:add_signal("focused", function ()
        if timer ~= nil then
            timer:stop()
        end
        icon_selected:set_opacity(0)
        local reducing = 1
        timer = sol.timer.start(50, function ()
            local opacity = icon_selected.opacity
            
            if opacity > 245 then
                opacity = 245
            elseif opacity < 10 then
                opacity = 10
            end

            if opacity < 245 and reducing == 1 then
                icon_selected:set_opacity(opacity + 10*reducing)
                return true
            elseif opacity > 10 and reducing == -1 then
                icon_selected:set_opacity(opacity + 10*reducing)
                return true
            else
                reducing = reducing * -1
                return true
            end
        end)
        if app.id == "achievements" then
        if require("scripts/conquista"):has_new() then
            notification_icon:set_opacity(255)
        else 
            notification_icon:set_opacity(0)
        end
    end
    end)

    app:add_signal("unfocused", function ()
        if timer ~= nil then
            timer:stop()
        end
        icon_selected:set_opacity(0)
        if app.id == "achievements" then
        if require("scripts/conquista"):has_new() then
            notification_icon:set_opacity(255)
        else 
            notification_icon:set_opacity(0)
        end
    end
    end)
    grid:add_child(app)
    app:add_child(icon)
    app:add_child(icon_selected)
    app:add_child(text_name)
    icon:add_child(notification_icon)
end

grid:get_child_by_id("achievements"):set_focus(true)

return root