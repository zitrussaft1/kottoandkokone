local saltk = require("scripts/saltk/main")
local conquistas = require("scripts/conquista")

local root = saltk.Image.new({id="root_gallery", image="phone/phone.png",
    position={80, 30}, size={160, 240}, scale=true,
    special_scale=3, sensitive=false})


local grid = saltk.Grid.new({id="grid", grid={1,8},
    position={16, 32}, size={160, 200},
    padding={2, 2},
    sensitive=false})
root:add_child(grid)

for id, conquista in ipairs(conquistas.a) do
    local save_slots = saltk.GridElement.new({id="save_slots"..id, position={0, 0}, size={124, 16},
        sensitive=true, background={60,60,0}})
    local save_text = saltk.Text.new({id="save_text", text=conquista.description,
        position={4, 0}, size={60, 16}, sensitive=false, font="enter_command",
        font_size=8})
    local notification_icon = saltk.WidgetColor.new({
        id="notification_icon"..id,
        background={255,0,0}, size={8,8}, sensitive=false,
        opacity=0, visible=false, position={-4, -4}
    })
    if conquista.state == 1 then
        notification_icon:set_opacity(255)
    end

    if conquista.state > 0 then
        save_slots:set_background_color({170,170,0})
    end

    save_slots:add_signal("selected", function ()
        if conquistas:get_state(conquista.id) == 1 then
            conquistas:set_state(conquista.id, 2)
            notification_icon:set_opacity(0)
        end
    end)

    save_slots:add_signal("clicked", function ()
        if conquistas:get_state(conquista.id) == 1 then
            conquistas:set_state(conquista.id, 2)
            notification_icon:set_opacity(0)
        end
    end)
    
    
    grid:add_child(save_slots)
    save_slots:add_child(save_text)
    save_slots:add_child(notification_icon)
end

function root:on_started()
    for id, conquista in ipairs(conquistas.a) do
        if conquista.state == 1 then
            grid:get_child_by_id_r("notification_icon"..id):set_opacity(255)
        end
        if conquista.state > 0 then
            grid:get_child_by_id_r("save_slots"..id):set_background_color({170,170,0})
        end
    end
end

grid:get_children()[1]:set_focus(true)

root:render()

return root