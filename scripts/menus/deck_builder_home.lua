local saltk = require("scripts/saltk/main")

local root = saltk.Image.new({id="root_card_chooser", size={320, 240},
    position={0, 0}, scale=true, sensitive=false,
    image="menus/background.png",special_scale =1,
})

-- Create a 1x3 grid to display the cards
local grid = saltk.Grid.new({id="grid", size={280, 180}, position={0, 20},
    grid={3,1}, margin={0, 0}, padding={8, 0},
    sensitive=false})

-- Add to the grid 3 cards (bronze, silver and gold colored), on the bottom add (cards player has/total available cards)
local card_manager = require("scripts/cards")
local bronze_cards_total = card_manager:get_number_of_cards(1)
local silver_cards_total = card_manager:get_number_of_cards(2)
local gold_cards_total = card_manager:get_number_of_cards(3) or 0

local bronze_cards_owned = card_manager:get_number_of_card_player(1)
local silver_cards_owned = card_manager:get_number_of_card_player(2)
local gold_cards_owned = card_manager:get_number_of_card_player(3) or 0

local bronze_card = saltk.GridElement.new({id="bronze_card",
    position={0, 0}, size={100, 180},
    background={220, 144, 89}, sensitive=true})

local silver_card = saltk.GridElement.new({id="silver_card",
    position={0, 0}, size={100, 180},
    background={192, 192, 192}, sensitive=true})

local gold_card = saltk.GridElement.new({id="gold_card",
    position={0, 0}, size={100, 180},
    background={255, 215, 0}, sensitive=true})

local bronze_card_label = saltk.Text.new({id="bronze_card_label", text="Bronze Cards",
    position={10, 10}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})

local silver_card_label = saltk.Text.new({id="silver_card_label", text="Silver Cards",
    position={10, 10}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})

local gold_card_label = saltk.Text.new({id="gold_card_label", text="Gold Cards",
    position={10, 10}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})

local bronze_card_text = saltk.Text.new({id="bronze_card_text", text="" .. bronze_cards_owned .. "/" .. bronze_cards_total,
    position={40, 210}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})

local silver_card_text = saltk.Text.new({id="silver_card_text", text="" .. silver_cards_owned .. "/" .. silver_cards_total,
    position={150, 210}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})

local gold_card_text = saltk.Text.new({id="gold_card_text", text="" .. gold_cards_owned .. "/" .. gold_cards_total,
    position={250, 210}, size={100, 50}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})


root:add_child(grid)
grid:add_child(bronze_card)
grid:add_child(silver_card)
grid:add_child(gold_card)

bronze_card:add_child(bronze_card_label)
silver_card:add_child(silver_card_label)
gold_card:add_child(gold_card_label)

root:add_child(bronze_card_text)
root:add_child(silver_card_text)
root:add_child(gold_card_text)


-- Create a close button that opens the phone menu when pressed
local phone_button = saltk.WidgetColor.new({id="phone_button", size={50, 20},
    position={270, 0}, scale=true,
    background={255, 255, 255}, sensitive=true})

local phone_button_text = saltk.Text.new({id="phone_button_text", text="<",
    position={5, 5}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

root:add_child(phone_button)
phone_button:add_child(phone_button_text)

local function open_phone()
    sol.menu.stop(root)
    sol.menu.start(
        sol.main.get_game(),
        require("scripts/menus/phone/phone")
    )
end

local function open_cards(Widget)
    sol.menu.stop(root)
    local deck_builder = require("scripts/menus/deck_builder")
    local lvl = 1
    if Widget.id == "silver_card" then
        lvl = 2
    elseif Widget.id == "gold_card" then
        lvl = 3
    end
    deck_builder:init(lvl)
    sol.menu.start(
        sol.main.get_game(),
        deck_builder
    )
end

phone_button:add_signal("selected", open_phone)
phone_button:add_signal("clicked", open_phone)

bronze_card:add_signal("selected", open_cards)
bronze_card:add_signal("clicked", open_cards)
silver_card:add_signal("selected", open_cards)
silver_card:add_signal("clicked", open_cards)

bronze_card:set_focus(true)

return root