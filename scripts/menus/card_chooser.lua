local saltk = require("scripts/saltk/main")
local card_manager = require("scripts/cards")

local is_corrupting = false
local is_switching = false

local c_points = 0
local switch_points = 0

local root = saltk.WidgetColor.new({id="root_card_chooser",
    position={0, 0}, size={320, 240}, scale=true,
    sensitive=false})

local bg = saltk.Image.new({id="root_card_chooser_2", image="menus/card_chooser_background.png",
    position={0, 0}, size={320, 240}, scale=true, special_scale=3,
    sensitive=false})

root:add_child(bg)

for i = 1, 3 do
    local w_card = saltk.WidgetColor.new({id="card_" .. i, size={90, 200},
        position={10 + (i - 1) * 100, 10}, scale=true,
        background={255, 255, 255}, sensitive=true})
    
    local card_title = saltk.Text.new({id="card_title_" .. i, text="Card " .. i,
        position={16 + (i - 1) * 100, 40}, size={80, 100}, sensitive=false,
        font="enter_command", font_size=16, color={0, 0, 0}})
    
    local card_description = saltk.Text.new({id="card_description_" .. i, text="Card " .. i,
        position={16 + (i - 1) * 100, 74}, size={80, 200}, sensitive=false,
        font="enter_command", font_size=8, color={0, 0, 0}})


    local function update_card()
        local new_card
        if is_corrupting and c_points > 0 then
            c_points = c_points - 1
            w_card.card.c = true
            new_card = w_card.card
        elseif is_switching and switch_points > 0 then
            switch_points = switch_points - 1
            new_card = card_manager:draw_card(nil, nil)
            new_card["uid"] = i
        end
        if new_card then
            root:set_card(i, new_card)
        end
    end

    w_card:add_signal("selected", update_card)
    w_card:add_signal("clicked", update_card)


    root:add_child(w_card)
    root:add_child(card_title)
    root:add_child(card_description)
end

local can_corrupt = true
-- Create a corrupt button using a widget color on the bottom right corner
local corrupt_button = saltk.WidgetColor.new({id="corrupt_button", size={50, 50},
    position={270, 190}, scale=true,
    background={255, 255, 255}, sensitive=true})
-- Create a text on the corrupt button
local corrupt_button_text = saltk.Text.new({id="corrupt_button_text", text="Corrupt " .. c_points,
    position={275, 195}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

root:add_child(corrupt_button)
root:add_child(corrupt_button_text)

-- Create another button on the bottom left corner for switching
local switch_button = saltk.WidgetColor.new({id="switch_button", size={50, 50},
    position={0, 190}, scale=true,
    background={255, 255, 255}, sensitive=true})

local switch_button_text = saltk.Text.new({id="switch_button_text", text="Switch " .. switch_points,
    position={5, 195}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

local function update_corrupt_button()
    if not is_corrupting then
        corrupt_button:set_background_color({255, 255, 255})
        corrupt_button_text:set_text("Corrupt" .. c_points)
    else
        corrupt_button:set_background_color({255, 0, 0})
        corrupt_button_text:set_text("Done")
    end
    -- Stop switch button
    is_switching = false
    switch_button:set_background_color({255, 255, 255})
    switch_button_text:set_text("Switch " .. switch_points)
    
end

-- When the corrupt button is pressed, turn it into a cancel button
corrupt_button:add_signal("selected", function()
    is_corrupting = not is_corrupting
    update_corrupt_button()
end)

corrupt_button:add_signal("clicked", function()
    is_corrupting = not is_corrupting
    update_corrupt_button()
end)


root:add_child(switch_button)
root:add_child(switch_button_text)

local function update_switch_button()
    if not is_switching then
        switch_button:set_background_color({255, 255, 255})
        switch_button_text:set_text("Switch" .. switch_points)
    else
        switch_button:set_background_color({255, 0, 0})
        switch_button_text:set_text("Done")
    end

    -- Stop corrupt button
    is_corrupting = false
    corrupt_button:set_background_color({255, 255, 255})
    corrupt_button_text:set_text("Corrupt " .. c_points)

end

switch_button:add_signal("selected", function(k)
    is_switching = not is_switching
    update_switch_button()
end)

switch_button:add_signal("clicked", function(k)
    is_switching = not is_switching
    update_switch_button()
end)

-- Create a Ready! button
local ready_button = saltk.WidgetColor.new({id="ready_button", size={50, 50},
    position={130, 190}, scale=true,
    background={255, 255, 255}, sensitive=true})

local ready_button_text = saltk.Text.new({id="ready_button_text", text="Ready!",
    position={5, 5}, size={50, 50}, sensitive=false,
    font="enter_command", font_size=8, color={0, 0, 0}})

root:add_child(ready_button)
ready_button:add_child(ready_button_text)

ready_button:add_signal("selected", function()
    sol.main.get_game():set_paused(false)
    sol.menu.stop(root)
end)

ready_button:add_signal("clicked", function()
    sol.main.get_game():set_paused(false)
    sol.menu.stop(root)
end)

function root:set_card(i, card)
    local card_title = self:get_child_by_id_r("card_title_" .. i)
    local card_description = self:get_child_by_id_r("card_description_" .. i)
    local w_card = self:get_child_by_id_r("card_" .. i)
    w_card.card = card
    card_title:set_text(card.title)
    card_description:set_text(card.description)
    if card.c == true then
        w_card:set_background_color({255, 0, 255})
    else
        w_card:set_background_color(
            card_manager:get_color(card)
        )
    end
end

function root:set_cards(cards)
    for i, card in ipairs(cards) do
        self:set_card(i, card)
    end
end

function root:get_cards()
    local cards = {}
    for i = 1, 3 do
        local w_card = self:get_child_by_id_r("card_" .. i)
        table.insert(cards, w_card.card)
    end
    return cards
end

function root:set_c_points(points)
    c_points = points
    update_corrupt_button()
end

function root:set_switch_points(points)
    switch_points = points
    update_switch_button()
end

function root:on_finished()
    sol.main.get_game():set_paused(false)
end

root:get_children()[1]:set_focus(true)

return root