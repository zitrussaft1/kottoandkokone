local saltk = require("scripts/saltk/main")
local card_manager = require("scripts/cards")
local game = sol.main.get_game()
local t1 = {}

local root = saltk.WidgetColor.new({id="root_cooldowns",
    position={0, 0}, size={320, 240}, scale=true,
    special_scale=3, sensitive=false})

-- Draw three cards at the bottom of the screen
for i = 1, 3 do
    local w_card = saltk.WidgetColor.new({id="card_" .. i, size={90, 30},
        position={10 + (i - 1) * 100, 210}, scale=true,
        background={255, 255, 255}})
    
    local card_title = saltk.Text.new({id="card_title_" .. i, text="Card " .. i,
        position={16 + (i - 1) * 100, 213}, size={100, 100}, sensitive=false,
        font="enter_command", font_size=16, color={0, 0, 0}})

    local cooldown = saltk.Text.new({id="cooldown_" .. i, text="0",
        position={51 + (i - 1) * 100, 220}, size={80, 200}, sensitive=false,
        font="enter_command", font_size=20, color={0, 0, 0}})

    root:add_child(w_card)
    root:add_child(cooldown)
    root:add_child(card_title)
end

-- Add a health bar to the top left corner
local max_health = game:get_max_life()
local starting_position_ui = 100
local health_bar = saltk.Image.new({id="health_bar", image="hud/hp/HP BAR 1.png",
    position={0, starting_position_ui}, size={12, 100}, scale=true,
    special_scale=3, sensitive=false})

local health_text = saltk.Text.new({id="health_text", text="Health: ",
    position={10, starting_position_ui + 10}, size={100, 100}, sensitive=false,
    font="enter_command", font_size=16, color={0, 0, 0}})


local boss_health_bar = saltk.Image.new({id="boss_health_bar", image="hud/boss_hp/HP Boss 1.png",
    position={308, starting_position_ui}, size={12, 100}, scale=true,
    special_scale=3, sensitive=false})


function root:set_cards(cards)
    t1 = {}
    for i = 1, 3 do
        -- Update the color based on type
        local card = cards[i]
        local w_card = self:get_child_by_id("card_" .. i)
        w_card.card = card
        if card.c == true then
            w_card:set_background_color({255, 0, 255})
        else
            w_card:set_background_color(
                card_manager:get_color(card)
            )
        end
        -- Update the title
        local card_title = self:get_child_by_id("card_title_" .. i)
        card_title:set_text(card.title)

        -- Update the cooldown
        local cooldown = self:get_child_by_id("cooldown_" .. i)
        table.insert(t1, sol.timer.start(game, 300, function()
            if card.timer == nil then
                return true
            end
            cooldown:set_text(tostring(math.floor(card.timer:get_remaining_time() / 1000)))
            return true
        end))
    end
    table.insert(t1, sol.timer.start(game:get_map(), 500, function()
        local health = game:get_life()
        -- Update the position of the health bar
        local y = math.floor(health/10)
        print("hud/hp/HP BAR " .. (13 - y) .. ".png")
        if y <= 0 then
            y = 1
        end
        health_bar:set_image("hud/hp/HP BAR " .. (13 - y) .. ".png")
        health_text:set_text("Health: " .. y .. " / " .. math.floor(max_health/10))

        local map = game:get_map()
        if map then
            local boss = map:get_entity("boss")
            if boss then
                local boss_health = boss:get_life()
                local boss_max_health = boss:get_property("max_health")
                -- Boss y is equal to half when it's health is 1/3 empty, and 0 when it's 2/3 empty
                print((boss_health - (boss_max_health/3)) / (2*boss_max_health/3)*10)
                local boss_y = math.floor((boss_health - (boss_max_health/3)) / (2*boss_max_health/3)*10)
                print(boss_y, boss_health, boss_max_health)
                if boss_y <= 1 then
                    boss_y = 1
                end
                boss_health_bar:set_image("hud/boss_hp/HP Boss " .. (11 - boss_y) .. ".png")
                return true
            end
        end
        --health_bar:render()

        return true
    end))
end

root:add_child(boss_health_bar)
root:add_child(health_bar)
root:add_child(health_text)

function root:on_finished()
    for _, timer in ipairs(t1) do
        timer:stop()
    end
end


return root