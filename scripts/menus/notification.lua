local saltk = require("scripts/saltk/main")

local notify_menu = saltk.Image.new({id="notify_menu", image="notification.png",
    position={110, 180}, size={100, 20}, scale=true,
    special_scale=3, sensitive=false, opacity = 0})

local notification_text = saltk.Text.new({id="notification_text", text="Hello World!",
    position={28, 8}, size={120, 40}, sensitive=false, font="enter_command",
    font_size=8, opacity = 0, color={255,255,255}})

function notify_menu:achievement(args)
    sol.menu.bring_to_front(notify_menu)
    notification_text:set_text(args)
    -- Create a fade in effect
    local opacity = 0
    local fadein_phase = true
    sol.audio.play_sound("bird_cute")
    notify_menu:set_position({110, 180})
    notify_menu:set_size({100, 20})
    notification_text:set_position({110 + 28, 180 + 8})
    notification_text:set_size({120, 40})
    sol.timer.start(sol.main, 50, function()
        if fadein_phase then
            opacity = opacity + 10
            if opacity >= 255 then
                opacity = 255
                fadein_phase = false
            end
        else
            opacity = opacity - 10
            if opacity <= 0 then
                opacity = 0
                notify_menu:set_opacity(opacity)
                notification_text:set_opacity(opacity)
                return false
            end
        end
        notify_menu:set_opacity(opacity)
        notification_text:set_opacity(opacity)
        return true
    end)
end

notify_menu:add_child(notification_text)

return notify_menu