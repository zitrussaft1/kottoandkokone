local function init(game)
    require("scripts/phone/apps/achievements")(game)
    require("scripts/phone/apps/gallery")(game)
    require("scripts/phone/apps/contacts")(game)
    require("scripts/phone/apps/health")(game)
end

return init