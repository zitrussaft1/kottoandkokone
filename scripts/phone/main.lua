local saltk = require("scripts/saltk/main")

local function new_achievements()
    for _, achievement in ipairs(conquista.a) do
        if achievement.state == 1 then
            return true
        end
    end
    return false
end

local function new_scenes()
    for _, scene in ipairs(scenes.a) do
        if scene.state == 1 then
            return true
        end
    end
    return false
end

local phone_bg = saltk.Image.new({id="phone_bg", 
    image="phone/bg.png", size={160, 240}, 
    position={80, 20}, sensitive=false
})

local phone = saltk.Image.new({id="phone", 
    image="phone/phone.png", size={160, 280}, 
    position={80, 20}, scale=true, can_be_selected=false,
})



function phone_setup(game)
    local phone = saltk.Widget.new({id="phone", size={160, 280},
        position={80, 20}, scale=true, can_be_selected=false,
    })
    local phone_a = saltk.Image.new({id="phone_a", 
        image="phone/phone.png", size={160, 280}, 
        position={0, 0}, can_be_selected=false
    })

    local phone_bg = saltk.Image.new({id="phone_bg", 
        image="phone/bg.png", size={160, 240}, 
        position={0, 0}, can_be_selected=false
    })

    local grid = saltk.Grid.new({
        id="grid", grid={4,5}, size={160, 180}, 
        position={6, 12}, can_be_selected=false
    })
    local apps = {
        "camera",
        "gallery",
        "messages",
        "achievements",
        "contacts",
        "health",
    }
    require("scripts/phone/start_apps")(game)
    
    saltk.Widget.add_child(phone, phone_a)
    saltk.Widget.add_child(phone, phone_bg)
    saltk.Widget.add_child(phone, grid)
    for i in ipairs(apps) do
        local app = saltk.Grid.new({id=apps[i], can_be_selected=true,
            grid={1,2}, padding={0,0},
            on_key_press = function(widget, key)
                if key == "return" then
                    if apps[i] == "messages" then 
                        sol.main.get_game():set_saltk_enabled("phone", false)
                        sol.main.get_game():set_saltk_enabled("contacts_app", true)
                    elseif apps[i] == "camera" then
                        sol.main.get_game():set_saltk_enabled("phone", false)
                    else
                        sol.main.get_game():set_saltk_enabled("phone", false)
                        sol.main.get_game():set_saltk_enabled(apps[i] .. "_app",
                            not sol.main.get_game():is_saltk_enabled(apps[i] .. "_app")
                        )
                    end
                elseif key == "x" then
                    sol.main.get_game():set_saltk_enabled("phone", false)
                end
            end
        } )

        local app_name = saltk.Text.new({id="app_name", halign="center",
            valign="middle", text_overflow="...",
            overflow=false, font_size=8,
            text=apps[i], font="enter_command",
            padding={0,0}
        })
        local app_icon = saltk.Image.new({id="app_icon", 
            image="phone/"..apps[i]..".png",
            padding={4,0}
        })
        local app_notify = saltk.Widget.new({id="app_notify", 
            size={4, 4}, position={0, 0},
            can_be_selected=false,
            render = function (widget)
                widget.surface:clear()
                if apps[i] == "achievements" and new_achievements() then
                        widget.surface:fill_color({255, 0, 0})
                elseif apps[i] == "gallery" and new_scenes() then
                        widget.surface:fill_color({255, 0, 0})
                end
            end
        })
        saltk.Grid.add_child(grid, app)
        saltk.Grid.add_child(app, app_icon)
        saltk.Widget.set_size(app_icon, 16, 16)
        saltk.Widget.add_child(app, app_notify)
        saltk.Grid.add_child(app, app_name)
    end
    saltk.init(game, "phone", phone, true)
end

return phone_setup