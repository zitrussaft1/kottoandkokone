local saltk = require("scripts/saltk/main")

local function health_setup(game)
    local phone = saltk.Widget.new({id="achievement_app", size={160, 280},
        position={80, 20}, scale=true, can_be_selected=false,
    })
    local phone_a = saltk.Image.new({id="phone_a", 
        image="phone/phone.png", size={160, 280}, 
        position={0, 0}, can_be_selected=false
    })
    local phone_bg = saltk.Widget.new({id="phone_bg", 
        background_color={100,100,255}, size={140, 240}, 
        position={10, 20}, can_be_selected=false
    })

    local i = 0

    local grid = saltk.Grid.new({
        id="grid", grid={1,9}, size={140, 180}, 
        can_be_selected=false,
        position={10, 20},
        padding={2,2},
    })
    saltk.Widget.add_child(phone, phone_a)
    saltk.Widget.add_child(phone, phone_bg)
    saltk.Widget.add_child(phone, grid)

    local function add_skills(i)
        for j = 1, 7 do
            if skills.a[i + j] and skills.a[i + j].state > 0 then
                local contact = saltk.Widget.new({
                    id="skill_"..j, 
                    can_be_selected=true,
                    render=function(widget) 
                        widget.surface:fill_color({255,255,255})
                    end,
                    on_key_press=function(widget, key)
                        if key == "return" then
                            sol.main.get_game():set_saltk_enabled("health_app", false)
                            sol.main.get_game():set_saltk_enabled("phone", true)
                        elseif key == "x" then
                            sol.main.get_game():set_saltk_enabled("health_app", false)
                            sol.main.get_game():set_saltk_enabled("phone", true)
                        end
                    end,
                })
                saltk.Grid.add_child(grid, contact)

                local contact_text = saltk.Text.new({
                    id="contact_text_"..j, 
                    font="enter_command",
                    text=skills.a[i + j].name, 
                    color={0,0,0},
                    position={32, 5}, can_be_selected=false,
                })
                saltk.Widget.add_child(contact, contact_text)
            else
                local contact = saltk.Widget.new({
                    id="skill_"..j, 
                    can_be_selected=false,
                })
            end
        end
    end

    local function is_there(i)
        if skills.a[i] then
            return true
        else
            return false
        end
    end

    local function is_there_next(i)
        return {
            is_there(i-6),
            is_there(i+7),
        }
    end

    local bs = {}

    local corp_status = saltk.Widget.new({
        id="corp_status", 
        can_be_selected=false,
        on_key_press=function(widget, key)
            if key == "return" then
                sol.main.get_game():set_saltk_enabled("health_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("health_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
    })
    saltk.Grid.add_child(grid, corp_status)
    local corp_status_text = saltk.Text.new({
        id="corp_status_text", 
        font="enter_command",
        text="Health", 
        halign="center",
        valign="middle",
        color={255,30,10},
        size = {138, 18},
        position={0, 0},
        can_be_selected=false,
        render = function(text)
            text.text = "" .. game:get_corp() .. " Corps"
            if text.scale then
                text.font_s = sol.text_surface.create({
                    font = text.font,
                    font_size = text.font_size * text.width_scale,
                    color = text.color,
                    text = text.text,
                })
            end
            local x = 0
            local y = 4
            local t_x, t_y = text.surface:get_size()
            local tt_x, tt_y = text.font_s:get_size()

            if text.should_fit then
                while t_x < tt_x do
                    text.font_s = sol.text_surface.create({
                        font = text.font,
                        font_size = text.font_size * text.width_scale,
                        color = text.color,
                        text = text.text,
                    })
                    tt_x, tt_y = text.font_s:get_size()
                    text.font_size = text.font_size - 1
                end
            end
            if not text.overflow then
                while t_x < tt_x do
                    text.text = text.text:sub(1, -5)
                    text.text = text.text .. text.text_overflow
                    text.font_s = sol.text_surface.create({
                        font = text.font,
                        font_size = text.font_size * text.width_scale,
                        color = text.color,
                        text = text.text,
                    })
                    tt_x, tt_y = text.font_s:get_size()
                end
            end


            if text.halign == "center" then
                x = (t_x - tt_x) / 2
            elseif text.halign == "right" then
                x = tt_y - t_x 
            end
            if text.valign == "middle" then
                y = (t_y - tt_y)
            elseif text.valign == "bottom" then
                y = tt_x - t_y
            end
            text.font_s:draw(text.surface, x, y)
        end,
    })
    saltk.Widget.add_child(corp_status, corp_status_text)

    local prev = saltk.Widget.new({
        id="prev", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[1] then
                i = i - 8
                saltk.Widget.remove_all_children(grid)
                saltk.Grid.add_child(grid, bs[1])
                add_skills(i)
                saltk.Grid.add_child(grid, bs[2])
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("health_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[1] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })
    saltk.Grid.add_child(grid, prev)

    add_skills(i)

    local nexta = saltk.Widget.new({
        id="next", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[2] then
                i = i + 8
                saltk.Widget.remove_all_children(grid)
                saltk.Grid.add_child(grid, bs[1])
                add_skills(i)
                saltk.Grid.add_child(grid, bs[2])
                return grid.children[1]
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("health_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[2] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })
    saltk.Grid.add_child(grid, nexta)

    bs = {
        prev,
        nexta,
    }

    saltk.init(game, "health_app", phone, true)
end

return health_setup