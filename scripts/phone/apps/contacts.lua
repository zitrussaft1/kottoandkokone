local saltk = require("scripts/saltk/main")

local function contacts_setup(game)
    local phone = saltk.Widget.new({id="achievement_app", size={160, 280},
        position={80, 20}, scale=true, can_be_selected=false,
    })
    local phone_a = saltk.Image.new({id="phone_a", 
        image="phone/phone.png", size={160, 280}, 
        position={0, 0}, can_be_selected=false
    })
    local phone_bg = saltk.Widget.new({id="phone_bg", 
        background_color={100,100,255}, size={140, 240}, 
        position={10, 20}, can_be_selected=false
    })

    local i = 0

    local grid = saltk.Grid.new({
        id="grid", grid={1,9}, size={140, 180}, 
        can_be_selected=false,
        position={10, 20},
        padding={2,2},
    })
    saltk.Widget.add_child(phone, phone_a)
    saltk.Widget.add_child(phone, phone_bg)
    saltk.Widget.add_child(phone, grid)

    local function add_contacts(i)
        for j = 1, 7 do
            if contacts.a[i + j] then
                local contact = saltk.Widget.new({
                    id="contact_"..j, 
                    can_be_selected=true,
                    render=function(widget) 
                        widget.surface:fill_color({255,255,255})
                    end,
                    on_key_press=function(widget, key)
                        if key == "return" then
                            sol.main.get_game():set_saltk_enabled("contacts_app", false)
                            sol.main.get_game():set_saltk_enabled("phone", true)
                        elseif key == "x" then
                            sol.main.get_game():set_saltk_enabled("contacts_app", false)
                            sol.main.get_game():set_saltk_enabled("phone", true)
                        end
                    end,
                })
                saltk.Grid.add_child(grid, contact)

                if contacts.a[i + j].icone ~= nil then
                    local contact_icon = saltk.Image.new({
                        id="contact_icon_"..j, 
                        image=contacts.a[i + j].icone, size={20, 20}, 
                        position={0, 2}, can_be_selected=false,
                    })
                    saltk.Widget.add_child(contact, contact_icon)
                end

                if contacts.a[i + j].name ~= nil then
                    local contact_text = saltk.Text.new({
                        id="contact_text_"..j, 
                        font="enter_command",
                        text=contacts.a[i + j].name, 
                        color={0,0,0},
                        position={32, 5}, can_be_selected=false,
                    })
                    saltk.Widget.add_child(contact, contact_text)
                end
            end
        end
    end

    local function is_there(i)
        if contacts.a[i] then
            return true
        else
            return false
        end
    end

    local function is_there_next(i)
        return {
            is_there(i-6),
            is_there(i+7),
        }
    end

    local bs = {}

    local prev = saltk.Widget.new({
        id="prev", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[1] then
                i = i - 8
                saltk.Widget.remove_all_children(grid)
                saltk.Grid.add_child(grid, bs[1])
                add_contacts(i)
                saltk.Grid.add_child(grid, bs[2])
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("contacts_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[1] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })
    saltk.Grid.add_child(grid, prev)

    add_contacts(i)

    local nexta = saltk.Widget.new({
        id="next", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[2] then
                i = i + 8
                saltk.Widget.remove_all_children(grid)
                saltk.Grid.add_child(grid, bs[1])
                add_contacts(i)
                saltk.Grid.add_child(grid, bs[2])
                return grid.children[1]
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("contacts_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[2] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })
    saltk.Grid.add_child(grid, nexta)

    bs = {
        prev,
        nexta,
    }

    saltk.init(game, "contacts_app", phone, true)
end

return contacts_setup