local saltk = require("scripts/saltk/main")

local function gallery_setup(game)
    local phone = saltk.Widget.new({id="achievement_app", size={160, 280},
        position={80, 20}, scale=true, can_be_selected=false,
    })
    local phone_a = saltk.Image.new({id="phone_a", 
        image="phone/phone.png", size={160, 280}, 
        position={0, 0}, can_be_selected=false
    })
    local phone_bg = saltk.Widget.new({id="phone_bg", 
        background_color={100,100,255}, size={140, 240}, 
        position={10, 20}, can_be_selected=false
    })

    local i = 0

    local grid = saltk.Grid.new({
        id="grid", grid={2,5}, size={140, 180}, 
        can_be_selected=false,
        position={10, 20},
        padding={2,2}
    })
    saltk.Widget.add_child(phone, phone_a)
    saltk.Widget.add_child(phone, phone_bg)
    saltk.Widget.add_child(phone, grid)
    local function add_scenes(i)
        for j = 1, 8 do
            if scenes.a[i + j] then
                local scene = saltk.Widget.new({
                    id="scene_"..j, 
                    can_be_selected=true,
                    on_key_press=function(widget, key)
                        local game = sol.main.get_game()
                        if key == "return" then
                            if scenes:get_state(scenes.a[i+j].id) ~= 0 then
                               scenes:set_state(scenes.a[i+j].id, 2)
                                game:set_saltk_enabled("gallery_app", false)
                                game:start_dialog(scenes.a[i+j].id,
                                    nil, function()
                                        game:set_saltk_enabled("gallery_app", true)
                                    end
                                )
                            end
                        elseif key == "x" then
                            game:set_saltk_enabled("gallery_app", false)
                            game:set_saltk_enabled("phone", true)
                        end
                    end,
                })
                saltk.Grid.add_child(grid, scene)

                if scenes.a[i + j].icone ~= nil and scenes.a[i+j].state ~= 0 then
                    local scene_icon = saltk.Image.new({
                        id="scene_icon_"..j, 
                        image=scenes.a[i + j].icone, size={70, 40}, 
                        position={0, 0}, can_be_selected=false,
                    })
                    saltk.Widget.add_child(scene, scene_icon)
                else
                    local scene_icon_black = saltk.Widget.new({
                        id="scene_icon_black_"..j, 
                        background_color={0,0,0}, size={70, 40}, 
                        position={0, 0}, can_be_selected=false,
                    })
                    saltk.Widget.add_child(scene, scene_icon_black)
                end

                local scene_notify = saltk.Widget.new({
                    id="scene_notify_"..j, 
                    background_color={255,0,0}, size={10, 10}, 
                    position={0, 0}, can_be_selected=false,
                    render=function(widget)
                        if scenes:get_state(scenes.a[i + j].id) == 1 then       
                            widget.surface:fill_color({255,0,0})
                        else
                            widget.surface:clear()
                        end
                    end,
                })
                saltk.Widget.add_child(scene, scene_notify)

                -- Nome das cenas ?
                --local scene_name = saltk.Text.new({
                --    id="scene_name_"..j, 
                --    text=scenes.a[i + j].pretty_name, size={70, 40}, 
                --    position={0, 40}, can_be_selected=false,
                --})
                --saltk.Widget.add_child(scene, scene_name)
            else
                local scene = saltk.Widget.new({
                    id="scene_"..j, 
                    can_be_selected=false,
                })
                saltk.Grid.add_child(grid, scene)
            end
        end

    end

    add_scenes(i)

    local function is_there(i)
        if scenes.a[i] then
            return true
        else
            return false
        end
    end

    local function is_there_next(i)
        return {
            is_there(i-7),
            is_there(i+9),
        }
    end

    local bs = {}

    local nexta = saltk.Widget.new({
        id="nexta", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[2] then
                i = i + 8
                saltk.Widget.remove_all_children(grid)
                add_scenes(i)
                saltk.Grid.add_child(grid, bs[1])
                saltk.Grid.add_child(grid, bs[2])
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("gallery_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[2] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })

    local prev = saltk.Widget.new({
        id="prev", 
        can_be_selected=true,
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[1] then
                i = i - 8
                saltk.Widget.remove_all_children(grid)
                add_scenes(i)
                saltk.Grid.add_child(grid, bs[1])
                saltk.Grid.add_child(grid, bs[2])
            elseif key == "x" then
                sol.main.get_game():set_saltk_enabled("gallery_app", false)
                sol.main.get_game():set_saltk_enabled("phone", true)
            end
        end,
        render=function(widget)
            if is_there_next(i)[1] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })

    bs = {prev, nexta}

    saltk.Grid.add_child(grid, prev)
    saltk.Grid.add_child(grid, nexta)

    saltk.init(game, "gallery_app", phone, true)
end

return gallery_setup
