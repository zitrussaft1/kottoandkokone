local saltk = require("scripts/saltk/main")
local conquista = require("scripts/conquista")

local function achievements_setup(game)
    local phone = saltk.Widget.new({id="achievement_app", size={160, 280},
        position={80, 20}, scale=true, can_be_selected=false,
    })
    local phone_a = saltk.Image.new({id="phone_a", 
        image="phone/phone.png", size={160, 280}, 
        position={0, 0}, can_be_selected=false
    })
    local phone_bg = saltk.Widget.new({id="phone_bg", 
        background_color={100,100,255}, size={140, 240}, 
        position={10, 20}, can_be_selected=false
    })

    local i = 0

    local grid = saltk.Grid.new({
        id="grid", grid={1,5}, size={140, 180}, 
        can_be_selected=false,
        position={10, 20},
        padding={2,2}
    })
    saltk.Widget.add_child(phone, phone_a)
    saltk.Widget.add_child(phone, phone_bg)
    saltk.Widget.add_child(phone, grid)
    
    local function add_achievement(i)
        for j = 1, 4 do
            if conquista.a[i + j] then
                local achievement = saltk.Widget.new({
                    id="achievement_"..j, 
                    can_be_selected=true,
                    render=function(widget) 
                        if conquista.a[i+j].state == 0 then
                            widget.surface:fill_color({127,127,127})
                        else
                            widget.surface:fill_color({255,255,255})
                        end
                    end,
                    on_key_press=function(widget, key)
                        if key == "return" then
                            if conquista:get_state(conquista.a[i+j].id) == 1 then
                                conquista:set_state(conquista.a[i+j].id, 2)
                            end
                        elseif key == "x" then
                            sol.main.get_game():set_saltk_enabled("achievements_app", false)
                            sol.main.get_game():set_saltk_enabled("phone", true)
                        end
                    end,
                })
                saltk.Grid.add_child(grid, achievement)

                if conquista.a[i + j].icone ~= nil then
                    local achievement_icon = saltk.Image.new({
                        id="achievement_icon_"..j, 
                        image=conquista.a[i + j].icone, size={24, 24}, 
                        position={110, 5}, can_be_selected=false,
                    })
                    saltk.Widget.add_child(achievement, achievement_icon)
                end

                if conquista.a[i + j].description ~= nil then
                    local achievement_description = saltk.Text.new({
                        id="achievement_description_"..j, halign="left",
                        text_overflow="...",
                        overflow=false, font_size=12,
                        text=conquista.a[i + j].description, font="enter_command",
                        color={0,0,0},
                        position={4,16},
                        size={100, 20},
                        break_line=true,
                    })
                    saltk.Widget.add_child(achievement, achievement_description)
                end

                local notification = saltk.Widget.new({
                    id="notification_"..j, 
                    size={4, 4}, 
                    position={0, 0}, can_be_selected=false,
                    render=function(widget) 
                        if conquista:get_state(conquista.a[i+j].id) == 1 then
                            widget.surface:fill_color({255,0,0})
                        else
                            widget.surface:clear()
                        end
                    end,
                })

                local achievement_type = saltk.Text.new({
                    id="achievement_type_"..j, halign="left",
                    text_overflow="...",
                    overflow=false, font_size=14,
                    text=conquista.a[i+j].id, font="enter_command",
                    color={0,0,0},
                    position={4, 4}
                })

                saltk.Widget.add_child(achievement, notification)
                saltk.Widget.add_child(achievement, achievement_type)
            else
                local a = saltk.Widget.new({
                    id="achievement_"..j, 
                    can_be_selected=true,
                    render=function(widget) 
                        widget.surface:fill_color({127,127,127})
                    end,
                })
                saltk.Grid.add_child(grid, a)
            end
        end
    end

    add_achievement(i)

    local button_box = saltk.Grid.new({
        id="button_box", grid={2,1}, 
        can_be_selected=true,
        padding={2,2},
        on_key_press=function(widget, key)
            if key == "return" then
                saltk.Widget.unselect(widget)
                saltk.Widget.select(widget.children[1])
                return widget.children[1]
            end
        end,
    })

    local function is_there(i)
        if conquista.a[i] then
            return true
        else
            return false
        end
    end

    local function is_there_next(i)
        return {
            is_there(i-3),
            is_there(i+4),
        }
    end

    local nexta = saltk.Widget.new({
        id="next", size={160, 20}, 
        can_be_selected=true,
        background_color={255,255,255},
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[2] then
                i = i + 4
                saltk.Widget.remove_all_children(grid)
                add_achievement(i)
                saltk.Grid.add_child(grid, button_box)
                saltk.Widget.unselect(widget)
                saltk.Widget.select(grid.children[1])
                return grid.children[1]
            elseif key == "x" then
                saltk.Widget.unselect(widget)
                saltk.Widget.select(grid.children[1])
                return grid.children[1]
            end
        end,
        render=function(widget)
            if is_there_next(i)[2] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })
    
    local back = saltk.Widget.new({
        id="back", size={160, 20}, 
        can_be_selected=true,
        background_color={255,255,255},
        on_key_press=function(widget, key)
            if key == "return" and is_there_next(i)[1] then
                i = i - 4
                saltk.Widget.remove_all_children(grid)
                add_achievement(i)
                saltk.Grid.add_child(grid, button_box)
                saltk.Widget.unselect(widget)
                saltk.Widget.select(grid.children[1])
                return grid.children[1]
            elseif key == "x" then
                saltk.Widget.unselect(widget)
                saltk.Widget.select(grid.children[1])
                return grid.children[1]
            end
        end,
        render=function(widget)
            if is_there_next(i)[1] then
                widget.surface:fill_color({255,255,255})
            else
                widget.surface:fill_color({127,127,127})
            end
        end,
    })

    saltk.Grid.add_child(grid, button_box)
    saltk.Grid.add_child(button_box, back)
    saltk.Grid.add_child(button_box, nexta)

    saltk.init(game, "achievements_app", phone, true)
end

return achievements_setup