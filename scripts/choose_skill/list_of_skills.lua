local function strengthen()
    print(BONUS_DAMAGE)
    BONUS_DAMAGE = BONUS_DAMAGE + 2
    print(BONUS_DAMAGE)
end

local function undo_strengthen()
    print(BONUS_DAMAGE)
    BONUS_DAMAGE = BONUS_DAMAGE - 2
    print(BONUS_DAMAGE)
end

local function ap_strengthen()
    print(BONUS_ATTACK_POWER)
    BONUS_ATTACK_POWER =  BONUS_ATTACK_POWER + 2
    print(BONUS_ATTACK_POWER)
end

local function undo_ap_strengthen()
    print(BONUS_ATTACK_POWER)
    BONUS_ATTACK_POWER =  BONUS_ATTACK_POWER - 2
    print(BONUS_ATTACK_POWER)
end

local function speed_strengthen()
    print(BONUS_ATTACK_SPEED)
    BONUS_ATTACK_SPEED =  BONUS_ATTACK_SPEED + 0.2
    print(BONUS_ATTACK_SPEED)
end

local function undo_speed_strengthen()
    print(BONUS_ATTACK_SPEED)
    BONUS_ATTACK_SPEED =  BONUS_ATTACK_SPEED - 0.2
    print(BONUS_ATTACK_SPEED)
end

local function dummy()
end

SKILLS_LIST = {
    kokonexmike_routea_intro = {
        title = "Pfff, I would like to see you try",
        action = function ()
            NEXT_DIALOG = "kokonexmike.routea.intro"
            strengthen()
        end,
        undo_action = undo_strengthen
    },
        kokonexmike_routea_option1 = {
            title = "Sure sure, just to calm you down...",
            action = function ()
                NEXT_DIALOG = "kokonexmike.routea.option1"
                strengthen()
            end,
            undo_action = undo_strengthen
        },
            kokonexmike_routea_endinga = {
                title = "Hum, I guess I need a better look...",
                lewd = true,
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routea.endinga"
                    BOSS_RESPAWN = false
                    ap_strengthen()
                end,
                undo_action = undo_ap_strengthen
            },
            kokonexmike_routea_endingb1 = {
                title = "Amazing! How can I get like that too?!",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routea.endingb1"
                    BOSS_RESPAWN = false
                    ap_strengthen()
                end,
                undo_action = undo_ap_strengthen
            },
        kokonexmike_routea_option2 = {
            title = "Nah, I can see from here...",
            action = function ()
                NEXT_DIALOG = "kokonexmike.routea.option2"
                speed_strengthen()
            end,
            undo_action = undo_speed_strengthen
        },
            kokonexmike_routea_endingc = {
                title = "Suuuure, how about I post the video of you...",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routea.endingc"
                    BOSS_RESPAWN = false
                    GAME_OVER = "mike"
                    speed_strengthen()
                end,
                undo_action = undo_speed_strengthen
            },
            kokonexmike_routea_endingd = {
                title = "You’re a weirdo that throws cash away...",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routea.endingd"
                    BOSS_RESPAWN = false
                    strengthen()
                end,
                undo_action = undo_strengthen
            },
    kokonexmike_routeb_intro = {
        title = "Y-you will?",
        action = function ()
            NEXT_DIALOG = "kokonexmike.routeb.intro"
            strengthen()
        end,
        undo_action = undo_strengthen
    },
        kokonexmike_routeb_option1 = {
            title = "Oh? What kind of cardio? We’re going to run?",
            action = function ()
                NEXT_DIALOG = "kokonexmike.routeb.option1"
                ap_strengthen()
            end,
            undo_action = undo_ap_strengthen
        },
            kokonexmike_routeb_endinga = {
                title = "What are you waiting for then?",
                lewd = true,
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routeb.endinga"
                    BOSS_RESPAWN = false
                    strengthen()
                end,
                undo_action = undo_strengthen
            },
            kokonexmike_routeb_endingb2 = {
                title = "Yeah! Let’s go!",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routeb.endingb2"
                    BOSS_RESPAWN = false
                    ap_strengthen()
                end,
                undo_action = undo_ap_strengthen
            },
        kokonexmike_routeb_option2 = {
            title = "Now now aren’t you being a naughty boy?",
            action = function ()
                NEXT_DIALOG = "kokonexmike.routeb.option2"
                speed_strengthen()
            end,
            undo_action = undo_speed_strengthen
        },
            kokonexmike_routeb_endinge = {
                title = "Ew, why would I want that?",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routeb.endinge"
                    BOSS_RESPAWN = false
                    GAME_OVER = "mike"
                    speed_strengthen()
                end,
                undo_action = undo_speed_strengthen
            },
            kokonexmike_routeb_endingg = {
                title = "Now now, first off you have to earn that right",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routeb.endingg"
                    BOSS_RESPAWN = false
                    ap_strengthen()
                end,
                undo_action = undo_ap_strengthen
            },
    kokonexmike_routec_intro = {
        title = "Well big man, I may be sweating, but~",
        action = function ()
            NEXT_DIALOG = "kokonexmike.routec.intro"
            strengthen()
        end,
        undo_action = undo_strengthen
    },
        kokonexmike_routec_option1 = {
            title = "Oh and you’re gonna help me out with that?",
            action = function ()
                strengthen()
            end,
            undo_action = undo_strengthen
        },
        kokonexmike_routec_option2 = {
            title = "My my, after we’re done Mikey~",
            action = function ()
                NEXT_DIALOG = "kokonexmike.routec.option2"
                ap_strengthen()
            end,
            undo_action = undo_ap_strengthen
        },
            kokonexmike_routec_endinga = {
                title = "What are you waiting for then?",
                lewd = true,
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routec.endinga"
                    BOSS_RESPAWN = false
                    strengthen()
                end,
                undo_action = undo_strengthen
            },
            kokonexmike_routec_endingg = {
                title = "Now now, first off you have to earn it",
                action = function ()
                    NEXT_DIALOG = "kokonexmike.routec.endingg"
                    BOSS_RESPAWN = false
                    ap_strengthen()
                end,
                undo_action = undo_ap_strengthen
            },
    kick_ass = {
        title = "Kick ass",
        action = strengthen,
        undo = undo_strengthen
    },
    show_booba = {
        title = "Show booba",
        action = strengthen,
        undo_action = undo_strengthen
    },
    lol = {
        title = "Laugh out loud",
        action = dummy,
        undo_action = undo_strengthen
    },
}


SKILLS = {
    SKILLS_LIST["kick_ass"],
    SKILLS_LIST["show_booba"],
    SKILLS_LIST["lol"]
}
