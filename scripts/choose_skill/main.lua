local saltk = require("scripts/saltk/main")

require("scripts/choose_skill/list_of_skills")

function skill_setup(game)

    local bg = saltk.Image.new({id="skill_bg",
        position={0, 0}, can_be_selected=false,
        size={320,240},
        image=BG,
        scale=false,
    })

    local menu = saltk.Widget.new({id="skill_menu", size={220, 240},
        position={50, 0}, scale=false, can_be_selected=false,
        margin={0,0}, padding={0,0}
    })

    local empty_dialog = saltk.Image.new({id="empty_dialog",
        position={0, 160}, can_be_selected=false,
        size={220,80},
        image="skill_select/dialog_box.png",
        scale=true,
    })

    local grid = saltk.Grid.new({
        id="grid", grid={1,3}, size={220, 160}, 
        position={0, 0}, can_be_selected=false,
        margin={0,0}, padding={0,20}
    })
    saltk.Widget.add_child(bg, menu)
    saltk.Widget.add_child(menu, grid)
    saltk.Widget.add_child(menu, empty_dialog)
    local i = 0
    for _,skill in pairs(SKILLS) do
        local image2 = "skill_select/piece.png"
        if skill.lewd then
            image2 = "skill_select/hpiece.png"
        end 
        local widget = saltk.Sprite.new({
            id="skill_"..i, size={64,64},
            position={190, 20}, can_be_selected=false,
            margin={0,0}, padding={0,0},
            image="hud/dialog_box_message_end",
            render=function(image)
                image.surface:clear()
                if not image.parent.selected then 
                else
                    if image.scale then
                        local scale_x = image.size[1] / image.image_s_x
                        local scale_y = image.size[2] / image.image_s_y
                        image.image_s:set_scale(
                            scale_x * image.width_scale,
                            scale_y * image.height_scale
                        )
                    end
                    image.image_s:draw(image.surface)
                end
            end
        })
        local s = saltk.Image.new({id="skill_image_"..i,
            position={0, 0}, can_be_selected=true,
            size={220,76},
            image=image2,
            scale=true,
            on_key_press=function(widget, key)
                if key == "space" then
                    skill.action()
                    local game = sol.main.get_game()
                    game:get_map():set_reducer(1)
                    game:set_saltk_enabled("skill_menu", false)
                end
            end,
        })

        local text_title = saltk.Text.new({
            id="text_title", text=skill.title, 
            font="Peepo",
            font_size=8, 
            color={0,0,0}, position={20, 20}, 
            can_be_selected=false,
            halign="left"
        })

        saltk.Grid.add_child(grid, s)

        saltk.Widget.add_child(s, text_title)
        saltk.Widget.add_child(s, widget)

        i = i + 1
    end
    saltk.init(game, "skill_menu", bg, false)
end

return skill_setup



