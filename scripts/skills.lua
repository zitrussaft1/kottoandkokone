skills = {
    a = {
        {
            name = "strength",
            state = 1,
        },
        {
            name = "time_spike",
            state = 0,
        },
        {
            name = "dash",
            state = 1,
        },
        {
            name = "time_bubbles",
            state = 0,
        },
        {
            name = "frenzy",
            state = 1
        },
        {
            name = "fur_ball",
            state = 1
        },
        {
            name = "ancient_battle",
            state = 1
        }
    }
}

function skills:get_state(name)
    for _, skill in pairs(self.a) do
        if skill.name == id then
            return skill.state
        end
    end
end

function skills:set_state(name, state)
    for _, skill in pairs(self.a) do
        if skill.name == id then
            skill.state = state
            game:set_value("skill__"..skill.name, state)
        end
    end
end

function skills:update_skills()
    for _, skill in pairs(self.a) do
        skill.state = game:get_value("skill__"..skill.name)
    end
end

function skills:update_game()
    for _, skill in pairs(self.a) do
        game:set_value("skill__"..skill.name, skill.state)
    end
end
