scenes = {
    a = {
        {
            id = "book_on_treehouse",
            state = 1, -- Notify state,
            icone = "phone/bg.png",
            pretty_name = "Book on tree house",
        }
    }
}

function scenes:get_state(id)
    for _, scene in pairs(self.a) do
        if scene.id == id then
            return scene.state
        end
    end
end

function scenes:set_state(id, state)
    for _, scene in pairs(self.a) do
        if scene.id == id then
            scene.state = state
        end
    end
end

return true