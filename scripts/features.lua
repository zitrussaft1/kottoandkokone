-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

require("scripts/hud/hud")
require("scripts/menus/new_dialog_box")
require("scripts/meta/entity")
require("scripts/meta/game")
require("scripts/meta/camera")
require("scripts/meta/map")
require("scripts/meta/main_enemy")
require("scripts/meta/hero")
require("scripts/scenes")
require("scripts/contacts")
require("scripts/skills")
require("scripts/meta/master")

local features = {}
require("scripts/trillium/utility/savegame_tables")
require("items/trillium/charms/charm_manager")
function features.init(game)
  require("scripts/trillium/button_inputs"):initialize(game)
  require("scripts/cards"):initialize(game)
  require("scripts/conquista"):initialize(game)
  --require("scripts/trillium/fx/lighting_effects"):initialize()
end

return features
