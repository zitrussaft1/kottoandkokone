-- This script initializes game values for a new savegame file.
-- You should modify the initialize_new_savegame() function below
-- to set values like the initial life and equipment
-- as well as the starting location.
--
-- Usage:
-- local initial_game = require("scripts/initial_game")
-- initial_game:initialize_new_savegame(game)

local initial_game = {}

-- Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game)

  -- You can modify this function to set the initial life and equipment
  -- and the starting location.
  game:set_starting_location("tutorial", nil)  -- Starting location.

  -- Stats
  game:set_max_life(120)
  game:set_value("corruption", 0)
  game:set_value("cash", 0)
  game:set_life(game:get_max_life())
  game:set_max_money(100)
  game:set_ability("lift", 1)
  game:set_ability("sword", 0)
  game:set_value("equipped_weapon", "trillium/solforge/basic_sword")
  game:set_value("bomb", 3)
  game:set_value("light_beam", 1)
  game:set_value("time_slow", 1)
  game:set_value("shield", false)
  game:set_value("current_quest", 1)


  -- cards
  game:set_value("card__1__bully", 1)
  game:set_value("card__1__blood_thirst", 1)
  game:set_value("card__1__have_fun", 1)
  game:set_value("card__1__joyful_moment", 1)
  game:set_value("card__1__show_off", 1)
  game:set_value("card__1__compliment", 1)
  game:set_value("card__1__friendly_resolve", 1)
  game:set_value("card__1__dirty_mouth", 0)
  game:set_value("card__1__humiliate", 0)
  game:set_value("card__1__tease", 0)
  game:set_value("card__1__gently_advise", 0)
  game:set_value("card__1__stay_cute", 0)

  game:set_value("card__2__get_physical", 1)
  game:set_value("card__2__humiliate_ii", 1)
  game:set_value("card__2__tease_ii", 1)
  game:set_value("card__2__friendly_resolve_ii", 1)
  game:set_value("card__2__take_care", 1)
  game:set_value("card__2__furious_sprint", 0)
  game:set_value("card__2__magic_trick", 0)
  game:set_value("card__2__jester", 0)
  game:set_value("card__2__good_advise", 0)

  game:set_value("active_card__1__bully", 1)
  game:set_value("active_card__1__blood_thirst", 1)
  game:set_value("active_card__1__have_fun", 1)
  game:set_value("active_card__1__joyful_moment", 1)
  game:set_value("active_card__1__show_off", 1)
  game:set_value("active_card__1__compliment", 1)
  game:set_value("active_card__1__friendly_resolve", 1)

  game:set_value("active_card__2__get_physical", 1)
  game:set_value("active_card__2__humiliate_ii", 1)
  game:set_value("active_card__2__tease_ii", 1)
  game:set_value("active_card__2__friendly_resolve_ii", 1)
  game:set_value("active_card__2__take_care", 1)
  
  game:set_value("played_intro", 0)
  -- hero skills
  game:set_value("enable__dash", 0)
  game:set_value("can_dash", true)

  -- Skills
  game:set_value("skill__strength", 0)
  game:set_value("skill__time_spike", 0)
  game:set_value("skill__dash", 1)
  game:set_value("skill__time_bubbles", 0)

  game:set_value("skill__ancient_battle", 1)
  game:set_value("skill__ancient_battle__active", 0)

  game:set_value("skill__frenzy", 1)
  game:set_value("skill__frenzy__stack", 0)
  game:set_value("skill__frenzy__on_cooldown", 0)
  game:set_value("skill__frenzy__activated", 0)

  game:set_value("skill__fur_ball", 1)
  game:set_value("skill__fur_ball__stack", 0)
  game:set_value("skill__fur_ball__show_heal", 0)

  game:set_value("in_combat", 0)

  --Dialog 
  game:set_value("hide", "false")
  game:set_value("character_dialog", "kokone_sora")
  game:set_value("clothes", "pajama")
  game:set_value("mood", "Happeh")

  -- Map Stuff
  game:set_value("tint_r", nil)
  game:set_value("tint_g", nil)
  game:set_value("tint_b", nil)
  game:set_value("time", "day")
  game:set_value("dialog", nil)

  -- Game settings
  game:set_value("skill_cost", 5)
  game:set_value("character", "kokone_sora")

  -- Other stuff
  game:set_value("is_dashing", false)
  game:set_value("quest", false)
  

  --require("scripts/features").init(game)
end

return initial_game
