local conquista = {
    a = {
        --[[{
        type = "corruption",
        state = 0, -- Locked state
        id = "primeira vez",
        uid = "primeira_vez",
        --icone = "imageadsadjad",
        description = "tenha sua primeira cena, bem vindo ao mundo dos adultos",
        condition = function(a)
            if sol.main.get_game():get_value("corruption") >= 5 and a.state == 0 then
                menu:achievement(a.id)
                a.state = 1
            end
        end
    },
    {
        type = "corruption",
        state = 0, -- Locked state
        id = "gostosa",
        uid = "gostosa",
        icone = "phone/messages.png",
        description = ":)",
        condition = function(a)
            if  sol.main.get_game():get_value("corruption") >= 10 and a.state == 0 then
                menu:achievement(a.id)
                a.state = 1
            end
        end
    },
    {
        type = "corruption",
        state = 0, -- Locked state
        id = "gostosa 2",
        uid = "gostosa_2",
        --icone = "imageadsadjad",
        description = ":)",
        condition = function(a)
            if  sol.main.get_game():get_value("corruption") >= 15 and a.state == 0 then
                menu:achievement(a.id)
                a.state = 1
            end
        end
    },
    {
        type = "corruption",
        state = 0, -- Locked state
        id = "gostosa 3",
        uid = "gostosa_3",
        --icone = "imageadsadjad",
        description = ":)",
        condition = function(a)
            if  sol.main.get_game():get_value("corruption") >= 20 and a.state == 0 then
                menu:achievement(a.id)
                a.state = 1
            end
        end
    },
    {
        type = "corruption",
        state = 0, -- Locked state
        id = "gostosa 4",
        uid = "gostosa_4",
        --icone = "imageadsadjad",
        description = ":)",
        condition = function(a)
            if  sol.main.get_game():get_value("corruption") >= 25 and a.state == 0 then
                menu:achievement(a.id)
                a.state = 1
            end
        end
    }--]]
    }
}

for c_name, card in pairs(require("scripts/cards"):all_cards()) do
    if not (c_name == "bully" or c_name == "blood_thirst" or
        c_name == "have_fun" or c_name == "joyful_moment" or
        c_name == "show_off" or c_name == "compliment" or
        c_name == "friendly_resolve" or c_name == "get_physical" or
        c_name == "humiliate_ii" or c_name == "tease_ii" or
        c_name == "friendly_resolve_ii" or c_name == "take_care") then
        table.insert(conquista.a, {
            type = "card",
            state = 0, -- Locked state
            id = card.title,
            uid = c_name,
            icone = "phone/messages.png",
            description = "Unlock the card " .. card.title,
            condition = function(a)
                if sol.main.get_game():get_value("card__" .. card.lvl .. "__" ..c_name) == 1 and a.state == 0 then
                    a.state = 1
                elseif sol.main.get_game():get_value("card__" .. card.lvl .. "__"..c_name) == 1 and a.state == 0 then
                    a.state = 1
                end
            end
        })
    end
end

function conquista:initialize(game)
    for _, achievement in pairs(self.a) do
        if game:get_value("achievement_" .. achievement.uid) == 1 then
            achievement.state = 1
        elseif game:get_value("achievement_" .. achievement.uid) == 2 then
            achievement.state = 2
        end
    end
end

function conquista:update(type)
    local counter = 0
    for _, achievement in pairs(self.a) do
        if achievement.type == type then
            counter = counter + 1
            sol.main.get_game():set_value(achievement.type, counter)
            if achievement.state == 0 then
                achievement:condition()
                if achievement.state == 1 then
                    require("scripts/menus/notification"):achievement(achievement.id)
                    sol.main.get_game():set_value("achievement_" .. achievement.uid, 1)
                end
            end
        end
    end
end

function conquista:set_state(id , state)
    for _, achievement in pairs(self.a) do
        if achievement.id == id then
            achievement.state = state
            sol.main.get_game():set_value("achievement_" .. achievement.uid, state)
        end
    end
end

function conquista:get_state(id)
    for _, achievement in pairs(self.a) do
        if achievement.id == id then
            return achievement.state
        end
    end
end

function conquista:has_new()
    for _, achievement in pairs(self.a) do
        if achievement.state == 1 then
            return true
        end
    end
    return false
end

return conquista
