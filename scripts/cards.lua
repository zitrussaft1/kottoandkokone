
-- Use others/cards.html as reference
local CARDS = {
  -- Level 1, Aggressive
  dirty_mouth = {
    name = "dirty_mouth",
    effect = function()
      RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER + 0.4
      RATE_TOTAL_DAMAGE = RATE_TOTAL_DAMAGE + 0.4

      local boss = sol.main.get_game():get_map():get_entity("boss")
      boss:set_speed(1.35)

      sol.timer.start(sol.main,  10000, function()
        RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER - 0.4
        RATE_TOTAL_DAMAGE = RATE_TOTAL_DAMAGE - 0.4
        boss:set_speed(1)
      end)
    end,
    c_effect = function()
      RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER + 0.8
      RATE_TOTAL_DAMAGE = RATE_TOTAL_DAMAGE + 0.8

      local boss = sol.main.get_game():get_map():get_entity("boss")
      boss:set_speed(1.35)

      sol.timer.start(sol.main,  10000, function()
        RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER - 0.8
        RATE_TOTAL_DAMAGE = RATE_TOTAL_DAMAGE - 0.8
        boss:set_speed(1)
      end)
    end,
    cooldown = 15,
    type = "aggressive",
    title = "Dirty Mouth",
    description = "Boss takes 20% more damage but gets 35% more movement speed for 10 seconds.    (Cooldown: 15 seconds)",
    c = false,
    lvl = 1,
  },
  -- Humiliate: boss deals 50% more damage but gets stunned for 2 seconds
  -- after taking 5 attacks in the next 10 seconds
  humiliate = {
    name = "humiliate",
    effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        boss:set_adamage(1)
        running = false
      end)
      boss:set_adamage(1.5)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 5 then
            attacks = 0
            boss:confuse(4)
          end
        end
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        boss:set_adamage(1)
        running = false
      end)
      boss:set_adamage(1.5)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 5 then
            attacks = 0
            boss:confuse(8)
          end
        end
      end)
    end,
    cooldown = 15,
    type = "aggressive",
    title = "Humiliate",
    description = "Boss deals 50% more damage but gets stunned for 2 seconds after taking 5 attacks in the next 10 seconds. (Cooldown: 15 seconds)",
    c = false,
    lvl = 1,
  },
  -- Bully: Hero attacks are 15% faster for 5 seconds
  bully = {
    name = "bully",
    effect = function()
      local bonus_speed = BONUS_ATTACK_SPEED * 0.30
      BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED + bonus_speed

      sol.timer.start(sol.main,  5000, function()
        BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED - bonus_speed
      end)
    end,
    c_effect = function()
      local bonus_speed = BONUS_ATTACK_SPEED * 0.50
      BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED + bonus_speed

      sol.timer.start(sol.main,  5000, function()
        BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED - bonus_speed
      end)
    end,
    cooldown = 14,
    type = "aggressive",
    title = "Bully",
    description = "Koto attacks 15% faster for 5 seconds.    (Cooldown: 14 seconds)",
    c = false,
    lvl = 1,
  },
  -- Blood thirst: For 20 seconds, hitting the boss 10 times heals 10 life
  blood_thirst = {
    name = "blood_thirst",
    effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  20000, function()
        running = false
      end)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 10 then
            attacks = 0
            game:add_life(20)
          end
        end
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  20000, function()
        running = false
      end)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 7 then
            attacks = 0
            game:add_life(20)
          end
        end
      end)
    end,
    cooldown = 30,
    type = "aggressive",
    title = "Blood thirst",
    description = "After hitting the boss 10 times under 20 seconds heals half heart. (Cooldown: 30 seconds)",
    c = false,
    lvl = 1,
  },
  -- Level 1 Playful
  have_fun = {
    name = "have_fun",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local speed = hero:get_walking_speed() * 0.30
      hero:set_walking_speed(
        hero:get_walking_speed() + speed
      )
      game:set_value("have_fun", "true")
      sol.timer.start(sol.main,  10000, function()
        hero:set_walking_speed(
          hero:get_walking_speed() - speed
        )
      end)
      sol.timer.start(sol.main,  20000, function()
        game:set_value("have_fun", "false")
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local speed = hero:get_walking_speed() * 0.40
      hero:set_walking_speed(
        hero:get_walking_speed() + speed
      )
      game:set_value("have_fun", "true")
      sol.timer.start(sol.main,  10000, function()
        hero:set_walking_speed(
          hero:get_walking_speed() - speed
        )
      end)
      sol.timer.start(sol.main,  20000, function()
        game:set_value("have_fun", "false")
      end)
    end,
    cooldown = 20,
    type = "playful",
    title = "Have fun",
    description = "Gets 15% more movement speed and an additional 15% after dashing for 10 seconds. (Cooldown: 20 seconds)",
    c = false,
    lvl = 1,
  },
  -- Joyful moment: The cooldown of all cards is reduced by 20% for 20 seconds
  joyful_moment = {
    name = "joyful_moment",
    effect = function()
      BONUS_SKILL_SPEED = BONUS_SKILL_SPEED + 0.4
      sol.timer.start(sol.main,  20000, function()
        BONUS_SKILL_SPEED = BONUS_SKILL_SPEED - 0.4
      end)
    end,
    c_effect = function()
      BONUS_SKILL_SPEED = BONUS_SKILL_SPEED + 0.7
      sol.timer.start(sol.main,  20000, function()
        BONUS_SKILL_SPEED = BONUS_SKILL_SPEED - 0.7
      end)
    end,
    cooldown = 40,
    type = "playful",
    title = "Joyful moment",
    description = "All skills cooldowns are 20% faster for 20 seconds. (Cooldown: 40 seconds)",
    c = false,
    lvl = 1,
  },
  -- Tease: Dash recharges 30% faster for 7 seconds
  tease = {
    name = "tease",
    effect = function()
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.6
      sol.timer.start(sol.main,  7000, function()
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN + 0.6
      end)
    end,
    c_effect = function()
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.9
      sol.timer.start(sol.main,  7000, function()
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN + 0.9
      end)
    end,
    cooldown = 14,
    type = "playful",
    title = "Tease",
    description = "Dash recharges 30% faster for 7 seconds. (Cooldown: 14 seconds)",
    c = false,
    lvl = 1,
  },
  -- Show Off: Movement speed is increased by 15% for 7 seconds
  show_off = {
    name = "show_off",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local speed = hero:get_walking_speed() * 0.30
      hero:set_walking_speed(
        hero:get_walking_speed() + speed
      )
      sol.timer.start(sol.main,  7000, function()
        hero:set_walking_speed(
          hero:get_walking_speed() - speed
        )
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local speed = hero:get_walking_speed() * 0.6
      hero:set_walking_speed(
        hero:get_walking_speed() + speed
      )
      sol.timer.start(sol.main,  7000, function()
        hero:set_walking_speed(
          hero:get_walking_speed() - speed
        )
      end)
    end,
    cooldown = 14,
    type = "playful",
    title = "Show Off",
    description = "Movement speed is increased by 15% for 7 seconds. (Cooldown: 14 seconds)",
    c = false,
    lvl = 1,
  },
  -- Level 1 Kindness
  compliment = {
    name = "compliment",
    effect = function()
      local game = sol.main.get_game()
      -- Heal after 10 seconds
      local timer = sol.timer.start(sol.main,  10000, function()
        game:add_life(20)
      end)

      local hero = game:get_map():get_hero()
      -- If hero is hit, cancel timer
      hero:register_event("on_taking_damage", function()
        timer:stop()
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      -- Heal after 10 seconds
      local timer = sol.timer.start(sol.main,  10000, function()
        game:add_life(40)
      end)

      local hero = game:get_map():get_hero()
      -- If hero is hit, cancel timer
      hero:register_event("on_taking_damage", function()
        timer:stop()
      end)
    end,
    cooldown = 25,
    type = "kindness",
    title = "Compliment",
    description = "If you are not hit in 10 seconds heal 1 health. (Cooldown: 25 seconds)",
    c = false,
    lvl = 1,
  },
  -- Gently Advice: Boss deals 20% more damage for 7 seconds, but gets stunned for 2 seconds right after.
  gently_advise = {
    name = "gently_advise",
    effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  7000, function()
        boss:set_adamage(1)
        running = false
        boss:confuse(4)
      end)
      boss:set_adamage(1.2)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  7000, function()
        boss:set_adamage(1)
        running = false
        boss:confuse(8)
      end)
      boss:set_adamage(1.2)
    end,
    cooldown = 20,
    type = "kindness",
    title = "Gently Advise",
    description = "Boss deals 20% more damage for 7 seconds, but gets stunned for 2 seconds afterwards. (Cooldown: 20 seconds)",
    c = false,
    lvl = 1,
  },
  -- Stay Cute: All cards coooldowns reset 20% faster for 20 seconds
  stay_cute = {
    name = "stay_cute",
    effect = function()
      BONUS_SKILL_SPEED = BONUS_SKILL_SPEED + 0.4
      sol.timer.start(sol.main,  20000, function()
        BONUS_SKILL_SPEED = BONUS_SKILL_SPEED - 0.4
      end)
    end,
    c_effect = function()
      BONUS_SKILL_SPEED = BONUS_SKILL_SPEED + 0.8
      sol.timer.start(sol.main,  20000, function()
        BONUS_SKILL_SPEED = BONUS_SKILL_SPEED - 0.8
      end)
    end,
    cooldown = 40,
    type = "kindness",
    title = "Stay Cute",
    description = "All cards cooldowns are 20% faster for 20 seconds. (Cooldown: 40 seconds)",
    c = false,
    lvl = 1,
  },
  -- Friendly Resolve: Takes no damage in the next attack taken for 2 seconds
  friendly_resolve = {
    name = "friendly_resolve",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      hero:set_invincible(true)
      local timer = sol.timer.start(sol.main,  2000, function()
        running = false
        hero:set_invincible(false)
      end)
      hero:register_event("on_taking_damage", function()
        if not running then
          return
        end
        hero:set_invincible(false)
        running = false
        timer:stop()
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      hero:set_invincible(true)
      local timer = sol.timer.start(sol.main,  4000, function()
        running = false
        hero:set_invincible(false)
      end)
      hero:register_event("on_taking_damage", function()
        if not running then
          return
        end
        hero:set_invincible(false)
        running = false
        timer:stop()
      end)
    end,
    cooldown = 30,
    type = "kindness",
    title = "Friendly Resolve",
    description = "Takes no damage in next attack for 2 seconds. (Cooldown: 20 seconds)",
    c = false,
    lvl = 1,
  },
  -- Level 2 Aggressive
  -- Furious Sprint: Skills deal 300% more damage and dash recharges 30% faster for 7 seconds
  furious_sprint = {
    name = "furious_sprint",
    effect = function()
      RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER + 4
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.6
      sol.timer.start(sol.main,  7000, function()
        RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER - 4
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN + 0.6
      end)
    end,
    c_effect = function()
      RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER + 10
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.9
      sol.timer.start(sol.main,  7000, function()
        RATE_TOTAL_ATTACK_POWER = RATE_TOTAL_ATTACK_POWER - 10
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN + 0.9
      end)
    end,
    cooldown = 15,
    type = "aggressive",
    title = "Furious Sprint",
    description = "Koto deals 300% more damage and recharges the dash 30% faster for 7 seconds. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
  -- Get Physical: All skills deal 50% more damage for 10 seconds
  get_physical = {
    name = "get_physical",
    effect = function()
      RATE_BASE_ATTACK_POWER = RATE_BASE_ATTACK_POWER + 1
      sol.timer.start(sol.main,  10000, function()
        RATE_BASE_ATTACK_POWER = RATE_BASE_ATTACK_POWER - 1
      end)
    end,
    c_effect = function()
      RATE_BASE_ATTACK_POWER = RATE_BASE_ATTACK_POWER + 1.5
      sol.timer.start(sol.main,  10000, function()
        RATE_BASE_ATTACK_POWER = RATE_BASE_ATTACK_POWER - 1.5
      end)
    end,
    cooldown = 15,
    type = "aggressive",
    title = "Get Physical",
    description = "All skills deal 50% more damage for 10 seconds. (Cooldown: 30 seconds)",
    c = false,
    lvl = 2,
  },
  -- Humiliate II: Boss deals 100% more damage but gets stunned for 5 seconds, after taking 5 attacks
  -- in the next 10 seconds
  humiliate_ii = {
    name = "humiliate_ii",
    effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        boss:set_adamage(1)
        running = false
      end)
      boss:set_adamage(2)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 7 then
            attacks = 0
            boss:confuse(6)
          end
        end
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local boss = game:get_map():get_entity("boss")
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        boss:set_adamage(1)
        running = false
      end)
      boss:set_adamage(2)
      local attacks = 0
      boss:register_event("on_taking_damage", function()
        if not running then
          return
        end
        attacks = attacks + 1
        if timer:get_remaining_time() > 1 then
          if attacks >= 5 then
            attacks = 0
            boss:confuse(10)
          end
        end
      end)
    end,
    cooldown = 15,
    type = "aggressive",
    title = "Humiliate II",
    description = "Boss doubles its damage for 10 seconds but gets stunned for 5 seconds after taking 5 attacks. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
  -- Level 2 Playful
  -- Magic Trick: For 5 seconds if hit teleport to a random location and become invincible for 2 seconds
  magic_trick = {
    name = "magic_trick",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      local timer = sol.timer.start(sol.main,  5000, function()
        running = false
      end)
      hero:register_event("on_taking_damage", function(damage)
        if not running then
          return
        end
        local map = game:get_map()
        local width, height = map:get_size()

        local x = math.random(32, width - 32)
        local y = math.random(32, height - 32)
        hero:set_position(x, y)
        hero:set_invincible(true)
        -- Heal for damage taken
        sol.timer.start(sol.main,  2000, function()
          hero:set_invincible(false)
        end)
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      local timer = sol.timer.start(sol.main,  5000, function()
        running = false
      end)
      hero:register_event("on_taking_damage", function(damage)
        if not running then
          return
        end
        local map = game:get_map()
        local width, height = map:get_size()

        local x = math.random(32, width - 32)
        local y = math.random(32, height - 32)
        hero:set_position(x, y)
        hero:set_invincible(true)
        hero:set_blinking(true)
        -- Heal for damage taken
        hero:add_life(damage)
        sol.timer.start(sol.main,  2000, function()
          hero:set_invincible(false)
          hero:set_blinking(false)
        end)
      end)
    end,
    cooldown = 15,
    type = "playful",
    title = "Magic Trick",
    description = "If hit in the next 5 seconds teleports to a random location and is immune for 2 seconds. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
  -- Tease II: For 10 seconds, dash recharges 50% faster, 50% faster if kotto dodges an attack
  tease_ii = {
    name = "tease_ii",
    effect = function()
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.5
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        running = false
        BONUS_DASH_COOLDOWN = 0
      end)
      hero:register_event("on_taking_damage", function()
        if not running then
          BONUS_DASH_COOLDOWN = 0
          return
        end
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 0.5
      end)
    end,
    c_effect = function()
      BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 1
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local speed = hero:get_walking_speed() * 0.5
      local running = true
      local timer = sol.timer.start(sol.main,  10000, function()
        running = false
        BONUS_DASH_COOLDOWN = 0
        hero:set_walking_speed(
          hero:get_walking_speed() - speed
        )
      end)
      hero:set_walking_speed(
          hero:get_walking_speed() + speed
      )
      hero:register_event("on_taking_damage", function()
        if not running then
          BONUS_DASH_COOLDOWN = 0
          hero:set_walking_speed(
            hero:get_walking_speed() - speed
          )
          return
        end
        BONUS_DASH_COOLDOWN = BONUS_DASH_COOLDOWN - 1
        hero:set_walking_speed(
          hero:get_walking_speed() + speed
        )
      end)
    end,
    cooldown = 20,
    type = "playful",
    title = "Tease II",
    description = "For 10 seconds dash recharges 50% faster. If Kotto dodges an attack, cooldown for dash becomes 0 seconds. (Cooldown: 20 seconds)",
    c = false,
    lvl = 2,
  },
  -- Jester: The next bomb use will stun the boss for 3 seconds
  jester = {
    name = "jester",
    effect = function()
      local game = sol.main.get_game()
      game:set_value("jester", "true")
    end,
    c_effect = function()
      local game = sol.main.get_game()
      game:set_value("jester", "true")
    end,
    cooldown = 15,
    type = "playful",
    title = "Jester",
    description = "Toss an additional bomb. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
  -- Friendly resolve: Immediately heal 10 life
  friendly_resolve_ii = {
    name = "friendly_resolve_ii",
    effect = function()
      local game = sol.main.get_game()
      game:add_life(10)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      game:add_life(30)
    end,
    cooldown = 15,
    type = "kindness",
    title = "Friendly resolve II",
    description = "Heal 1 HP. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
  -- Take care: Hero heals 24 life if not hit for 10 seconds
  take_care = {
    name = "take_care",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local timer = sol.timer.start(sol.main,  10000, function()
        game:add_life(24)
      end)
      hero:register_event("on_taking_damage", function()
        timer:stop()
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local timer = sol.timer.start(sol.main,  5000, function()
        game:add_life(48)
      end)
      hero:register_event("on_taking_damage", function()
        timer:stop()
      end)
    end,
    cooldown = 30,
    type = "kindness",
    title = "Take care",
    description = "Kotto heals 20% HP if not hit  for 10 seconds. (Cooldown: 20 seconds)",
    c = false,
    lvl = 2,
  },
  -- Good advice: For 7 seconds if Hero is hit, summon a bomb at the boss
  good_advise = {
    name = "good_advise",
    effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      local timer = sol.timer.start(sol.main,  7000, function()
        running = false
      end)
      hero:register_event("on_taking_damage", function()
        if not running then
          return
        end
        local map = game:get_map()
        local width, height = map:get_size()

        local x = math.random(32, width - 32)
        local y = math.random(32, height - 32)
        map:create_custom_entity{
          name="bomb",
          width=16, height=16, direction=0,
          layer = 1,
          x = x,
          y = y,
          model = "bomb"
        }
      end)
    end,
    c_effect = function()
      local game = sol.main.get_game()
      local hero = game:get_map():get_hero()
      local running = true
      local timer = sol.timer.start(sol.main,  15000, function()
        running = false
      end)
      hero:register_event("on_taking_damage", function()
        if not running then
          return
        end
        local map = game:get_map()
        local width, height = map:get_size()

        local x = math.random(32, width - 32)
        local y = math.random(32, height - 32)
        map:create_custom_entity{
          name="bomb",
          width=16, height=16, direction=0,
          layer = 1,
          x = x,
          y = y,
          model = "bomb"
        }
        map:create_custom_entity{
          name="bomb",
          width=16, height=16, direction=0,
          layer = 1,
          x = x,
          y = y,
          model = "bomb"
        }
        map:create_custom_entity{
          name="bomb",
          width=16, height=16, direction=0,
          layer = 1,
          x = x,
          y = y,
          model = "bomb"
        }
      end)
    end,
    cooldown = 20,
    type = "kindness",
    title = "Good advise",
    description = "For 7 seconds after activated if Kotto takes damage a bomb will appear. (Cooldown: 15 seconds)",
    c = false,
    lvl = 2,
  },
}

local P_CARDS = {
    [1] = {
        aggressive = {},
        playful = {},
        kindness = {},
    },
    [2] = {
        aggressive = {},
        playful = {},
        kindness = {},
    },
}

local A_CARDS = {
    [1] = {
        aggressive = {},
        playful = {},
        kindness = {},
    },
    [2] = {
        aggressive = {},
        playful = {},
        kindness = {},
    },

}

local card_manager = {}

function card_manager:initialize(game)
    for card, data in pairs(CARDS) do
        if game:get_value("card__1__"..card) == 1 then
          table.insert(P_CARDS[1][data.type], CARDS[card])
        end
        if game:get_value("card__2__"..card) == 1 then
          table.insert(P_CARDS[2][data.type], CARDS[card])
        end
        if game:get_value("active_card__1__"..card) == 1 then
          table.insert(A_CARDS[1][data.type], CARDS[card])
        end
        if game:get_value("active_card__2__"..card) == 1 then
          table.insert(A_CARDS[2][data.type], CARDS[card])
        end
        function data:is_usable()
          if self.timer == nil then
            return true
          else if self.timer:get_remaining_time() == 0 then
            return true
          end
          return false
        end
      end
    end
    print(#P_CARDS[1]["aggressive"], #P_CARDS[1]["playful"], #P_CARDS[1]["kindness"])
end

function card_manager:unlock_card(card_name)
    local game = sol.main.get_game()
    local card = CARDS[card_name]
    game:set_value("card__"..card.lvl.."__"..card_name, 1)
    table.insert(P_CARDS[card.lvl][card.type], card)
    print("Unlocked card " .. card_name)
    require("scripts/conquista"):update("card")
    game:save()
end

function card_manager:draw_card(lvl, style)
    local styles = {"aggressive", "playful", "kindness"}
    if style == nil then
        style = styles[math.random(1, 3)]
    end

    if lvl == nil then
        lvl = math.random(1, 2)
    end

    local card = A_CARDS[lvl][style][math.random(1, #A_CARDS[lvl][style])]
    if nil == card then
        return card_manager:draw_card(lvl, styles[math.random(1, 3)])
    end
    return Table:deepcopy(card)
end

function card_manager:get_card(card)
    return CARDS[card]
end

local battle_dialog
function card_manager:activate_card(card, boss)
    if battle_dialog == nil then
        battle_dialog = require("scripts/menus/battle_dialog_box")
    end
    local dialog = ""
    if card.c == true then
        card.c_effect()
        dialog = "card_c" .. card.name .. "." .. boss
    else
        card.effect()
        dialog = "card." .. card.name .. "." .. boss
    end
    if sol.language.get_dialog(dialog) == nil then
        dialog = "card.default"
    end
    card.timer = sol.timer.start(sol.main,  SKILL_TIME((1000 * card.cooldown)), function()
    end)
    print(dialog)

    battle_dialog:start_dialog(dialog)
end

function card_manager:card_distribute(round, preffered_style, c_points)
  local lvls = {1,1,1}
  local c_cards = {nil, nil, nil}
  local game = sol.main.get_game()

  local c_level = game:get_value("corruption")
  if round == 1 then
    lvls = {2,1,1}
  elseif round == 2 then
    lvls = {2,2,1}
  elseif round == 3 then
    lvls = {2,2,2}
  end

  for i = 1, 3 do
    c_cards[i] = card_manager:draw_card(
      lvls[i],
      preffered_style
    )
    c_cards[i]["uid"] = i
  end

  for i = 1, math.min(c_level + c_points -1, 3) do
    c_cards[i]["c"] = true
  end

  for i = 1, 3 do
    local j = math.random(i)
    c_cards[i], c_cards[j] = c_cards[j], c_cards[i]
  end
  return c_cards
end

function card_manager:all_cards()
  return CARDS
end

function card_manager:get_number_of_cards(lvl)
  local count = 0
  for card, data in pairs(CARDS) do
    if data.lvl == lvl then
      count = count + 1
    end
  end
  return count
end

function card_manager:get_number_of_card_player(lvl)
  local count = 0
  local cards = P_CARDS[lvl]
  if cards == nil then
    return 0
  end
  for style, style_cards in pairs(cards) do
    for i, card in pairs(style_cards) do
      count = count + 1
    end
  end
  
  return count
end

function card_manager:get_number_of_active_cards(lvl)
  local count = 0
  local cards = A_CARDS[lvl]
  if cards == nil then
    return 0
  end
  for style, style_cards in pairs(cards) do
    for i, card in pairs(style_cards) do
      print(card.title)
      count = count + 1
    end
  end
  
  return count
end

function card_manager:is_an_active_card(card)
  local game = sol.main.get_game()
  return game:get_value("active_card__"..card.lvl.."__"..card.name) == 1
end

function card_manager:set_active_card(card)
  local game = sol.main.get_game()
  game:set_value("active_card__"..card.lvl.."__"..card.name, 1)
  -- Test if card is already active
  for i, c in pairs(A_CARDS[card.lvl][card.type]) do
    if c.name == card.name then
      return false
    end
  end
  table.insert(A_CARDS[card.lvl][card.type], card)
  return true
end

function card_manager:remove_active_card(card)
  local game = sol.main.get_game()
  game:set_value("active_card__"..card.lvl.."__"..card.name, 0)
  for i, c in pairs(A_CARDS[card.lvl][card.type]) do
    if c.name == card.name then
      table.remove(A_CARDS[card.lvl][card.type], i)
      return true
    end
  end
  return false
end

function card_manager:get_player_cards(lvl)
    if lvl == nil then
    return P_CARDS
  end
  local cards = {}
  for style, style_cards in pairs(P_CARDS[lvl]) do
    for i, card in pairs(style_cards) do
      table.insert(cards, card)
    end
  end
  
  return cards
end

function card_manager:max_active_cards(lvl)
  if lvl == 1 then
    return 7
  elseif lvl == 2 then
    return 3
  elseif lvl == 3 then
    return 1
  end
end

function card_manager:min_active_cards(lvl)
  return 2
end

function card_manager:get_color(card)
  if card.type == "aggressive" then
    return {255, 0, 0}
  elseif card.type == "playful" then
    return {0, 255, 0}
  elseif card.type == "kindness" then
    return {0, 0, 255}
  end
end
  
return card_manager