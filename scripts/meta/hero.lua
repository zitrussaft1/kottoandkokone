local hero_meta = sol.main.get_metatable("hero")

setmetatable(hero_meta, {__index = require("scripts/meta/trillium/hero")})

function hero_meta:on_taking_damage(damage)
    local game = self:get_game()
    local life = game:get_life()
    if game:get_value("is_dashing") then
      return
    end
    life = math.floor(life - damage/game:get_ability("tunic"))
    print(life)
    if game ~= nil and life <= 0 then
      game:start_game_over()
    end
    game:set_life(life)
end

local modulation_lock = false
function hero_meta:tint_hero(modulation, callback)
  if modulation_lock then return end
  local sprite = self:get_sprite()
  local current_modulation = sprite:get_color_modulation()

  local modulation_diff = {
    (modulation[1] - current_modulation[1])/10,
    (modulation[2] - current_modulation[2])/10,
    (modulation[3] - current_modulation[3])/10
  }

  modulation_lock = true

  local i = 0
  local function tint_hero()
    i = i + 1
    current_modulation = {
      current_modulation[1] + modulation_diff[1],
      current_modulation[2] + modulation_diff[2],
      current_modulation[3] + modulation_diff[3]
    }
    sprite:set_color_modulation(current_modulation)
    if i < 10 then
      sol.timer.start(self, 50, tint_hero)
    else
      modulation_lock = false
      if callback ~= nil then
        callback()
      end
    end
  end
  -- Create a recursive function to tint the hero over time
  tint_hero()
end

function hero_meta:blocks(entity, angle)
  local game = self:get_game()
  print(game:get_value("shield"), self:get_direction4_to(entity), self:get_sprite():get_direction())
  return (game:get_value("shield") == true and 
    self:get_direction4_to(entity) == self:get_sprite():get_direction())
end
local timer
local function start_heal_timer(hero,game)
  timer = sol.timer.start(hero, 10000, function()
    -- heal the hero
    if game:get_value("in_combat") == 1 then
      game:add_life(1)

      -- Add one stack of fur ball
      game:set_value("skill__fur_ball__stack", 
        game:get_value("skill__fur_ball__stack") + 1
      )

      sol.audio.play_sound("throw")

      -- Tint the character green
      hero:tint_hero({0, 255, 0}, function()
        hero:tint_hero({255, 255, 255})
      end)
    end

    start_heal_timer(hero, game)
  end)
end

function hero_meta:on_position_changed(x, y, layer)
  if timer ~= nil then
    timer:stop()
    timer = nil
  end

  local game = self:get_game()
  if game:get_value("skill__fur_ball") == 1 then
    start_heal_timer(self, game) 
  end
end

hero_meta:register_event("on_state_changed", function(self, state)
  local hero = self
  local game = sol.main.get_game()

  if state == "back to solid ground" then
    hero:set_invincible(true,1500)

  --weird bad fire sword
  elseif state == "sword swinging" and game.sword_on_fire then
    local dx = {[0]=16,[1]=0,[2]=-16,[3]=0}
    local dy = {[0]=0,[1]=-16,[2]=0,[3]=16}
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    local spread = {math.rad(-50), math.rad(-25), math.rad(25), math.rad(50)}
    for i=1, 4 do
      sol.timer.start(map, 50 * i-1, function()
        local flame = map:create_fire{ x = x+dx[direction], y= y+dy[direction], layer=z}
        local m = sol.movement.create"straight" m:set_speed(100) m:set_max_distance(16)
        m:set_ignore_obstacles()
        m:set_angle( direction*math.pi/2  + spread[i])
        m:start(flame, function() flame:remove() end)
        sol.timer.start(map, 1000, function() flame:remove() end)
      end)
    end

  --Sword Beam
  elseif state == "sword swinging" and game:get_life() == game:get_max_life() and 2==4 --turned off
  and game:get_value"hard_mode" and not hero.sword_beam_cooldown then
    hero.sword_beam_cooldown = true
    sol.timer.start(game,500,function() hero.sword_beam_cooldown = false end)
    local dx = {[0]=16,[1]=0,[2]=-16,[3]=0}
    local dy = {[0]=0,[1]=-16,[2]=0,[3]=16}
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()

    sol.audio.play_sound"sword_beam"
    local beam = map:create_custom_entity{
      x=x+dx[direction], y=y+dy[direction], layer=z, direction=direction, width=8, height=8,
      sprite="entities/trillium/sword_beam", model="damaging_sparkle"
    }
    beam:get_sprite():set_animation"head"
    sol.timer.start(beam, 60, function()
      local bx,by,bz=beam:get_position()
      local tail = map:create_custom_entity{x=bx,y=by,layer=bz,direction=direction,width=16,height=16,
      sprite="entities/trillium/sword_beam", model="ephereral_effect"}
      tail:get_sprite():set_animation"tail"
      if beam:exists() then return true end
    end)
    local m = sol.movement.create"straight"
    m:set_speed(250) m:set_angle(direction * math.pi/2) m:set_smooth(false)
    m:start(beam, function() beam:explode() end)
    function m:on_obstacle_reached() beam:explode() end
    function beam:explode()
      local bx,by,bz = beam:get_position()
      local pop = map:create_custom_entity{x=bx,y=by,layer=bz,direction=direction,width=16,height=16,
      sprite="enemies/trillium/enemy_killed_small", model="ephereral_effect"} 
      beam:remove()     
    end

  elseif game.spirit_counter_sword and state == "sword swinging" and hero:is_blinking() then
    local map = game:get_map()
    local x, y, z = hero:get_position()
    sol.audio.play_sound"sword_beam"
    map:create_custom_entity{x=x,y=y,layer=z,width=16,height=16,direction=0,
      model="damaging_sparkle",sprite="entities/trillium/spirit_counter"
    }

  end
end)

--[[function hero_meta:dash_new()
  local dash_speed = 100
  local dash_recovery = 1000
  self:set_walking_speed(self:get_walking_speed() + dash_speed)
  self:get_game():set_value("is_dashing", true)
  local hero_sprite = self:get_sprite()
  hero_sprite:set_color_modulation({0,0,255,255})
  BONUS_DAMAGE = BONUS_DAMAGE + AP()
  sol.timer.start(self, dash_recovery, function()
    self:set_walking_speed(self:get_walking_speed() - dash_speed)
    self:get_game():set_value("is_dashing", false)
    hero_sprite:set_color_modulation({255,255,255,255})
    BONUS_DAMAGE = BONUS_DAMAGE - AP()
  end)
end]]--

function hero_meta:shoot(damage, direction, size, speed, max_distance, is_piercing)
  local game = sol.main.get_game()
  local map = game:get_map()
  local x, y, layer = self:get_solid_ground_position()
  local direction = direction or self:get_direction()
  local projectile = map:create_custom_entity({
    name = "projectile",
    direction = direction,
    layer = layer,
    x = x,
    y = y,
    width = size or 16,
    height = size or 16,
    sprite = "entities/arrow",
    --model = nil,
    enabled_at_start = true
  })
  projectile:get_sprite():set_scale(size / 16, size / 16)
  function projectile:shoot()
    local is_piercing = is_piercing
    -- sprite:set_transformation_origin(0, -1)
    -- :set_xy(0, -14)
    -- sprite:set_rotation(angle)
    local m = sol.movement.create "straight"
    m:set_speed(speed or 300)
    m:set_max_distance(max_distance or 700)
    m:set_angle(direction * math.pi / 4)
    m:set_smooth(false)
    if is_piercing then
      m:set_ignore_obstacles(true)
    end
    m:start(projectile)
  end

  local collided_entities = {}
  projectile:add_collision_test("sprite", function(weapon_entity, other_entity, sprite, other_sprite)
    --Dedup check
    if other_entity:get_type() ~= "enemy" or collided_entities[other_entity] then return end
    collided_entities[other_entity] = true

    sol.audio.play_sound("bow")
    --Damage
    other_entity:remove_life(damage or 1)
  end)


  projectile:shoot()
end

hero_meta:register_event("on_taking_damage", function(damage)
  if sol.menu.is_started(require("scripts/menus/using_light_beam")) then
    sol.menu.stop(require("scripts/menus/using_light_beam"))
  end
end)