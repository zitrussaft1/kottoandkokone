require("scripts/meta/extra/dash")
local meta_enemy = sol.main.get_metatable("enemy")

require("scripts/meta/enemy/groundAttack")
require("scripts/meta/enemy/wallDash")
require("scripts/meta/enemy/movements")
require("scripts/meta/enemy/objectRain")
require("scripts/meta/enemy/atPlayerBite")
require("scripts/meta/enemy/boomerang")
require("scripts/meta/enemy/seduction")
require("scripts/meta/enemy/bulletShoot")
require("scripts/meta/enemy/spin")
require("scripts/meta/enemy/suck")
require("scripts/meta/enemy/invisible")
require("scripts/meta/enemy/kottoClone")
require("scripts/meta/enemy/confused")

local damage_taken = 0
local x, y = 0, 0
local looking = false
local time_spike = false
local to_draw = {}
local is_frozen = false
local is_invincible = false
local layer = 0

function meta_enemy:get_rekt(type)
  local d = damage()
  if type == "explosion" or type == "laser" then
    d = AP()
  end

  if self:get_property("invincible") == "true" then
    self:draw_damage({text = "Invulnerable"})
  else
    self:remove_life(d)
    self:draw_damage({text = tostring(d)})
  end
end


function meta_enemy:on_hurt(attack)
  self:get_rekt(attack)
end

function meta_enemy:time_spike()
  time_spike = true
  local d = time_spike_damage()
  self:remove_life(d)
  self:draw_damage({text = tostring(d), font="Peepo", color = {0, 0, 255}, size = 24, timer = 2})
end


meta_enemy:register_event("on_hurt", function(enemy)
  local game = sol.main.get_game()
  if game:get_value("skill__time_spike") == 1 then
    enemy.time_spike_stack = (enemy.time_spike_stack or 0) + 1
    print(enemy.time_spike_stack)
    if enemy.time_spike_stack > 3 then
      enemy.time_spike_stack = 0
      enemy:time_spike()
    end
  end
end)

function meta_enemy:draw_damage(props)
  local x,y,_ = self:get_position()
  local font = props.font or "8_bit"
  local color = props.color or {255, 255, 255}
  local size = props.size or 16
  local text = props.text or "0"
  local timer = props.timer or 4
  table.insert(to_draw, {
      sol.text_surface.create({
          font_size = size,
          font = font,
          text = text,
          color = color,
          horizontal_alignment = "center",
          vertical_alignment = "middle",
      }),
      x,
      y,
      255,
      timer
    })
end

--function meta_enemy:on_hurt_by_sword(hero, attack)
--  local d = damage()
--    self:remove_life(d)
--    self:draw_damage({text = tostring(d)})
--end

local sprite = sol.sprite.create("world_objects/trillium/world_objects")
meta_enemy.confused_on_draw = meta_enemy.on_post_draw
function meta_enemy:on_post_draw(camera)
  if self.confused_on_draw then
    self:confused_on_draw(camera)
  end
  local cx, cy = camera:get_position()
  local sx, sy = self:get_position()

  local compensate = 0
  for i,value in ipairs(to_draw) do
    value[1]:set_opacity(value[4])
    value[4] = value[4] - value[5]
    value[1]:draw(camera:get_surface(), (value[2] or sx) - cx, (value[3] or sy) - cy)
    if value[4] <= 0 then
      table.remove(to_draw, i - compensate)
      compensate = compensate + 1
    end
  end

  local game = sol.main.get_game()
    if damage_taken > 0 then
        local map = self:get_game():get_map()
    end
    if game:get_value("skill__time_spike") == 1 then
      sprite:set_direction(3 - (self.time_spike_stack or 0))
      
      sprite:draw(camera:get_surface(), sx - cx, sy - cy)
    end
    if time_spike then
      camera:shake({count = 4, amplitude = 2, speed = 200})
      time_spike = false
    end
end

local speed = 1
local attack_speed = 1
local damage = 10
local bonus_damage = 1
function meta_enemy:get_speed()
  return speed
end

function meta_enemy:get_attack_speed()
  return attack_speed
end

function meta_enemy:set_speed(s)
  speed = s
end

function meta_enemy:set_attack_speed(s)
  attack_speed = s
end

function meta_enemy:get_adamage()
  return damage*bonus_damage
end

function meta_enemy:set_adamage(d)
  bonus_damage = d
end