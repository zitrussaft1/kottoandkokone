local function deepcopy (orig)
	local orig_type = type(orig)
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do
			copy[deepcopy(orig_key)] = deepcopy(orig_value)
		end
		setmetatable(copy, deepcopy(getmetatable(orig)))
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
	
end
local master_script = {
	-- This is where the script will generate all metatables for the game.
	-- The idea is that you can set the metatable for a specific entity to a specific library.
	-- For example, if you want to set the metatable for the hero to the "trillium" library, you would do:
	-- master_script.set("trillium", "hero")
	-- This would set the metatable for the hero to the "trillium" library's "hero" metatable.
	-- If you want to set the metatable for the hero to the "default" library, you would do:
	-- master_script.set("default", "hero")
	-- This would set the metatable for the hero to the "default" library's "hero" metatable.
	
	-- The code generated looks like this:
	--[[
		camera = {
			current = "default",
			default = {
				camera = deepcopy(sol.main.get_metatable("camera")),
			},
			tables = {
				camera = sol.main.get_metatable("camera"),
			},
		}
	]]

game = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("r")),
		deepcopy(sol.main.get_metatable(" g")),
		deepcopy(sol.main.get_metatable(" b")),
	},
	tables = {
		sol.main.get_metatable("r"),
		sol.main.get_metatable(" g"),
		sol.main.get_metatable(" b"),
	},
},
teletransporter_meta = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("teletransporter")),
	},
	tables = {
		sol.main.get_metatable("teletransporter"),
	},
},
switch = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("switch")),
	},
	tables = {
		sol.main.get_metatable("switch"),
	},
},
sensor = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("sensor")),
	},
	tables = {
		sol.main.get_metatable("sensor"),
	},
},
map = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("map")),
	},
	tables = {
		sol.main.get_metatable("map"),
	},
},
item = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("item")),
	},
	tables = {
		sol.main.get_metatable("item"),
	},
},
hero = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("hero")),
	},
	tables = {
		sol.main.get_metatable("hero"),
	},
},
enemy = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("enemy")),
	},
	tables = {
		sol.main.get_metatable("enemy"),
	},
},
door = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("door")),
	},
	tables = {
		sol.main.get_metatable("door"),
	},
},
custom_entity = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("custom_entity")),
	},
	tables = {
		sol.main.get_metatable("custom_entity"),
	},
},
chest = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("chest")),
	},
	tables = {
		sol.main.get_metatable("chest"),
	},
},
camera = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("camera")),
	},
	tables = {
		sol.main.get_metatable("camera"),
	},
},
bush = {
	current = "default",
	default = {
		deepcopy(sol.main.get_metatable("map")),
	},
	tables = {
		sol.main.get_metatable("map"),
	},
},

}

function master_script:set(library, value)
	if library == "default" then
		for k,v in pairs(self[value]["default"]) do
			setmetatable(
				self[value]["tables"][k],
				{__index = v}
			)
		end
		self[value]["current"] = library
	elseif self[value]["current"] ~= library then
		for k,v in ipairs(require("scripts/meta/" .. library .. "/" .. value)) do
			setmetatable(
				self[value]["tables"][k],
				{__index = v}
			)
		end
		self[value]["current"] = library
	end
end

return master_script