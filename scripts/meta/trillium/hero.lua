local master_script = require("scripts/meta/master")
-- Initialize hero behavior specific to this quest.

require("scripts/trillium/multi_events")

local hero_meta = {}


function hero_meta:on_taking_damage(damage)
  master_script:set("trillium", "hero")
  local hero = self
  local game = self:get_game()

  if damage < 1 then
    damage = 1
  end
  --if this attack would kill you in 1 hit at above a certain percent of max life
  local guts_save_percentage = .4
  local guts_save_cooldown = 60 * 1000
  if damage >= game:get_life()
  and game:get_life() >= game:get_max_life() * guts_save_percentage
  and damage >= game:get_max_life() * (1 - guts_save_percentage)
  and not game.guts_save_used then
    --leave you with 1hp
    damage = game:get_life() - 1
    sol.audio.play_sound"ohko"
    --set this mechanic on a cooldown
    game.guts_save_used = true
    sol.timer.start(game, guts_save_cooldown, function() game.guts_save_used = false end)
  elseif damage >= game:get_max_life() * .5 then
    sol.audio.play_sound"oh_lotsa_damage"
  end

  game:remove_life(damage)

  --Trigger charm event:
  if game.charm_manager then game.charm_manager:trigger_event("on_hurt") end
  hero:start_iframes() --defined below

  --Screenshake
  hero:get_map():get_camera():shake{shake_count = 12, amplitude = 3, zoom_scale = 1.02}
  master_script:set("default", "hero")
end


--[[hero_meta:register_event("on_state_changed", function(self, state)
  local hero = self
  local game = sol.main.get_game()

  if state == "back to solid ground" then
    hero:set_invincible(true,1500)

  --weird bad fire sword
  elseif state == "sword swinging" and game.sword_on_fire then
    local dx = {[0]=16,[1]=0,[2]=-16,[3]=0}
    local dy = {[0]=0,[1]=-16,[2]=0,[3]=16}
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()
    local spread = {math.rad(-50), math.rad(-25), math.rad(25), math.rad(50)}
    for i=1, 4 do
      sol.timer.start(map, 50 * i-1, function()
        local flame = map:create_fire{ x = x+dx[direction], y= y+dy[direction], layer=z}
        local m = sol.movement.create"straight" m:set_speed(100) m:set_max_distance(16)
        m:set_ignore_obstacles()
        m:set_angle( direction*math.pi/2  + spread[i])
        m:start(flame, function() flame:remove() end)
        sol.timer.start(map, 1000, function() flame:remove() end)
      end)
    end

  --Sword Beam
  elseif state == "sword swinging" and game:get_life() == game:get_max_life() and 2==4 --turned off
  and game:get_value"hard_mode" and not hero.sword_beam_cooldown then
    hero.sword_beam_cooldown = true
    sol.timer.start(game,500,function() hero.sword_beam_cooldown = false end)
    local dx = {[0]=16,[1]=0,[2]=-16,[3]=0}
    local dy = {[0]=0,[1]=-16,[2]=0,[3]=16}
    local map = game:get_map()
    local x, y, z = hero:get_position()
    local direction = hero:get_direction()

    sol.audio.play_sound"sword_beam"
    local beam = map:create_custom_entity{
      x=x+dx[direction], y=y+dy[direction], layer=z, direction=direction, width=8, height=8,
      sprite="entities/trillium/sword_beam", model="damaging_sparkle"
    }
    beam:get_sprite():set_animation"head"
    sol.timer.start(beam, 60, function()
      local bx,by,bz=beam:get_position()
      local tail = map:create_custom_entity{x=bx,y=by,layer=bz,direction=direction,width=16,height=16,
      sprite="entities/trillium/sword_beam", model="ephereral_effect"}
      tail:get_sprite():set_animation"tail"
      if beam:exists() then return true end
    end)
    local m = sol.movement.create"straight"
    m:set_speed(250) m:set_angle(direction * math.pi/2) m:set_smooth(false)
    m:start(beam, function() beam:explode() end)
    function m:on_obstacle_reached() beam:explode() end
    function beam:explode()
      local bx,by,bz = beam:get_position()
      local pop = map:create_custom_entity{x=bx,y=by,layer=bz,direction=direction,width=16,height=16,
      sprite="enemies/trillium/enemy_killed_small", model="ephereral_effect"} 
      beam:remove()     
    end

  elseif game.spirit_counter_sword and state == "sword swinging" and hero:is_blinking() then
    local map = game:get_map()
    local x, y, z = hero:get_position()
    sol.audio.play_sound"sword_beam"
    map:create_custom_entity{x=x,y=y,layer=z,width=16,height=16,direction=0,
      model="damaging_sparkle",sprite="entities/trillium/spirit_counter"
    }

  end
end)
]]

local MAX_BUFFER_SIZE = 48
function hero_meta:on_position_changed(x,y,z)
  local hero = self
  if not hero.position_buffer then hero.position_buffer = {} end
  local hero = self
  local dir = hero:get_sprite():get_direction()
  table.insert(hero.position_buffer, 1, {x=x,y=y,layer=l,direction=dir})

  if #hero.position_buffer > MAX_BUFFER_SIZE then
    table.remove(hero.position_buffer)
  end
end


function hero_meta:get_can_be_hurt()
  local hero = self
  local can_be_hurt = not hero:is_invincible()
  local state, state_ob = hero:get_state()
  if state == "custom" then state = state_ob:get_description() end
  if (state == "falling") or (state == "stairs") or (state == "treasure") or (state == "victory")
  or (state == "feather_jumping") then
    can_be_hurt = false
  end
  return can_be_hurt
end


function hero_meta:start_iframes(length)
  local hero = self
  local game = hero:get_game()
  local base_iframe_length = length or 600
  local iframe_length = base_iframe_length + (game:get_value("hurt_iframes_bonus") or 0)
  hero:set_invincible(true, iframe_length)
  hero:set_blinking(true, iframe_length)
end


function hero_meta:ragdoll(direction, distance)
  master_script:set("trillium", "hero")
  local hero = self
  local state = sol.state.create("ragdolling")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_traverse_ground("hole", true)
  state:set_can_traverse_ground("deep_water", true)
  state:set_can_traverse_ground("lava", true)
  state:set_affected_by_ground("hole", false)
  state:set_affected_by_ground("deep_water", false)
  state:set_affected_by_ground("lava", false)
  state:set_gravity_enabled(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation"ragdolling"
  local m = sol.movement.create"straight"
  m:set_speed(200)
  m:set_angle(direction)
  m:set_max_distance(distance)
  m:start(hero, function() hero:start_knock_down() end)
  function m:on_obstacle_reached()
    hero:start_knock_down()
  end
  master_script:set("default", "hero")
end


function hero_meta:start_knock_down(duration)
  local hero = self
  local game = self:get_game()
  duration = duration or 700
  local state = sol.state.create("knocked_down")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(false)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  hero:start_state(state)
  hero:set_animation"dead"
  sol.timer.start(hero, duration, function()
    if not game.playing_knock_down_getup_sound then
      game.playing_knock_down_getup_sound = true
      sol.audio.play_sound"roll_2"
      sol.timer.start(game, 100, function() game.playing_knock_down_getup_sound = false end)
    end
    hero:unfreeze()
  end)
end


function hero_meta:step_forward(distance, speed)
  local hero = self
  local map = self:get_map()
  distance = distance or 8
  speed = speed or 90
  local speed_mod = (distance - 24) * 4
  if distance > 24 then speed = speed + speed_mod end
  local angle = hero:get_direction() * math.pi / 2
  local x, y, z = hero:get_position()
  local is_obstacle = false
  for i = 0, distance do
    dx = math.cos(angle) * i
    dy = math.sin(angle) * i * -1
    local ground = map:get_ground(x + dx, y + dy, z)
    if hero:test_obstacles(dx, dy) or ground == "deep_water" or ground == "shallow_water" or ground == "hole" or ground == "lava" then
      is_obstacle = true
      distance = math.max(0, i - 4)
      break
    end
  end
  if distance < 4 then return end
  local m = sol.movement.create("straight")
  m:set_angle(angle)
  m:set_speed(speed)
  m:set_max_distance(distance)
  m:start(hero)
end


function hero_meta:has_los(entity) 
  local los = true
  local map = self:get_map()
  local x, y, z = self:get_position()
  local dx, dy = 0, 0
  local distance = self:get_distance(entity)
  local angle = self:get_angle(entity)
  for i=0, distance do
    dx, dy = math.floor(math.cos(angle)*i), -math.floor(math.sin(angle)*i)
    if self:test_obstacles(dx, dy) then
        local ground = map:get_ground(x + dx, y + dy, z)
        if not ground == "deep_water" and not ground == "shallow_water" and not ground == "hole" and not ground == "lava" then
          los = false
          break
        end
    end
  end

  return los
end


local hero = hero_meta
return {hero}