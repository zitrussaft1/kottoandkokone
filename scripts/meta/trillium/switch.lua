local master_script = require("scripts/meta/master")
local switch_meta = {}


function switch_meta:on_activated()
  local switch = self
  local map = self:get_map()

  local door_prefix = switch:get_property("door_prefix")
  if door_prefix then
    map:open_doors(door_prefix)
  end

end


function switch_meta:toggle()
  master_script:set("trillium", "switch")
  master_script:set("trillium", "sensor")
  local switch = self
  if not switch:is_activated() then
    sol.audio.play_sound("switch")
    switch:set_activated(true)
    switch:on_activated()
  else
    sol.audio.play_sound("switch")
    switch:set_activated(false)
    if switch.on_inactivated then switch:on_inactivated() end
  end
  master_script:set("default", "sensor")
  master_script:set("default", "switch")
end


local switch = switch_meta
return {switch}