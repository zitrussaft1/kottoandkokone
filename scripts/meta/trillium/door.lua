local door_meta = {}

function door_meta:on_created()
  local door = self
  self:set_drawn_in_y_order(true)
  if door:get_sprite():get_direction() == 3 then
    local width, height = door:get_size()
    local sw, sh = door:get_sprite():get_size()
    door:set_size(sw, height)
    door:set_origin(sw / 2 - 8, height - 16)
  end

  if door:get_property"initial_state" == "open" then
    door:set_open(true)
  end
end
local door = door_meta
return {door}