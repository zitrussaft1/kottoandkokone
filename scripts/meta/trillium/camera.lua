local master_script = require("scripts/meta/master")
-- Provides additional camera features for this quest.

local camera_meta = {}


function camera_meta:shake(config, callback)
	local camera = self
	local camera_surface = camera:get_surface()

	local amplitude = config and config.amplitude or 3
	local speed = config and config.speed or 50
	local zoom_scale = config and config.zoom_scale or 1.05
	local shake_count = config and config.shake_count or 8
	if (shake_count % 2) == 0 then shake_count = shake_count + 1 end

	--zoom
	local cam_wid, cam_hig = camera:get_size()
	camera_surface:set_transformation_origin(cam_wid / 2, cam_hig / 2)
	local dx = {[1] = 1, [0] = zoom_scale}
	local dy = {[1] = 1, [0] = zoom_scale}
	local i = 1
	sol.timer.start(camera, 1, function()
	    camera_surface:set_scale(dx[i % 2], dy[i % 2])
	    if i <= shake_count * 1.5 then
	      i = i + 1
	      return 20
	    else
	      camera_surface:set_scale(1, 1)
	    end
	end)

	--shake
	local j = 1
	local shaking_right = true
	sol.timer.start(camera, 0, function()
		if j <= shake_count then
			local dir_mod = 1
			if not shaking_right then dir_mod = -1 end
			camera_surface:set_xy(amplitude * dir_mod, 0)
			shaking_right = not shaking_right
			j = j + 1
			return 1000 / speed
		else
      camera_surface:set_xy(0,0)
      if callback then callback() end
		end
	end)
end


function camera_meta:scroll_to_hero(hero, speed)
  local camera = self
  local tracking_threshold = 3

  local m = sol.movement.create("target")
  m:set_target(camera:get_position_to_track(hero))
  m:set_speed(speed)
  m:set_ignore_obstacles(true)
  m:start(camera, function()
    camera:start_tracking(hero)
  end)
  local x1, y1 = hero:get_position()
  function m:on_position_changed()
    local x2, y2 = hero:get_position()
    if sol.main.get_distance(x1, y1, x2, y2) > tracking_threshold then
      x1, y1 = hero:get_position()
      m:set_target(camera:get_position_to_track(hero))
    end
  end
end


function camera_meta:scroll_to(target_x, target_y, target_speed) --can do either scroll_to(x, y, speed) or scroll_to(entity, speed)
  master_script:set("trillium", "camera")
  local camera = self
  target_speed = target_speed or 200
  if type(target_x) ~= "number" then
    assert(type(target_x) == "userdata", "What did you pass as argument #1 to camera:scroll_to()?")
    target_speed = target_y or 200
    if target_x:get_type() == "hero" then
      camera:scroll_to_hero(target_x, target_speed)
    else
      camera:scroll_to_entity(target_x, target_speed)
    end

  else
    local m = sol.movement.create("target")
    m:set_target(camera:get_position_to_track(target_x, target_y))
    m:set_speed(target_speed)
    m:set_ignore_obstacles(true)
    m:start(camera)  
  end
  master_script:set("default", "camera")
end


function camera_meta:scroll_to_entity(entity, speed)
  master_script:set("trillium", "camera")
  local camera = self
  local x, y = entity:get_position()
  camera:scroll_to(x, y, speed)
  local m = entity:get_movement()
  sol.timer.start(camera, 10, function()
    if m and m:get_speed() > 0 then
      camera:get_movement():set_target(camera:get_position_to_track(entity:get_position()))
      return true
    end
  end)
  master_script:set("default", "camera")
end

local camera = camera_meta
return {camera}