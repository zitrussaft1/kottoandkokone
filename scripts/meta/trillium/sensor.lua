local sensor_meta = {}

function sensor_meta:on_activated()
  local sensor = self
  local map = self:get_map()

  --Enemy ambushes
  local ambush_enemy_id = self:get_property("ambush_enemy_id")
  if ambush_enemy_id then
    local enemy = map:get_entity(ambush_enemy_id)
    if not enemy then return end
    assert(enemy.ambush_attack, "Error in ambush sensor. Enemy with id: '" .. ambush_enemy_id .. "' does not have enemy:ambush_attack() defined")
    if not enemy.aggro then
      enemy.aggro = true
      sol.timer.stop_all(enemy)
      enemy:ambush_attack()
    end
    sensor:remove()
  end

  --Aggro nearby enemies
  local enemy_aggro_range = self:get_property("nearby_enemy_aggro_range")
  if enemy_aggro_range then
    local x, y, z = sensor:get_position()
    for entity in map:get_entities_in_rectangle(x - enemy_aggro_range, y - enemy_aggro_range, enemy_aggro_range * 2, enemy_aggro_range * 2) do
      if entity:get_type() == "enemy" and entity.start_aggro then
        entity:start_aggro()
      end
    end
  end

end

local sensor = sensor_meta
return {sensor}