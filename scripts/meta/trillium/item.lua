local master_script = require("scripts/meta/master")
local item_meta = {}

--Get/Set Max Amount - honestly these should be in the API but that's fine
function item_meta:get_max_amount()
  local game = self:get_game()
  local save_name = self:get_name():gsub("/", "_")
  return game:get_value(save_name .. "_max_amount")
end

function item_meta:set_max_amount(new_amount)
  local game = self:get_game()
  local save_name = self:get_name():gsub("/", "_")
  game:set_value(save_name .. "_max_amount", new_amount)
end


function item_meta:setup_amount_stuff(default_max_amount)
  master_script:set("trillium", "item")
  --Uses item name to set savegame variable, amount variable, and max amount
  local item = self
  local game = item:get_game()

  default_max_amount = default_max_amount or 1
  local save_name = item:get_name():gsub("/", "_")
  item:set_savegame_variable("possession_" .. save_name)
  item:set_amount_savegame_variable("amount_" .. save_name)

  local max_amount = item:get_max_amount()
  if not max_amount then
    max_amount = default_max_amount
  end
  item:set_max_amount(max_amount)
  item:set_assignable(true)
  item:set_ammo("_amount")
  master_script:set("default", "item")
end


function item_meta:set_fill_on_checkpoint(refill_bool)
  if refill_bool == nil then refill_bool = true end
  local item = self
  local game = self:get_game()
  if not game.fill_on_checkpoint_items then game.fill_on_checkpoint_items = {} end
  game.fill_on_checkpoint_items[item:get_name()] = (refill_bool and item or nil)
end


--item_meta:register_event("on_created", function(item)
--  item:set_brandish_when_picked(false)
--end)


--Show item popup modal HUD element:
local popup_menu = require"scripts/trillium/menus/components/item_popup"
function item_meta:show_popup(variant)
  local item = self
  popup_menu:set_item(item:get_name(), variant)
  sol.menu.start(sol.main.get_game(), popup_menu)
end

local item = item_meta
return {item}