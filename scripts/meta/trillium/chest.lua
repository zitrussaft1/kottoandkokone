local chest_meta = {}

function chest_meta:on_created()
  local chest = self
  local game = self:get_game()
  treasure, variant, savegame_var = chest:get_treasure()
  if not savegame_var then
    local x, y, z = self:get_position()
    local map_id = self:get_map():get_id():gsub("/", "_")
    savegame_var = map_id .. "_chest_" .. treasure:gsub("/", "_") .. "_" .. x .. "_" .. y .. "_" .. z
    if game:get_value(savegame_var) then --already opened chest
      chest:set_open(true)
    else
      chest:set_treasure(treasure, variant, savegame_var)
    end
  end
end

local chest = chest_meta
return {chest}