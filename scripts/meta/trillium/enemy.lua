local master_script = require("scripts/meta/master")
local enemy_meta = {}

--[[
enemy_meta:register_event("on_hurt", function(self, attack)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local enemy = self
  local game = enemy:get_game()
  if attack == "explosion" then
    enemy:remove_life(game:get_value("explosion_damage") or 10)
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)
]]


function enemy_meta:immobilize(duration)
  local enemy = self
  local sprite = enemy:get_sprite()
  local map = enemy:get_map()

  sol.timer.stop_all(enemy)
  enemy:stop_movement()
  if sprite:has_animation"immobilized" then
    sprite:set_animation"immobilized"
  elseif sprite:has_animation"stopped" then
    sprite:set_animation"stopped"
  elseif sprite:has_animation"walking" then
    sprite:set_animation"walking"
  end

  if enemy.on_immobilized then enemy:on_immobilized() end

  sol.timer.start(map, duration or 3000, function()
    enemy:restart()
  end)
end


function enemy_meta:is_on_screen()
  local enemy = self
  local map = enemy:get_map()
  local camera = map:get_camera()
  local camx, camy = camera:get_position()
  local camwi, camhi = camera:get_size()
  local enemyx, enemyy = enemy:get_position()

  local on_screen = enemyx >= camx and enemyx <= (camx + camwi) and enemyy >= camy and enemyy <= (camy + camhi)
  return on_screen
end


function enemy_meta:set_has_shadow(has_shadow, size)
  local enemy = self
  if not has_shadow then has_shadow = true end
  if not size then size = "medium" end
  assert(type(has_shadow) == "boolean", "enemy:set_has_shadow() - argument 1 must be a boolean" )
  if has_shadow then
    local sprite = enemy:create_sprite("shadowstrillium//shadow_" .. size, "shadow")
    sprite:set_direction(0)
    enemy:bring_sprite_to_back(sprite)
  else
    enemy:remove_sprite(enemy:get_sprite"shadow")
  end
end

--[[
enemy_meta:register_event("on_dead", function(self)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  local enemy = self
  local game = enemy:get_game()
  --Trigger charm event
  game.charm_manager:trigger_event("on_enemy_killed", enemy)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)
]]
local enemy = enemy_meta
return {enemy}