local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:spin(seconds, callback, args)
    local spinning = 0
    local timer = 0
    sol.timer.start(TIME(100) * 1/(self:get_speed() or 1), function()
        self:get_sprite():set_direction(spinning)
        spinning = spinning + 1
        if spinning == 4 then
            spinning = 0
        end
        timer = timer + 100
        return timer < seconds * 1000
    end)
    callback(args)
end