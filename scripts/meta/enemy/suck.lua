local meta_enemy = sol.main.get_metatable("enemy")

local streams = {}
function meta_enemy:turn_stream_towards(size, strength, time, callback, args)
    local x, y, _ = self:get_position()

    local direction = self:get_sprite():get_direction()
    local x1, y1, x2, y2

    self:set_can_attack(true)
    self:set_damage(
        self:get_adamage()
    )
    if direction == 0 then
        x1 = x
        y1 = y - size
        x2 = size
        y2 = 2*size
    elseif direction == 1 then
        x1 = x - size
        y1 = y - size
        x2 = 2*size
        y2 = size
    elseif direction == 2 then
        x1 = x - size
        y1 = y - size
        x2 = size
        y2 = 2*size
    elseif direction == 3 then
        x1 = x - size
        y1 = y
        x2 = 2*size
        y2 = size
    end
    for stream in self:get_map():get_entities_in_rectangle(x1, y1, x2, y2) do
        if stream:get_type() == "stream" then
            local angle = stream:get_angle(self)
            local direction = math.floor((angle + 0.125) / (2 * math.pi / 8)) % 8
            stream:set_direction(direction)
            stream:set_visible(true)
            streams[#streams + 1] = stream
            local s = 1
            sol.timer.start(stream, TIME(100), function()
                s = s + strength/10
                print(s, strength, s < strength)
                stream:set_speed(SPEED(s))
                return s < strength
            end)
        end
    end

    sol.timer.start(self, TIME(time), function()
        for _, stream in ipairs(streams) do
            stream:set_speed(1)
            stream:set_visible(false)
            self:set_can_attack(false)
            self:set_damage(0)
        end
        callback(args)
    end)
end