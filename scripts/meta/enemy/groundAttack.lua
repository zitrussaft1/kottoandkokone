local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:ground_attack(radius_initial, radius_final, is_semi_circle, damage, callback, args)
  local hero = self:get_map():get_hero()
  local direction = self:get_sprite():get_direction()
  if radius_initial == nil then
    radius_initial = 0
  end
  if radius_final == nil then
    radius_final = 16
  end
  if is_semi_circle == nil then
    is_semi_circle = false
  end
  if damage == nil then
    damage = 1
  end
  -- Test if the hero is in range in either semi-circle or full circle
  local function is_in_range()
    local hero_x, hero_y, _ = hero:get_position()
    local distance = self:get_distance(hero_x, hero_y)
    local x, y, _ = self:get_position()
    if is_semi_circle then
      if direction == 0 then
        return distance < radius_final and hero_x > x
      elseif direction == 1 then
        return distance < radius_final and hero_y < y
      elseif direction == 2 then
        return distance < radius_final and hero_x < x
      elseif direction == 3 then
        return distance < radius_final and hero_y > y
      end
    else
      return distance < radius_final
    end
  end

  if is_in_range() and not hero:is_blinking() then
    if is_semi_circle and hero:blocks(self) then
    else
      hero:start_hurt(self, damage *self:get_adamage())
    end
  end

  callback(args)
end

function meta_enemy:ground_attack_draw(radius_initial, radius_final, is_semi_circle, time, wait, callback, args)
  local hero = self:get_map():get_hero()
  local radius = radius_initial or 16 -- Set default initial radius to 16 if not provided

  local x, y, _ = self:get_position()
  local e, e2

  if is_semi_circle then
    e = self:get_map():create_custom_entity({
      name = "temp",
      direction = 0,
      layer = 0,
      x = x,
      y = y,
      width = radius,
      height = radius,
      sprite = "ground_effects/red_outline_half_circle",
    })
    e:get_sprite():set_rotation((self:get_sprite():get_direction() - 1) * math.pi / 2)
    e2 = self:get_map():create_custom_entity({
      name = "temp",
      direction = 0,
      layer = 0,
      x = x,
      y = y,
      width = radius,
      height = radius,
      sprite = "ground_effects/red_half_circle",
    })
    e2:get_sprite():set_rotation((self:get_sprite():get_direction() - 1) * math.pi / 2)
  else
    e = self:get_map():create_custom_entity({
      direction = 0,
      layer = 0,
      x = x,
      y = y,
      width = radius,
      height = radius,
      sprite = "ground_effects/red_outline",
    })
    e2 = self:get_map():create_custom_entity({
      direction = 0,
      layer = 0,
      x = x,
      y = y,
      width = radius,
      height = radius,
      sprite = "ground_effects/red_circle",
    })
  end

  e:get_sprite():set_blend_mode("add")
  e:get_sprite():set_opacity(100)
  e:get_sprite():set_scale(radius_final * 2 / 64, radius_final * 2 / 64)

  e2:get_sprite():set_blend_mode("add")
  e2:get_sprite():set_opacity(100)
  e2:get_sprite():set_scale(0.1, 0.1)

  local i = 0
  local max_scale = radius_final * 2 / 64 -- Use radius_final instead of initial radius for scaling
  local function draw()
    i = i + 1
    e2:get_sprite():set_scale(i * max_scale / (time * 10), i * max_scale / (time * 10))
    if i < time * 10 then
      sol.timer.start(self,TIME(100), draw)
    else
      if wait == nil then
        wait = 0
      end
      sol.timer.start(self,TIME(wait * 1000), function()
        e:remove()
        e2:remove()
        callback(args)
      end)
    end
  end
  draw()
end

function meta_enemy:wrap_ground_attack(radius_initial, radius_final, is_semi_circle, damage, time, wait, callback, args)
  self:ground_attack_draw(radius_initial, radius_final, is_semi_circle, time, wait, function ()
    if is_semi_circle then
      sol.audio.play_sound("enemy_awake")
    else
      sol.audio.play_sound("stone")
    end
    self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function()
      callback(args)
    end)
  end)
end