local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:throw_boomerang(callback, args)
    local x,y = self:get_position()
    local thrower = self
    local hero = self:get_map():get_hero()

    -- Create a boomerang entity
    local boomerang = self:get_map():create_custom_entity({
      name="bullet_boomerang",
      direction = 0,
      layer = 0,
      x = x,
      y = y,
      width = 16,
      height = 16,
      sprite = "entities/trillium/boomerang1",
    })
    boomerang:set_can_traverse(true)

    -- Set the boomerang's movement
    local movement = sol.movement.create("straight")
    movement:set_speed(SPEED(150))
    movement:set_angle(self:get_angle(hero))
    movement:set_max_distance(300)
    movement:set_ignore_obstacles(true)

    function movement:on_finished()
        local follow_self = sol.movement.create("target")
        follow_self:set_speed(SPEED(100))
        follow_self:set_target(thrower)
        follow_self:set_ignore_obstacles(true)

        function follow_self:on_finished()
            boomerang:remove()
        end
        
        function follow_self:on_obstacle_reached()
            follow_self:stop()
            follow_self:on_finished()
        end

        if boomerang:exists() then
            sol.timer.start(boomerang,TIME(100), function()
                if boomerang:exists() then
                    follow_self:start(boomerang)
                end 
            end)
        end
    end

    function movement:on_position_changed()
        local map_x, map_y = thrower:get_map():get_size()
        local x,y = boomerang:get_position()
        
        if boomerang:get_position() == nil then
            return
        end

        if x < 16 or x > map_x - 16 or
            y < 16 or y > map_y - 16 then
            movement:stop()
            movement:on_finished()
        end
    end

    local entities_touched = {}
    entities_touched[self] = true
    boomerang:add_collision_test("touching", function(boom, other)
        if entities_touched[other] then
            return
        end
        entities_touched[other] = true
        if other:get_type() == "enemy" then--and 
            --(not other:get_name() or other:get_name():sub(1, 5) ~= "spike") then
            other:hurt(1)
        elseif other:get_type() == "switch" and not other:is_walkable() then
            other:toggle()
        elseif other:get_type() == "hero" then
            if not other:blocks(boom) then
                -- TODO: Pass damage as argument
                other:start_hurt(boom, 1*self:get_adamage())
            end
        elseif other:get_type() == "custom_entity" then
            return
        end
        sol.timer.start(TIME(500), function()
            movement:stop()
            movement:on_finished()
        end)
    end)    

    movement:start(boomerang)
            
    self:wait(1, callback, args)
end