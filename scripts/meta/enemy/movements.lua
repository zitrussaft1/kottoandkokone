local meta_enemy = sol.main.get_metatable("enemy")

-- Skills
function meta_enemy:look_at_player(seconds, callback, args)
  local hero = self:get_map():get_hero()
  local i = 0;
  local function look_at_hero()
    self:stop_movement()

    self:get_sprite():set_direction(
      self:get_direction4_to(hero)
    )
    i = i + 1
    if i < seconds * 10 then
      sol.timer.start(self,TIME(100), look_at_hero)
    else
        callback(args)
    end
  end

  look_at_hero()
end

function meta_enemy:wait(seconds, callback, args)
  local i = 0;
  local function wait()
    i = i + 1
    if i < seconds * 10 then
      sol.timer.start(self,TIME(100), wait)
    else
      callback(args)
    end
  end

  wait()
end

function meta_enemy:follow_hero(seconds, callback, args)
  local hero = self:get_map():get_hero()
  local enemy = self

  local movement = sol.movement.create("target")
  movement:set_speed(SPEED(80))
  movement:set_target(hero)
  movement:get_ignore_obstacles(false) 
  function movement:on_position_changed()
    if enemy:get_distance(hero) < 40 then
      movement:stop()
    end
  end
  movement:start(self)
  if enemy:get_distance(hero) < 40 then
    movement:stop()
  end
  -- update sprite direction
  local i = 0
  local function look_at_hero()
    local hero_x, hero_y, _ = hero:get_position()
    local x, y, _ = self:get_position()
    local angle = self:get_angle(hero)
    self:get_sprite():set_direction(
      self:get_direction4_to(hero)
    )
    i = i + 1
    if i < 100*seconds then
      sol.timer.start(self,TIME(10), look_at_hero)
    end
  end
  look_at_hero()


  sol.timer.start(self,TIME(seconds*1000), function()
    movement:stop()
    callback(args)
  end)
end