local meta_enemy = sol.main.get_metatable("enemy")

local function detectCollision(wallX, wallY, wallWidth, wallHeight, objectX, objectY, objectAngle)
    local leftDistance = objectX - wallX
    local rightDistance = (wallX + wallWidth) - objectX
    local topDistance = objectY - wallY
    local bottomDistance = (wallY + wallHeight) - objectY

    local minDistance = math.min(leftDistance, rightDistance, topDistance, bottomDistance)

    local collisionSide
    if minDistance == leftDistance then
        collisionSide = "left"
    elseif minDistance == rightDistance then
        collisionSide = "right"
    elseif minDistance == topDistance then
        collisionSide = "top"
    elseif minDistance == bottomDistance then
        collisionSide = "bottom"
    end

    return collisionSide
end

local function calculateReflectionAngle(collisionSide, objectAngle)
    local angle

    if collisionSide == "left" or collisionSide == "right" then
        angle = math.pi - objectAngle
    elseif collisionSide == "top" or collisionSide == "bottom" then
        angle = -objectAngle
    end

    return angle
end

function meta_enemy:shoot(angle, num_bounces, bullet_size, callback, args, speed)
    local map = self:get_map()
    local game = map:get_game()
    local hero = map:get_hero()

    local angle = angle or self:get_angle(hero)
    local num_bounces = num_bounces or 0
    local bullet_size = bullet_size or 8

    local x, y, l = self:get_position()
    local bullet = map:create_custom_entity({
        name="bullet",
        x = x,
        y = y,
        layer = l,
        width = 8,
        height = 8,
        direction = 0,
        sprite = "ground_effects/red_circle",
    })

    local sprite = bullet:get_sprite()
    sprite:set_scale(bullet_size / 64, bullet_size / 64)

    local movement = sol.movement.create("straight")
    movement:set_speed(SPEED((speed or 100) * (self:get_attack_speed() or 1)))
    movement:set_angle(angle)
    movement:set_max_distance(500)
    movement:set_ignore_obstacles(true)
    movement:set_smooth(false)

    local bounce_counter = 0
    function movement:on_wall_reached(wall)
        bounce_counter = bounce_counter + 1
        if bounce_counter > num_bounces then
            bullet:remove()
        else
            -- Calculate the angle of reflection
            local wallX, wallY, wallWidth, wallHeight = map:get_entity(wall):get_bounding_box()
            local objectX, objectY = bullet:get_position()
            local objectAngle = movement:get_angle()
            local collisionSide = detectCollision(wallX, wallY, wallWidth, wallHeight, objectX, objectY, objectAngle)
            local reflectionAngle = calculateReflectionAngle(collisionSide, objectAngle)
            movement:set_angle(reflectionAngle)
        end
    end

    bullet:add_collision_test("overlapping", function(bullet, other)
        if other:get_type() == "hero" then
            if not hero:is_blinking() and not hero:blocks(bullet) then
                -- TODO: Make the damage an argument
                hero:start_hurt(bullet, 1*self:get_adamage())
            end
            bullet:remove()
        elseif other:get_type() == "custom_entity" then
            local entity = other:get_name()
            if entity and entity:find("wall") then
                movement:on_wall_reached(entity)
            end
        end
    end)

    function movement:on_finished()
        bullet:remove()
    end

    movement:start(bullet)
    callback(args)
end