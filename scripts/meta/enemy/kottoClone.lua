local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:summon_kotto(pos_x, pos_y)
    local map = self:get_map()
    local game = map:get_game()
    local hero = map:get_hero()

    local x, y, l = self:get_position()
    pos_x = pos_x or x
    pos_y = pos_y or y

    local kotto = map:create_enemy({
        name="kotto",
        x = pos_x,
        y = pos_y,
        layer = l,
        width = 16,
        height = 16,
        direction = 0,
        enabled_at_start = true,
        breed = "chameleon",
    })
    kotto:set_damage(
        self:get_adamage()
    )

end