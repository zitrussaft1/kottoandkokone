local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:get_random_object_postions(n)
  local positions = {}
  local map_width, map_height = self:get_map():get_size()
  local x, y = math.random(0, map_width), math.random(0, map_height)
  for i = 1, n do
    x = math.random(0, map_width - 0)
    y = math.random(0, map_height - 0)
    while self:get_distance(x, y) < 64 do
      x = math.random(0, map_width - 0)
      y = math.random(0, map_height - 0)
    end
    positions[#positions + 1] = {x, y}
  end
  return positions
end

function meta_enemy:get_random_object_sizes(n, size, variance)
  local sizes = {}
  for i = 1, n do
    sizes[#sizes + 1] = size + math.random(-variance, variance)
  end
  return sizes
end


function meta_enemy:draw_rain_objects(object_positions, diameters, time, callback, args)
  -- Create an animation with outline and scale similar to ground_attack_draw but for multiple objects.
  local i = 0
  local max_i = #object_positions
  local collect_entities = {}
  local collect_entities2 = {}

  for i = 1, max_i do
    local e = self:get_map():create_custom_entity({
      name ="temp",
      direction = 0,
      layer = 0,
      x = object_positions[i][1],
      y = object_positions[i][2],
      width = 16,
      height = 16,
      sprite = "ground_effects/red_outline",
    })
    e:get_sprite():set_blend_mode("add")
    e:get_sprite():set_opacity(100)
    e:get_sprite():set_scale(diameters[i]/64, diameters[i]/64)
    
    local e2 = self:get_map():create_custom_entity({
      name = "temp",
      direction = 0,
      layer = 0,
      x = object_positions[i][1],
      y = object_positions[i][2],
      width = 16,
      height = 16,
      sprite = "ground_effects/red_circle",
    })
    e2:get_sprite():set_blend_mode("add")
    e2:get_sprite():set_opacity(100)
    e2:get_sprite():set_rotation(0)
    e2:get_sprite():set_scale(0.1, 0.1)

    collect_entities[#collect_entities + 1] = e
    collect_entities2[#collect_entities2 + 1] = e2
  end

  local j = 1
  local function charge_up()
    for i = 1, max_i do
      collect_entities2[i]:get_sprite():set_scale(j*diameters[i]/(time*10)/64, j*diameters[i]/(time*10)/64)
    end

    if j < time*10 then
      sol.timer.start(self,TIME(100), charge_up)
    else
      for i = 1, #collect_entities do
        collect_entities[i]:remove()
      end
      for i = 1, #collect_entities2 do
        collect_entities2[i]:remove()
      end
      callback(args)
    end
    j = j + 1
  end

  charge_up()
end

function meta_enemy:rain_objects(object_positions, diameters, damage, callback, args)
  local hero = self:get_map():get_hero()
  local i = 1
  local max_i = #object_positions

  --DEBUG_BITE = object_positions
  for i = 1, max_i do

    -- DEBUG: Add coordinates of a circle to DEBUG_BITE
    --[[for j = 0, 360, 30 do
      local x = object_positions[i][1] + math.cos(j) * (diameters[i]/2)
      local y = object_positions[i][2] - math.sin(j) * (diameters[i]/2)
      DEBUG_BITE[#DEBUG_BITE + 1] = {x, y}
    end]]--

    if hero:get_distance(object_positions[i][1], object_positions[i][2]) < (diameters[i]/2) then
      hero:start_hurt(damage *self:get_adamage())
      callback(args)
      return
    end
  end

  callback(args)
end

function meta_enemy:wrap_rain_objects(number_of_objects, avg_size, variance, damage, time, callback, args)
  local object_positions = self:get_random_object_postions(number_of_objects)
  local diameters = self:get_random_object_sizes(number_of_objects, avg_size, variance)
  self:get_map():get_camera():shake({count = 4, amplitude = 2, speed = 200})
  self:draw_rain_objects(object_positions, diameters, time, function()
    sol.audio.play_sound("stone")
    self:rain_objects(object_positions, diameters, damage, function()
      callback(args)
    end)
  end)
end