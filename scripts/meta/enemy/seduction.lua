local meta_enemy = sol.main.get_metatable("enemy")

require("scripts/meta/enemy/groundAttack")
require("scripts/meta/enemy/movements")

local timer
function meta_enemy:seduction(callback,args)
    local hero = self:get_map():get_hero()
    local game = self:get_game()
    local enemy = self

    if self:get_distance(hero) < 32 then
        callback(args)
        return
    end

    -- Tint purple
    game:set_tint_map({140, 0, 140})

    local to_kotto = sol.movement.create("target")
    to_kotto:set_target(hero)
    to_kotto:set_speed(SPEED(16))
        
    local from_kotto = sol.movement.create("target")
    from_kotto:set_target(self)
    from_kotto:set_speed(SPEED(32))

    function to_kotto:on_finished()
        game:reset_tint_map()
        if enemy:get_distance(hero) > 48 then
            enemy:wait(1, function()
                callback(args)
            end)
        else
            callback(args)
        end
    end

    to_kotto:start(self)
    from_kotto:start(hero)

    sol.menu.start(sol.main.get_game(),
        require("scripts/menus/pressa"))
    
    game:set_value("can_dash", false)

    timer = sol.timer.start(self,TIME(4000), function()
        timer = nil
        sol.menu.stop(
            require("scripts/menus/pressa")
        )
        from_kotto:stop()
        hero:unfreeze()
        game:set_value("can_dash", true)
        to_kotto:stop()
        to_kotto:on_finished()
    end)

    sol.timer.start(self,TIME(320), function()
        enemy:wrap_ground_attack(0, 64, false, 1, 0.3, 0.1, function() 
        end)
        return timer ~= nil
    end)
end