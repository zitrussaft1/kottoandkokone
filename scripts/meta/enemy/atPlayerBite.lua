local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:bite_draw(angle,callback,args)
  local map = self:get_map()
  local hero = map:get_hero()
  if angle == nil then
    angle = self:get_angle(hero)
  end

  -- Draw a red outline of the bite
  local x, y, _ = self:get_position()
  local e = map:create_custom_entity({
    direction = 0,
    layer = 0,
    x = x + math.cos(angle) * 16,
    y = y - math.sin(angle) * 16,
    width = 72,
    height = 16,
    sprite = "ground_effects/red_outline_square",
  })
  e:get_sprite():set_animation("left")
  e:get_sprite():set_blend_mode("add")
  e:get_sprite():set_opacity(100)
  e:get_sprite():set_scale(4, 1)
  e:get_sprite():set_rotation(angle)

  -- Over time scale an inside rectangle
  local e2 = map:create_custom_entity({
    direction = 0,
    layer = 0,
    x = x + math.cos(angle) * 16,
    y = y - math.sin(angle) * 16,
    width = 72,
    height = 16,
    sprite = "ground_effects/red_square",
  })
  e2:get_sprite():set_animation("left")
  e2:get_sprite():set_blend_mode("add")
  e2:get_sprite():set_opacity(100)
  e2:get_sprite():set_rotation(angle)

  local i = 0
  local function draw()
    i = i + 1
    e2:get_sprite():set_scale(4, i/10)
    if i < 10 then
      sol.timer.start(self,TIME(100), draw)
    else
      e:remove()
      e2:remove()
      callback(angle, args)
    end
  end
  draw()  
end

function meta_enemy:bite(angle, damage, callback, args)
  local map = self:get_map()
  local hero = map:get_hero()
  self:stop_movement()
  if angle == nil then
    angle = self:get_angle(hero)
  end
  local attack_width = 72
  local attack_height = 16

  local width = 32
  local height = 32

  local x, y, _ = self:get_position()
  local hero_x, hero_y, _ = hero:get_position()

  local dangle = math.atan((attack_height/2)/(height/2))
  local polygon_bite = {
    {x + math.cos(angle + dangle) * attack_height/2, y - math.sin(angle + dangle) * attack_height/2},
    {x + math.cos(angle + dangle) * attack_height/2 + math.cos(angle) * attack_width, y - math.sin(angle + dangle) * attack_height/2 - math.sin(angle) * attack_width},
    {x + math.cos(angle - dangle) * attack_height/2 + math.cos(angle) * attack_width, y - math.sin(angle - dangle) * attack_height/2 - math.sin(angle) * attack_width},
    {x + math.cos(angle - dangle) * attack_height/2, y - math.sin(angle - dangle) * attack_height/2},
  }
  DEBUG_BITE = polygon_bite

  if point_in_polygon(hero_x, hero_y, polygon_bite, 0) then
    hero:start_hurt(self, damage*self:get_adamage())
    callback(true, args)
  else
    callback(false, args)
  end

end

function meta_enemy:wrap_bite(angle, damage, callback, args)
  self:bite_draw(angle, function (new_angle, _)
    self:bite(new_angle, damage, function (hit, _)
      callback(hit, args)
    end)
  end)
end