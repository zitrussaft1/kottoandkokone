local meta_enemy = sol.main.get_metatable("enemy")
local setup_confused = false

function meta_enemy:confuse(seconds, can_stop_earlier)
    if can_stop_earlier == nil then
        can_stop_earlier = false
    end
    
    self:set_property("confused", "true")

    -- After the time, the enemy will be back to normal
    local timer = sol.timer.start(self, seconds * 1000, function()
        self:set_property("confused", false)
        self:decide_action()
    end)

    self:register_event("on_hurt", function(self, attacker, life_lost)
        if self:get_property("confused") == "true" and can_stop_earlier == true  and
            self:get_property("invincible") ~= "true" then
            timer:stop()
            self:set_property("confused", "false")
            self:decide_action()
        end
    end)
    self:confused_action()
end

function meta_enemy:confused_action()
    -- Stop all movements
    if self:get_movement() ~= nil then
        self:get_movement():stop()
    end
end

local confused_sprite = sol.sprite.create("elements/stars")
function meta_enemy:on_post_draw(camera)
    if self:get_property("confused") == "true" then
        local x, y, l = self:get_position()
        local cx, cy = camera:get_position()
        local w, h = self:get_size()
        confused_sprite:set_scale(w/48, w/48)
        confused_sprite:draw(camera:get_surface(), x - cx, y - cy - h)
    end
end