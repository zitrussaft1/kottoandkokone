local meta_enemy = sol.main.get_metatable("enemy")

function meta_enemy:invisible(time, callback, args)
    local sprite = self:get_sprite()
    local opacity = 255

    sol.timer.start(self, TIME(100), function()
        opacity = opacity - (255 / (time * 10))
        sprite:set_opacity(opacity)
        if opacity <= 0 then
            callback(args)
            return false
        end
        return opacity > 0
    end)
end

function meta_enemy:visible(time, callback, args)
    local sprite = self:get_sprite()
    local opacity = 0

    sol.timer.start(self, TIME(100), function()
        opacity = opacity + (255 / (time * 10))
        sprite:set_opacity(opacity)
        if opacity >= 255 then
            callback(args)
            return false
        end
        return opacity < 255
    end)
end