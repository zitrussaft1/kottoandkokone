local meta_enemy = sol.main.get_metatable("enemy")

local function destroy_wall(map, column)
  local name = column:get_name()
  if name == nil or not string.find(name, "col") then
    return
  end
  for entities in map:get_entities(column:get_name()) do
    entities:remove()
  end
  column:remove()
end

function meta_enemy:wall_rush(angle, callback, args)
    local map = self:get_map()
    local hero = map:get_hero()
    local map_width, map_height = self:get_map():get_size()

    local x, y, _ = self:get_position()
    local mov = sol.movement.create("target")
    mov:set_speed(SPEED(200 * (self:get_speed() or 1)))

    local obstacles = {}
    local map = self:get_map()
    
    for tiles in map:get_entities_by_type("custom_entity") do
      -- All custom entities have a wall key
      -- create a polygon with values x,y,width,height from the entities with the same wall value
      if tiles:get_property("wall") then
        obstacles[#obstacles + 1] = tiles
      end
    end
    
    if angle == nil then
        angle = self:get_angle(hero)
    end

    local a, possible_column = move_character({x, y}, angle, 16, obstacles, 8, self:get_map())
    mov:set_target(a[1], a[2])
    function mov:on_finished()
      destroy_wall(map, possible_column)
      map:get_camera():shake()
      callback(args)
    end
    mov:start(self)
end

function meta_enemy:wall_rush_draw(angle_og, wait, charge_time, callback, args)
  local hero = self:get_map():get_hero()
  local map_width, map_height = self:get_map():get_size()

  local hero_x, hero_y, _ = hero:get_position()
  local x, y, _ = self:get_position()

  local function get_angle()
    local hero_x, hero_y, _ = hero:get_position()
    local size_x, size_y = self:get_size()
    if hero_x < size_x then
      hero_x = size_x
    elseif hero_x > map_width - size_x then
      hero_x = map_width - size_x
    end
    if hero_y < size_y then
      hero_y = size_y
    elseif hero_y > map_height - size_y then
      hero_y = map_height - 3*size_y/4
    end
    return self:get_angle(hero_x, hero_y)
  end

  local angle
  if angle_og == nil then
   angle = get_angle()
  else
    angle = angle_og
  end

  local obstacles = {}
  local map = self:get_map()
  for tiles in map:get_entities_by_type("custom_entity") do
    -- All custom entities have a wall key
    -- create a polygon with values x,y,width,height from the entities with the same wall value
    if tiles:get_property("wall") then
      obstacles[#obstacles + 1] = tiles
    end
  end

  local function get_positions(angle)
    return step_move_character({x, y}, angle, 16, obstacles, 8, 32, self:get_map())
  end

  local positions_until_wall, collect_entities = get_positions(angle)
  local max_i = #positions_until_wall
  local i = 1
  local x, y = 0, 0

  local w, h  = self:get_size()

  -- Update entities positions
  local function update_positions(s, angle)
    for _,e in ipairs(collect_entities) do
      e:remove()
    end
    positions_until_wall, collect_entities = get_positions(angle)
    for _,e in ipairs(collect_entities) do
      local spri = e:create_sprite("ground_effects/red_square")
      spri:set_rotation(angle)
      spri:set_scale(1, s)
      spri:set_blend_mode("add")
    end
  end

  -- For 3 seconds, update the entities positions
  local s = 4/w
  local function update()
    update_positions(s, angle)
    i = i + 1
    if i < wait*10  then
      sol.timer.start(self,TIME(100 * 1/(self:get_speed() or 1)), function ()
        if angle_og ~= nil then
          angle = angle_og
        else
          angle = get_angle()
        end
        update()
      end)
    elseif i < wait*10 + charge_time*10 then
      -- Scale up until entity width
      s = s + 1.5 / (charge_time*10)
      sol.timer.start(self,TIME(100), function ()
        update()
      end)
    else
        for i = 1, #collect_entities do
          collect_entities[i]:remove()
        end
        callback(angle, args)
    end
  end

  -- Draw a short line
  local function draw_path()
    x, y = positions_until_wall[i][1], positions_until_wall[i][2]

    local e = collect_entities[i]
    if e then
      e:create_sprite("ground_effects/red_square")
      e:get_sprite():set_blend_mode("add")
      e:get_sprite():set_opacity(100)
      e:get_sprite():set_rotation(angle)
      e:get_sprite():set_scale(1, 4/h)
    end

    i = i + 1
    if i < #positions_until_wall then
      sol.timer.start(self,TIME(1), draw_path)
    else
      i = 1
      update()
    end
  end

  draw_path()
end

function meta_enemy:wrap_dash_wall(angle, wait, charge_time, callback, args)
  sol.audio.play_sound("walk_on_grass")
  self:wall_rush_draw(angle, wait, charge_time, function (angle, _)
    sol.audio.play_sound("walk_on_grass")
    self:wall_rush(angle, function ()
      callback(args)
    end)
  end)
end
