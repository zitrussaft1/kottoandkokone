function point_in_polygon(x, y, polygon, char_size)
  local x1, y1, x2, y2

  local poly = {}

  for _, v in ipairs(polygon) do
      poly[#poly + 1] = v[1]
      poly[#poly + 1] = v[2]
  end

  local len = #poly
  x2, y2 = poly[len - 1], poly[len]
  local wn = 0
  for idx = 1, len, 2 do
    x1, y1 = x2, y2
    x2, y2 = poly[idx], poly[idx + 1]

    if y1 > y then
      if (y2 <= y) and (x1 - x) * (y2 - y) < (x2 - x) * (y1 - y) then
        wn = wn + 1
      end
    else
      if (y2 > y) and (x1 - x) * (y2 - y) > (x2 - x) * (y1 - y) then
        wn = wn - 1
      end
    end
  end
  return wn % 2 ~= 0 -- even/odd rule
end

-- Function to check if a point (x, y) collides with any of the shapes
local function check_collision(x, y, shapes, char_size)
    if shapes == nil then
        return false
    end

    for _, shape in ipairs(shapes) do
        if point_in_polygon(x, y, shape, char_size) then
            return true
        end
    end

    return false
end

function step_move_character(position, angle, char_size, obstacles, speed, size, map)
    if speed == nil then
        speed = 5
    end
    -- Canvas size
    local canvas_width = 320
    local canvas_height = 240

    local steps = {position}

    -- Convert angle from degrees to radians
    local angle_rad = angle

    -- Calculate the change in x and y positions based on angle
    if math.abs(angle_rad - math.pi/2) < 0.1 or math.abs(angle_rad - 3*math.pi/2) < 0.1 
        or math.abs(angle_rad - 5*math.pi/2) < 0.1 or math.abs(angle_rad - 7*math.pi/2) < 0.1 then
        angle_rad = math.floor(2*angle_rad/math.pi)*math.pi/2
    end
    local delta_x = speed * math.cos(angle_rad)
    local delta_y = -speed * math.sin(angle_rad)

    local entities = {}

    local new_position = position
    while true do
        local new_x = new_position[1] + delta_x
        local new_y = new_position[2] + delta_y

        -- Create entity for the new position
        local e = map:create_custom_entity({
            name = "temp",
            direction = 0,
            layer = 0,
            x = new_x,
            y = new_y,
            width = 32,
            height = 32,
            is_traversable = true,
        })
        e:set_origin(16, 16)

        -- Check if the new position collides with any obstacle entities
        for _, obstacle in ipairs(obstacles) do
            if e:overlaps(obstacle) then
                e:remove()
                --local new_steps = Table:deepcopy(steps)
                return steps, entities, obstacle -- Character hit an obstacle
            end
        end

        new_position = {new_x, new_y}
        steps[#steps + 1] = new_position
        entities[#entities + 1] = e

    end
end

function move_character(position, angle, char_size, obstacles, speed, map)
    -- Return the final position after moving the character
    local steps, e, column = step_move_character(position, angle, char_size, obstacles, speed, 16, map)
    for _, e in ipairs(e) do
        e:remove()
    end
    return steps[#steps], column
end

