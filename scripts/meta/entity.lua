local meta_entity = sol.main.get_metatable("npc")

function meta_entity:on_created()
  -- Check for the game property "time" and show or hide the entity accordingly.
    local time = sol.main.get_game():get_value("time")
    local char_time = self:get_property("time")
    if (char_time ~= nil and char_time ~= time) then
        self:set_enabled(false)
    end
end


local speed = 1
local attack_speed = 1
function meta_entity:get_speed()
  return speed
end

function meta_entity:get_attack_speed()
  return attack_speed
end