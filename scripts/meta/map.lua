local meta_map = sol.main.get_metatable("map")
local angle = -math.pi

-- Based on https://github.com/ZeldoRetro/defi_zeldo_chap_2/blob/master/data/scripts/meta/map.lua
local frenzy = sol.sprite.create("frenzy")
local frenzy_opacity = 0

function meta_map:on_draw(dst_surface)
  -- Check for the game property "time" and show or hide the entity accordingly.
  local game = sol.main.get_game()
  local time = game:get_value("time")
  local tint = game:get_tint_map()

  self:draw_quest_indicator(dst_surface)

  -- Tint the screen if needed
  if tint[1] ~= nil then
    local tint_surface = sol.surface.create(320, 240)
    tint_surface:fill_color(tint)
    tint_surface:set_blend_mode("multiply")
    tint_surface:draw(dst_surface, 0, 0)
  end

  -- If not day, draw the light sources
  -- Lighting for night only
  if time ~= "day" then
    -- Iterate through all NPCs
    for entity in self:get_entities_by_type("npc") do
      -- Get the light intensity property from the NPC
      local light_source = entity:get_property("light_intensity")
      -- If the NPC has a light intensity property, then draw the light
      if light_source ~= nil then
        -- Get the light size and load the light image
        local light_size =64*light_source
        local ls = sol.surface.create("light.png")
        -- scale the light source
        local width, height = ls:get_size()
        local scale = light_size/width
        ls:set_scale(scale, scale)
        ls:set_opacity(50)
        local x, y, _ = entity:get_position()
        ls:draw(dst_surface, x - light_size/2 - 4, y - light_size/2 - 4)
      end
    end
  end

  -- Draw a corruption overlay
  local corruption = game:get_value("corruption")
  local corruption_text = sol.text_surface.create{
    text = "C: " .. corruption,
    color = {255, 255, 255},
    font_size = 16,
  }
  --corruption_text:draw(dst_surface, 200, 200)

  local boss = self:get_boss()
  --[[if boss ~= nil then
    local boss_text = sol.text_surface.create{
      text = "Health left: " .. boss:get_life(),
      color = {255, 255, 255},
      font_size = 16,
    }
    boss_text:draw(dst_surface,160, 220)
  end]]--

  if game:get_value("skill__frenzy__activated") == 1 or game:get_value("skill__frenzy__on_cooldown") == 1 then
    frenzy:set_opacity(2^(2*game:get_value("skill__frenzy__stack")) -1)
    frenzy:draw(dst_surface, 0, 0)
  end

-- Draw rectangles that fills up as DASH_CHARGE increases
--[[
local box = sol.surface.create(100, 10)
box:fill_color({255, 255, 255})
local box2 = sol.surface.create(DASH_CHARGE + 1, 10)
box2:fill_color({0, 0, 255})
box:draw(dst_surface, 10, 10)
box2:draw(dst_surface, 10, 10)
]]--

-- Draw interface quick and dirty
--local interface = sol.surface.create("INTERFACE_TESTE.png")
--interface:draw(dst_surface, 0, 0)

-- Create DEBUG_BITE surfaces, fill them with color and draw them
--[[for i in ipairs(DEBUG_BITE) do
  if DEBUG_BITE[i][1] ~= 0 then
    local debug_bite_surface = sol.surface.create(5, 5)
    debug_bite_surface:fill_color({0, 255, 0})
    local camx, camy = self:get_camera():get_position()
    debug_bite_surface:draw(dst_surface, DEBUG_BITE[i][1] - camx, DEBUG_BITE[i][2] - camy)
  end
end]]--

end

function meta_map:get_quest_pos(name)
  local char = self:get_entity(name)
  if char ~= nil then
    return char:get_position()
  end
end

local icon = sol.surface.create("elements/quest.png")
function meta_map:draw_quest_indicator(dst_surface)
  local game = sol.main.get_game()
  local quest = game:get_value("current_quest")
  local camera = self:get_camera()
  local camera_x, camera_y = camera:get_position()
  local x, y
  if quest == 1 then
    x, y = self:get_quest_pos("quest_mike")
  elseif quest == 2 or quest == 5 then
    x, y = self:get_quest_pos("quest_miyuki")
  elseif quest == 3 then
    x, y = self:get_quest_pos("quest_karen")
  elseif quest == 4 then
    x, y = self:get_quest_pos("quest_enzo")
  elseif quest == 6 then
    x, y = self:get_quest_pos("quest_luna")
  end
  if x ~= nil and y ~= nil then
    icon:draw(dst_surface, x - camera_x -24, y - camera_y - 32)
  end
end

function meta_map:draw_entity_bounding_boxes()
  local map = self
  map.enemy_bounding_boxes_active = true
  sol.timer.start(map, 50, function()
    for e in map:get_entities() do
      if not e.bounding_box_surface then
        e.bounding_box_surface = sol.surface.create(1,1)
        e.bounding_box_surface:set_transformation_origin(0,0)
        if e:get_type() == "enemy" then
          e.bounding_box_surface:fill_color({255, 0, 0})
        elseif e:get_type() == "hero" then
          e.bounding_box_surface:fill_color({0, 255, 0})
        else
          e.bounding_box_surface:fill_color({0, 0, 255})
        end
        e.bounding_box_surface:set_opacity(50)
      end
    end
    return true
  end)
end

function meta_map:start_attacking()
  local game = sol.main.get_game()
  -- Create a timer following the global variale ATTACK_SPEED
  local function attack()
    sol.timer.start(self, 1000 * (BASE_ATTACK_SPEED - BONUS_ATTACK_SPEED), function()
      --if game:get_value("in_combat") == 1 then
      for e in self:get_entities_by_type("enemy") do
        if e:get_distance(self:get_hero()) < 80 then
          game:queue_command_until_free"attack"

          -- Ancient battle
          if game:get_value("skill__ancient_battle__active") == 1 then
            game:set_value("skill__ancient_battle__active", 0)
            sol.timer.start(self, 200, function()
              game:queue_command_until_free"attack"
            end)
          end
          break
        end
      end

      attack()
    end)
  end

  local function ancient_battle()
    sol.timer.start(self, 3000, function()

      if game:get_value("in_combat") == 1 then
        if game:get_value("skill__ancient_battle__active") == 0 then
          game:set_value("skill__ancient_battle__active", 1)
        end
      end
      ancient_battle()
    end)
  end

  -- Enable ancient attack
  ancient_battle()
  attack()
end

-- New battle mechanics
local card_manager = require("scripts/cards")

local current_boss = nil
local c_points = 1
local switch_points = 1
local used_cards = {}
local round = 1
local preffered_style = nil
local c_cards = {nil, nil, nil}

function meta_map:fight_start(boss)
  local game = sol.main.get_game()
  round = 1
  c_points = 1
  switch_points = 1
  current_boss = boss
  game:set_value("in_combat", 1)
  music("eduardo/boss")
  game:set_character("kotto")
  self:start_attacking()
  self:card_distribute()
  self:show_card_menu()
end

function meta_map:reset_fight()
  round = 1
  local game = sol.main.get_game()
  game:set_life(game:get_max_life())
  self:reset_used_cards()
  self:reset_cards()
  local hud = require("scripts/menus/cooldowns_hud")
  sol.menu.stop(hud)
end

function meta_map:on_fight_end()
  local game = sol.main.get_game()
  local quest = game:get_value("current_quest")
  self:reset_fight()
  game:start_dialog("battle." .. current_boss .. ".end", function ()
    game:get_hero():teleport("streets/liberty")
    if quest == 1 then
      unlock_card("furious_sprint")
    elseif quest == 3 then
      unlock_card("magic_trick")
    end
  end)
  game:set_value("in_combat", 0)
  game:set_value("current_quest", quest + 1)
end

function meta_map:get_boss()
  return self:get_entity("boss")
end

function meta_map:get_round()
  return round
end

function meta_map:on_opening_transition_finished()
  local boss = self:get_entity("boss")
  if boss ~= nil then
    sol.main.get_game():set_value("bomb", 3)
    sol.main.get_game():set_character("kotto")
    self:fight_start(
      boss:get_property("name")
    )
  else
    music("eduardo/village")
    local game = sol.main.get_game()
    game:set_value("bomb", 3)
    game:save()
    game:set_character("MC")
  end
  
end

-- Card related
function meta_map:activate_card(card)
  card_manager:activate_card(card, current_boss)

  -- Test if the card is already used, if not, add it to the used cards
  if not used_cards[card.uid] then
    used_cards[card.uid] = true
    if card.c == true then
      c_points = c_points + 1
    end
  end
end

function meta_map:reset_cards()
  c_cards = {nil, nil, nil}
end

function meta_map:reset_used_cards()
  used_cards = {}
end

function meta_map:get_card(index)
  return c_cards[index]
end

function meta_map:card_distribute()
  c_cards = card_manager:card_distribute(round, preffered_style, c_points) 
end

function meta_map:get_one_card(i)
  c_cards[i] = card_manager:draw_card(nil, preffered_style)
end

function meta_map:show_card_menu()
  local game = sol.main.get_game()
  local card_menu = require("scripts/menus/card_chooser")
  local hud = require("scripts/menus/cooldowns_hud")
  sol.menu.stop(hud)
  game:set_paused(true)
  card_menu:set_cards(c_cards)
  card_menu:set_c_points(c_points)
  card_menu:set_switch_points(switch_points)
  function card_menu:on_finished()
    c_cards = card_menu:get_cards()
    local hud = require("scripts/menus/cooldowns_hud")
    sol.menu.start(game, hud)
    hud:set_cards(c_cards)
    game:set_paused(false)
  end
  sol.menu.start(game, card_menu)
end

function meta_map:next_round()
  round = round + 1
  print(round)
  if round >= 3 then
    self:on_fight_end()
    return
  end

  -- Add non used cards to the switch points
  for i = 1, 3 do
    if not used_cards[c_cards[i].uid] then
      switch_points = switch_points + 1
    end
  end

  switch_points = math.max(switch_points, 1)
  c_points = math.max(c_points - 1, 1)

  -- Reset the used cards
  self:reset_used_cards()
  self:reset_cards()

  -- Redistribute the cards
  self:card_distribute()
  self:show_card_menu()

  end