local meta_game = sol.main.get_metatable("game")

-- Day/night cycle
function meta_game:move_time()
  local game = sol.main.get_game()
  local time = game:get_value("time")
  if time == "day" then
    game:set_value("time", "evening")
    game:set_tint_map({255, 173, 226})
  elseif time == "evening" then
    game:set_value("time", "night")
    game:set_tint_map({0, 33, 164})
  elseif time == "night" then
    game:set_value("time", "midnight")
    game:set_tint_map({70, 70, 70})
    -- Ask the player what to do
    game:start_dialog("midnight_brain", 
      function(answer)
        if answer == 1 then
          self:get_hero():teleport("debug")
          self:move_time()
        elseif answer == 2 then
          self:get_hero():teleport("brain_room")
        end
      end
    )
    meta_game:set_character("kotto")
  elseif time == "midnight" then
    game:set_value("time", "day")
    game:reset_tint_map()
    meta_game:set_character("MC")
  end
end

function meta_game:add_corp(value)
  local game = sol.main.get_game()
  local corp = game:get_value("corruption")

  game:set_tint_map({200, 0, 200})
  sol.timer.start(game, 300, function()
    game:reset_tint_map()
  end)
  require("scripts/conquista"):update("corruption")
  print("me chamou")
end

function meta_game:get_corp()
  local corp = sol.main.get_game():get_value("corruption")
  return corp
end

function meta_game:add_cash(value)
  local game = sol.main.get_game()
  local cash = game:get_value("cash")
  game:set_value("cash", cash + value)
end

function meta_game:get_cash()
  local cash = sol.main.get_game():get_value("cash")
  return cash
end

function meta_game:set_tint_map(color)
  local game = sol.main.get_game()
  -- If the end color is nil, set and return
  if color[1] == nil then
    game:set_value("screen_r", nil)
    game:set_value("screen_g", nil)
    game:set_value("screen_b", nil)
    return
  end

  local start_colors = game:get_tint_map()
  -- If the start color is nil, set it to white
  if start_colors[1] == nil then
    start_colors = {255, 255, 255}
  end

  -- Slowly tint the map
  function tint_surface(start_colors, end_colors)
    local r, g, b = start_colors[1], start_colors[2], start_colors[3]
    local dr, dg, db = (end_colors[1] - start_colors[1])/30, (end_colors[2] - start_colors[2])/30, (end_colors[3] - start_colors[3])/30
    local i = 0
    sol.timer.start(game, 10, function()
      game:set_value("screen_r", r)
      game:set_value("screen_g", g)
      game:set_value("screen_b", b)
      r = r + dr
      g = g + dg
      b = b + db
      i = i + 1
      if i < 30 then
        return true
      end
    end)
  end
  
  tint_surface(start_colors, color)
end

function meta_game:reset_tint_map()
  local game = sol.main.get_game()
  local time = game:get_value("time")
  if time == "day" then
    meta_game:set_tint_map({255, 255, 255})
  elseif time == "evening" then
    meta_game:set_tint_map({255, 173, 226})
  elseif time == "night" then
    meta_game:set_tint_map({0, 33, 164})
  elseif time == "midnight" then
    meta_game:set_tint_map({70, 70, 70})
  end
end

function meta_game:get_tint_map()
  local game = sol.main.get_game()
  local r = game:get_value("screen_r")
  local g = game:get_value("screen_g")
  local b = game:get_value("screen_b")
  return {r, g, b}
end

function meta_game:set_clothes(clothes)
  meta_game:set_value("clothes", clothes)
end

function meta_game:get_clothes()
  local clothes = meta_game:get_value("clothes")
  return clothes
end

function meta_game:set_character_dialog(character)
  local game = sol.main.get_game()
  -- Test if character is in list
  if Table:contains(MC_NAMES, character) then
    game:set_value("character_dialog", "kokone_sora")
  elseif Table:contains(CAT_NAMES, character) then
    game:set_value("character_dialog", "cat")
  else
    game:set_value("character_dialog", character)
  end
end

function meta_game:get_character_dialog_name()
  local game = sol.main.get_game()
  local character = game:get_value("character_dialog")
  if Table:contains(MC_NAMES, character) then
    return "Kokone Sora"
  elseif Table:contains(CAT_NAMES, character) then
    return "Cat"
  else
    return character
  end
end

function meta_game:on_dialog_finished(dialog_id)
  local game = sol.main.get_game()
  if game:get_value("time") == "midnight" then
    game:set_character_dialog("kotto")
  else
    game:set_character_dialog("kokone_sora")
  end
end

function meta_game:set_character(character)
  local game = sol.main.get_game()
  local hero = game:get_hero()
  if Table:contains(MC_NAMES, character) then
    hero:set_size(24, 24)
    game:set_value("character", "kokone_sora")
    game:set_value("character_dialog", "kokone_sora")
    game:get_hero():set_tunic_sprite_id("npc/kokone")
  elseif Table:contains(CAT_NAMES, character) then
    hero:set_size(12,12)
    game:set_value("character", "kotto")
    game:set_value("character_dialog", "kotto")
    game:get_hero():set_tunic_sprite_id("animals/meow_orange")
  else
    hero:set_size(16, 16)
    game:set_value("character", character)
    game:set_value("character_dialog", character)
  end
end

function meta_game:on_key_pressed(key)
  local game = sol.main.get_game()
  if key == "p" and not game:is_paused() then
  -- Cheat Keys
  elseif key == "l" then
    game:add_life(1)
  elseif key == "m" then
    if BASE_DAMAGE == 1 then
      BASE_DAMAGE = 50
    else
      BASE_DAMAGE = 1
    end
  -- Movement keys
  --[[elseif key == "up" and game:get_command_keyboard_binding("up") ~= "up" then
    game:set_command_keyboard_binding("up", "up")
  elseif key == "down" and game:get_command_keyboard_binding("down") ~= "down" then
    game:set_command_keyboard_binding("down", "down")
  elseif key == "left" and game:get_command_keyboard_binding("left") ~= "left" then
    game:set_command_keyboard_binding("left", "left")
  elseif key == "right" and game:get_command_keyboard_binding("right") ~= "right" then
    game:set_command_keyboard_binding("right", "right")
  elseif key == "w" and game:get_command_keyboard_binding("up") ~= "w" then
    game:set_command_keyboard_binding("up", "w")
  elseif key == "s" and game:get_command_keyboard_binding("down") ~= "s" then
    game:set_command_keyboard_binding("down", "s")
  elseif key == "a" and game:get_command_keyboard_binding("left") ~= "a" then
    game:set_command_keyboard_binding("left", "a")
  elseif key == "d" and game:get_command_keyboard_binding("right") ~= "d" then
    game:set_command_keyboard_binding("right", "d")--]]
  end
end

function meta_game:unlock_card(card)
  self:set_value(card, 1)
end

function meta_game:on_game_over_started()
  self:get_map():reset_fight()
  self:set_paused(false)
  self:get_hero():teleport("brain_room")
  self:stop_game_over()
end

    