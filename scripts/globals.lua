-- Purpose: Global variables and functions

-- DEVEL DIR
DEVEL_DIR = "scripts/"

-- Names
MC_NAMES = { "MC", "Kokone", "Ko", "kokone_sora", "kokone", "ko" }
CAT_NAMES = { "cat", "Cat", "kotto" }

-- Dialog
DIALOG = nil
DIALOG_BACKGROUND_COLOR = nil
DIALOG_BACKGROUND_OPACITY = 0
DIALOG_BACKGROUND_LOCK = false
SKILLS = nil
SKILLS_LIST = nil
GAME_OVER = nil

-- Functional
UNDO_SKILLS = nil

-- Formulas
BASE_DAMAGE = 1
BONUS_DAMAGE = 0
RATE_BASE_DAMAGE = 1
RATE_BONUS_DAMAGE = 1
RATE_TOTAL_DAMAGE = 1

GAME_SPEED = 1

BASE_ATTACK_POWER = 1
BONUS_ATTACK_POWER = 0
RATE_BASE_ATTACK_POWER = 1
RATE_BONUS_ATTACK_POWER = 1
RATE_TOTAL_ATTACK_POWER = 1

BASE_ATTACK_SPEED = 1
BONUS_ATTACK_SPEED = 0

BONUS_SKILL_SPEED = 1

BASE_DASH_COOLDOWN = 0.5
BONUS_DASH_COOLDOWN = 0

local offset_damage = 0
function damage()
    local d = (BASE_DAMAGE * RATE_BASE_DAMAGE + BONUS_DAMAGE * RATE_BONUS_DAMAGE) * RATE_TOTAL_DAMAGE
    offset_damage = offset_damage + d - math.floor(d)
    if offset_damage > 1 then
        d = d + 1
        offset_damage = offset_damage - 1
    end
    return d
end

local offset_ap = 0
function AP()
    local ap = (BASE_ATTACK_POWER * RATE_BASE_ATTACK_POWER + BONUS_ATTACK_POWER * RATE_BONUS_ATTACK_POWER) * RATE_TOTAL_ATTACK_POWER
    offset_ap = offset_ap + ap - math.floor(ap)
    if offset_ap > 1 then
        ap = ap + 1
        offset_ap = offset_ap - 1
    end
    return ap
end

function TIME(seconds)
    return seconds / GAME_SPEED
end

function SPEED(speed)
    return speed * GAME_SPEED
end

function SKILL_TIME(seconds)
    return seconds * (2 - BONUS_SKILL_SPEED)
end

function time_spike_damage()
    return (BONUS_DAMAGE * RATE_BONUS_DAMAGE) * RATE_TOTAL_DAMAGE
end

function sound(sound_file)
    sol.audio.play_sound(sound_file)
end

function music(music_file)
    sol.audio.play_music(music_file)
end

function color(r, g, b, callback, target_a, a, lock)
    local a = a or 0
    local target_a = target_a or 255

    if lock == true or lock == nil then
        if DIALOG_BACKGROUND_LOCK == true then
            sol.timer.start(10, function()
                color(r, g, b, callback, target_a, a)
            end)
        end
    end


    DIALOG_BACKGROUND_LOCK = true
    DIALOG_BACKGROUND_COLOR = { r, g, b }
    if a > 255 then
        a = 255
    elseif a < 0 then
        a = 0
    end
    DIALOG_BACKGROUND_OPACITY = a
    if a < target_a then
        a = a + 10
        sol.timer.start(sol.main.get_game(), 10, function()
            color(r, g, b, callback, target_a, a, false)
        end)
    elseif a >= target_a then
        DIALOG_BACKGROUND_LOCK = false
        if callback ~= nil then
            callback()
        end
    end
end

function decolor(callback, target_a, a, lock)
    local a = a or 255
    local target_a = target_a or 0

    if lock == true or lock == nil then
        if DIALOG_BACKGROUND_LOCK == true then
            sol.timer.start(10, function()
                decolor(callback, target_a, a)
            end)
        end
    end


    DIALOG_BACKGROUND_LOCK = true
    if a > 255 then
        a = 255
    elseif a < 0 then
        a = 0
    end
    DIALOG_BACKGROUND_OPACITY = a
    if a > target_a then
        a = a - 10
        sol.timer.start(sol.main.get_game(), 10, function()
            decolor(callback, target_a, a, false)
        end)
    elseif a <= target_a then
        DIALOG_BACKGROUND_LOCK = false
        if callback ~= nil then
            callback()
        end
    end
end

function unlock_card(card)
    local card_manger = require("scripts/cards")
    card_manger:unlock_card(card)
end

-- UI
DASH_CHARGE = 0

NEXT_DIALOG = nil
BOSS_RESPAWN = false

DEBUG_BITE = {
    { 0, 0 },
    { 0, 0 },
    { 0, 0 },
    { 0, 0 }
}
