local entity = ...
local game = entity:get_game()

--Shared NPC. The dialog pointer = the name of the entity in the map (in the editor).
--If you want all NPC to have a movement then create the movement in on_created()

function entity:on_created()
-- here, self is the entity
self:set_size(32,32) -- only multiple of 8 are allowed
self:set_traversable_by("hero", false)
end


function entity:on_interaction()
  self:set_direction(game:get_hero():get_direction() / 2)
  game:start_dialog(self:get_name())
end
