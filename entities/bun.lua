-- Lua script of custom entity bun.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local npc = ...
local game = npc:get_game()
local map = npc:get_map()

-- Event called when the custom entity is initialized.
function npc:on_created()
  self:set_can_traverse("hero", false)
  self:set_traversable_by("hero", false)
end

function npc:on_interaction()
  -- sprite = npc:create_sprite("/animals/bun/bun")
  local local_sprite = npc:get_sprite()
  function local_sprite:on_animation_finished()
    if local_sprite:get_direction() == 1 then
      game:start_dialog("book_on_treehouse")
      local_sprite:set_direction(0)
    else
      local_sprite:set_animation("stand") 
    end
  end
  local_sprite:set_animation("raise")
  local_sprite:set_direction(1)
end 