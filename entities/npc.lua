-- Lua script of custom entity npc.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local entity = ...
local game = entity:get_game()

--Shared NPC. The dialog pointer = the name of the entity in the map (in the editor).
--If you want all NPC to have a movement then create the movement in on_created()

function entity:on_created()
  -- here, self is the entity
  self:set_size(32,32) -- only multiple of 8 are allowed
  self:set_traversable_by("hero", false)
  print(self:get_name())
  self:get_sprite():set_frame_delay(300)
  if self:get_name() == nil then
    self:get_sprite():set_frame(1)
    self:get_sprite():set_paused(true)
  end
end


function entity:on_interaction()
  self:get_sprite():set_paused(false)
  self:set_direction(self:get_direction4_to(game:get_hero()))
  self:get_sprite():set_direction(self:get_direction4_to(game:get_hero()))
  self:get_sprite():set_paused(true)
  local name = self:get_name()
  if name == "" then
    game:start_dialog()
  end
end