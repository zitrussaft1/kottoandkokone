local master_script = require("scripts/meta/master")
local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite
local movement


function entity:on_created()
  entity.is_projectile = true
  entity:set_drawn_in_y_order(true)
  entity:set_can_traverse(true)
  entity:set_can_traverse("enemy", true)
  entity:set_can_traverse("hero", true)
  entity:set_can_traverse("custom_entity", true)
  entity:set_can_traverse_ground("shallow_water", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("low_wall", true)
end

function entity:shoot(angle)
  local damage = entity.damage or 1
  local type = entity.damage_type or "physical"
  local sprite = entity:get_sprite()
  local ignore_obstacles = entity.ignore_obstacles or false
  --sprite:set_transformation_origin(0, -1)
  --sprite:set_xy(0, -14)
  sprite:set_rotation(angle)
  local m = sol.movement.create"straight"
  m:set_speed(entity.speed or 300)
  m:set_angle(angle)
  m:set_max_distance(entity.range or 700)
  m:set_smooth(false)
  if ignore_obstacles then
    m:set_ignore_obstacles(true)
  end
  m:start(entity, function()
    entity:hit_obstacle()
  end)
  function m:on_obstacle_reached()
    entity:hit_obstacle()
  end
  function m:on_changed()
    --entity:hit_obstacle() --can't remember why I forced an obstacle collison whenever the movement was changed, but this is incompatible with tracking enemies
  end

  entity:activate_collision()

end


function entity:hit_obstacle()
  if entity.obstacle_callback then
    entity:obstacle_callback()
  else
    entity:stop_movement()
    local pop_sprite = entity:create_sprite("hero_projectilestrillium//pop")
    entity:remove_sprite()
    pop_sprite:set_animation("killed", function() entity:remove() end)
  end
end


function entity:activate_collision()
  master_script:set("trillium", "switch")
  local damage = entity.damage or 1
  local type = entity.damage_type or "physical"
  local sprite = entity:get_sprite()

  entity.collided_entities = {}
  function process_collision(entity, other)
    if entity.collided_entities[other] then return end
    entity.collided_entities[other] = true
    if other:get_type() == "enemy" and (other:get_life() > 0) then
      if not ignore_obstacles then
        entity:clear_collision_tests()
      end
      if other.process_hit then
        other:process_hit(damage)
      else
        other:hurt(damage)
      end
      if not ignore_obstacles then
        entity:hit_obstacle()
      end
    elseif other:get_type() == "switch" and not other:is_walkable() then
      entity:clear_collision_tests()
      entity:hit_obstacle()
      other:toggle()
    elseif other.react_to_hero_projectile then
      other:react_to_hero_projectile()
    end
  master_script:set("default", "switch")
  end

  entity:add_collision_test("sprite", function(entity, other)
    process_collision(entity, other)
  end)
  entity:add_collision_test("touching", function(entity, other)
    process_collision(entity, other)
  end)
end


--Will cause the projectile to arc toward enemies, even as they move
--Set entity.tracking_accuracy to increase or decrease lock-on amount, default 3
function entity:track_target(target)
  sol.timer.start(entity, 20, function()
    local m = entity:get_movement()
    if m and m:get_speed() > 0 then
      local angle = (m:get_angle())
      local target_angle = (entity:get_angle(target))
      local step = entity.tracking_accuracy or 3
      if target_angle < angle then step = step * -1 end
      local diff = math.abs(target_angle - angle)
      if diff > math.rad(180) then
        step = step * -1
      end
      if diff < math.rad(step) then step = 0 end
      angle = angle + math.rad(step)
      entity:get_sprite():set_rotation(angle)
      m:set_angle(angle)
    end
    return true
  end)
end


function entity:set_type(type)
  if type == "arrow" then

  elseif type == "fireball" then
    entity.obstacle_callback = function()
      local x, y, z = entity:get_position()
      map:create_fire{x=x, y=y, layer=z}
      entity:remove()
    end
  end
end
