local entity = ...
local game = entity:get_game()
local map = entity:get_map()


function entity:on_created()
  entity:set_traversable_by(false)
  entity:set_drawn_in_y_order(true)
end

function entity:on_interaction()
  game:start_flash()
  game:save_checkpoint()
  entity:heal_hero()
  game:refill_respawn_items()
  map:respawn_enemies()
  game:stop_flash(10)
  entity:sparkle_effect()
  --game:save()
  print("Saving disabled during testing")
  sol.menu.start(game, require("scripts/trillium/menus/level_up/level_up"))
  --sol.menu.start(game, require("scripts/trillium/menus/inventory/charms"))
end


function entity:heal_hero()
  game:set_life(game:get_max_life())
  game:set_magic(game:get_max_magic())
end



function entity:sparkle_effect()
  local x, y, z = entity:get_position()
  for i=1, 12 do
    local sparkle = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=8, height=16,
      sprite = "entities/trillium/lantern_sparkle",
    }
    sparkle:get_sprite():set_animation("sparkle_" .. math.random(1,2), function()
      sparkle:remove()
    end)
    sparkle:get_sprite():set_ignore_suspend(true)
    sparkle:set_drawn_in_y_order(true)
    local m = sol.movement.create"straight"
    m:set_speed(120)
    m:set_angle(math.random(100) * 2 * math.pi / 100)
    m:set_max_distance(math.random(8, 24))
    m:set_ignore_obstacles(true)
    m:set_ignore_suspend(true)
    m:start(sparkle)
  end
end