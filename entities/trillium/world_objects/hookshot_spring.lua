local entity = ...
local game = entity:get_game()
local map = entity:get_map()


local function launch_collision(entity, other)
    if other:get_type() == "hero" and ( other:get_direction() == other:get_direction4_to(entity) )
    and (other:get_state() == "custom") and (other:get_state_object():get_description() == "hookshot") then
      local hero = other
      game:get_item("inventory/hookshot").hook:remove() --to unhook from target without ending hookshot state
      local state = game:get_item("inventory/feather"):get_jumping_state()
      hero:start_state(state) --start jumping directly from hookshot state
      hero:set_animation"jumping_2"
      sol.audio.play_sound"jump"
      local m = sol.movement.create"straight"
      m:set_angle(hero:get_direction() * math.pi / 2)
      m:set_speed(entity.launch_speed)
      m:set_max_distance(entity.launch_distance)
      m:start(hero, function()
        hero:unfreeze()
      end)
      function m:on_obstacle_reached() hero:unfreeze() end
    end
end


function entity:on_created()
  entity.hookshot_target = true
  entity:set_drawn_in_y_order(true)
  entity:set_traversable_by("hero", true)
  entity.launch_distance = 96
  entity.launch_speed = 250
  entity:add_collision_test("touching", function(entity, other) launch_collision(entity, other) end)
end