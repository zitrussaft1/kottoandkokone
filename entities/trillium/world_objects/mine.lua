--[[
Created by Max Mraz, licensed MIT
A proximity mine, can be activated by either the hero, enemies, or either
Values you can set:
  entity.explosion_sprite : sprite of the explosion created
  entity.range = entity.range : how close targets can get before the mine is triggered
  entity.fuse_length = entity.fuse_length : how long after being triggered before the mine goes off
  entity.ignore_enemies = entity.ignore_enemies : if true, enemies won't set off this mine. Useful for traps to catch the player
  entity.ignore_hero = entity.ignore_hero or false : if true, the hero won't set off this mine. Useful if the hero is placing these
--]]

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity.range = entity.range or 24
  entity.fuse_length = entity.fuse_length or 500
  entity.ignore_enemies = entity.ignore_enemies or true
  entity.ignore_hero = entity.ignore_hero or false

  sol.timer.start(entity, 100, function()
    local x, y, z = entity:get_position()
    local range = entity.range
    for e in map:get_entities_in_rectangle(x - range, y - range, range * 2, range * 2) do
      if e:get_type() == "enemy" and not entity.ignore_enemies then
        entity:start_countdown()
      elseif e:get_type() == "hero" and not entity.ignore_hero then
        entity:start_countdown()
      end
    end
    return true
  end)
end


function entity:start_countdown()
  sol.audio.play_sound"bird_chirp"
  sol.timer.stop_all(entity)
  sol.timer.start(entity, entity.fuse_length, function()
    local x, y, z = entity:get_position()
    entity:remove()
    map:create_explosion{
      x=x, y=y, layer=z,
      sprite = entity.explosion_sprite or "items/trillium/explosion_grenade",
    }
  end)
end