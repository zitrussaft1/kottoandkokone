local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = game:get_hero()

local bump_damage = 10 --how much damage enemies do when pushing against obstacle, or hero does when rolling


function entity:on_created()
  entity.can_burn = true
  entity:set_traversable_by(false)
  entity:set_drawn_in_y_order(true)
  entity.hp = entity:get_property("hp") or 1

  --Touching test
  local function process_collision(entity, other_entity)
    if other_entity:get_type() == "enemy" and other_entity.aggro then
      entity:take_damage(bump_damage)
    end

    if other_entity:get_type() == "hero" then
      local state, state_ob = other_entity:get_state()
      if state == "custom" and state_ob:get_description() == "dashing" then
        entity:take_damage(bump_damage)
      end
    end
  end

  entity:add_collision_test("touching", function(entity, other_entity)
    process_collision(entity, other_entity)
  end)

  entity:add_collision_test("sprite", function(entity, other_entity, sprite, other_sprite)
    process_collision(entity, other_entity)
    if other_sprite:get_animation_set() == hero:get_sword_sprite_id() then
      entity:take_damage(bump_damage)
    end
  end)

end


function entity:react_to_solforge_weapon()
  entity:take_damage(bump_damage)
end


function entity:take_damage(amount)
  local sprite = entity:get_sprite()
  sprite:set_blend_mode("add")
  sol.timer.start(entity, 100, function()
    sprite:set_blend_mode("blend")
  end)

  entity.hp = entity.hp - amount
  if entity.hp <= 0 then
    entity:destroy()
  end
end




function entity:destroy()
  entity:clear_collision_tests()
  if not game.breakable_object_sound_playing then
    sol.audio.play_sound("breaking_crate")
    game.breakable_object_sound_playing = true
    sol.timer.start(game, 50, function()
      game.breakable_object_sound_playing = nil
    end)
  end
  entity:get_sprite():set_animation("breaking", function()
    entity:set_traversable_by(true)
    entity:remove()
  end)
  --alert nearby enemies
  local ALERT_DISTANCE = 100
  for enemy in map:get_entities_by_type"enemy" do
    if enemy.start_aggro and enemy:get_distance(entity) < ALERT_DISTANCE and enemy:get_layer() == hero:get_layer() and not enemy.aggro then
      enemy:start_aggro()
    end
  end
end