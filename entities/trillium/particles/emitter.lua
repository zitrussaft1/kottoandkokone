-- Particle Emitter
-- Author: Diarandor (Solarus Team), Rheytendo, Max Mraz.
-- License: GPL v3-or-later.
--[[
To use:
I'd recommend making a helper function like map:create_particle_emitter(x, y, layer)  - but otherwise you can just create a custom entity the old fashined way
After creating and setting the parameters, use emitter:emit() to start it
You can also create an entity on the map
If you set an entity property called "type" to a map entity, it will automatically start emitting with parameters preset to that style
For example, I've created the types "smoke" and "burst" as examples.
--]]


local entity = ...
local map = entity:get_map()
local particle_list, timers = {}, {}  


local function clear_stuff()
  entity.max_particles = nil
  entity.target = nil
  entity.shape = nil
  entity.width, entity.height = nil, nil
  entity.frequency = nil
  entity.particles_per_loop = nil
  entity.duration = nil
  entity.angle = nil
  entity.angle_variance = nil
  entity.point_target_dead_zone = nil
  entity.particle_sprite = nil
  entity.particle_animation = nil
  entity.particle_animation_loops = nil
  entity.particle_scaling = nil
  entity.particle_rotation = nil
  entity.particle_rotation_speed = nil
  entity.particle_opacity = nil
  entity.particle_fade_speed = nil
  entity.particle_duration = nil
  entity.particle_color = nil
  entity.particle_speed = nil
  entity.particle_speed_variance = nil
  entity.particle_acceleration = nil
  entity.particle_distance = nil
  entity.particle_distance_variance = nil
  entity.particle_surface = nil

  entity.set_type = nil
  entity.update_particle_surface = nil
  entity.stop_particles = nil
  entity.emit = nil
end


function map:on_finished()
  clear_stuff()
end

--Presets
--See entity:on_created for possible parameters and notes
function entity:set_type(type)
  if type == "smoke" then
    entity.particle_speed = 12
    entity.frequency = 30
    entity.particle_sprite = "effects/smoke_particle"
    entity.particle_scaling = {1, 2.5}
    entity.particle_color = {220, 230, 240}
    --entity.particle_color = {255,255,255}
    entity.particle_opacity = {50,100}
    entity.particle_fade_speed = 1
    entity.shape = "circle"
    entity.width = 6
    entity.angle = 1
    entity.angle_variance = math.rad(50)

  elseif type == "burst" then
    entity.frequency = 10
    entity.particles_per_loop = 5
    entity.duration = 100
    entity.angle_variance = math.pi
    entity.particle_speed = 120

  elseif type == "reverse" then
    entity.shape = "circle"
    entity.particle_fade_speed = -3
    entity.particle_speed = 20
    entity.particle_acceleration = 5
    entity.particle_distance = 96
    entity.particle_opacity = {80, 100}
    entity.width = 96
    entity.angle = "point"
    entity.angle_variance = math.pi * 2

  end
end


function entity:on_created()
  --Default values:
  entity.max_particles = 200
  entity.target = entity --if the target is not the emitter itself, then it emits from the x/y of the target instead
  entity.shape = "square" --can be square or circle, particles will be created randomly within this shape.
  entity.width, entity.height = entity:get_size()
  entity.frequency = 10
  entity.particles_per_loop = 1 --can create multiple particles at once if every 10ms isn't fast enough
  entity.duration = nil --how long the emitter will last, nil means forever
  entity.angle = math.pi / 2 --direction in which the particles are emitted --special value: "point" will cause a reverse emitter, where particles are spawned and sucked in
  entity.angle_variance = math.pi
  entity.point_target_dead_zone = 8 --distance to the center of the entity at which particles in a reverse emitter will disappear
  entity.particle_sprite = "effects/particle_small"
  entity.particle_animation = nil --Leave nil for the default animation. NOTE: if there are multiple directions to the animation, a random one will be used per particle
  entity.particle_animation_loops = true
  entity.particle_scaling = {1, 1} --specify min scale, max scale amount, for example {.8, 1.5}
  entity.particle_rotation = false --options are false (no rotation), true (starts at a random rotation), "clockwise" and "counterclockwise" (rotates as it moves), and "random" (either clockwise or counterclockwise)
  entity.particle_rotation_speed = { math.rad(5), math.rad(10)}
  entity.particle_opacity = {50, 110} --range from lowest to highest opacity, assigned randomly
  entity.particle_fade_speed = 2 --set to 0 and particles will not fade
  entity.particle_duration = 5000
  entity.particle_color = {255, 255, 255}
  entity.particle_speed = 16
  entity.particle_speed_variance = 16
  entity.particle_acceleration = 0
  entity.particle_distance = 24
  entity.particle_distance_variance = 10

  if entity:get_property("type") then
    entity:set_type(entity:get_property("type"))
    entity:emit()
  end
  --Apply any values specified from entity properties
  entity.particle_sprite = entity:get_property("particle_sprite") or entity.particle_sprite
end


local function create_particles()
  local num_particles = 0
  local w, h = sol.video.get_quest_size()
  local particle_surface = sol.surface.create(w, h)
  --map will have a single draw function to avoid registering to the on_draw event for every emitter
  entity.particle_surface = particle_surface
  map.particle_emitters = map.particle_emitters or {}
  map.particle_emitters[entity] = entity

  local sparkle_sprite = sol.sprite.create(entity.particle_sprite)
  if entity.particle_animation then sparkle_sprite:set_animation(entity.particle_animation) end
  sparkle_sprite:set_color_modulation(entity.particle_color)

  for i = 0, entity.max_particles - 1 do
    particle_list[i] = {index = i}
  end

  local function make_particle()
    -- Prepare next slot.
    local index, particle = 0, particle_list[0]
    --TODO: this loop puts the particle at the lowest index where not particle.exists - this could be written better
    while index < entity.max_particles and particle.exists do
      index = index + 1
      particle = particle_list[index]
    end
    if particle == nil or particle.exists then return end

    --Set initial location around emitter:
    local x, y, z = entity.target:get_center_position()
    if entity.shape == "circle" then
      particle.init_x = x + math.cos(math.random(0, math.pi * 2)) * math.random(0, entity.width)
      particle.init_y = y + math.sin(math.random(0, math.pi * 2)) * math.random(0, entity.width)
    elseif entity.shape == "square" then
      particle.init_x = x + math.random(entity.width * -1 / 2, entity.width / 2)
      particle.init_y = y + math.random(-3, entity.height - 3)
    end
    -- Set properties for new particle
    particle.x, particle.y, particle.frame = 0, 0, math.random(0, sparkle_sprite:get_num_frames() - 1)
    particle.point_target_x, particle.point_target_y = x, y --For reverse emitters
    particle.direction = math.random(0, sparkle_sprite:get_num_directions() - 1)
    	particle.opacity = math.random(entity.particle_opacity[1], entity.particle_opacity[2])
    particle.scale = {math.random(entity.particle_scaling[1] * 100, entity.particle_scaling[2] * 100) / 100, math.random(entity.particle_scaling[1] * 100, entity.particle_scaling[2] * 100) / 100}
    particle.rotation_angle = entity.particle_rotation and (math.rad(1, 360)) or 0
    particle.rotation_speed = math.random(entity.particle_rotation_speed[1] * 1000, entity.particle_rotation_speed[2] * 1000) / 1000
    particle.rotation_direction = entity.particle_rotation == "clockwise" and 1 or -1
    if entity.particle_rotation == "random" then particle.rotation_direction = (math.random(1,2) == 1) and 1 or -1 end
    particle.speed = entity.particle_speed + math.random(entity.particle_speed_variance * - 1, entity.particle_speed_variance)
    particle.max_distance = entity.particle_distance + math.random(entity.particle_distance_variance * -1, entity.particle_distance_variance)
    --for reverse emitters:
    if entity.angle == "point" then
      particle.angle = sol.main.get_angle(particle.init_x, particle.init_y, particle.point_target_x, particle.point_target_y)
    else
      particle.angle = entity.angle + (math.random(entity.angle_variance * -1 * 10000, entity.angle_variance * 10000) / 10000)
    end
    particle.lifespan = entity.particle_duration
    num_particles = num_particles + 1
 
    particle.exists = true
  end
 
  function entity:update_particle_surface()
    particle_surface:clear()
    local camera = map:get_camera()
    local cx, cy, cw, ch = camera:get_bounding_box()
    -- Draw particles on surface.
    	for _, particle in pairs(particle_list) do
      if particle.exists then
        sparkle_sprite:set_frame(particle.frame)
        local x = (particle.init_x + particle.x - cx)
        local y = (particle.init_y + particle.y - cy)
		    sparkle_sprite:set_opacity(particle.opacity)
        sparkle_sprite:set_scale(particle.scale[1], particle.scale[2])
        sparkle_sprite:set_rotation(particle.rotation_angle)
        sparkle_sprite:set_direction(particle.direction)
        sparkle_sprite:draw(particle_surface, x, y)
      end
    end
  end

  local function start_particle()
    -- Initialize particle creation timer.
    local elapsed_time = 0
    timers["particle_creation_timer"] = sol.timer.start(map, entity.frequency, function()
      if not entity.target:exists() then return end
      for i = 1, entity.particles_per_loop do
        make_particle()
      end
      elapsed_time = elapsed_time + entity.frequency
      if not entity.duration then return true end --run forever if there's no duration on the emitter
      if elapsed_time >= entity.duration then
        entity:remove()
      else
        return true
      end
    end)
 
    -- Initialize particle position timer. Also adjust opacity, speed, rotation, etc.
    if timers["particle_position_timer"] == nil then
      local dt = 30 -- Timer delay.
      timers["particle_position_timer"] = sol.timer.start(map, dt, function()
        --print("Num particles:", num_particles)
        if num_particles <= 0 then --do some clean up
          map.particle_emitters[entity] = nil
          return false
        end
        for index, particle in pairs(particle_list) do

          if particle.exists then
            particle.lifespan = particle.lifespan - dt
            if particle.lifespan <=0 then
              particle.exists = false
            end

            local distance_increment = particle.speed * (dt / 1000)
            particle.speed = particle.speed + entity.particle_acceleration
            if particle.speed < 0 then particle.speed = 0 end
            particle.x = particle.x + distance_increment * math.cos(particle.angle)
            particle.y = particle.y + distance_increment * math.sin(particle.angle) * (-1)
 
            local distance = math.sqrt((particle.x)^2 + (particle.y)^2)
 
            --If going toward point target, check and remove if close enough
            local actual_x = particle.init_x + particle.x
            local actual_y = particle.init_y + particle.y
            if entity.angle == "point" and sol.main.get_distance(actual_x, actual_y, particle.point_target_x, particle.point_target_y) <= math.max(distance_increment, entity.point_target_dead_zone) then
              particle.exists = false
            end

            if particle.opacity > 0 then
              particle.opacity = particle.opacity - entity.particle_fade_speed
              if particle.opacity < 0 then
                particle.exists = false
              end
            end
 
            if distance >= particle.max_distance then
              -- Disable particle.
              particle.exists = false
            end

            if entity.particle_rotation then
              particle.rotation_angle = particle.rotation_angle + particle.rotation_speed * particle.rotation_direction
            end
            if particle.exists == false then
  			      num_particles = num_particles - 1
            end
          end
        end
        return true
      end)
    end
 
    -- Update particles frames for all particles at once.
    local frame_delay = sparkle_sprite:get_frame_delay()
    local num_frames = sparkle_sprite:get_num_frames()
    if timers["particle_frame_timer"] == nil and frame_delay then
      timers["particle_frame_timer"] = sol.timer.start(map, frame_delay, function()
        for _, particle in pairs(particle_list) do
          if particle.exists then
            particle.frame = (particle.frame + 1)
            if particle.frame > (num_frames - 1) and entity.particle_animation_loops then
              particle.frame = particle.frame % num_frames
            elseif particle.frame > (num_frames - 1) then
              particle.frame = 0
              particle.opacity = 0
            end
          end
        end
        return true
      end)
    end

  end

  start_particle()
 
  function entity:stop_particles()
    local timers_list = {"particle_creation_timer"}
    for _, key  in pairs(timers_list) do
      local t = timers[key]
      if t then t:stop() end
      timers[key] = nil
    end
  end

end


function entity:emit()
  create_particles(entity.particle_sprite)
end

