local master_script = require("scripts/meta/master")
local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite
local movement

--[[Preset projectile types, use with entity:set_projectile_type(type)
"arrow"
"fireball"
"iceball"
lightningball"
"webball"
--]]

--[[Parameters
entity.damage
entity.damage_type
entity.speed
entity.max_distance
entity.rotational_sprite - sprite only faces direction 0, is rotated to face angle of travel
entity.can_damage_enemies
entity.hero_collision_callback - function, called when hits the hero. If not defined, it just does damage
entity.obstacle_callback - function, called when hits an obstacle
--]]

function entity:on_created()
  entity.is_projectile = true
  entity:set_drawn_in_y_order(true)
  entity:set_can_traverse("enemy", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("shallow_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("low_wall", true)
end

function entity:shoot(angle)
  master_script:set("trillium", "hero")
  local damage = entity.damage or 1
  local type = entity.damage_type or "physical"
  local max_distance = entity.max_distance or 700
  local speed = entity.speed or 150
  local distance_damage_reduction_rate = entity.distance_damage_reduction_rate

  if entity.rotational_sprite then
    entity:get_sprite():set_rotation(angle)
  end

  local m = sol.movement.create"straight"
  m:set_speed(speed)
  m:set_angle(angle)
  m:set_max_distance(max_distance)
  m:set_smooth(false)
  m:start(entity, function() entity:hit_obstacle() end)
  function m:on_obstacle_reached()
    entity:hit_obstacle()
  end
  function m:on_changed()
    entity:hit_obstacle()
  end

  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "hero" and hero:get_can_be_hurt() then
      if entity.harmless then return end
      entity:clear_collision_tests()
      if entity.hero_collision_callback then
        entity.hero_collision_callback()
      elseif hero.process_hit then
        hero:process_hit({damage = damage, enemy = entity, type = type})
      else
        hero:start_hurt(entity, damage)
      end
      entity:hit_obstacle()
    --Some projectiles can also damage other enemies. Because that's fun.
    elseif entity.can_damage_enemies and (other:get_type() == "enemy") and not (entity.firing_entity == other) then
      entity:clear_collision_tests()
      if other.process_hit then
        other:process_hit(damage)
      else
        other:hurt(damage)
      end
      entity:hit_obstacle()
    end
  master_script:set("default", "hero")
  end)

  --Reduce damage as the projectile goes further
  if distance_damage_reduction_rate then
    local rate_step = 10
    sol.timer.start(entity, rate_step, function()
      entity.damage = entity.damage - (distance_damage_reduction_rate)
      damage = entity.damage
      if damage <= 0 then damage = 1 entity.damage = 1 return false end
      return true
    end)
  end
  
end


function entity:hit_obstacle()
  entity:stop_movement()
  if entity.obstacle_callback then
    entity.obstacle_callback()
  else
    entity:pop_remove()
  end
end


function entity:deflect()
  entity.deflected = true
  sol.audio.play_sound("bullet_deflect")
  entity.can_damage_enemies = true
  entity.damage = (entity.damage or 1) * 4
  --game:add_magic((game:get_value("sword_magic_regen_amount") or 15) * 2)
  entity.firing_entity = nil
  entity:stop_movement()
  --[[
  --Hitstop
  game:set_suspended(true)
  sol.timer.start(game, 40, function()
    game:set_suspended(false)
  end)
  --]]
  entity:shoot(hero:get_angle(entity))
end


function entity:react_to_solforge_weapon(item)
  if entity.melee_deflect then
    entity:deflect()
  elseif entity.melee_destroy then
    entity:stop_movement()
    entity:pop_remove()
  end
end


function entity:pop_remove()
  entity:clear_collision_tests()
  local pop_sprite = entity:create_sprite("enemiestrillium//enemy_killed_projectile")
  entity:remove_sprite()
  pop_sprite:set_animation("killed", function() entity:remove() end)
end



function entity:track_hero()
  sol.timer.start(entity, 40, function()
    local m = entity:get_movement()
    if m and m:get_speed() > 0 then
      local angle = (m:get_angle())
      local target_angle = (entity:get_angle(hero))
      local step = 3
      if target_angle < angle then step = step * -1 end
      local diff = math.abs(target_angle - angle)
      if diff > math.rad(180) then
        step = step * -1
      end
      if diff < math.rad(step) then step = 0 end
      angle = angle + math.rad(step)
      entity:get_sprite():set_rotation(angle)
      m:set_angle(angle)
    end
    return true
  end)
end


function entity:set_projectile_type(type)
  master_script:set("trillium", "map")
  if type == "arrow" then
    entity.rotational_sprite = true
    entity.speed = 250
    entity.can_damage_enemies = true
    entity.melee_destroy = true

  elseif type == "bomb" then
    entity.harmless = true
    entity.speed = 180
    entity.max_distance = 100
    entity.obstacle_callback = function()
      sol.timer.start(entity, entity.fuse_length or 400, function()
        local x,y,z = entity:get_position()
        local blast = map:create_explosion({
          x=x, y=y, layer=z, sprite = entity.explosion_sprite,
        })
        blast.hero_damage = entity.damage
        blast.enemy_damage = entity.damage
        entity:pop_remove()
      end)
    end

  elseif type == "bullet" then
    entity.speed = 300
    entity.rotational_sprite = true
    entity.melee_deflect = game:get_value("ability_deflect_bullets")

  elseif type == "fireball" then
    entity.rotational_sprite = true
    entity.obstacle_callback = function()
      local x, y, z = entity:get_position()
      map:create_fire{x=x, y=y, layer=z}
      entity:pop_remove()
    end

  elseif type == "iceball" then
    entity.rotational_sprite = true
    entity.speed = 200
    entity.obstacle_callback = function()
      local ice_blast = map:create_ice_blast(entity:get_position())
      ice_blast:set_duration(200)
      entity:pop_remove()
    end

  elseif type == "lightningball" then
    entity.rotational_sprite = true
    entity.speed = 250
    entity.obstacle_callback = function()
      local x,y,z = entity:get_position()
      local blast = map:create_lightning({
        x=x, y=y, layer=z, type="lightning_zap"
      })
      entity:pop_remove()
    end

  elseif type == "webball" then
    entity.rotational_sprite = true
    entity.speed = 200
    entity.hero_collision_callback = function()
      hero:start_status_effect("webbed", 3000)
    end

  elseif type == "scatter_grenade" then
    entity.harmless = true
    entity.speed = 180
    entity.distance_variance = entity.distance_variance or 75
    entity.max_distance = 100 + math.random(entity.distance_variance * -1.2, entity.distance_variance)
    entity.fuse_variance = entity.fuse_variance or 500
    entity.fuse_length = (entity.fuse_length or 1000) + math.random(entity.fuse_variance * -1, entity.fuse_variance)
    entity.obstacle_callback = function()
      sol.timer.start(entity, entity.fuse_length or 400, function()
        local x,y,z = entity:get_position()
        local blast = map:create_explosion({
          x=x, y=y, layer=z, sprite = entity.explosion_sprite or "items/trillium/explosion_small",
        })
        blast.hero_damage = entity.damage
        blast.enemy_damage = entity.damage
        entity:pop_remove()
      end)
    end

  end
  master_script:set("default", "map")
end
