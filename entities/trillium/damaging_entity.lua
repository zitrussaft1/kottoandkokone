--An entity that damages enemies.
--[[
optional parameters to set are:
entity.damage (how much damage it causes, default 1)
entity.damage_cooldown (cooldown between hurting the same entity another time, default 200ms)
entity.duration (how long the entity lasts)
--]]

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  sol.timer.start(entity, 10, function()
    if not entity.start_without_collision then
      entity:activate_collision()
    end
  end)
end


function entity:activate_collision()
  entity.collided_entities = {}
  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "enemy" and not entity.collided_entities[other] then
      entity.collided_entities[other] = other
      sol.timer.start(entity, entity.damage_cooldown or 200, function() entity.collided_entities[other] = nil end)
      other:hurt(entity.damage or 1)
    end
  end)

  --determine removal
  sol.timer.start(entity, 10, function() --wait 10ms so the code that created the entity can set a duration
    if entity.duration then
      sol.timer.start(entity, entity.duration, function() entity:remove() end)
    else
      local sprite = entity:get_sprite()
      function sprite:on_animation_finished() entity:remove() end
    end
  end)
end