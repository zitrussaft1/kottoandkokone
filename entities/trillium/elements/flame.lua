local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local sprite

function entity:on_created()
  entity:set_size(16,16)
  entity:set_drawn_in_y_order(true)
  entity:set_follow_streams(true)
  entity:set_can_traverse_ground("shallow_water", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  local burn_duration = entity:get_property("burn_duration") or 1000

  --Animate, then burn out
  sprite = entity:create_sprite("elementstrillium//fire")
  sprite:set_animation("fire")
  sol.timer.start(entity, burn_duration, function()
    sprite:set_animation("fire_" .. math.random(1,2), function()
      entity:remove()
    end)
  end)

  --Interact with other entities
  map.burned_entities = map.burned_entities or {}

  entity:add_collision_test("sprite", function(entity, other_entity, fire_sprite, other_entity_sprite)
    if map.burned_entities[other_entity] then return end
    if other_entity:get_type() == "hero" then return end --hero collides based on overlapping collision, not sprite collision

    if other_entity.react_to_fire and not map.burned_entities[other_entity] then
      other_entity:react_to_fire(entity)

    elseif other_entity.can_burn or other_entity:get_property("can_burn")
    and not map.burned_entities[other_entity] then
      sol.timer.start(entity, 500, function()
        local x, y, z = other_entity:get_position()
        other_entity:remove()
        map:propagate_fire(x, y, z)
      end)
    end

    --only check this once per entity
    map.burned_entities[other_entity] = true
    sol.timer.start(map, 600, function() map.burned_entities[other_entity] = false end)
  end)


  --Special collision with hero for damage
  entity:add_collision_test("overlapping", function(entity, other_entity)
    if other_entity:get_type() == "hero" and not other_entity:is_blinking() and not entity.harmless_to_hero then
      if other_entity.react_to_fire then other_entity:react_to_fire(entity)
      else other_entity:start_hurt(entity, (game:get_value"fire_damage" or 1)) end
    end
  end)

end