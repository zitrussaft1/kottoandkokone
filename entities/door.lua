-- Lua script of custom entity door.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

local door_opened = true
local door_closing = false

function entity:open_door()
  self:set_traversable_by("hero", true)
  self:set_traversable_by("enemy", true)
  self:set_visible(false)
  sol.audio.play_sound("door_open")
  door_opened = true
end

function entity:close_door()
  self:set_traversable_by("hero", false)
  self:set_traversable_by("enemy", false)
  self:set_visible(true)
  sol.audio.play_sound("door_closed")
  door_opened = false
end
-- Event called when the custom entity is initialized.
function entity:on_created()
  self:close_door()
  -- Initialize the properties of your custom entity here,
  -- like the sprite, the size, and whether it can traverse other
  -- entities and be traversed by them.
end
