local entity = ...
local game = entity:get_game()

--Shared NPC. The dialog pointer = the name of the entity in the map (in the editor).
--If you want all NPC to have a movement then create the movement in on_created()

function entity:on_created()
-- here, self is the entity
self:set_size(32,32) -- only multiple of 8 are allowed
self:set_traversable_by("hero", false)
self:get_sprite():set_frame(1)
    self:get_sprite():set_paused(true)
end


function entity:on_interaction()
  self:set_direction(game:get_hero():get_direction() / 2)
  game:set_life(game:get_max_life())
  local quest = game:get_value("current_quest")
  game:start_dialog(self:get_name(), function ()
    if quest == 1 and entity:get_name() == "quest_mike" then
      game:start_dialog("battle.anger.intro", function ()
        game:get_hero():teleport("battle/battle_test_map")
      end)
    elseif quest == 2 and entity:get_name() == "quest_miyuki" then
      game:set_value("current_quest", 3)
      game:start_dialog("MiyukiQuest.intro", function ()
        unlock_card("good_advise")
      end)
    elseif quest == 3 and entity:get_name() == "quest_karen" then
      game:start_dialog("battle.greed.intro", function ()
        game:get_hero():teleport("battle/greed_map")
      end)
    elseif quest == 4 and entity:get_name() == "quest_enzo" then
      game:set_value("current_quest", 5)
      game:start_dialog("EnzoQuest.intro", function ()
        unlock_card("jester")
      end)
    elseif quest == 5 and entity:get_name() == "quest_miyuki" then
      game:start_dialog("battle.lust.intro", function ()
        game:get_hero():teleport("battle/lust_map")
      end)
    elseif quest == 6 and entity:get_name() == "quest_luna" then
      game:start_dialog("DialogosLuna.LunIintro")
    end
  end)
end
