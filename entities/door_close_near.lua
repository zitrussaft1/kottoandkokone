-- Lua script of custom entity door_close_near.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local entity = ...
local game = entity:get_game()
local map = entity:get_map()


local door_opened = true
local door_closing = false

function entity:open_door()
  self:set_traversable_by("hero", true)
  self:set_visible(false)
  sol.audio.play_sound("door_open")
  door_opened = true
end

function entity:close_door()
  self:set_traversable_by("hero", false)
  self:set_visible(true)
  sol.audio.play_sound("door_closed")
  door_opened = false
end

local function check_position(door, hero)
  if door:get_distance(hero) < 90 and door_opened and not door_closing then
    door_closing = true
    sol.timer.start(door, 700, function()
      door_closing = false
      door:close_door()
    end)
  elseif door:get_distance(hero) > 90 and not door_opened then
    door:open_door()
  end
  sol.timer.start(door, 100, function()
    check_position(door, hero)
  end)
end

-- Event called when the custom entity is initialized.
function entity:on_created()
  self:open_door()
  check_position(self, map:get_hero())
  self:add_collision_test("touching", function(door, other)
    if other:get_type() == "hero" then
      local hero_x, hero_y, hero_z = other:get_position()
      -- After 1 second if the hero is still touching the door, move him to the start of the map.
      sol.timer.start(door, 1000, function()
        local new_hero_x, new_hero_y, new_hero_z = other:get_position()
        if hero_x == new_hero_x and hero_y == new_hero_y and hero_z == new_hero_z then
          other:teleport("tutorial")
        end
      end)
    end
  end)
end
