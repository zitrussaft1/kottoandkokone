-- Lua script of item bomb.
-- This script is executed only once for the whole game.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
    item:set_savegame_variable("bomb")
    item:set_assignable(true)
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.
end

-- Event called when the hero starts using this item.
function item:on_using()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local dx = {[0]=16, [1]= 0, [2]=-16, [3]=0}
  local dy = {[0]=0, [1]=-16, [2]=0, [3]=16}
  local bomb = map:create_custom_entity{
    name="bomb",
    width=16, height=16, direction=0,
    layer = z,
    x = x + dx[hero:get_direction()],
    y = y + dy[hero:get_direction()],
    model = "bomb"
  }
  game:set_value("bomb", game:get_value("bomb") - 1)
  sol.timer.start(game, 5000, function()
        game:set_tint_map({255,255,255})
        GAME_SPEED = 1
        game:set_value("bomb", game:get_value("bomb") + 1)
    end)
    item:set_finished()
end
