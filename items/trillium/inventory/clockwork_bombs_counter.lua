local master_script = require("scripts/meta/master")
local item = ...
local game = item:get_game()
local bomb_speed = 140
local bomb_max_distance = 240

item:register_event("on_created", function(self)
  master_script:set("trillium", "item")
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  item:set_savegame_variable("possession_clockwork_bombs_counter")
  item:set_amount_savegame_variable("amount_clockwork_bombs_counter")
  item:set_assignable(true)
  item:set_amount_savegame_variable("amount_clockwork_bombs_counter")
  item:set_max_amount(999)
  item:set_amount(20)
  item:set_ammo("_amount")
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  master_script:set("default", "item")
end)


item:register_event("on_using", function(self)
  master_script:set("trillium", "item")
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  if item:try_spend_ammo() then
    local x, y, layer = item:summon_bomb()
    sol.audio.play_sound("bomb")
    item:set_finished()
  else
    item:set_finished()
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
  master_script:set("default", "item")
end)

function item:summon_bomb()
  master_script:set("trillium", "game")
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local bomb = map:create_custom_entity{
    width=16, height=16, direction=direction,
    layer = z,
    x = x + game:dx(16)[direction],
    y = y + game:dy(16)[direction],
    sprite = "entities/trillium/clockwork_bomb",
  }
  bomb:set_can_traverse("enemy", false)
  local m = sol.movement.create"straight"
  m:set_angle(direction * math.pi / 2)
  m:set_speed(bomb_speed)
  m:set_max_distance(bomb_max_distance)
  m:start(bomb, function()
    item:explode(bomb)
  end)
  function m:on_obstacle_reached()
    item:explode(bomb)
  end
  master_script:set("default", "game")
end


function item:explode(bomb)
  local map = bomb:get_map()
  local x, y, z = bomb:get_position()
  bomb:remove()
  map:create_explosion{x=x, y=y, layer=z}
  sol.audio.play_sound"explosion"
end