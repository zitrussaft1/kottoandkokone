local master_script = require("scripts/meta/master")
local item = ...
local game = item:get_game()

local config = require("items/trillium/inventory/library/hookshot_config.lua")
local hookshot_manager = require("items/trillium/inventory/library/hookshot_manager.lua")

function item:on_started()
  item:set_savegame_variable("possession_hookshot")
  item:set_assignable(true)
end


function item:on_using()
  master_script:set("trillium", "switch")
  master_script:set("trillium", "sensor")
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()

  hero:set_animation"hookshot"
  local hook = item:create_hook()
  item.hook = hook
  hero:start_state(item:get_casting_state())
  local m = sol.movement.create"straight"
  m:set_angle(hero:get_direction() * math.pi / 2)
  m:set_speed(config.speed)
  m:set_max_distance(config.distance)
  m:set_smooth(false)
  m:start(hook, function() item:retract_hook() end)
  function m:on_obstacle_reached()
    item:retract_hook()
    sol.audio.play_sound"sword_tapping"
  end
  --Collision with entities
  hook:add_collision_test("touching", function(hook, entity)
    local hook_direction = hook:get_sprite():get_direction()
    if hook_direction ~= hook:get_direction4_to(entity) then return end
    local type = entity:get_type()
    if entity.hookshot_target or entity:get_property"hookshot_target"
    or (entity.is_hookable and entity:is_hookable()) then
      hook:stop_movement()
      hook:clear_collision_tests()
      sol.audio.play_sound"arrow_hit"
      item:pull_hero(hook)
    elseif entity.hookshot_catchable or entity:get_property"hookshot_catchable"
    or (entity.is_catchable_with_hookshot and entity.is_catchable_with_hookshot()) then
      item:catch_entity(entity)
      hook:stop_movement()
      hook:clear_collision_tests()
      item:retract_hook()
    elseif entity.react_to_hookshot then
      entity:react_to_hookshot()
    elseif entity:get_type() == "enemy" then
      entity:hurt(item:get_damage())
  master_script:set("default", "bush")
    end
  master_script:set("default", "enemy")
  master_script:set("default", "sensor")
  master_script:set("default", "switch")
  end)
  --Special test for some entities
  hook:add_collision_test("overlapping", function(hook, entity)
    local type = entity:get_type()
    if type == "crystal" then
      hook:clear_collision_tests()
      item:retract_hook()
      sol.audio.play_sound("switch")
      map:change_crystal_state()
    elseif type == "switch" then
      if entity:is_walkable() then return end
      hook:clear_collision_tests()
      item:retract_hook()
      sol.audio.play_sound("switch")
      if entity:is_activated() then
        entity:set_activated(false)
        if entity.on_inactivated then entity:on_inactivated() end
      else
        entity:set_activated(true)
        if entity.on_activated then entity:on_activated() end
      end
    end
  end)
end


  master_script:set("trillium", "enemy")
function item:retract_hook()
  master_script:set("trillium", "bush")
  local hero = game:get_hero()
  local hook = item.hook
  hook:clear_collision_tests()
  if hook:get_distance(hero) <= 16 then
    item:unhook()
    return
  end
  sol.timer.start(hook, config.slack_delay or 120, function() item.rope_sprite:set_animation"rope_slack" end)
  local m = sol.movement.create"straight"
  m:set_max_distance(hook:get_distance(hero))
  m:set_angle(hook:get_angle(hero))
  m:set_ignore_obstacles(true)
  m:set_speed(config.speed)
  m:start(hook, function()
    item:unhook()
  end)
  sol.timer.start(hero, 2500, function()
    if hook:exists() then
      item:unhook()
    end 
  end)
end


function item:pull_hero(hook)
  local hero = game:get_hero()
  if hero:get_distance(hook) <=8 then
    item:unhook()
    return
  end
  hero:set_animation"hookshot_pulling"
  sol.timer.start(hook, config.slack_delay or 120, function() item.rope_sprite:set_animation"rope_slack" end)
  local state = item:get_hookshot_state()
  hero:start_state(state)
  local previous_positions = {}
  previous_positions[1] = {hero:get_position()}
  local m = sol.movement.create"straight"
  m:set_max_distance(hero:get_distance(hook))
  m:set_angle(hero:get_angle(hook))
  m:set_speed(config.speed)
  m:start(hero, function()
    item:unhook()
  end)
  function m:on_obstacle_reached()
    item:unhook()
  end
  function state:on_position_changed()
    previous_positions[#previous_positions + 1] = {hero:get_position()}
  end
  function state:on_finished()
    if item:test_obstacles_below(hero:get_position()) then
      --iterate backward through previous_positions until a safe one is found
      local safe_position = previous_positions[1] --we know the starting one is safe
      for i = #previous_positions, 1, -1 do
        local x, y, z = previous_positions[i][1],previous_positions[i][2],previous_positions[i][3] 
        local is_obstacle = item:test_obstacles_below(x, y, z)
        if not is_obstacle then
          safe_position = {x, y, z} break
        end
      end
      sol.timer.start(hero, 50, function()
        hero:set_position(safe_position[1], safe_position[2], safe_position[3])
        hero:set_blinking(true, 200)
      end)
    end
  end
end


function item:catch_entity(entity)
  local hook = item.hook
  hook:register_event("on_position_changed", function()
    entity:set_position(hook:get_position())
  end)
end


function item:create_hook()
  local map = game:get_map()
  local hero = game:get_hero()
  local x, y, z = hero:get_position()
  local direction = hero:get_direction()
  local hook = map:create_custom_entity{
    x=x, y=y, layer=z, direction = direction, width = 16, height=16, sprite="items/trillium/hookshot"
  }
  hook:get_sprite():set_animation"hook"
  hook:set_drawn_in_y_order(true)
  hook:set_can_traverse_ground("hole", true)
  hook:set_can_traverse_ground("deep_water", true)
  hook:set_can_traverse_ground("shallow_water", true)
  hook:set_can_traverse_ground("lava", true)
  hook:set_can_traverse_ground("prickles", true)
  hook:set_can_traverse_ground("low_wall", true) --TODO: eh? eh? Maybe? What happens with this?
  hook:set_can_traverse("hero", true)
  hook:set_can_traverse("crystal", true)
  hook:set_can_traverse("jumper", true)
  hook:set_can_traverse("stairs", true)
  hook:set_can_traverse("stream", true)
  hook:set_can_traverse("switch", true)
  hook:set_can_traverse("teletransporter", true)

  local direction_offset_x = {[0]=0, [1]=1, [2]=1, [3]=1}
  local direction_offset_y = {[0]=-6, [1]=-2, [2]=-5, [3]=-2}
  local rope_sprite = hook:create_sprite"itemstrillium//hookshot"
  item.rope_sprite = rope_sprite
  rope_sprite:set_animation"rope_taut"
  rope_sprite:set_transformation_origin(0,1)
  rope_sprite:set_xy(0, direction_offset_y[direction])
  function hook:on_pre_draw()
    local rope_length = 96
    rope_sprite:set_scale((hook:get_distance(hero) - 4) / rope_length, 1)
    rope_sprite:set_rotation(hook:get_angle(hero))
  end
  return hook
end


function item:unhook()
  local hero = game:get_hero()
  local hook = item.hook
  hook:remove()
  hero:unfreeze() --to end hookshot state
end



function item:get_damage()
  local damage
  damage = game:get_value"hookshot_damage" or 2
  return damage
end


function item:test_obstacles_below(x, y, z)
  local map = game:get_map()
  local hero = game:get_hero()
  local hx, hy, hz = hero:get_position()
  if hero:test_obstacles(x - hx, y - hy, z - hz) then
    return true
  elseif map:get_ground(x, y, z) == "empty" and (map:get_min_layer() > z) then
    return item:test_obstacles_below(x, y, z - 1)
  else
    return false
  end
end


function item:get_casting_state()
  local state = sol.state.create("hookshot_casting")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_be_hurt(true)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(true)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  return state
end


function item:get_hookshot_state()
  local state = sol.state.create("hookshot")
  state:set_can_control_direction(false)
  state:set_can_control_movement(false)
  state:set_can_traverse_ground("hole", true)
  state:set_can_traverse_ground("deep_water", true)
  state:set_can_traverse_ground("lava", true)
  state:set_affected_by_ground("hole", false)
  state:set_affected_by_ground("deep_water", false)
  state:set_affected_by_ground("lava", false)
  state:set_gravity_enabled(false)
  state:set_can_come_from_bad_ground(false)
  state:set_can_be_hurt(false)
  state:set_can_use_sword(false)
  state:set_can_use_item(false)
  state:set_can_interact(false)
  state:set_can_grab(false)
  state:set_can_push(false)
  state:set_can_pick_treasure(true)
  state:set_can_use_teletransporter(false)
  state:set_can_use_switch(false)
  state:set_can_use_stream(false)
  state:set_can_use_stairs(false)
  state:set_can_use_jumper(false)
  state:set_carried_object_action("throw")
  return state
end