local manager = {}

--Takes an intial argument, game, that is the game object
--that the item exists within.
--Takes a secondary argument, weapon, that is the colliding weapon.
--weapon is a string that is equal to the item.item_id.
--Takes a tertiary argument, other_entity, that is the entity the weapon is colliding with.
--Checks the durability of the weapon
function manager:check_durability(game, weapon, other_entity)
  local durability = game:get_value(weapon .. "_durability")    ---Think of this as the Health Points of the Weapon.
  local degradation = game:get_value(weapon .. "_degradation")    ---This is the amount of "damage" the Durability takes on collision.

  if durability ~= nil then
    if degradation == nil then 
      game:set_value(weapon .. "_degradation", 0)
      degradation = game:get_value(weapon .. "_degradation")
    end
    local new_durability = durability - degradation
    if new_durability < 0 then new_durability = 0 end
    game:set_value((weapon .. "_durability"), (new_durability))

  end
end

return manager