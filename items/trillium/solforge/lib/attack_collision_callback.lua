local master_script = require("scripts/meta/master")
--Created by Max Mraz and J. Cournoyer, licensed with the MIT license
--This script manages what happens when a solforge weapon hits something.
--Alter this script in whatever way suits your game, default behavior provided will hurt enemies
--The solforge weapon is passed to the process_collision function as item, and the attack that collided as attack

local manager = {}

function manager:process_collision(props)
  local game = props.game
  local map = props.map
  local hero = props.hero
  local item = props.item --the item itself
  local attack = props.attack --the current attack
  local weapon_entity = props.weapon_entity --the custom entity of the weapon
  local other_entity = props.other_entity --the entity registering the sprite collision
  local weapon_sprite = props.weapon_sprite --the sprite of the weapon entity
  local other_sprite = props.other_sprite --the sprite of the other entity


  --Allow any entity to define its own behavior when getting hit:
  --For example, if you have a custom entity that can be destroyed, you can use this method
  if other_entity.react_to_solforge_weapon then other_entity:react_to_solforge_weapon(item) end


  --ENEMY
  --In this implementation, I'm allowing weapon attacks to have an `attack_power_bonus` value to be added to the attack power.
  --That's the case with some default weapons included, but you can just as easily create weapons that have a combo_percentage value,
  --Which adds a percent of the attack_power savegame value, so its as effective as you upgrade.
  --Or you could have a ohko_chance that increases every attack, which is a percent chance to just kill the enemy.
  --Point is, you can add any arbitrary value to attacks or to the weapons that you want available when hitting stuff.
  if other_entity:get_type() == "enemy" then
    local attack_power = (game:get_value(item.item_id .. "_attack_power") or 1) + (attack.attack_power_bonus or 0)

    --The following line will update the durability before dealing damage to the enemy. 
    require("items/trillium/solforge/lib/durability_manager"):check_durability(game, item.item_id, other_entity)

    --Following function will take into account enemy's attack consequence settings,etc.
    manager:initiate_attack_consequence(other_entity, item, weapon_entity, attack_power)

   --Check if the enemy should push the hero back when struck; if so, move hero away from enemy slightly.
    if other_entity:get_push_hero_on_sword() then
      hero:stop_movement()
      local m = sol.movement.create"straight"
      m:set_speed(128)
      m:set_max_distance(64)
      m:set_angle(other_entity:get_angle(hero))
      m:start(hero)
    end

    if game:get_value("skill__frenzy") == 1 then
      if game:get_value("skill__frenzy__on_cooldown") == 0 then
        local stacks = game:get_value("skill__frenzy__stack")
        if stacks == 4 then
          game:set_value("skill__frenzy__activated", 1)
          game:set_value("skill__frenzy__on_cooldown", 1)

          BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED + 0.5
          -- Start a timer
          sol.timer.start(game, 6000, function()
            BONUS_ATTACK_SPEED = BONUS_ATTACK_SPEED - 0.5
            game:set_value("skill__frenzy__activated", 0)
            game:set_value("skill__frenzy__on_cooldown", 0)
            game:set_value("skill__frenzy__stack", 0)
          end)
          -- to implemented
        else
          game:set_value("skill__frenzy__stack", stacks + 1)
          -- Start a timer
          game:set_value("skill__frenzy__on_cooldown", 1)
          sol.timer.start(game, 1500, function()
            game:set_value("skill__frenzy__on_cooldown", 0)
          end)
        end
      end
    end

  end

  --Replicate the engine sword's built-in behavior for things like switches, destrictibles, etc.
  require("items/trillium/solforge/lib/boilerplate_attack_collision"):check_collision({
    game = game, map = map, hero = hero,
    other_entity = other_entity, other_sprite = other_sprite,
  })

end



--Hurt Enemies:
function manager:initiate_attack_consequence(enemy, item, weapon_entity, attack_power)
  master_script:set("trillium", "enemy")
  local game = enemy:get_game()
  local hero = game:get_hero()
  local item_id = item.item_id
  local attack_consequence = enemy:get_attack_consequence"sword"

  --allow weapons to define a callback for hitting an enemy, good for applying status effects, etc
  if item.enemy_hit_callback then
    item:enemy_hit_callback(enemy)
  end

  --Trigger event for charms to catch
  game.charm_manager:trigger_event("on_enemy_attacked")

  --Shake the screen a bit when you hit one
  --game:get_map():get_camera():shake{shake_count = 4, zoom_scale = 1, amplitude = 1,}  

  if enemy.process_hit then --allow enemies to define their own getting hurt methods
    enemy:process_hit(attack_power)
  elseif type(attack_consequence) == "number" then
    enemy:hurt(attack_power)
  elseif type(attack_consequence) == "function" then
    attack_consequence()
  else
    --If the attack_consequence is not a number or a function, then it is a string.
    if attack_consequence == "ignored" then
      --Do nothing. Enemy ignores the collision and damage.
    elseif attack_consequence == "protected" then
      if game:get_value(item_id .. "_attack_blocked_animation") and game:get_value(item_id .. "_attack_blocked_sound") then 
        weapon_entity:get_sprite():set_animation(game:get_value(item_id .. "_attack_blocked_animation"))
        sol.audio.play_sound(game:get_value(item_id .. "_attack_blocked_sound"))
      end
    elseif attack_consequence == "immobilized" then
      enemy:immobilize()
    elseif attack_consequence == "custom" then
      enemy:hurt(attack_power)
    end

  end

  master_script:set("default", "enemy")
end






return manager