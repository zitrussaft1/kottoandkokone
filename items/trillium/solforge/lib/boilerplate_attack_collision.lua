local master_script = require("scripts/meta/master")
--[[
Written by Max Mraz and J. Cournoyer, licensed MIT
This function imitates the engine built-in sword's behavior when it comes to hitting things other than enemies
--]]

local manager = {}

local function process_hit()

end




function manager:check_collision(props)
  master_script:set("trillium", "switch")
  master_script:set("trillium", "sensor")
  local game = props.game
  local map = props.map
  local hero = props.hero
  local other_entity = props.other_entity
  local other_sprite = props.other_sprite

  --DESTRUCTIBLE
  if other_entity:get_type() == "destructible" then
    local x,y,z = other_entity:get_position()

    --If it blows up, explode it
    if other_entity:get_can_explode() then
      map:create_explosion{x=x, y=y, layer=z}
      sol.audio.play_sound"explosion"
      if other_entity.on_exploded then other_entity:on_exploded() end
      other_entity:remove()

    --If it can be cut, cut it
    elseif other_entity:get_can_be_cut() then
      if other_entity:get_destruction_sound() then
        sol.audio.play_sound(other_entity:get_destruction_sound())
      end
      if other_entity:get_treasure() then
        local tname, tvar, tsave = other_entity:get_treasure()
        map:create_pickable{
          x=x, y=y, layer=z,
          treasure_name = tname, treasure_variant = tvar, treasure_savegame_variable = tsave,
        }
      end
      if other_sprite:has_animation("destroy") then
        other_sprite:set_animation("destroy", function()
          other_entity:remove()
        end)
      else
        other_entity:remove()
      end
      if other_entity.on_cut then other_entity:on_cut() end
    end
  end


  --SWITCH
  --Note: due to lack of accessor methods, there's no way to know if a switch is solid or arrow type
  --Therefore, this toggles ALL switches (other than walkable), even arrow type
  --Arrow type is a bit depricated anyway, as they only respond to built-in arrows. Custom arrows have no way to know either.
  if other_entity:get_type() == "switch"
    and not other_entity:is_walkable() then
    local switch = other_entity
    sol.audio.play_sound("switch")
    switch:set_activated(not switch:is_activated())
    if switch:is_activated() then
      other_sprite:set_animation("activated")
      if switch.on_activated then switch:on_activated() end
    else
      other_sprite:set_animation("inactivated")
      if switch.on_inactivated then switch:on_inactivated() end
    end
  end


  --Flip Crystal switches
  if other_entity:get_type() == "crystal" and not other_entity.solforge_activation_cooldown then
    sol.audio.play_sound("switch")
    map:change_crystal_state()
  end
  master_script:set("default", "sensor")
  master_script:set("default", "switch")
end

return manager