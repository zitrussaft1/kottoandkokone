local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  local m = sol.movement.create("straight")
  local direction = hero:get_direction()
  m:set_angle(direction * math.pi / 2)
  m:set_max_distance(4)
  m:start(hero)
end

require("items/trillium/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        windup_duration = 100,
        hero_windup_animation = "sword",
        hero_attack_animation = "sword",
        weapon_sprite = "solforge_weapons/trillium/sword_1",
        weapon_windup_animation = "swing_windup",
        weapon_attack_animation = "swing",
        weapon_sound = "sword1",
        --callback = callback,
      },
      {
        hero_attack_animation = "sword",
        weapon_sprite = "solforge_weapons/trillium/sword_1",
        weapon_attack_animation = "reverse_swing",
        weapon_sound = "sword1",
        --callback = callback,
      },
      {
        windup_duration = 80,
        hero_windup_animation = "sword",
        hero_attack_animation = "sword",
        weapon_sprite = "solforge_weapons/trillium/sword_1",
        weapon_windup_animation = "swing_windup",
        weapon_attack_animation = "swing",
        weapon_sound = "sword1",
        --attack_power_bonus = 1,
      },
    },


    weapon_parameters = {
      attack_power = 0,
    },

  }
)