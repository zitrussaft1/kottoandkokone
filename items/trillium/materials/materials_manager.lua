local manager = {}

local all_materials = {}

animals = sol.main.get_items_in_directory("items/trillium/materials/animal")
ingredients = sol.main.get_items_in_directory("items/trillium/materials/ingredients")
minerals = sol.main.get_items_in_directory("items/trillium/materials/mineral")
plants = sol.main.get_items_in_directory("items/trillium/materials/plant")
local all_tables = {animals, ingredients, minerals, plants}
for _, mat_table in pairs(all_tables) do
  for _, material in pairs(mat_table) do
    all_materials[material] = material
  end
end

function manager:get_all_materials()
  return all_materials
end


function manager:init(game)
  --Create common behavior for all materials
  for _, item_entry in pairs(all_materials) do
    local item = game:get_item(item_entry)
    local save_name = item:get_name():gsub("/", "_")
    item:set_savegame_variable("possession_" .. save_name)
    item:set_amount_savegame_variable("amount_" .. save_name)
    --print("For item:", item:get_name(), "set savegame variable", item:get_savegame_variable())
    item:set_brandish_when_picked(not game:has_item(item:get_name()))

    function item:on_obtaining(variant)
      item:set_brandish_when_picked(not game:has_item(item:get_name()))
      item:add_amount(variant)
    end

    function item:on_using()
      if item.heal_amount then
        if item:has_amount(1) then
          game:add_life(item.heal_amount)
          item:remove_amount(1)
        else
          sol.audio.play_sound"wrong"
        end
      end
      item:set_finished()
    end
  end
end


return manager