local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
    item:set_savegame_variable("time_slow")
    item:set_assignable(true)
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.
end

-- Event called when the hero starts using this item.
function item:on_using()
    game:set_tint_map({127,127,0})
    GAME_SPEED = 0.5
    game:set_value("time_slow", 0)
    local map = item:get_map()
    for bullet in map:get_entities("bullet") do
        bullet:get_movement():set_speed(bullet:get_movement():get_speed() * GAME_SPEED)
    end
    sol.timer.start(game, 5000, function()
        game:set_tint_map({255,255,255})
        GAME_SPEED = 1
    end)
    sol.timer.start(game, 12000, function()
        game:set_value("time_slow", 1)
    end)
    item:set_finished()
end