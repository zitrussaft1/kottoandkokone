local item = ...
local game = item:get_game()

-- Event called when all items have been created.
function item:on_started()
    item:set_savegame_variable("light_beam")
    item:set_assignable(true)
  -- Initialize the properties of your item here,
  -- like whether it can be saved, whether it has an amount
  -- and whether it can be assigned.
end

-- Event called when the hero starts using this item.
function item:on_using()
    local light_beam = require("scripts/menus/using_light_beam")
    
    if sol.menu.is_started(light_beam) then
        sol.menu.stop(light_beam)
        game:get_hero():unfreeze()
    else
        light_beam:set_angle(game:get_hero():get_direction() * -math.pi / 2)
        light_beam:can_quit(false)
        light_beam:set_focus(true)
        sol.menu.start(game, light_beam)
        -- Stop the hero from moving.
        game:get_hero():freeze()
        game:get_hero():unfreeze()
        game:get_hero():stop_movement()
    end
    item:set_finished()
end