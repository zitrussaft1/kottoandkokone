# Check if directory is provided as argument
if [ $# -eq 0 ]; then
  echo "Usage: $0 <directory>"
  exit 1
fi

# Iterate through each file in the directory
for file in "$1"/*; do
  if [ -f "$file" ]; then
    filename=$(basename -- "$file")
    filename="${filename%.*}"
    output_file="$1/${filename}.dat"

    # Generate animation file content
    echo "animation{" > "$output_file"
    echo "  name = \"$filename\"," >> "$output_file"
    echo "  src_image = \"npc/Sprites/$filename\"," >> "$output_file"
    echo "  frame_delay = 150," >> "$output_file"
    echo "  frame_to_loop_on = 0," >> "$output_file"
    echo "  directions = {" >> "$output_file"

    # Generate directions content
    directions=(
      "{ x = 0, y = 64, frame_width = 32, frame_height = 32, origin_x = 16, origin_y = 19, num_frames = 3 }"
      "{ x = 0, y = 96, frame_width = 32, frame_height = 32, origin_x = 16, origin_y = 19, num_frames = 3 }"
      "{ x = 0, y = 32, frame_width = 32, frame_height = 32, origin_x = 16, origin_y = 19, num_frames = 3 }"
      "{ x = 0, y = 0, frame_width = 32, frame_height = 32, origin_x = 16, origin_y = 19, num_frames = 3 }"
    )
    
    # Iterate through each direction
    for direction in "${directions[@]}"; do
      echo "    $direction," >> "$output_file"
    done

    echo "  }," >> "$output_file"
    echo "}" >> "$output_file"
    echo "" >> "$output_file"
  fi
done
