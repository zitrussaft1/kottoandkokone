-- Lua script of enemy cookie.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy:register_event("on_restarted", function()
  enemy:set_traversable(false)
end)

function enemy:final_attack()
  self:stop_movement()
  self:get_map():get_camera():shake({count = 16, amplitude = 10, speed = 40})
  sol.audio.play_sound("enemy_killed")
  if map:get_round() < 2 then
    self:wrap_ground_attack(0, 192, false, 1, 4, 0, function()
      sol.audio.play_sound("thunder2")
      -- Do wrap_rain_objects 4 times
      self:wrap_rain_objects(60, 40, 5, 1, 1.5, function()
        self:wrap_rain_objects(60, 40, 5, 1, 1.5, function()
          self:wrap_rain_objects(60, 40, 5, 1, 1.5, function()
            self:wrap_rain_objects(60, 40, 5, 1, 1.5, function()
              self:set_property("invincible", "false")
              self:get_sprite():set_direction(0)
              self:set_property("final_attack", "true")
              self:confuse(100, true)
            end)
          end)
        end)
      end)
    end)
  else
    self:wrap_ground_attack(0, 192, false, 1, 3.5, 0, function()
      sol.audio.play_sound("thunder1")
      -- Do wrap_rain_objects 4 times
      self:wrap_rain_objects(20, 92, 5, 1, 1, function()
        self:wrap_rain_objects(20, 92, 5, 1, 1, function()
          self:wrap_rain_objects(20, 92, 5, 1, 1, function()
            self:wrap_rain_objects(20, 92, 5, 1, 1, function()
      self:wrap_rain_objects(20, 92, 5, 1, 1, function()
        self:wrap_rain_objects(20, 92, 5, 1, 1, function()
          self:wrap_rain_objects(20, 92, 5, 1, 1, function()
            self:wrap_rain_objects(20, 92, 5, 1, 1, function()
      self:wrap_dash_wall(nil, 2, 1, function()
        self:wrap_rain_objects(10, 128, 0, 1, 1.5, function()
          self:set_property("invincible", "false")
          self:set_property("final_attack", "true")
          self:get_sprite():set_direction(0)
          self:confuse(100, true)
        end)
      end)
            end)
          end)
        end)
      end)
            end)
          end)
        end)
      end)
    end)
  end
end

enemy.default_on_hurt = enemy.on_hurt
function enemy:on_hurt(attack)
  self:default_on_hurt(attack)
  if self:get_property("invincible") == "true" then
    return
  end
  if self:get_property("final_attack") == "true" then
    print("next round", map:get_round())
    if map:get_round() < 3 then
      self:set_property("final_attack", "false")
      map:next_round()
      --self:decide_action()
    else
      self:remove_life(100)
    end
  else
    print((self:get_life() - 100) / tonumber(self:get_property("max_health") - 100))
    print((3-map:get_round())/3)
    if (self:get_life() - 100) / tonumber(self:get_property("max_health") - 100) <= (3-map:get_round())/3 then
      print("fallen")
      self:set_property("invincible", "true")
      self:set_property("final_attack", "doing")
    end
  end
end

function enemy:cookie_decide_action()
  --self:set_animation("walking")
  self:set_traversable(false)
  if self:get_property("final_attack") == "doing" then
    self:final_attack()
    return
  end
  -- For a few seconds stop moving and look at the hero.
  self:stop_movement()

  local function rain_objects(callback, args)
    self:wrap_rain_objects(15, 48, 0, 1, 1.5, function()
      callback(args)
    end)
  end

  local function keep_swiping(size, seconds, rate, distance, callback, args)
    if seconds == nil then
      seconds = 1
    elseif seconds < 0.5 then
      seconds = 0.5
    end
    local hero = self:get_map():get_hero()
    if self:get_distance(hero) < distance then
      self:look_at_player(0.1, function()
        self:wrap_ground_attack(0, size, true, 1, seconds/2, seconds/2, function ()
            keep_swiping(size,seconds + seconds * rate, rate, distance, callback, args)
        end)
      end)
    else
      callback(args)
    end
  end

  local function swipe(size,callback,args)
    self:look_at_player(0.1, function()
      self:wrap_ground_attack(0, size, true, 1, 0.5, 0.5, function()
        keep_swiping(size, 1, -0.1, size, function ()
          callback(args)
        end)
      end)
    end)
  end

  local function ground_attack(callback, args)
    self:look_at_player(0.1, function()
      self:wrap_ground_attack(0, 72, false, 1, 0.5, 0.5, function ()
        sol.timer.start(self, TIME(1000), function()
          local hero = self:get_map():get_hero()
          if self:get_distance(hero) < 100 then
            self:wrap_ground_attack(72, 96, false, 1, 1, 0.5, function ()
              callback(args)
            end)
          else
            callback(args)
          end
        end)
      end)
    end)
  end

  local function wall_rush(callback, args)
    self:look_at_player(2, function()
      self:set_can_attack(true)
      self:set_damage(
        self:get_adamage()
      )
      self:wrap_dash_wall(nil, 2, 1, function ()
        self:set_can_attack(false)
        self:set_damage(0)
        callback(args)
      end)
    end)
  end

  local function follow_hero(callback, args)
    self:follow_hero(2, function ()
      callback(args)
    end)
  end

  local function bite(callback, args)
    self:look_at_player(0.1, function()
      self:wrap_bite(nil, 1, function (biten, args)
        if biten then 
          self:decide_action()
        else
          print("Stunned")
          self:wait(1, function()
            callback(args)
          end)
        end
      end)
    end)
  end

  local hero = self:get_map():get_hero()
  local distance = self:get_distance(hero)
  if distance > 160 then
    wall_rush(function()
      rain_objects(function()
        self:decide_action()
      end)
    end)
  else
    if distance < 72 then
      local random = math.random(0, 6)
      if random == 4 then
        print("Swipe")
        swipe(72, function()
            self:decide_action()
        end)
      elseif random == 5 then
        print("Ground attack")
        ground_attack(function()
          self:decide_action()
        end)
      elseif random <= 3 then
        print("Bite")
        bite(function()
          self:decide_action()
        end)
      else
        print("Follow")
        follow_hero(function()
          self:decide_action()
        end)
      end
    else
      local random = math.random(0, 3)
      if random == 0 then
        print("Ground attack 2")
        ground_attack(function()
          self:decide_action()
        end)
      else
        print("Follow 2")
        follow_hero(function()
          self:decide_action()
        end)
      end
    end
  end
end

enemy.decide_action = enemy.cookie_decide_action

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 310 + 100,
  detection_distance = 1000,
  sprite = "enemies/cheemz",
  stagger_when_hurt = false,
  aggro = true,
  abandon_hero_distance = 1000,
  stunlock_limit = 0,
  --contact_damage = 1,
})