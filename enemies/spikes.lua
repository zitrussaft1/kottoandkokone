local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local state = false
local lock_state = false
local timer

function enemy:on_created()
    enemy:create_sprite("ground_effects/red_outline_square")
    enemy:set_visible(false)
    enemy:set_size(16, 16)
    enemy:set_traversable(true)
    enemy:set_can_attack(false)
    enemy:set_invincible()
end

function enemy:new_remove_sprite()
    for sprite_name, sprite in enemy:get_sprites() do
        enemy:remove_sprite(sprite)
    end
end

function enemy:change_state(new_state)
    if new_state == true then
        if not lock_state then
            lock_state = true
            enemy:set_visible(true)
            enemy:new_remove_sprite()
            enemy:create_sprite("ground_effects/red_outline_square")
            timer = sol.timer.start(500, function()
                enemy:new_remove_sprite()
                enemy:create_sprite("ground_effects/red_square")
                enemy:set_size(16, 16)
                enemy:set_traversable(false)
                enemy:set_can_attack(true)
                lock_state = false
            end)
        end
    elseif new_state == false then
        lock_state = false
        enemy:new_remove_sprite()
        enemy:create_sprite("ground_effects/red_outline_square")
        enemy:set_visible(false)
        if timer ~= nil then
            timer:stop()
        end
    end
end