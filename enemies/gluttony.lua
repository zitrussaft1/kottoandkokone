local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

function enemy:gluttony_behavior()
  local function basic()
    self:look_at_player(0.1, function()
      self:wrap_bite(nil, 1, function (biten, args)
        if biten then 
          self:decide_action()
        else
          self:wait(0.4, function()
            self:decide_action()
          end)
        end
      end)
    end)
  end

  local function ranged()
    self:look_at_player(0.1, function()
      self:shoot(self:get_angle(hero), 0, 8, function()
        self:wait(0.4, function()
          self:decide_action()
        end)
      end)
    end)
  end

  self:look_at_player(1, function()
    self:turn_stream_towards(128, 128, 5000, function()
        local max_angle = math.pi
        local starting_angle = self:get_angle(hero) - math.pi/2
        local angle_increment = 0.4
        for angle = starting_angle, starting_angle + max_angle, angle_increment do
          self:shoot(
            angle,
            0, 8, function()
          end)
        end
        self:decide_action()
    end)
  end)
end

enemy.decide_action = enemy.gluttony_behavior

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 310 + 100,
  detection_distance = 1000,
  sprite = "animals/worf_brown",
  stagger_when_hurt = false,
  aggro = true,
  abandon_hero_distance = 1000,
  stunlock_limit = 0,
  contact_damage = false,
})