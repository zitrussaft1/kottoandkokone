local master_script = require("scripts/meta/master")
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite

require("enemies/trillium/gooseberry_enemies/behavior/goblin_behavior"):apply(enemy, {
  life = 8,
  damage = 1,
  alert_distance = 80,
  unalert_distance = 130,
  attack_distance = 48,
  wander_speed = 24,
  chase_speed = 60,
  attack_sprite = "enemies/trillium/gooseberry_enemies/attacks/slash",
  attack_sound = "sword3",
  attack_cooldown_length = 2000,
  has_melee_attack = true,
})

enemy:register_event("on_created", function(self)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)