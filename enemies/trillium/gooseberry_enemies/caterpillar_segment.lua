local master_script = require("scripts/meta/master")
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement


function enemy:on_created()
  master_script:set("trillium", "enemy")
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_pushed_back_when_hurt(false)
  enemy:set_dying_sprite_id("enemies/trillium/enemy_killed_projectile")
  enemy:set_has_shadow(true, "18")
  master_script:set("default", "enemy")
end