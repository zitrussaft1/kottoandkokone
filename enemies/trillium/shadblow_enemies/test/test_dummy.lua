local master_script = require("scripts/meta/master")
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

require"enemies/trillium/shadblow_enemies/lib/line_of_sight"


local props = {
  life = 15,
  detection_distance = 150,
  abandon_hero_distance = 300,
}

function enemy:on_created()
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy.attack_entities = {}
  enemy:set_life(props.life)
  enemy.max_poise = props.max_poise or 3
  enemy.poise = enemy.max_poise
  enemy:set_damage(1)
  enemy.detection_distance = props.detection_distance or 150
  enemy.abandon_hero_distance = props.abandon_hero_distance or 500
  enemy.home_x, enemy.home_y, enemy.home_z = enemy:get_position()

  enemy.aggro = false
end


function enemy:on_movement_changed(m)
  sprite:set_direction(m:get_direction4())
end


function enemy:on_restarted()
  for _, entity in pairs(enemy.attack_entities) do entity:remove() end
  enemy.attack_entities = {}
  enemy:set_can_attack(false)

  if not enemy.aggro then enemy:start_idle() end
end


function enemy:start_idle()
  enemy.aggro = false
  if sprite:has_animation"idle" then sprite:set_animation"idle"
  elseif sprite:has_animation"stopped" then sprite:set_animation"stopped"
  elseif sprite:has_animation"walking" then sprite:set_animation"walking" end
  sol.timer.start(enemy, 50, function()
    enemy:check_for_hero()
    return true
  end)
end


function enemy:check_for_hero()
  master_script:set("trillium", "hero")
  if (enemy.aggro)
  or (enemy:get_distance(hero) <= 24)
  or ((enemy:get_distance(hero) <= enemy.detection_distance) and enemy:has_los(hero)) then
    enemy.aggro = true
    sol.timer.stop_all(enemy)
    enemy:decide_attack()
  end
  master_script:set("default", "hero")
end


function enemy:decide_attack()
  if enemy:get_distance(hero) >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  elseif enemy:get_distance(hero) <= 24 then
    enemy:swipe_attack()
  else
    enemy:approach_then_attack({
      attack_function = function() enemy:swipe_attack() end
    })
  end
end


function enemy:return_to_idle()
  enemy.aggro = false
end


function enemy:approach_then_attack(props)
  local speed = props.speed or 40
  local dist_threshold = props.dist_threshold or 32
  local m = sol.movement.create"target"
  m:set_speed(speed)
  m:start(enemy)
  sprite:set_animation"walking"
  sol.timer.start(enemy, 50, function()
    if enemy:get_distance(hero) <= dist_threshold then
      props.attack_function()
    else
      return true
    end
  end)
end


function enemy:swipe_attack()
  enemy:stop_movement()
  local x, y, z = enemy:get_position()
  local direction = sprite:get_direction()
  sprite:set_animation("attack", "stopped")
  local attack_entity = map:create_custom_entity{
    x = x, y = y, layer = z, direction = direction,
    width = 16, height = 16,
    sprite = "enemies/trillium/shadblow_enemies/test/dummy_sword"
  }
  enemy.attack_entities[attack_entity] = attack_entity
  attack_entity:add_collision_test("sprite", function(attack_entity, other)
    if other:get_type() == "hero" and not hero:is_invincible() then
      attack_entity:clear_collision_tests()
      hero:start_hurt(1)
    end
  end)

  sol.timer.start(enemy, 1000, function() enemy:decide_attack() end)
end


function enemy:stagger(duration)
  duration = duration or 700
  enemy:restart()
  sprite:set_animation"staggered"
  enemy:stop_movement()
  sol.timer.start(enemy, duration, function()
    sprite:set_animation"walking"
    enemy:decide_attack()
  end)
end


function enemy:process_hit(damage)
  enemy.aggro = true
  sprite:set_blend_mode"add"
  sol.timer.start(map, 100, function() sprite:set_blend_mode"blend" end)
  enemy:remove_life(damage)
  enemy.poise = enemy.poise - damage
  if enemy.poise <= 0 then
    enemy.poise = enemy.max_poise
    enemy:stagger()
  end
end


