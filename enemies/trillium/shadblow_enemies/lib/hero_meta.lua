--[[
By Max Mraz, MIT license.
This function is necessary for the Shadblow enemy system- it also allows enemies and projectiles to call hero:process_hit() instead of hero:start_hurt
Here, when the hero is hit, the shield, parry, etc are taken into account rather than simply hurting the hero.
--]]

local hero_meta = sol.main.get_metatable("hero")

function hero_meta:process_hit(props)
  local hero = self
  local enemy = props.enemy
  local damage = props.damage or 1
  local type = props.type or "physical"

  if hero.parrying then
    hero:parry(enemy)

  elseif hero.shielding then
    if hero:get_direction4_to(enemy) == hero:get_direction() then
      sol.audio.play_sound("shield")
      if enemy.get_breed and enemy:get_breed():match("shadblow_enemies") then
        enemy:restart()
        enemy:attack_recovery(800)
      elseif enemy.is_projectile then
        enemy:stop_movement()
        enemy:hit_obstacle()
      elseif enemy.restart then
        enemy:restart()
      end

    else
      if hero:get_sprite("shield") then hero:remove_sprite(hero:get_sprite("shield")) end
      hero:start_hurt(enemy, damage)
    end
  else
    --TODO maybe check for defense and stuff
      if hero:get_sprite("shield") then hero:remove_sprite(hero:get_sprite("shield")) end
    hero:start_hurt(enemy, damage)
  end
end
