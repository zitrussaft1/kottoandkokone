local master_script = require("scripts/meta/master")
--[[

Max Mraz, MIT license
Pass an enemy through this script to give it behavior similar to an enemy from Dark Souls.
Enemies will idle until the notice the player, then decide what attack to use based on the decide_action() function.
The decide_action function must be defined on the enemy script and call actions or attacks itself.
The end of each action is responsible for decide_action again.

Some common functions are provided:
enemy:approach_then_attack({
  master_script:set("trillium", "hero")
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "custom_entity")
  master_script:set("trillium", "bush")
  speed = 90, --speed at which the enemy approaches the hero
  distance = 32, --distance at which the enemy is close enough to attack
  attack_function = --a function, the attack action
  approach_duration = 3000, --after this time, the enemy will stop approaching the player and call enemy:decide_action()
})

Most enemy events are defined. Some are registered using the multievents script, which is required for this system.
on_created
on_restarted
on_dying
This allows you to register on_created and on_restarted events for specific enemies or breeds in addition.

Usage:
local enemy = ...
require("enemies/trillium/shadblow_enemies/lib/enemizer"):apply_behavior(enemy, {
  life = 15,
  max_poise = 5,
})

Properties table
life
detection_distance - distance at which the enemy can spot the hero
abandon_hero_distance - distance at which the enemy will stop chasing the hero and return to idle
max_poise - the amount of damage this enemy can take before being staggered
hurt_sound - sound the enemy makes when hit

Misc notes:
enemy.facing_locked - setting this will prevent an enemy from facing in the direction of its movement, which it will otherwise


--]]

require"enemies/trillium/shadblow_enemies/lib/line_of_sight"
require"enemies/trillium/shadblow_enemies/lib/hero_meta"

local enemizer = {}

function enemizer:apply_behavior(enemy, props)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()
  local sprite
  local movement

  enemy:register_event("on_created", function()
    sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
    enemy:set_traversable(false)
    enemy.attack_entities = {}
    enemy:set_life(props.life)
    enemy.hurt_sound = props.hurt_sound or "enemy_hurt"
    enemy.max_poise = props.max_poise or 3
    enemy.poise = enemy.max_poise
    enemy:set_damage(1)
    enemy.detection_distance = props.detection_distance or 150
    enemy.abandon_hero_distance = props.abandon_hero_distance or 500
    enemy.home_x, enemy.home_y, enemy.home_z = enemy:get_position()
    enemy.idle_movement_speed = props.idle_movement_speed or 20

    enemy.aggro = false or props.aggro
  end)


  function enemy:on_movement_changed(m)
    if not enemy.lock_facing then
      sprite:set_direction(m:get_direction4())
    end
  end

  enemy:register_event("on_position_changed", function()
    if enemy:overlaps(hero) then
      --enemy:set_traversable(true)
    else
      --enemy:set_traversable(false)
    end
  end)


  enemy:register_event("on_restarted", function()
    for _, entity in pairs(enemy.attack_entities) do entity:remove() end
    enemy.attack_entities = {}
    enemy:set_can_attack(false)

    if not enemy.aggro then enemy:start_idle() end
  end)


  function enemy:start_idle()
    enemy.aggro = false
    if enemy:get_property("idle_movement_type") and enemy:get_property("idle_movement_type") == "walk" then
      sprite:set_animation"walking"
      local m = sol.movement.create"random"
      m:set_speed(enemy.idle_movement_speed)
      m:start(enemy)
      function m:on_changed()
        if enemy:get_distance(enemy.home_x, enemy.home_y) >= 100 then
          m = sol.movement.create"target"
          m:set_target(enemy.home_x, enemy.home_y)
          m:set_speed(enemy.idle_movement_speed)
          m:start(enemy)
          sol.timer.start(enemy, 3000, function() enemy:start_idle() end)
        end
      end
    else
      if sprite:has_animation"idle" then sprite:set_animation"idle"
      elseif sprite:has_animation"stopped" then sprite:set_animation"stopped"
      elseif sprite:has_animation"walking" then sprite:set_animation"walking" end
    end
    sol.timer.start(enemy, 50, function()
      enemy:check_for_hero()
      return true
    end)
  end


  function enemy:check_for_hero()
    local is_on_screen = not enemy.is_on_screen or enemy:is_on_screen()
    if (enemy.aggro) or ((enemy:get_distance(hero) <= enemy.detection_distance) and enemy:has_los(hero) and is_on_screen) then
      enemy.aggro = true
      sol.timer.stop_all(enemy)
      enemy:decide_action()
    elseif (enemy:get_distance(hero) <= 24) then
      sol.timer.start(enemy, 500, function() enemy.aggro = true end)
    end
  end


  function enemy:return_to_idle()
    enemy.aggro = false
    enemy:restart()
  end


  function enemy:approach_then_attack(props)
    local speed = props.speed or 40
    local dist_threshold = props.dist_threshold or 32
    local approach_duration = props.approach_duration or nil
    local m = sol.movement.create"target"
    m:set_speed(speed)
    m:start(enemy)
    sprite:set_animation"walking"
    local elapsed_time = 0
    sol.timer.start(enemy, 50, function()
      elapsed_time = elapsed_time + 50
      if enemy:get_distance(hero) <= dist_threshold then
        props.attack_function()
      elseif approach_duration and (elapsed_time >= approach_duration) then
        enemy:stop_movement()
        enemy:restart()
        enemy:decide_action()
      else
        return true
      end
    end)
  end


  function enemy:attack_recovery(duration)
    sol.timer.start(enemy, duration or 500, function()
      enemy:decide_action()
    end)
  end


  function enemy:stagger(duration)
    duration = duration or 700
    enemy:restart()
    sprite:set_animation"staggered"
    enemy:stop_movement()
    sol.timer.start(enemy, duration, function()
      sprite:set_animation"walking"
      enemy:decide_action()
    end)
  end


  function enemy:stun(duration)
    duration = duration or 1500
    enemy.stunned = true
    enemy:restart()
    sprite:set_animation"stunned"
    enemy:stop_movement()
    sol.timer.start(enemy, duration, function()
      enemy.stunned = false
      sprite:set_animation"walking"
      enemy:decide_action()
    end)
  end


  function enemy:process_hit(damage)
    if enemy:get_life() <= 0 then return end
    if enemy:would_be_crit() then
      enemy.stunned = false
      enemy:start_crit(damage)
    else
      enemy.aggro = true
      sprite:set_blend_mode"add"
      sol.timer.start(map, 100, function() sprite:set_blend_mode("blend") end)
      sol.audio.play_sound(enemy.hurt_sound)
      enemy:remove_life(damage)
      enemy.poise = enemy.poise - damage
      if enemy.poise <= 0 then
        enemy.poise = enemy.max_poise
        enemy:stagger()
      end
    end
  end

  function enemy:hurt(damage)
    enemy:process_hit(damage)
  end
  function enemy:on_hurt() end
  
    

  function enemy:would_be_crit()
    local crit = false
      if (enemy.stunned and enemy:get_distance(hero) <= 48)
      or (not enemy.aggro and sprite:get_direction() == hero:get_direction() ) then
        crit = true
      end
    return crit
  end


  function enemy:start_crit(damage)
    enemy:restart()
    enemy:stop_movement()
    sol.audio.play_sound(enemy.hurt_sound)
    sprite:set_blend_mode"add"
    sol.timer.start(map, 100, function() sprite:set_blend_mode("blend") end)
    enemy:remove_life(damage * 5)
    local m = sol.movement.create"straight"
    m:set_angle(hero:get_angle(enemy))
    m:set_max_distance(32)
    m:set_speed(200)
    enemy.lock_facing = true
    m:start(enemy, function()
      enemy.lock_facing = false
      enemy:stagger()
    end)
    function m:on_obstacle_reached()
      enemy.lock_facing = false
      enemy:stagger()
    end
  end


  enemy:register_event("on_dying", function()
    for _, entity in pairs(enemy.attack_entities) do entity:remove() end
  end)

  master_script:set("default", "bush")
  master_script:set("default", "custom_entity")
  master_script:set("default", "enemy")
  master_script:set("default", "hero")
end



return enemizer