local manager = {}

function manager:apply_ranged(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()

  function enemy:ranged_attack(props)
    local sprite = enemy:get_sprite()
    local windup_animation = props.windup_animation
    local windup_time = props.windup_time or 700
    local attack_animation = props.attack_animation
    local projectile_model = props.projectile_breed or "enemy_projectiles/generic_projectile"
    local aim_type = props.aim_type or "any" --options are 4, 8, or "any". Determines angle of projectile
    local projectile_sprite = props.projectile_sprite or "entities/trillium/enemy_projectiles/generic_projectile"
    local projectile_width = props.projectile_width or 16
    local projectile_height = props.projectile_height or 16
    local projectile_offset_radius = props.projectile_offset_radius or 16
    local projectile_offset_x = props.projectile_offset_x or 0
    local projectile_offset_y = props.projectile_offset_y or 0
    local attack_sound = props.attack_sound or "bow"
    local damage = props.damage or 1
    local type = props.type or "physical"
    local recovery_delay = props.recovery_delay or 500
    local next_action = props.next_action or enemy.decide_action

    enemy:stop_movement()
    local x, y, z = enemy:get_position()
    local direction = enemy:get_direction4_to(hero)
    sprite:set_direction(direction)
    sprite:set_animation(windup_animation)
    sol.timer.stop_all(enemy)
    sol.timer.start(enemy, windup_time, function()
      direction = enemy:get_direction4_to(hero)
      sprite:set_direction(direction)
      if attack_animation then
        sprite:set_animation(attack_animation, "stopped")
      else
        sprite:set_animation("stopped")
      end
      local step = projectile_offset_radius
      local dx = {[0] = step + projectile_offset_x, [1]=0, [2]= step * -1 - projectile_offset_x, [3]=0}
      local dy = {[0]=0, [1] = step * -1 - projectile_offset_y, [2]=0, [3]= step + projectile_offset_y}
      local projectile = map:create_custom_entity{
        x = x + dx[direction],
        y = y + dy[direction],
        layer = z,
        width = projectile_width, height = projectile_height, direction = direction, 
        model = props.projectile_model, sprite = projectile_sprite,
      }
      local shoot_angle
      if aim_type == "any" then
        shoot_angle = enemy:get_angle(hero)
      elseif aim_type == 4 then
        shoot_angle = direction * math.pi / 2
      elseif aim_type == 8 then
        shoot_angle = enemy:get_direction8_to(hero) * math.pi / 4
      end
      projectile.damage = damage
      projectile.damage_type = type
      if projectile_sprite then
        projectile:create_sprite(projectile_sprite)
        projectile:remove_sprite()
      end
      projectile.firing_entity = enemy
      projectile:shoot(shoot_angle)
      sol.audio.play_sound(attack_sound)
      sol.timer.start(enemy, recovery_delay, function()
        next_action()
      end)
    end)

  end



  function enemy:ranged_combo(attacks)
    for i = 1, #attacks - 1 do
      attacks[i].next_action = function()
        enemy:ranged_attack(attacks[i + 1])
      end
    end
    enemy:ranged_attack(attacks[1])
  end


end


return manager