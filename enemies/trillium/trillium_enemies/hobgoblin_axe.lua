local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 16,
  detection_distance = 200,
  abandon_hero_distance = 400,
})
require("enemies/trillium/trillium_enemies/attacks/melee"):apply_melee(enemy)

local thrust_attack = {
  windup_animation = "thrust_windup",
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 4,
  type = "physical",
  recovery_delay = 500,
}

local slam_attack = {
  windup_animation = "slam_windup",
  attack_animation = "slam_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slam_windup",
  weapon_attack_animation = "slam_attack",
  damage = 6,
  type = "physical",
  recovery_delay = 500,
}

local slash_attack = {
  windup_animation = "slash_windup",
  windup_delay = 300,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 200,
}

local backslash_attack = {
  windup_animation = "backslash_windup",
  windup_delay = 200,
  attack_animation = "backslash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 200,
}

local combo_a1 = {
  windup_animation = "slash_windup",
  windup_delay = 500,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 30,
}

local combo_a2 = {
  windup_animation = "backslash_windup",
  windup_delay = 30,
  attack_animation = "backslash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 30,
}

local combo_a3 = {
  windup_animation = "thrust_windup",
  windup_delay = 300,
  attack_animation = "thrust_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "thrust_windup",
  weapon_attack_animation = "thrust_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 800,
}

local combo_b3 = {
  windup_animation = "slash_windup",
  windup_delay = 30,
  attack_animation = "slash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "slash_windup",
  weapon_attack_animation = "slash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 30,
}

local combo_b4 = {
  windup_animation = "backslash_windup",
  windup_delay = 30,
  attack_animation = "backslash_attack",
  weapon_sprite = "enemies/trillium/trillium_enemies/weapons/hob_axe",
  weapon_windup_animation = "backslash_windup",
  weapon_attack_animation = "backslash_attack",
  damage = 2,
  type = "physical",
  recovery_delay = 1000,
}

local combo_a = {combo_a1, combo_a2, combo_a3}

local combo_b = {combo_a1, combo_a2, combo_b3, combo_b4}



function enemy:decide_action()
  local distance = enemy:get_distance(hero)
  if distance >= enemy.abandon_hero_distance then
    enemy:return_to_idle()
  else
    enemy:approach_then_attack({
      approach_duration = 1500,
      speed = 60,
      attack_function = function()
        local rand = math.random(1,4)
        if rand == 1 then
          enemy:melee_combo(combo_a)
        elseif rand == 2 then
          enemy:melee_combo(combo_b)
        elseif enemy:is_aligned(hero, 10) then
          enemy:melee_attack(slam_attack)
        else
          enemy:melee_attack(backslash_attack)
        end
      end
    })
  end
end