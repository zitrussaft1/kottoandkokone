local master_script = require("scripts/meta/master")
local manager = {}

function manager:apply_melee(enemy)
  master_script:set("trillium", "hero")
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()

  function enemy:melee_attack(props)
    local sprite = enemy:get_sprite()
    local windup_delay = props.windup_delay or 500
    local windup_animation = props.windup_animation
    local attack_animation = props.attack_animation
    local recovery_animation = props.recovery_animation or "stopped"
    local weapon_sprite = props.weapon_sprite
    local weapon_windup_animation = props.weapon_windup_animation
    local weapon_attack_animation = props.weapon_attack_animation
    local attack_sound = props.attack_sound or "sword1"
    local damage = props.damage or 1
    local type = props.type or "physical"
    local has_aoe = props.has_aoe or false
    local turn_toward_hero = props.turn_toward_hero if props.turn_toward_hero == nil then turn_toward_hero = true end
    local step_distance = props.step_distance
    local step_speed = props.step_speed or 80
    local recovery_delay = props.recovery_delay or 500
    local next_action = props.next_action or (function() enemy:decide_action() end)

    local function step_forward(angle, attack_entity)
      local m = sol.movement.create"straight"
      local step_angle = enemy:get_angle_from_facing_direction(angle)
      m:set_angle(step_angle)
      m:set_speed(step_speed)
      m:set_max_distance(step_distance)
      m:start(enemy)

      function m:on_position_changed()
        --move attack entity to stay on top of enemy
        attack_entity:set_position(enemy:get_position())
      end
    end

    local x, y, z = enemy:get_position()
    local direction = enemy:get_facing_direction_to(hero)
    if not turn_toward_hero then direction = sprite:get_direction() end
    local y_sorting_offset = direction == 1 and -1 or 1
    enemy:stop_movement()
    sprite:set_direction(direction)
    sprite:set_animation(windup_animation)
    local attack_entity = map:create_custom_entity{
      x = x, y = y + y_sorting_offset, layer = z, direction = direction,
      width = 16, height = 16,
      name = "enemy_melee_attack",
    }
    attack_entity:set_drawn_in_y_order(true)
    enemy.attack_entities[attack_entity] = attack_entity
    local attack_sprite = weapon_windup_animation and attack_entity:create_sprite(weapon_sprite) or nil
    if attack_sprite then
      attack_sprite:set_animation(weapon_windup_animation)
      attack_sprite:set_direction(direction)
    end

    --Wait for windup, then attack!:
    sol.timer.start(enemy, windup_delay, function()
      sol.audio.play_sound(attack_sound)
      --Step into attack to close distance:
      if step_distance then step_forward(direction * math.pi / 2, attack_entity) end
      sprite:set_animation(attack_animation, recovery_animation)
      if not weapon_windup_animation then --we haven't yet created a sprite if it didn't have a windup animation
        attack_sprite = attack_entity:create_sprite(weapon_sprite)
        attack_sprite:set_direction(direction)
      end
      attack_sprite:set_animation(weapon_attack_animation)
      attack_entity:add_collision_test("sprite", function(attack_entity, other)
        if other:get_type() == "hero" and hero:get_can_be_hurt() then
          attack_entity:clear_collision_tests()
          if hero.process_hit then
            hero:process_hit({damage = damage, enemy = enemy, type = type})
          else
            hero:start_hurt(enemy, damage)
          end
        end
      end)
      if has_aoe then
        enemy:create_melee_aoe(props)
      end
      local attack_duration = sprite:get_num_frames(attack_animation, direction) * sprite:get_frame_delay(attack_animation)
      sol.timer.start(enemy, attack_duration + recovery_delay, function()
        enemy.attack_entities[attack_entity]:remove()
        next_action()
      end)
    end)
  master_script:set("default", "hero")
  end


  function enemy:create_melee_aoe(props)
  master_script:set("trillium", "map")
  master_script:set("trillium", "hero")
  master_script:set("trillium", "game")
    local aoe_offset = props.aoe_offset or 0
    local aoe_delay = props.aoe_delay or 200
    local aoe_sprite = props.aoe_sprite or "enemies/trillium/trillium_enemies/shockwave_8x7"
    local aoe_damage = props.aoe_damage or 1
    local aoe_sound = props.aoe_sound
    local aoe_screenshake = props.aoe_screenshake
    local aoe_scale = props.aoe_scale or {1, 1}

    local x, y, z = enemy:get_position()
    local direction = enemy:get_sprite():get_direction()
    sol.timer.start(map, aoe_delay, function()
      if aoe_sound then sol.audio.play_sound(aoe_sound) end
      if aoe_screenshake then map:screenshake({shake_count = aoe_screenshake}) end
      local shockwave = map:create_custom_entity{
        x = x + game:dx(aoe_offset)[direction], y = y + game:dy(aoe_offset)[direction], layer = z,
        direction = 0, width = 16, height = 16,
        sprite = aoe_sprite,
      }
      if aoe_scale then shockwave:get_sprite():set_scale(aoe_scale[1], aoe_scale[2]) end
      shockwave:add_collision_test("sprite", function(shockwave, other)
        if (other:get_type() == "hero") and hero:get_can_be_hurt() then
          shockwave:clear_collision_tests()
          hero:start_hurt(aoe_damage)
        end
      end)
    end)
  master_script:set("default", "game")
  master_script:set("default", "hero")
  master_script:set("default", "map")
  end



  function enemy:melee_combo(attacks)
    local new_attacks = {}
    for i = 1, #attacks do
      new_attacks[i] = {}
      for k, v in pairs(attacks[i]) do new_attacks[i][k] = v end
    end
    for i = 1, #new_attacks - 1 do
      new_attacks[i].next_action = function()
        enemy:melee_attack(new_attacks[i + 1])
      end
    end
    enemy:melee_attack(new_attacks[1])
  end


end


return manager