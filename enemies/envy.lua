-- Lua script of enemy envy.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement


local cooldown_over = true
function enemy:envy_behavior()
  local map_x_min, map_y_min = 32, 32
  local map_x_max, map_y_max = map:get_size()
  map_x_max, map_y_max = map_x_max - 32, map_y_max - 32

  local function basic()
    self:look_at_player(0.1, function()
      self:wrap_ground_attack(0, 48, true, 1, 0.5, 0.5, function()
        self:wait(1, function()
          self:decide_action()
        end)
      end)
    end)
  end

  local function ranged()
    self:look_at_player(0.1, function()
      local timer = 1
      self:invisible(1 * (1/self:get_speed()), function() 
        local distance = 64
        local x, y, l = hero:get_position()
        local random_place = math.random(0, math.pi*100)/100
        local x2, y2 = x + math.cos(random_place)*distance, y + math.sin(random_place)*distance

        while x2 < map_x_min or x2 > map_x_max or y2 < map_y_min or y2 > map_y_max do
          random_place = math.random(0, math.pi*100)/100
          x2, y2 = x + math.cos(random_place)*distance, y + math.sin(random_place)*distance
        end

        self:set_position(x2, y2, l)
        self:visible(0.5 * (1/self:get_speed()), function()
          local angle = self:get_angle(hero)
          self:shoot(angle, 2, 12, function()
            self:look_at_player(4* (1/self:get_speed()), function()
              self:spin(0.5* (1/self:get_speed()), function()
                self:wait(0.5* (1/self:get_speed()), function()
                  self:set_position(math.random(map_x_min, map_x_max), math.random(map_y_min, map_y_max), l)
                  self:decide_action()
                end)
              end)
            end)
          end, 200)
        end)
      end)
    end)
  end

  local function i_am_more_than_you(number_of_copies)
    local angle = self:get_angle(hero) + math.pi/2
    local x, y, l = self:get_position()

    local i = 1
    sol.timer.start(1/number_of_copies, function()
      local distance = 16
      local x2, y2 = x + math.cos(angle)*distance*i, y + math.sin(angle)*distance*i
      if x2 > map_x_min and x2 < map_x_max and y2 > map_y_min and y2 < map_y_max then
        self:summon_kotto(x2, y2)
      end
      i = i + 1
      return i < number_of_copies + 1
    end)

    self:wait(1, function()
      self:decide_action()
    end)
  end

  local distance = self:get_distance(hero)
  local random = math.random(0, 2)
  if random == 0 then
      ranged()
  else
    if distance > 64 and cooldown_over then
        local copies = math.random(4, 20)      
        i_am_more_than_you(copies)
        cooldown_over = false
        self:wait(2*copies, function()
          cooldown_over = true
        end)
    else
      basic()
    end
  end
end

enemy.default_on_hurt = enemy.on_hurt
function enemy:on_hurt(attack)
  self:default_on_hurt(attack)
  if (self:get_life()) / tonumber(self:get_property("max_health")) <= (3-map:get_round())/3 then
    map:next_round()
  end
end

enemy.decide_action = enemy.envy_behavior

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 310 + 100,
  detection_distance = 1000,
  sprite = "animals/worf_brown",
  stagger_when_hurt = false,
  aggro = true,
  abandon_hero_distance = 1000,
  stunlock_limit = 0,
  contact_damage = false,
})
