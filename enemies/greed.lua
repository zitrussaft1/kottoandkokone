-- Lua script of enemy greed.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement


function enemy:greed_behavior()
  local function ranged()
    self:look_at_player(0.1, function()
      for i = 0, 32 do
        self:wait(0.2*i, function()
          self:shoot(self:get_angle(hero), 2, 12, function()
            if i == 32 then
              self:wait(3, function()
                self:decide_action()
              end)
            end
          end)
        end)
      end
    end)
  end

  local function saw_hello_to_my_little_friend()
    self:look_at_player(0.1, function()
      local player_angle = self:get_angle(hero)
      self:wrap_ground_attack(0, 48, true, 1, 0.5, 0.5, function()
        -- Shoot 3 times in 7 directions
        for i = 0, 2 do
          self:wait(0.7*i, function()
            self:stop_movement()
            local knockback = sol.movement.create("straight")
            knockback:set_speed(SPEED(200))
            knockback:set_angle(player_angle + math.pi)
            knockback:set_max_distance(16)
            knockback:set_smooth(false)
            knockback:start(self)
            for j = 0, 4 do
              self:shoot(
                player_angle - math.pi/4 + math.pi*j/8, 
                0, 12, function() end
              )
            end
            if i == 2 then
              self:wait(3, function()
                self:decide_action()
              end)
            end
          end)
        end
      end)
    end)
  end

  local function not_good()
      local hx, hy, _ = hero:get_position()
      local mx, my = map:get_size()
      -- Find the opposite position of the hero relative to the map
      local opposite_x = hx - (hx - mx/2)*2 + math.random(-32, 32)
      local opposite_y = hy - (hy - my/2)*2 + math.random(-32, 32)
      -- If opposite position is outside the map, or too close to the hero, then find a random position
      if opposite_x < 16 or opposite_x > mx or opposite_y < 16 or opposite_y > my or self:get_distance(opposite_x, opposite_y) < 64 then
        opposite_x = math.random(18, mx-16)
        opposite_y = math.random(18, my-16)
      end
      self:spin(1, function()
        self:wait(1, function()
          self:set_position(
            opposite_x,
            opposite_y
          )
          self:decide_action()
        end)
      end)
  end

  local function kafka(number_of_shots)
    for i = 1, number_of_shots do
      self:spin(1, function()
      self:wait(1, function()
        local width, height = map:get_size()
        local center_x, center_y = width/2, height/2
        self:set_position(center_x, center_y)
      self:wall_rush_draw(2*math.pi*i/number_of_shots, 2, 1, function()
        -- Start shooting in circles
        local max_angle = 2*math.pi
        local angle = 0
        local angle_increment = 0.1

        local function shoot_in_circle()
          self:shoot(
            2*math.pi*i/number_of_shots + angle,
            0, 12, function()
            angle_increment = angle_increment + 0.005
            angle = angle + angle_increment
            if angle < max_angle then
              self:wait(0.2, shoot_in_circle)
            else
              if i == number_of_shots then
                self:wait(3, function()
                  self:decide_action()
                end)
              end
            end
          end)
        end
        shoot_in_circle()
      end)
      end)
      end)
    end
  end

  local function kind_present(splash, num_of_presents)
    local diameter = 64
    local positions = {}
    local sizes = {}
    -- Depending on num_of_presents draw in the player position or around the player
    -- Use 2*math.pi*i/num_of_presents to draw in a circle
    local x, y, _ = hero:get_position()
    self:draw_rain_objects({{x,y}}, {diameter}, 1, function()
      self:rain_objects({{x,y}}, {diameter}, 1, function()
        if splash then
          for i = 1, num_of_presents do
            local angle = 2*math.pi*i/num_of_presents
            local new_x = x + math.sin(angle) * diameter
            local new_y = y + math.cos(angle) * diameter
            positions[#positions + 1] = {new_x, new_y}
            sizes[#sizes + 1] = diameter
          end
          
          self:draw_rain_objects(positions, sizes, 1, function()
            self:rain_objects(positions, sizes, 1, function()
              self:wait(3, function()
                self:decide_action()
              end)
            end)
          end)
        else
          self:wait(3, function()
            self:decide_action()
          end)
        end
      end)
    end)
  end

  local function basic()
    self:look_at_player(0.1, function()
      self:wrap_bite(nil, 1, function (biten, args)
          self:decide_action()
      end)
    end)
  end

  local random = math.random(1, 4)
  if random == 1 then
    kind_present(math.random(1, 2) == 1, 4)
  elseif self:get_distance(hero) < 48 then
    if random == 2 then
      not_good()
    elseif random == 3 then
      basic()
    else
      saw_hello_to_my_little_friend()
    end
  else
    if random == 2 then
      kafka(math.random(2, 4))
    else
      ranged()
    end
  end

end

enemy.default_on_hurt = enemy.on_hurt
function enemy:on_hurt(attack)
  self:default_on_hurt(attack)
  if (self:get_life()) / tonumber(self:get_property("max_health")) <= (3-map:get_round())/3 then
    map:next_round()
  end
end

enemy.decide_action = enemy.greed_behavior

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 310 + 100,
  detection_distance = 1000,
  sprite = "animals/worf_brown",
  stagger_when_hurt = false,
  aggro = true,
  abandon_hero_distance = 1000,
  stunlock_limit = 0,
  contact_damage = false,
})
