local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

enemy:register_event("on_restarted", function()
  enemy:set_traversable(true)
end)


local can_use_seduction = true

local first_action = true

function enemy:lust_behavior()
    self:set_traversable(true)
    local function follow_hero(callback, args)
        self:follow_hero(2, function ()
            callback(args)
        end)
    end

    local function throw_boomerang(callback, args)
        self:wait(0.2, function ()
            self:throw_boomerang(function ()
                callback(args)
            end)
        end)
    end

    local function seduction(callback, args)
        self:seduction(function ()
            callback(args)
        end)
    end

    local function swipe(size,callback,args)
        self:look_at_player(0.1, function()
            self:wrap_ground_attack(0, size, true, 1, 0.5, 0.5, function()
                callback(args)
            end)
        end)
    end

    local spin_damage

    local dance_counter = 0
    local function dancing_love(callback, args)
        local movement = sol.movement.create("straight")
        movement:set_speed(SPEED(150))
        movement:set_angle(self:get_angle(hero))
        movement:set_max_distance(300)

        function movement:on_finished()
            dance_counter = dance_counter + 1
            if dance_counter == 4 then
                dance_counter = 0
                callback(args)
            else
                dancing_love(callback, args)
            end
        end

        local is_semi_circle = false
        local damage = 1
        local radius_final = 48
        local radius_initial = 0

        self:ground_attack_draw(radius_final, radius_final, is_semi_circle, 0.1, 0.3, function ()
            self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function ()
                self:ground_attack_draw(radius_final, radius_final, is_semi_circle, 0.1, 0.3, function ()
                    self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function ()
                        self:ground_attack_draw(radius_final, radius_final, is_semi_circle, 0.1, 0.3, function ()
                            self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function ()
                                movement:stop()
                                movement:on_finished()
                            end)
                        end)
                    end)
                end)
            end)
        end)
        
        movement:start(self)
    end

    local function gently_rejection(radius_initial, radius_final, callback, args)
        local time = 1
        local wait = 0.5
        local is_semi_circle = false
        local damage = 1

        local spinning = 0
        local is_spinning = true

        self:ground_attack_draw(radius_initial, radius_final, is_semi_circle, time, wait, function ()
            self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function()
                self:ground_attack_draw(radius_final, radius_final, is_semi_circle, 0.1, 1, function ()
                    self:ground_attack(radius_initial, radius_final, is_semi_circle, damage, function()
                        if self:get_distance(hero) < 72 then
                            is_spinning = false
                            callback(args)
                        else
                            dancing_love(function ()
                                is_spinning = false
                                self:wait(5, function()
                                    callback(args)
                                end)
                            end)
                        end
                    end)
                end)
            end)
        end)

        sol.timer.start(TIME(100), function()
            if is_spinning then
                self:get_sprite():set_direction(spinning)
                spinning = spinning + 1
                if spinning == 4 then
                    spinning = 0
                end
            end
            return is_spinning
        end)
    end

    local random = math.random(0, 10)
    if self:get_distance(hero) < 40 then
        first_action = false
        if random < 4 then
            follow_hero(function ()
                self:decide_action()
            end)
        elseif random < 7 then
            swipe(40, function ()
                self:decide_action()
            end)
        else
            gently_rejection(0, 48, function ()
                self:decide_action()
            end)
        end
    else
        if random > 3 and can_use_seduction and not first_action then
            can_use_seduction = false
            seduction(function ()
                self:decide_action()
                sol.timer.start(10000, function()
                    can_use_seduction = true
                end)
            end)
        else
            first_action = false
            throw_boomerang(function ()
                self:decide_action()
            end)
        end
    end
end

enemy.default_on_hurt = enemy.on_hurt
function enemy:on_hurt(attack)
  self:default_on_hurt(attack)
  print("HEALTH:", self:get_life() / 100, (3-map:get_round())/3)
  if (self:get_life()) / 100 <= (3-map:get_round())/3 then
    map:next_round()
  end
end
enemy.decide_action = enemy.lust_behavior

require("enemies/trillium/trillium_enemies/lib/behavior_applicator"):apply_behavior(enemy, {
  life = 200,
  detection_distance = 1000,
  sprite = "animals/worf_brown",
  stagger_when_hurt = false,
  aggro = true,
  abandon_hero_distance = 1000,
  stunlock_limit = 0,
  contact_damage = false,
})