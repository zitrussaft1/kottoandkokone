# Check if ImageMagick is installed
if ! command -v convert &> /dev/null; then
    echo "ImageMagick is not installed. Please install it first."
    exit 1
fi

# Check if folder path is provided as argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <folder_path>"
    exit 1
fi

folder_path="$1"

# Check if provided path is a directory
if [ ! -d "$folder_path" ]; then
    echo "Error: $folder_path is not a directory."
    exit 1
fi

# Iterate over each image file in the folder
for image_file in "$folder_path"/*.{jpg,jpeg,png,gif}; do
    if [ -f "$image_file" ]; then
        # Get original image resolution
        original_resolution=$(identify -format "%wx%h" "$image_file")
        
        # Resize image to half of its original size
        convert "$image_file" -resize 50% "${image_file%.*}.${image_file##*.}"
        
        # Get new resized image resolution
        resized_resolution=$(identify -format "%wx%h" "${image_file%.*}.${image_file##*.}")
        
        echo "Resized $image_file from $original_resolution to $resized_resolution"
    fi
done
