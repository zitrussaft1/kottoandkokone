// Vertex shader:
#version 130
precision mediump float;

uniform mat4 sol_mvp_matrix;
uniform mat3 sol_uv_matrix;
attribute vec2 sol_vertex;
attribute vec2 sol_tex_coord;
attribute vec4 sol_color;

varying vec2 sol_vtex_coord;
varying vec4 sol_vcolor;
void main() {
  gl_Position = sol_mvp_matrix * vec4(sol_vertex, 0.0, 1.0);
  sol_vcolor = sol_color;
  sol_vtex_coord = (sol_uv_matrix * vec3(sol_tex_coord, 1.0)).xy;
}

// Fragment shader:
#version 100
precision mediump float;
uniform sampler2D sol_texture;
in vec2 sol_vtex_coord;
in vec4 sol_vcolor;
void main() {
  vec4 tex_color = texture(sol_texture, sol_vtex_coord);
  gl_FragColor = tex_color * sol_vcolor;
}