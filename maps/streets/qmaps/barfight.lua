-- Lua script of map streets/qmaps/barfight.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  game:set_character("MC")
  game:set_value("in_combat", 1)
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.


local barguy2 = map:get_entity("barguy2")

local l1 = map:get_entity("l1")
local l2 = map:get_entity("l2")
local l3 = map:get_entity("l3")
function barguy2:on_interaction()
  if not l1:exists() and not l2:exists() and not l3:exists() then
    game:set_value("in_combat", 0)
    game:start_dialog("barquest.barguy2")
    require("scripts/cards"):unlock_card("stay_cute")
    hero:teleport("streets/augusta","barexit")
  end
end