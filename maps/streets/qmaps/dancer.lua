local map = ...
local game = map:get_game()

function map:on_started()
  game:set_character("MC")
end

local current_set = 0
function dancer:on_interaction()
  if current_set >= 2 then
    game:start_dialog("dancer.dancerfinal", function() require("scripts/cards"):unlock_card("tease")
    game:get_hero():teleport("streets/liberty")end)
    
  else 
    game:start_dialog("dancer.dancemore")
  end
end

local textpoints = sol.text_surface.create{
  horizontal_alignment = "center",
  vertical_alignment = "middle",
  rendering_mode = "antialiasing",
  font_size = 20,
  text = "Points: 0"
}

local game_started = false
local points = 0

local sensor1 = map:get_entities("startdancing")
local arrows = {
  map:get_entity("arrow1"),
  map:get_entity("arrow2"),
  map:get_entity("arrow3"),
  map:get_entity("arrow4"),
}

local function on_player_collision_with_arrow(arrow)
  if game_started and not arrow.already_collided then
    points = points + 1
    arrow.already_collided = true 
    textpoints:set_text("Points: " .. points)
  end
end

for _, arrow in ipairs(arrows) do
  arrow:add_collision_test("overlapping", on_player_collision_with_arrow)
end



local function Set1()
  current_set = current_set + 1
  for _, arrow in ipairs(arrows) do
    arrow.already_collided = false
  end

  for i = 1, #arrows do
    sol.timer.start(map, i * 1000, function()
      for j = 1, #arrows do
        arrows[j]:set_enabled(j == i)
      end
    end)
  end

  sol.timer.start(map, (#arrows + 1) * 1000, function()
    for i = 1, #arrows do
      arrows[i]:set_enabled(false)
    end
    game_started = false
    for sensors in sensor1 do
      sensors:set_activated(false)
    end
  end)
end

local function Set2()
  current_set = current_set + 1
  for _, arrow in ipairs(arrows) do
    arrow.already_collided = false
  end

  for i = #arrows, 1, -1 do
    sol.timer.start(map, (4 - i) * 1000, function()
      for j = #arrows, 1, -1 do
        arrows[j]:set_enabled(j == i)
      end
    end)
  end

  sol.timer.start(map, (#arrows + 1) * 1000, function()
    for i = 1, #arrows do
      arrows[i]:set_enabled(false)
    end
    game_started = false
    for sensors in sensor1 do
      sensors:set_activated(false)
    end
  end)
end

local function Set3()
  current_set = current_set + 1
  for _, arrow in ipairs(arrows) do
    arrow.already_collided = false
  end

  sol.timer.start(map, 1000, function()
    arrows[1]:set_enabled(true)
    arrows[3]:set_enabled(true)
    arrows[2]:set_enabled(false)
    arrows[4]:set_enabled(false)

    sol.timer.start(map, 2000, function()
      arrows[1]:set_enabled(false)
      arrows[3]:set_enabled(false)
      arrows[2]:set_enabled(false)
      arrows[4]:set_enabled(false)

      sol.timer.start(map, 1000, function()
        arrows[2]:set_enabled(true)
        arrows[4]:set_enabled(true)

        sol.timer.start(map, 2000, function()
          arrows[2]:set_enabled(false)
          arrows[4]:set_enabled(false)

          sol.timer.start(map, 1000, function()
            arrows[1]:set_enabled(true)
            arrows[2]:set_enabled(true)
            arrows[3]:set_enabled(true)
            arrows[4]:set_enabled(true)
            game_started = false
          end)
        end)
      end)
    end)
  end)
end


function map:on_draw(screen)
  textpoints:draw(screen, 200, 200)
end

for sensors in sensor1 do

  function sensors:on_activated()
    if not game_started then
      if current_set == 0 then
        Set1()
      elseif current_set == 1 then
        Set2()
      elseif current_set == 2 then
        Set3()
      end

      game_started = true
    end
  end
end
