-- Lua script of map streets/augusta.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()


-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.

-- mendigo quest
local PegouRobocks = 0
local var = 0

local card_manager = require("scripts/cards")

local cemreal = map:get_entity("cemreal")
function cemreal:on_interaction()
  game:start_dialog("PegarDinheiro")
  PegouRobocks = 1
  cemreal:remove()
end

local mendigo = map:get_entity("mendigo")
function mendigo:on_interaction()
  if PegouRobocks == 1 then
  game:start_dialog("NPCMendigoDialogo.MendigoFinal", function()
  mendigo:remove()
  card_manager:unlock_card("dirty_mouth")
    end)
  else
    game:start_dialog("NPCMendigoDialogo.MendigoFistPart")
  end
end


-- barfight quest

local barguy1 = map:get_entity("barguy")

function barguy1:on_interaction()
  if game:get_value("card__1__stay_cute") == 1 then
    game:start_dialog("barquest.completebarguy")
  else
    game:start_dialog("barquest.barguy")
    hero:teleport("streets/qmaps/barfight")
  end
end

function map:on_started()
  game:set_character("MC")
  if game:get_value("played_intro") == 0 then
    local camera = map:get_camera()
    camera:start_manual()
    camera:set_position(0, 0)
  end
  if game:get_value("card__1__dirty_mouth") == 1 then
    mendigo:remove()
  end
end

function map:on_opening_transition_finished()
  if game:get_value("played_intro") == 0 then
    local hud = require("scripts/menus/cooldowns_hud")
    sol.menu.stop(hud)
    -- Get the camera and create a pan effect to the hero.
    local camera = map:get_camera()
    local hero = map:get_hero()
    camera:start_manual()
    local cam_mov = sol.movement.create("straight")
    cam_mov:set_speed(5)
    cam_mov:set_angle(0)
    cam_mov:start(camera, function()
    end)
    game:start_dialog("Intro.narr", function()
      game:set_value("played_intro", 1)
      camera:start_tracking(hero)
      cam_mov:stop()
      hero:teleport("first_map")
    end)
  else
    game:save()
  end
end