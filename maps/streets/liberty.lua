-- Lua script of map streets/liberty.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  game:set_character("MC")
  game:save()
  music("eduardo/village")
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.

function dancer:on_interaction()
  if game:get_value("card__1__tease") == 0 then 
  game:start_dialog("dancer.dancer", function() game:get_hero():teleport("streets/qmaps/dancer") end)
  else
   game:start_dialog("dancer.acabouporra")
  end
end