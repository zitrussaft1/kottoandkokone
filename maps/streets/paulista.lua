-- Lua script of map streets/paulista.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  game:set_character("MC")

  if game:get_value("card__1__humiliate") == 1 then moderador:remove() egirl:remove() end
    
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.

-- Egirl quest
local mods = 0
local moveegirl = sol.movement.create("target")
function moderador:on_interaction()
  if mods == 2 then
    game:start_dialog("DialogoEgirl.Egirl final")
    require("scripts/cards"):unlock_card("humiliate") 
    moderador:remove()
    egirl:remove()
  else
    game:start_dialog("DialogoEgirl.Egirl intro") 
    mods = 1
  end
end

function egirl:on_interaction()
  if mods == 1 then 
    game:start_dialog("DialogoEgirl.Egirl mid")
    egirl:set_size(1,1)
    moveegirl:set_target(game:get_hero())
    function moveegirl:on_position_changed()
      -- Change the direction of the sprite to match the movement.
      local direction_facing = egirl:get_direction4_to(game:get_hero())
      egirl:set_direction(direction_facing)

    end
    moveegirl:start(egirl)
    mods = 2
  end
end

-- Leks do futebol quest

local lekops = map:get_entity("lekops")
function Lek1:on_interaction()
  if game:get_value("card__1__gently_advise") == 0 then 
    game:start_dialog("dialogosNPCLek.lekQuestintro")
    game:set_value("in_combat", 1)
  else 
    game:start_dialog("dialogosNPCLek.lekQuestfinal")
  end
end

function lekops:on_exploded() 
  game:set_value("in_combat", 0) 
  require("scripts/cards"):unlock_card("gently_advise")
end



