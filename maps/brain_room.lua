-- Lua script of map brain_room.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local exit = map:get_entity("exit_brain")

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  BONUS_SKILL_SPEED = 1
  local pressa = require("scripts/menus/pressa")
  if sol.menu.is_started(pressa) then
    sol.menu.stop(pressa)
  end

  -- You can initialize the movement and sprites of various
  -- map entities here.
end

function purchase_skill(skill)
  local bought = 0 < game:get_value("skill." .. skill)
  if bought then
    game:start_dialog("skill."..skill..".bought")
  else
    game:start_dialog("skill."..skill..".buy",
      function(answer)
        if answer == 1 then
          local corp = game:get_corp()
          local cost = game:get_value("skill_cost")
          if corp >= cost then
            game:add_corp(-cost)
            skills:set_state(skill, 1)
          end
        end
      end
    )
  end
end

function strength_skill:on_interaction()
  local quest = game:get_value("current_quest")
  if quest == 1 then
    game:get_hero():teleport("battle/battle_test_map")
  elseif quest == 3 then
    game:get_hero():teleport("battle/greed_map")
  elseif quest == 5 then
    game:get_hero():teleport("battle/lust_map")
  else
    game:get_hero():teleport("first_map")
  end
end

function home:on_interaction()
  game:get_hero():teleport("first_map")
end

function exit:on_activated()
  --[[game:start_dialog("midnight_brain_exit", 
    function(answer)
      if answer == 1 then
        map:get_hero():teleport("debug")
        game:move_time()
      end
    end
  )]]--
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end
