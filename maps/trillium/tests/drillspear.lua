local map = ...
local game = map:get_game()

function gear_switch:on_activated()
  map:change_crystal_state()
end

function gear_switch:on_inactivated()
  map:change_crystal_state()
end