local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  map:set_darkness_level({30,35,50})
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)