local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  for e in map:get_entities_by_type("enemy") do
    e.idle_movement_speed = 50
  end
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)