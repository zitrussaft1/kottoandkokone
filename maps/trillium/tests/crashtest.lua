local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

function map:on_started()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  if not hero.made_thing then
    print"Making thing"
    local x, y, z = hero:get_position()
    hero.test_thing = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=16, height=16,
    }
    hero.made_thing = true
  end

  hero.test_thing:set_position(hero:get_position())


--[[
  if not hero.made_thing then
    print"Making the thing"
    local x, y, z = hero:get_position()
    hero.test_thing = map:create_custom_entity{
      x=x, y=y, layer=z, direction=0, width=16, height=16,
      sprite = "destructibles/pot"
    }

    local hero_meta = sol.main.get_metatable("hero")
    hero_meta:register_event("on_position_changed", function(self, x, y, z)
      if self.test_thing then
        self.test_thing:set_position(x, y, z)
      end
    end)

    hero.made_thing = true
    hero.test_thing:get_sprite():set_animation("destroy", function()
      hero.test_thing:remove()
    end)
  end
--]]
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end
