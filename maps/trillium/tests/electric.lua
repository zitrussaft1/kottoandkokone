local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  map:set_darkness_level("evening")
  local fog1 = require("scripts/trillium/fx/fog").new({
  	fog_texture = {png = "fogs/dust_1.png", mode = "blend", opacity = 70},
  	opacity_range = {60,100},
    drift = {8, 0, -1, 1},
    parallax_speed = 1,
  })
  local fog2 = require("scripts/trillium/fx/fog").new({
  	fog_texture = {png = "fogs/dust_2.png", mode = "blend", opacity = 50},
  	opacity_range = {50,90},
    drift = {10, 0, -1, 1},
    parallax_speed = 1,
  })
  sol.menu.start(map, fog1)
  sol.menu.start(map, fog2)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)


function lightswitch:on_activated()
  map:change_crystal_state()
end

function lightswitch:on_inactivated()
  map:change_crystal_state()
end