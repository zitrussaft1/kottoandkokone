local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  map:set_fog"forest"
  map:start_entity_movements()
  map:set_doors_open("boss_door")
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

--Switches
function central_post_switch:on_activated()
  map:open_doors"central_post_door"
end

function east_tower_door_switch:on_activated()
  local camera = map:get_camera()
  camera:start_tracking(boss_entry_door)
  sol.timer.start(map, 700, function()
    map:open_doors"boss_entry_door"
    sol.timer.start(map, 1000, function()
      camera:start_tracking(hero)
        sol.timer.start(map, 300, function()
          map:open_doors"east_tower_door"
        end)
    end)
  end)
end

function boomerang_bridge_switch:on_activated()
  map:open_doors"boomerang_bridge_door"
end


--entity movements

function map:start_entity_movements()
  local m = sol.movement.create"path"
  m:set_path{2,2,2,2,2,2,2,2,2,2,2,2,6,6,6,6,6,6,6,6,6,6,6,6}
  m:set_loop(true)
  m:set_ignore_obstacles(true)
  m:start(moving_twister_1)
end


--boss
function boss_sensor:on_activated()
  if not game:get_value"deepwood_fort_boss" then
    boss_sensor:remove()
    map:close_doors"boss_door"
  end
end

function boss:on_dead()
  map:open_doors"boss_door"
end