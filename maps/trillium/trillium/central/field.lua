local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  --Temp entities for spacing while making map
  for e in map:get_entities("grid_ref") do
    e:remove()
  end

  map:set_fog"clouds"
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

function map:on_opening_transition_finished()
  sol.timer.start(map, 500, function()
    --map:title_slam("FIELD OFFICE")
  end)
end

function east_forest_bridge_switch:on_activated()
  map:open_doors"east_forest_bridge_door"
end
