local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  --Temp entities for spacing while making map
  for e in map:get_entities("grid_ref") do
    e:remove()
  end

  map:set_fog"fog"
  game:set_world_rain_mode(map:get_world(), "rain")
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)