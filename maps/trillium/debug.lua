local master_script = require("scripts/meta/master")
local map = ...
local game = map:get_game()


local possible_items = {
  "inventory/flame_spell",
  "inventory/fire_rod",
  "inventory/spark_spell",
  "inventory/spark_rod",
  "inventory/ice_spell",
  "inventory/ice_rod",
  "inventory/drillspear",
  "inventory/summoning_spear",
  "inventory/hookseed_satchel",
  "inventory/feather",
  "inventory/metal_block_cane",
  "inventory/staff_of_teningur",
  "inventory/staff_of_mercury",
  "inventory/hookshot",
  "inventory/boomerang",
  "inventory/bombs_counter",
  "inventory/clockwork_bombs_counter",
  "inventory/bow",
  "inventory/bow_fire",
  "inventory/bow_ice",
  "inventory/bow_electric",
  "inventory/bow_bomb",
  "inventory/magnet_gauntlet",
  "inventory/homeward_talisman",
  "inventory/bugnet",
  "inventory/flamethrower",
  "inventory/crystal_dash",
  "inventory/gale_fan",
  "inventory/speed_boots",
  "test/gun",
  "inventory/grenade",
  "inventory/spell_arrow",
  "inventory/spell_arrow_storm",
  "inventory/spell_heal",
  "inventory/spell_bomb",
  "inventory/spell_sunbeam",
  "inventory/spell_soulsword",
  "inventory/spell_wraith",
  "inventory/spell_cinder",
  "inventory/spell_arcane_dart",
  "inventory/spell_arcane_bolt",
  "inventory/spell_poison_gas",
  "inventory/spell_life_siphon",
  "inventory/spell_life_siphon_circle",
  "inventory/spell_firestorm",
  "inventory/spell_thunderstorm",
  "inventory/spell_ice_wind",
  "inventory/spell_groundspark",
  "inventory/spell_flame_line",
  "inventory/spell_backdraft",
  "inventory/spell_boltpunch",
  "inventory/spell_hail",
  "inventory/spell_hyperbeam",
  "inventory/spell_plant_growth",
  "inventory/spell_hush",
  "inventory/spell_mending_tree",
  "inventory/spell_earthquake",
  "charms/gradual_heal",
}

map:register_event("on_started", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  --map:set_darkness_level(3)
  --game:get_item("inventory/feather"):set_variant(2)
  --game:set_value("jumping_shockwave_dive", true)
  --map:draw_entity_bounding_boxes()
  --hero:set_tunic_sprite_id("hero/poncho")


  if sol.main.debug_mode then
    for _, v in ipairs(possible_items) do game:get_item(v):set_variant(1) end
    game:set_money(5000)
  end

--
  local retriever = require"scripts/trillium/utility/item_name_retriever"
  for _, name in pairs(sol.main.get_items_in_directory("items/trillium/materials/animal")) do
    game:get_item(name):set_variant(1)
    game:get_item(name):add_amount(1)
  end
  for _, name in pairs(sol.main.get_items_in_directory("items/trillium/materials/ingredients")) do
    game:get_item(name):set_variant(1)
    game:get_item(name):add_amount(15)
  end
--
  for _, name in pairs(sol.main.get_items_in_directory("items/trillium/materials/mineral")) do
    game:get_item(name):set_variant(1)
    game:get_item(name):add_amount(1)
  end
  for _, name in pairs(sol.main.get_items_in_directory("items/trillium/materials/plant")) do
    game:get_item(name):set_variant(1)
    game:get_item(name):add_amount(1)
  end
  for _, name in pairs(sol.main.get_items_in_directory("items/trillium/meals")) do
    --game:get_item(name):set_variant(1)
    game:get_item(name):add_amount(3)
  end
--]]
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

map:register_event("on_opening_transition_finished", function()
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  sol.timer.start(map, 500, function()
    --map:title_slam("DEBUG ROOM")
  end)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

function shopkeep:on_interaction()
--[[
  game:start_dialog("test.2", function(answer)
    print("answer is ", answer)
  end)
--]]
--
  game:open_shop("test", {
    {id = "pickables/coin", price = 10,}, --item_id and price and required
    {id = "charms/flamestep", price = 200, quantity = 1,}, --quantity is optional, otherwise there will be an infinite quantity in the shop
    {id = "inventory/boomerang", price = 500, variant = 2, quantity = 1,}, --variant is another optional field
  })
--]]
end

function npc_2:on_interaction()
  game:start_dialog("test.tall_question", {
    config_override_file = "tall_dialog",
  }, function(answer)
    print("You answered", answer)
  end)
end


--[[
map:register_event("on_joypad_axis_moved", function(map, button)
  master_script:set("trillium", "enemy")
  master_script:set("trillium", "bush")
  print("Button pressed:", button)
  master_script:set("default", "bush")
  master_script:set("default", "enemy")
end)

--]]