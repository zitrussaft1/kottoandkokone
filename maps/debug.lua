-- Lua script of map debug.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
    --self:draw_entity_bounding_boxes()
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.

-- Get the time_change NPC and set its on_interaction function.
local time_change = map:get_entity("time_change")
function time_change:on_interaction()
  
  -- Get the current time of day.
  game:move_time()
  local time = game:get_value("time")
  if time == "day" then
    game:start_dialog("time.day")
  elseif time == "evening" then
    game:start_dialog("time.evening")
  elseif time == "night" then
    game:start_dialog("time.night")
  end
end

local lick = map:get_entity("lick")
function lick:on_interaction()
  game:add_corp(5)
  print("cash: $"..game:get_cash())
end

function switch_character:on_activated()
  game:set_character("kotto")
end

function reset:on_interaction()
  sol.game.delete("save2.dat")
end