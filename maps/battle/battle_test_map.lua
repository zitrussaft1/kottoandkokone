-- Lua script of map battle_test_map.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()


function map:on_started()
  --self:draw_entity_bounding_boxes()
  -- You can initialize the movement and sprites of various
  -- map entities here.
end