-- Lua script of map lust_map.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local switches = {}
local spikes = {}
-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
    for spike in map:get_entities("spike") do
        spikes[#spikes + 1] = spike
    end
    for switch in map:get_entities("switch") do
        switches[#switches + 1] = switch
        function switch:on_activated()
            for i = 1, #spikes do
                spikes[i]:change_state(true)
            end
            for i = 1, #switches do
                switches[i]:set_activated(true)
                switches[i]:set_locked(true)
                sol.timer.start(1000, function()
                    switches[i]:set_locked(false)
                end)
            end
        end

        function switch:on_inactivated()
            for i = 1, #spikes do
                spikes[i]:change_state(false)
            end
            for i = 1, #switches do
                switches[i]:set_activated(false)
            end
        end
    end
end
    

