-- Lua script of map battle/gluttony.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  -- Create over all parts of the map stream entities
  local width, height = self:get_size()
  local stream_width, stream_height = 16, 16
  local stream_x, stream_y = 0, 0


  for x = 0, width, stream_width do
    for y = 0, height, stream_height do
      local stream = self:create_stream({
        name="steam",
        x = x,
        y = y,
        layer = 0,
        width = stream_width,
        height = stream_height,
        direction = 0})

      stream:create_sprite("ground_effects/red_square")
      stream:get_sprite():set_blend_mode("add")
      stream:set_visible(false)
      stream:set_speed(1)

      stream_y = stream_y + stream_height
    end
    stream_x = stream_x + stream_width
  end

end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.