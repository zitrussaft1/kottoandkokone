-- Lua script of map tutorial.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

function boss_1:on_interaction()
  game:start_dialog("tutorial.boss1", function()
    --if game:get_value("skill__ancient_battle") == 1 then return end
    game:set_value("skill__ancient_battle", 1)
    boss1_real:set_enabled(true)
    game:set_value("in_combat", 1)
    map:start_attacking()
  end)
end

function boss_2:on_interaction()
  game:start_dialog("tutorial.boss2", function()
    game:set_value("skill__frenzy", 1)
    boss2_real:set_enabled(true)
  end)
end

function boss_3:on_interaction()
  game:start_dialog("tutorial.boss3", function()
    game:set_value("skill__fur_ball", 1)
    map:card_distribute()
    map:show_card_menu()
    map:get_entity("door3"):open_door()
  end)
end

function boss1_real:on_dead()
  map:get_entity("door1"):open_door()
end

function boss2_real:on_dead()
  map:get_entity("door2"):open_door()
end


-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  game:set_character("kotto")
  sol.audio.play_music("eduardo/mini_game")
  -- You can initialize the movement and sprites of various
  -- map entities here.
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end
